/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.item;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.cp.av.MediaServerDevice;

/**
 * Browser item for MediaServerDevice.
 *
 * @author Krzysztof Kapuscik
 */
public class MediaServerItem implements BrowserItem {

    /** Media server for which item is created. */
    private final MediaServerDevice mediaServer;

    /**
     * Constructs new item.
     *
     * @param mediaServer
     *            Media server for which item is created.
     */
    MediaServerItem(MediaServerDevice mediaServer) {
        this.mediaServer = mediaServer;
    }

    @Override
    public String getName() {
        return this.mediaServer.getFriendlyName();
    }

    @Override
    public String getDescription() {
        // TODO
        return this.mediaServer.getUDN().toString();
    }

    @Override
    public Object getDataObject() {
        return this.mediaServer;
    }

}
