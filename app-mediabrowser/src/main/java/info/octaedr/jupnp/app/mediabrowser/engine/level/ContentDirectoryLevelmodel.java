/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.level;

import java.awt.EventQueue;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.app.mediabrowser.engine.LevelModel;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionExecutionFailedEvent;
import info.octaedr.jupnp.cp.ActionFailedEvent;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.ActionRequestListener;
import info.octaedr.jupnp.cp.ActionSucceededEvent;
import info.octaedr.jupnp.cp.av.BrowseArguments;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;
import info.octaedr.jupnp.cp.av.BrowseArguments.BrowseFlag;

import javax.swing.AbstractListModel;
import javax.swing.JOptionPane;

class ContentDirectoryLevelmodel extends AbstractListModel implements LevelModel {

    private final ContentDirectoryService cds;
    private BrowserItem rootContainer;
    private ActionRequest<BrowseArguments> browseRequest;

    public ContentDirectoryLevelmodel(ContentDirectoryService cds) {
        this.cds = cds;

        this.rootContainer = null;
    }

    @Override
    public int getSize() {
        return this.rootContainer != null ? 1 : 0;
    }

    @Override
    public BrowserItem getElementAt(int index) {
        if (this.rootContainer != null && index == 0) {
            return this.rootContainer;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public void levelStashed() {
        // ignore
    }

    @Override
    public void levelActivated() {
        if (this.browseRequest != null) {
            return;
        }

        try {
            ActionRequest<BrowseArguments> request = this.cds.createBrowseRequest();

            request.getArguments().setObjectID("0");
            request.getArguments().setBrowseFlag(BrowseFlag.BROWSE_METADATA);
            request.getArguments().setFilter("*");
            request.getArguments().setStartingIndex(0);
            request.getArguments().setRequestedCount(0);
            request.getArguments().setSortCriteria("");

            request.execute(new ActionRequestListener<BrowseArguments>() {
                @Override
                public void actionSucceeded(final ActionSucceededEvent<BrowseArguments> event) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            processActionSucceededEvent(event);
                        }
                    });
                }

                @Override
                public void actionFailed(final ActionFailedEvent<BrowseArguments> event) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            processActionFailedEvent(event);
                        }
                    });
                }

                @Override
                public void actionExecutionFailed(final ActionExecutionFailedEvent<BrowseArguments> event) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            processExecutionFailedEvent(event);
                        }
                    });
                }
            });

            this.browseRequest = request;
        } catch (UpnpException e) {
            JOptionPane.showMessageDialog(null, "Root container browser request failed: " + e, "Browse Problem",
                    JOptionPane.WARNING_MESSAGE);
            e.printStackTrace();
        }
    }

    private void processActionSucceededEvent(ActionSucceededEvent<BrowseArguments> event) {
        if (event.getSource() != this.browseRequest) {
            return;
        }
        this.browseRequest = null;

        // TODO Auto-generated method stub
    }

    private void processActionFailedEvent(ActionFailedEvent<BrowseArguments> event) {
        if (event.getSource() != this.browseRequest) {
            return;
        }
        this.browseRequest = null;

        // TODO Auto-generated method stub

    }

    private void processExecutionFailedEvent(ActionExecutionFailedEvent<BrowseArguments> event) {
        if (event.getSource() != this.browseRequest) {
            return;
        }
        this.browseRequest = null;

        // TODO Auto-generated method stub

    }

    @Override
    public void levelExited() {
        if (this.browseRequest != null) {
            this.browseRequest.cancel();
        }
    }

}
