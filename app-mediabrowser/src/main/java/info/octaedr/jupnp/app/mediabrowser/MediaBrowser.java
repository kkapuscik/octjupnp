/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser;

import info.octaedr.jcommons.logging.LogManager;
import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.AboutPerson;
import info.octaedr.jupnp.app.mediabrowser.engine.MediaBrowserEngine;
import info.octaedr.jupnp.app.mediabrowser.ui.MediaBrowserFrame;

/**
 * DeviceSpy application class.
 */
public class MediaBrowser {

    /** Logger for printing debug messages. */
    private static Logger logger = Logger.getLogger(MediaBrowser.class);

    /**
     * Main function.
     *
     * @param args
     *            Execution arguments.
     */
    public static void main(final String[] args) {
        try {
            LogManager.init();

            /* prepare about data */
            final AboutData aboutData = new AboutData("Media Browser", "0.1");
            aboutData.setShortDescription("Utility for browsing contents of Media Servers");
            aboutData.setCopyrightStatement("2008-2012 OCTaedr Software");
            aboutData.addAuthor(new AboutPerson("Krzysztof Kapuscik", "Author",
                    "k.kapuscik@gmail.com"));

            /* create engine */
            final MediaBrowserEngine appEngine = new MediaBrowserEngine();

            /* create main frame */
            final MediaBrowserFrame appFrame = new MediaBrowserFrame(appEngine, aboutData);

            /* show main frame */
            appFrame.setVisible(true);
        } catch (final Throwable t) {
            if (logger.severe()) {
                logger.severe("Exception catched", t);
            }
        }
    }

}
