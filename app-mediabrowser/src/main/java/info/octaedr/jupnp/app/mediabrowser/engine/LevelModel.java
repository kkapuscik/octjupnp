/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

import javax.swing.ListModel;

/**
 * Model with elements at single level of browser model.
 *
 * @author Krzysztof Kapuscik
 */
public interface LevelModel extends ListModel {

    /**
     * Returns element at given index.
     *
     * @param index
     *            Index of the requested element.
     *
     * @return Element at given position.
     */
    @Override
    BrowserItem getElementAt(int index);

    /**
     * Notifies element set that browser moved to sublevel and this level was stashed for later reuse.
     *
     * @see #levelActivated()
     */
    void levelStashed();

    /**
     * Notifies element set that browser moved from to this level.
     */
    void levelActivated();

    /**
     * Notifies element set that browser no longer needs that level as it moved to parent level.
     */
    void levelExited();

}
