/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.item;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.cp.av.ScheduledRecordingService;

class ScheduledRecordingItem implements BrowserItem {

    private final ScheduledRecordingService srs;

    public ScheduledRecordingItem(ScheduledRecordingService srs) {
        this.srs = srs;
    }

    @Override
    public Object getDataObject() {
        return this.srs;
    }

    @Override
    public String getName() {
        return "Scheduled Recording";
    }

    @Override
    public String getDescription() {
        return "Shared scheduled recordings";
    }

}
