package info.octaedr.jupnp.app.mediabrowser.engine;

import info.octaedr.jupnp.app.mediabrowser.engine.item.BrowserItemFactory;

import javax.swing.AbstractListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Wrapper over level model that add the 'Level Up' element.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
class LevelModelWrapper extends AbstractListModel implements LevelModel, ListDataListener {

    /**
     * Special browser item - level up.
     *
     * <p>
     * It shall be the first item on all non-root levels.
     * </p>
     */
    private static final BrowserItem LEVEL_UP_ITEM = BrowserItemFactory.createItem(BrowserItem.LEVEL_UP_DATA_OBJECT);

    /** Wrapped model. */
    private final LevelModel model;
    /** Flag indicating if the wrapped level is at root level. */
    private final boolean isRoot;

    /**
     * Constructs new model wrapper.
     *
     * @param model
     *            Model to be wrapped.
     * @param isRoot
     *            Flag indicating if the wrapped level is at root level.
     */
    public LevelModelWrapper(LevelModel model, boolean isRoot) {
        this.model = model;
        this.isRoot = isRoot;

        this.model.addListDataListener(this);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        super.addListDataListener(l);
    }

    @Override
    public int getSize() {
        if (this.isRoot) {
            return this.model.getSize();
        } else {
            return this.model.getSize() + 1;
        }
    }

    @Override
    public BrowserItem getElementAt(int index) {
        if (this.isRoot) {
            return this.model.getElementAt(index);
        } else {
            if (index == 0) {
                return LEVEL_UP_ITEM;
            } else {
                return this.model.getElementAt(index - 1);
            }
        }
    }

    @Override
    public void levelStashed() {
        this.model.levelStashed();
    }

    @Override
    public void levelActivated() {
        this.model.levelActivated();
    }

    @Override
    public void levelExited() {
        this.model.levelExited();
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        if (this.isRoot) {
            fireIntervalAdded(e.getSource(), e.getIndex0(), e.getIndex1());
        } else {
            fireIntervalAdded(e.getSource(), e.getIndex0() + 1, e.getIndex1() + 1);
        }
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        if (this.isRoot) {
            fireIntervalRemoved(e.getSource(), e.getIndex0(), e.getIndex1());
        } else {
            fireIntervalRemoved(e.getSource(), e.getIndex0() + 1, e.getIndex1() + 1);
        }
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        if (this.isRoot) {
            fireContentsChanged(e.getSource(), e.getIndex0(), e.getIndex1());
        } else {
            fireContentsChanged(e.getSource(), e.getIndex0() + 1, e.getIndex1() + 1);
        }
    }

}
