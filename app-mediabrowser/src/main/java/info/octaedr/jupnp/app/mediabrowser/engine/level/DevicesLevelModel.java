/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.level;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.app.mediabrowser.engine.DeviceDatabaseListModel;
import info.octaedr.jupnp.app.mediabrowser.engine.LevelModel;
import info.octaedr.jupnp.app.mediabrowser.engine.item.BrowserItemFactory;
import info.octaedr.jupnp.cp.av.MediaServerDevice;

import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

class DevicesLevelModel extends AbstractListModel implements LevelModel {

    private final DeviceDatabaseListModel deviceListModel;

    private final ArrayList<BrowserItem> browserItems;

    DevicesLevelModel(DeviceDatabaseListModel deviceListModel) {
        this.deviceListModel = deviceListModel;
        this.browserItems = new ArrayList<BrowserItem>();

        this.deviceListModel.addListDataListener(new ListDataListener() {
            @Override
            public void intervalRemoved(ListDataEvent e) {
                DevicesLevelModel.this.processIntervalRemoval(e.getIndex0(), e.getIndex1());
            }

            @Override
            public void intervalAdded(ListDataEvent e) {
                DevicesLevelModel.this.processIntervalAdded(e.getIndex0(), e.getIndex1());
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                DevicesLevelModel.this.processContentsChanges(e.getIndex0(), e.getIndex1());
            }
        });
    }

    protected void processContentsChanges(int index0, int index1) {
        for (int index = index0; index <= index1; ++index) {
            MediaServerDevice device = this.deviceListModel.getElementAt(index);

            this.browserItems.set(index, BrowserItemFactory.createItem(device));
        }

        DevicesLevelModel.this.fireContentsChanged(this, index0, index1);
    }

    protected void processIntervalAdded(int index0, int index1) {
        for (int index = index0; index <= index1; ++index) {
            MediaServerDevice device = this.deviceListModel.getElementAt(index);

            this.browserItems.add(index, BrowserItemFactory.createItem(device));
        }

        DevicesLevelModel.this.fireIntervalAdded(this, index0, index1);
    }

    protected void processIntervalRemoval(int index0, int index1) {
        for (int index = index1; index >= index0; --index) {
            MediaServerDevice device = this.deviceListModel.getElementAt(index);

            this.browserItems.remove(index);
        }

        DevicesLevelModel.this.fireIntervalRemoved(this, index0, index1);
    }

    @Override
    public int getSize() {
        return this.browserItems.size();
    }

    @Override
    public BrowserItem getElementAt(int index) {
        return this.browserItems.get(index);
    }

    @Override
    public void levelStashed() {
        // TODO Auto-generated method stub

    }

    @Override
    public void levelActivated() {
        // TODO Auto-generated method stub
    }

    @Override
    public void levelExited() {
        throw new IllegalStateException();
    }

}
