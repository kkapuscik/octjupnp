/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.item;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;

public class LevelUpItem implements BrowserItem {

    public LevelUpItem() {
        // nothing to do
    }

    @Override
    public Object getDataObject() {
        return BrowserItem.LEVEL_UP_DATA_OBJECT;
    }

    @Override
    public String getName() {
        return "-Level Up-";
    }

    @Override
    public String getDescription() {
        return "Go to the parent level";
    }

}
