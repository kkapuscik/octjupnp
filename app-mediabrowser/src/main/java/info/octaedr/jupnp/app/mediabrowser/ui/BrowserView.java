/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.ui;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserModel;

import java.awt.CardLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class BrowserView extends JPanel {

    private static final String DETAILS_CARD = "Details";

    private CardLayout cardLayout;

    private JTable detailsTable;

    private JScrollPane detailsScroll;

    private BrowserModel model;

    public BrowserView() {
        super();

        createComponents();
        connectActions();
    }

    private void connectActions() {
        this.detailsTable.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    if (e.getClickCount() >= 2) {
                        BrowserView.this.processItemDoubleClick(e.getPoint());
                    }
                }
            }
        });
    }

    protected void processItemDoubleClick(Point position) {
        // int column = BrowserView.this.detailsTable.columnAtPoint(position);
        int row = this.detailsTable.rowAtPoint(position);

        if (row >= 0 && row < this.model.getElementCount()) {
            this.model.detailsLevelRequested(row);
        }

    }

    public void setModel(BrowserModel model) {
        this.model = model;

        if (model != null) {
            this.detailsTable.setModel(model.getDetailsModel());
        } else {
            this.detailsTable.setModel(null);
        }
    }

    private void createComponents() {
        this.detailsTable = new JTable();
        this.detailsScroll = new JScrollPane(this.detailsTable);

        this.cardLayout = new CardLayout();
        setLayout(this.cardLayout);

        add(this.detailsScroll, DETAILS_CARD);

        this.cardLayout.show(this, DETAILS_CARD);
    }

}
