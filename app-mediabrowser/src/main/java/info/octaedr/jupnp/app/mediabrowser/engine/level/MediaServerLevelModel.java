/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.level;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.app.mediabrowser.engine.item.BrowserItemFactory;
import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.av.MediaServerDevice;

import java.util.ArrayList;

public class MediaServerLevelModel extends ArrayLevelModel {

    private final MediaServerDevice msDevice;

    public MediaServerLevelModel(MediaServerDevice msDevice) {
        super(createChildItems(msDevice));

        this.msDevice = msDevice;
    }

    private static BrowserItem[] createChildItems(MediaServerDevice msDevice) {
        ArrayList<BrowserItem> childArray = new ArrayList<BrowserItem>();

        for (Service service : msDevice.getServices()) {
            BrowserItem item = BrowserItemFactory.createItem(service);
            if (item != null) {
                childArray.add(item);
            }
        }

        return childArray.toArray(new BrowserItem[childArray.size()]);
    }

}
