/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.resources;

import info.octaedr.jswing.resources.ResourceProvider;

import javax.swing.Icon;

/**
 * Application icon set.
 *
 * @author Krzysztof Kapuscik
 */
public class IconSet extends ResourceProvider {

    @SuppressWarnings("javadoc")
    public static final String ABOUT = "about";
    @SuppressWarnings("javadoc")
    public static final String EXIT = "exit";

    /**
     * Constructs icon set.
     *
     * @param pkgName
     *            Name of the package.
     */
    public IconSet(final String pkgName) {
        super(pkgName);
    }

    /**
     * Returns small (16x16) icon for given key.
     *
     * @param key
     *            Icon key.
     *
     * @return Icon object.
     */
    public Icon getSmallIcon(final String key) {
        return getIcon(key, "small", "png");
    }

}
