/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

import info.octaedr.jupnp.app.mediabrowser.engine.level.LevelModelFactory;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointEvent;
import info.octaedr.jupnp.cp.ControlPointFactory;
import info.octaedr.jupnp.cp.ControlPointListener;
import info.octaedr.jupnp.cp.av.AVPluginFactory;

import java.awt.EventQueue;

public class MediaBrowserEngine {

    private final DeviceDatabaseListModel deviceListModel;
    private final BrowserModelImpl browserModel;
    private final ControlPoint controlPoint;

    /**
     * Constructs new engine.
     */
    public MediaBrowserEngine() {
        /* construct device list model (device database) */
        this.deviceListModel = new DeviceDatabaseListModel();

        /* create control point factory */
        ControlPointFactory cpf = ControlPointFactory.getInstance();
        cpf.registerPlugin(AVPluginFactory.getInstance().createPlugin());

        /* create control point */
        this.controlPoint = cpf.createControlPoint();
        this.controlPoint.addControlPointListener(new ControlPointListener() {
            @Override
            public void rootDeviceRemoved(ControlPointEvent event) {
                // ignore
            }

            @Override
            public void rootDeviceAdded(ControlPointEvent event) {
                // ignore
            }

            @Override
            public void deviceRemoved(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        MediaBrowserEngine.this.deviceListModel.processDeviceRemoved(event.getDevice());
                    }
                });
            }

            @Override
            public void deviceAdded(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        MediaBrowserEngine.this.deviceListModel.processDeviceAdded(event.getDevice());
                    }
                });
            }
        });

        /* create browser model */
        this.browserModel = new BrowserModelImpl(LevelModelFactory.createModel(this.deviceListModel));
    }

    public void start() {
        this.controlPoint.start();
    }

    public void stop() {
        this.controlPoint.stop();
    }

    /**
     * Returns model for browser view.
     *
     * @return Browser model instance.
     */
    public BrowserModel getBrowserModel() {
        return this.browserModel;
    }

}
