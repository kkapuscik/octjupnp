/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

/**
 * Single browser element.
 *
 * @author Krzysztof Kapuscik
 */
public interface BrowserItem {

    public static final Object LEVEL_UP_DATA_OBJECT = new Object();

    Object getDataObject();

    String getName();

    String getDescription();

}
