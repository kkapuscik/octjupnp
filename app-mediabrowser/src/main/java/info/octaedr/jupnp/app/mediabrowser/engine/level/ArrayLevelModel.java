/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.level;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.app.mediabrowser.engine.LevelModel;

import javax.swing.AbstractListModel;

/**
 * Base class for level models with static collection of child elements.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
class ArrayLevelModel extends AbstractListModel implements LevelModel {

    /** Collecton of child items. */
    private final BrowserItem[] childItems;

    /**
     * Constructs new model.
     *
     * @param childItems
     *            Collection of child items.
     */
    protected ArrayLevelModel(BrowserItem[] childItems) {
        if (childItems == null) {
            throw new NullPointerException();
        }

        this.childItems = childItems;
    }

    @Override
    public int getSize() {
        return this.childItems.length;
    }

    @Override
    public BrowserItem getElementAt(int index) {
        return this.childItems[index];
    }

    @Override
    public void levelStashed() {
        // nothing to do
    }

    @Override
    public void levelActivated() {
        // nothing to do
    }

    @Override
    public void levelExited() {
        // nothing to do
    }

}
