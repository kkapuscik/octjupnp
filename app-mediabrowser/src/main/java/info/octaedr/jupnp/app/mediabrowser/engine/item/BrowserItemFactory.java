/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.item;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;
import info.octaedr.jupnp.cp.av.MediaServerDevice;

/**
 * Factory of browser items.
 *
 * @author Krzysztof Kapuscik
 */
public class BrowserItemFactory {

    /**
     * Constructs browser item for given data object.
     *
     * @param dataObject
     *            Data object.
     *
     * @return Created browser item or null if it cannot be created.
     */
    public static BrowserItem createItem(Object dataObject) {
        if (dataObject == BrowserItem.LEVEL_UP_DATA_OBJECT) {
            return new LevelUpItem();
        } else if (dataObject instanceof MediaServerDevice) {
            return new MediaServerItem((MediaServerDevice) dataObject);
        } else if (dataObject instanceof ContentDirectoryService) {
            return new ContentDirectoryItem((ContentDirectoryService) dataObject);
        }

        return null;
    }

    /**
     * Constructs level up item.
     *
     * @return Created browser item.
     */
    public static BrowserItem createLevelUpitem() {
        return new LevelUpItem();
    }

}
