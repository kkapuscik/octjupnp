/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

import java.util.ArrayList;

import javax.swing.AbstractListModel;

import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.av.MediaServerDevice;

// TODO sort support
public class DeviceDatabaseListModel extends AbstractListModel {

    private final ArrayList<MediaServerDevice> devices;

    public DeviceDatabaseListModel() {
        this.devices = new ArrayList<MediaServerDevice>();
    }

    @Override
    public int getSize() {
        return this.devices.size();
    }

    @Override
    public MediaServerDevice getElementAt(int index) {
        return this.devices.get(index);
    }

    public void processDeviceAdded(Device device) {
        if (device instanceof MediaServerDevice) {
            MediaServerDevice msDevice = (MediaServerDevice) device;

            int index = this.devices.size();
            this.devices.add(msDevice);

            fireIntervalAdded(this, index, index);
        }
    }

    public void processDeviceRemoved(Device device) {
        if (device instanceof MediaServerDevice) {
            MediaServerDevice msDevice = (MediaServerDevice) device;

            int index = getDeviceIndex(msDevice);
            if (index >= 0) {
                this.devices.remove(index);

                fireIntervalRemoved(this, index, index);
            }
        }
    }

    private int getDeviceIndex(MediaServerDevice device) {
        return this.devices.indexOf(device);
    }

}
