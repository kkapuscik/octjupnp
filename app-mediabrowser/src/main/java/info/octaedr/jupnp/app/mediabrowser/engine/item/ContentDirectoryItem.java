/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.item;

import info.octaedr.jupnp.app.mediabrowser.engine.BrowserItem;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;

class ContentDirectoryItem implements BrowserItem {

    private final ContentDirectoryService cds;

    public ContentDirectoryItem(ContentDirectoryService cds) {
        this.cds = cds;
    }

    @Override
    public Object getDataObject() {
        return this.cds;
    }

    @Override
    public String getName() {
        return "Content Directory";
    }

    @Override
    public String getDescription() {
        return "Shared content elements";
    }

}
