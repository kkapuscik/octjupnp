/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

import javax.swing.table.TableModel;

/**
 * Model for the browser view.
 *
 * @author Krzysztof Kapuscik
 */
public interface BrowserModel {

    /**
     * Returns model for the details view.
     *
     * @return Details view model instance.
     */
    TableModel getDetailsModel();

    /**
     * Returns number of elements in model (at current level).
     *
     * @return Number of elements in model.
     */
    int getElementCount();

    /**
     * Returns element at given index.
     *
     * @param index
     *            Index of the requested element.
     *
     * @return Element at given position.
     */
    BrowserItem getElementAt(int index);

    /**
     * TODO: change name
     *
     * @param row
     */
    void detailsLevelRequested(int row);

}
