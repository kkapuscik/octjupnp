/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.ui;

import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.MainFrame;
import info.octaedr.jswing.Menu;
import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jupnp.app.mediabrowser.engine.MediaBrowserEngine;
import info.octaedr.jupnp.app.mediabrowser.resources.IconSet;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * Main Frame for Media Browser application.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
public class MediaBrowserFrame extends MainFrame {

    /** Application engine. */
    private final MediaBrowserEngine engine;

    private SwingAction actionExitApplication;

    private SwingAction actionShowAbout;

    private BrowserView browserView;

    /**
     * Constructs new frame.
     *
     * @param appEngine
     *            Application engine.
     * @param aboutData
     *            Information about the application.
     */
    public MediaBrowserFrame(final MediaBrowserEngine appEngine, final AboutData aboutData) {
        super(aboutData);

        this.engine = appEngine;

        createActions();
        createComponents();

        connectModels();
        connectActions();

        initState();
    }

    private void initState() {
        this.engine.start();
    }

    private void createActions() {
        final IconSet iconSet = new IconSet(
                "info.octaedr.upnp.tools.mediabrowser.resources.icons.base");

        this.actionExitApplication = new SwingAction();
        this.actionExitApplication.setName("Exit");
        this.actionExitApplication.setMnemonicKey(KeyEvent.VK_X);
        this.actionExitApplication.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
                InputEvent.ALT_MASK));
        this.actionExitApplication.setShortDescription("Exists the application");
        this.actionExitApplication.setSmallIcon(iconSet.getSmallIcon(IconSet.EXIT));

        this.actionShowAbout = new SwingAction();
        this.actionShowAbout.setName("About...");
        this.actionShowAbout.setMnemonicKey(KeyEvent.VK_A);
        this.actionShowAbout.setShortDescription("Shows information about the application");
        this.actionShowAbout.setSmallIcon(iconSet.getSmallIcon(IconSet.ABOUT));

        // TODO Auto-generated method stub

    }

    private void createComponents() {
        createMenuBar();
        final JComponent toolBarPanel = createToolBars();
        final JComponent contentPanel = createContentComponents();

        final JPanel contentPanePanel = new JPanel(new BorderLayout());
        if (toolBarPanel != null) {
            contentPanePanel.add(toolBarPanel, BorderLayout.NORTH);
        }
        contentPanePanel.add(contentPanel, BorderLayout.CENTER);

        getContentPane().add(contentPanePanel);

        // TODO Auto-generated method stub

    }

    private void createMenuBar() {
        final Menu menuFile = new Menu("File");
        menuFile.setMnemonic(KeyEvent.VK_F);
        // menuFile.add(this.actionNewCapture);
        // menuFile.addSeparator();
        menuFile.add(this.actionExitApplication);

        // Menu menuCapture = new Menu("Capture");
        // menuCapture.setMnemonic(KeyEvent.VK_C);
        // menuCapture.add(this.actionCapturePackets);
        // menuCapture.addSeparator();
        // menuCapture.add(this.actionCaptureMulticasts);
        //
        // Menu menuSearch = new Menu("Search");
        // menuSearch.setMnemonic(KeyEvent.VK_S);
        // menuSearch.add(this.actionSearchAllDevices);
        // menuSearch.add(this.actionSearchRootDevices);
        // menuSearch.addSeparator();
        // menuSearch.add(this.actionSearchMediaServer1);
        // menuSearch.add(this.actionSearchMediaRenderer1);
        // menuSearch.add(this.actionSearchInternetGateway1);
        //
        final Menu menuHelp = new Menu("Help");
        menuHelp.setMnemonic(KeyEvent.VK_H);
        menuHelp.add(this.actionShowAbout);

        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuFile);
        // menuBar.add(menuCapture);
        // menuBar.add(menuSearch);
        menuBar.add(menuHelp);

        setJMenuBar(menuBar);
    }

    private JComponent createToolBars() {
        // TODO Auto-generated method stub
        return null;
    }

    private JComponent createContentComponents() {
        this.browserView = new BrowserView();

        return this.browserView;
    }

    private void connectModels() {
        this.browserView.setModel(this.engine.getBrowserModel());

        // TODO Auto-generated method stub

    }

    private void connectActions() {
        this.actionExitApplication.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                MediaBrowserFrame.this.closeRequested();
            }
        });

        this.actionShowAbout.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                MediaBrowserFrame.this.showAboutDialog();
            }
        });

        // TODO
    }

    private void closeRequested() {
        /* stop the engine */
        this.engine.stop();

        /* dispose the frame (stop the application) */
        dispose();
    }

}
