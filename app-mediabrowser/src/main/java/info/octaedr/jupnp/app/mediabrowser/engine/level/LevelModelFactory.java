/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine.level;

import info.octaedr.jupnp.app.mediabrowser.engine.DeviceDatabaseListModel;
import info.octaedr.jupnp.app.mediabrowser.engine.LevelModel;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;
import info.octaedr.jupnp.cp.av.MediaServerDevice;

/**
 * Factory of LevelListModels.
 *
 * @author Krzysztof Kapuscik
 */
public class LevelModelFactory {

    /**
     * Constructs model for given data object.
     *
     * @param dataObject
     *            Data object.
     *
     * @return Created model or null if it cannot be created.
     */
    public static LevelModel createModel(Object dataObject) {
        if (dataObject instanceof DeviceDatabaseListModel) {
            return new DevicesLevelModel((DeviceDatabaseListModel) dataObject);
        } else if (dataObject instanceof MediaServerDevice) {
            return new MediaServerLevelModel((MediaServerDevice) dataObject);
        } else if (dataObject instanceof ContentDirectoryService) {
            return new ContentDirectoryLevelmodel((ContentDirectoryService) dataObject);
        }
        return null;
    }

}
