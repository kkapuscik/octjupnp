/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

import info.octaedr.jupnp.app.mediabrowser.engine.level.LevelModelFactory;

import java.util.ArrayList;

import javax.swing.table.TableModel;

/**
 * Implementation of browser model.
 *
 * @author Krzysztof Kapuscik
 */
class BrowserModelImpl implements BrowserModel {

    /** Stack of level models. */
    private final ArrayList<LevelModel> levelModels;
    /** Details model for the UI. */
    private final DetailsModel detailsModel;

    /**
     * Constructs new browser model.
     *
     * @param rootLevelModel
     *            Root level model to be used.
     */
    public BrowserModelImpl(LevelModel rootLevelModel) {
        if (rootLevelModel == null) {
            throw new NullPointerException("Null root level model");
        }

        this.levelModels = new ArrayList<LevelModel>();
        this.detailsModel = new DetailsModel();

        pushModel(rootLevelModel, true);
    }

    @Override
    public TableModel getDetailsModel() {
        return this.detailsModel;
    }

    @Override
    public int getElementCount() {
        return peekModel().getSize();
    }

    @Override
    public BrowserItem getElementAt(int index) {
        return peekModel().getElementAt(index);
    }

    /**
     * Peeks the level at top of the stack (the current level).
     *
     * @return Current level.
     */
    private LevelModel peekModel() {
        return this.levelModels.get(this.levelModels.size() - 1);
    }

    /**
     * Moves back from the top level.
     *
     * <p>
     * The top level is popped from the top of the stack and discarded. The new level at top of the stack is activated.
     * </p>
     */
    private void popModel() {
        /* notify the top level model that it is no longer needed */
        peekModel().levelExited();

        /* remove the top level from the stack */
        this.levelModels.remove(this.levelModels.size() - 1);

        /* notify the current top level that it shall activate */
        peekModel().levelActivated();

        /* update the current level */
        this.detailsModel.setCurrentModel(peekModel());
    }

    /**
     * Moves into new sub level.
     *
     * <p>
     * The current level is notified about the move to sub view. The new level is placed on the stack and activated.
     * </p>
     *
     * @param levelModel
     *            The sub level to enter.
     * @param isRoot
     *            Flag indicating if the new level is the root level.
     */
    private void pushModel(final LevelModel levelModel, final boolean isRoot) {
        /* notify the top level model that it is no longer active */
        if (!isRoot) {
            peekModel().levelStashed();
        }

        /* add new level to stack */
        this.levelModels.add(new LevelModelWrapper(levelModel, isRoot));

        /* notify the current top level that is shall activate */
        peekModel().levelActivated();

        /* update the current level */
        this.detailsModel.setCurrentModel(peekModel());
    }

    @Override
    public void detailsLevelRequested(int index) {
        if (index >= 0 && index < getElementCount()) {
            BrowserItem item = getElementAt(index);
            if (item != null) {
                if (item.getDataObject() == BrowserItem.LEVEL_UP_DATA_OBJECT) {
                    popModel();
                } else {
                    LevelModel newModel = LevelModelFactory.createModel(item.getDataObject());
                    if (newModel != null) {
                        pushModel(newModel, false);
                    }
                }
            }
        }
    }

}
