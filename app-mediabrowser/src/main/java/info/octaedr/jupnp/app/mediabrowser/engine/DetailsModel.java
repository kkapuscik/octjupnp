/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-mediabrowser project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.mediabrowser.engine;

import info.octaedr.jswing.ColumnDescriptor;
import info.octaedr.jswing.TableModelBase;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Model for browse details table.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
class DetailsModel extends TableModelBase {

    /**
     * Table columns.
     */
    private static ColumnDescriptor[] COLUMNS = {
        /** Element name. */
        new ColumnDescriptor("Name", String.class),
        /** Description. */
        new ColumnDescriptor("Description", String.class) };

    /**
     * Listener for level model changes.
     */
    private final ListDataListener levelModelListener;

    /** Current level model. */
    private LevelModel levelModel = null;

    /**
     * Constructs new details model.
     */
    public DetailsModel() {
        super(COLUMNS);

        this.levelModelListener = new ListDataListener() {

            @Override
            public void intervalRemoved(final ListDataEvent e) {
                DetailsModel.this.processListModelIntervalRemoved(e.getIndex0(), e.getIndex1());
            }

            @Override
            public void intervalAdded(final ListDataEvent e) {
                DetailsModel.this.processListModelIntervalAdded(e.getIndex0(), e.getIndex1());
            }

            @Override
            public void contentsChanged(final ListDataEvent e) {
                DetailsModel.this.processListModelContentsChanged(e.getIndex0(), e.getIndex1());
            }
        };
    }

    /**
     * Processes list model contents change event.
     *
     * @param index0
     *            Lower index of the range.
     * @param index1
     *            Upper index of the range.
     */
    protected void processListModelContentsChanged(final int index0, final int index1) {
        fireTableRowsUpdated(index0, index1);
    }

    /**
     * Processes list model interval addition event.
     *
     * @param index0
     *            Lower index of the range.
     * @param index1
     *            Upper index of the range.
     */
    protected void processListModelIntervalAdded(final int index0, final int index1) {
        fireTableRowsInserted(index0, index1);
    }

    /**
     * Processes list model interval removed event.
     *
     * @param index0
     *            Lower index of the range.
     * @param index1
     *            Upper index of the range.
     */
    protected void processListModelIntervalRemoved(final int index0, final int index1) {
        fireTableRowsDeleted(index0, index1);
    }

    @Override
    public int getRowCount() {
        if (this.levelModel != null) {
            return this.levelModel.getSize();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        if (this.levelModel != null) {
            final BrowserItem item = this.levelModel.getElementAt(rowIndex);

            switch (columnIndex) {
            case 0:
                return item.getName();
            case 1:
                return item.getDescription();
            }
        }

        return null;
    }

    /**
     * Sets current level model to be used.
     *
     * @param levelModel
     *            Level model to be used.
     */
    public void setCurrentModel(final LevelModel levelModel) {
        if (this.levelModel != null) {
            this.levelModel.removeListDataListener(this.levelModelListener);
        }

        this.levelModel = levelModel;

        fireTableDataChanged();

        if (this.levelModel != null) {
            this.levelModel.addListDataListener(this.levelModelListener);
        }
    }

}
