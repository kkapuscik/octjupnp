/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.apache.http.HttpException;

/**
 * HTTP UDP socket.
 *
 * @author Krzysztof Kapuscik
 */
public class HttpUdpSocket {

    /** HTTP UDP client. */
    private final HttpUdpClient client;

    /** Peer socket. */
    private final DatagramSocket peerSocket;

    /**
     * Construct new socket.
     *
     * @param client
     *            HTTP UDP client to use.
     * @param peerSocket
     *            Socket to be used to send and receive packets.
     */
    public HttpUdpSocket(HttpUdpClient client, DatagramSocket peerSocket) {
        if (peerSocket == null) {
            throw new NullPointerException("Null peer socket given");
        }

        this.client = client;
        this.peerSocket = peerSocket;
    }

    /**
     * Returns HTTP UDP client passed to constructor.
     *
     * @return HTTP UDP client.
     */
    public HttpUdpClient getClient() {
        return this.client;
    }

    /**
     * Returns peer socket passed to constructor.
     *
     * @return Peer socket.
     */
    public DatagramSocket getPeerSocket() {
        return this.peerSocket;
    }

    /**
     * Closes the socket.
     */
    public void close() {
        this.peerSocket.close();
    }

    /**
     * Receives a HTTP UDP message from this socket.
     *
     * <p>
     * This method blocks until a datagram is received. If the datagram message is longer than the maximum length
     * allowed then the message is truncated.
     * </p>
     *
     * <p>
     * If there is a security manager, a packet cannot be received if the security manager's <code>checkAccept</code>
     * method does not allow it.
     * </p>
     *
     * <p>
     * See {@link DatagramSocket#receive(DatagramPacket)} to check other limitations and requirements.
     * </p>
     *
     * @return Received message.
     *
     * @throws HttpException
     *             if an HTTP error occurs.
     * @throws IOException
     *             if an I/O error occurs.
     *
     * @see java.net.DatagramPacket
     * @see java.net.DatagramSocket
     * @see java.net.DatagramSocket#receive(DatagramPacket)
     */
    public HttpUdpMessage receiveMessage() throws IOException, HttpException {
        DatagramPacket packet = DatagramMessageTools.createDatagramPacket(this.client);

        this.peerSocket.receive(packet);

        HttpUdpMessage message = DatagramMessageTools.parsePacket(this.client, packet);

        return message;
    }

    /**
     * Sends a HTTP UDP message from this socket.
     *
     * <p>
     * See {@link DatagramSocket#send(DatagramPacket)} to check other limitations and requirements.
     *
     * @param message
     *            Message to send.
     *
     * @throws HttpException
     *             if an HTTP error occurs.
     * @throws IOException
     *             if an I/O error occurs.
     *
     * @see java.net.DatagramPacket
     * @see java.net.DatagramSocket
     * @see java.net.DatagramSocket#send(DatagramPacket)
     */
    public void sendMessage(HttpUdpMessage message) throws IOException, HttpException {
        DatagramPacket packet = DatagramMessageTools.buildPacket(this.client, message);

        this.peerSocket.send(packet);
    }

}
