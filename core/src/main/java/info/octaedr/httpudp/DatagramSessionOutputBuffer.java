/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import java.io.ByteArrayOutputStream;

import org.apache.http.impl.io.AbstractSessionOutputBuffer;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

// TODO: datagram? or just bytearray?
class DatagramSessionOutputBuffer extends AbstractSessionOutputBuffer {

    private final ByteArrayOutputStream outStream;

    public DatagramSessionOutputBuffer(HttpParams params) {
        super();

        int bufferSize = HttpConnectionParams.getSocketBufferSize(params);

        this.outStream = new ByteArrayOutputStream();

        init(this.outStream, bufferSize, params);
    }

    public byte[] getData() {
        return this.outStream.toByteArray();
    }

}
