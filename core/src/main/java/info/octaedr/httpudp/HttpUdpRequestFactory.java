/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import org.apache.http.HttpRequest;

import org.apache.http.HttpRequestFactory;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.RequestLine;
import org.apache.http.impl.DefaultHttpRequestFactory;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicHttpRequest;

/**
 * HTTP UDP request factory.
 *
 * Copied from DefaultHttpRequestFactory.
 *
 * @author Krzysztof Kapuscik
 *
 * @see DefaultHttpRequestFactory
 */
public class HttpUdpRequestFactory implements HttpRequestFactory {

    /**
     * Common RFC2616 methods.
     */
    private static final String[] RFC2616_COMMON_METHODS = { "GET" };

    /**
     * Entity transmission RFC2616 methods.
     */
    private static final String[] RFC2616_ENTITY_ENC_METHODS = { "POST", "PUT" };

    /**
     * Special RFC2616 methods.
     */
    private static final String[] RFC2616_SPECIAL_METHODS = { "HEAD", "OPTIONS", "DELETE", "TRACE", "CONNECT" };

    /**
     * SSDP specific methods.
     */
    private static final String[] SSDP_METHODS = { "M-SEARCH", "NOTIFY" };

    /**
     * Constructs new factory.
     */
    public HttpUdpRequestFactory() {
        super();
    }

    @Override
    public HttpRequest newHttpRequest(final RequestLine requestline) throws MethodNotSupportedException {
        if (requestline == null) {
            throw new IllegalArgumentException("Request line may not be null");
        }
        String method = requestline.getMethod();
        if (isOneOf(RFC2616_COMMON_METHODS, method)) {
            return new BasicHttpRequest(requestline);
        } else if (isOneOf(RFC2616_ENTITY_ENC_METHODS, method)) {
            return new BasicHttpEntityEnclosingRequest(requestline);
        } else if (isOneOf(RFC2616_SPECIAL_METHODS, method)) {
            return new BasicHttpRequest(requestline);
        } else if (isOneOf(SSDP_METHODS, method)) {
            return new BasicHttpRequest(requestline);
        } else {
            throw new MethodNotSupportedException(method + " method not supported");
        }
    }

    @Override
    public HttpRequest newHttpRequest(final String method, final String uri) throws MethodNotSupportedException {
        if (isOneOf(RFC2616_COMMON_METHODS, method)) {
            return new BasicHttpRequest(method, uri);
        } else if (isOneOf(RFC2616_ENTITY_ENC_METHODS, method)) {
            return new BasicHttpEntityEnclosingRequest(method, uri);
        } else if (isOneOf(RFC2616_SPECIAL_METHODS, method)) {
            return new BasicHttpRequest(method, uri);
        } else if (isOneOf(SSDP_METHODS, method)) {
            return new BasicHttpRequest(method, uri);
        } else {
            throw new MethodNotSupportedException(method + " method not supported");
        }
    }

    /**
     * Checks if method exists in given array.
     *
     * @param methods
     *            Array with methods names.
     * @param method
     *            Method to be checked.
     *
     * @return True if method exists in array, false otherwise.
     */
    private static boolean isOneOf(final String[] methods, final String method) {
        for (int i = 0; i < methods.length; i++) {
            if (methods[i].equalsIgnoreCase(method)) {
                return true;
            }
        }
        return false;
    }

}
