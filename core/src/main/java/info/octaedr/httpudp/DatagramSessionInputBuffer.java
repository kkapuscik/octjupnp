/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;

import org.apache.http.impl.io.AbstractSessionInputBuffer;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

class DatagramSessionInputBuffer extends AbstractSessionInputBuffer {

    public DatagramSessionInputBuffer(DatagramPacket packet, HttpParams params) {
        super();

        int bufferSize = HttpConnectionParams.getSocketBufferSize(params);

        InputStream inStream = new ByteArrayInputStream(packet.getData(), packet.getOffset(), packet.getLength());

        init(inStream, bufferSize, params);
    }

    @Override
    public boolean isDataAvailable(int timeout) throws IOException {
        // TODO?
        return true;
    }

}
