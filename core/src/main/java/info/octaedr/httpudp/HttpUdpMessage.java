/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.apache.http.HttpMessage;

/**
 * HTTP UDP message.
 *
 * @author Krzysztof Kapuscik
 */
public class HttpUdpMessage {

    /**
     * Encapsulated HTTP message
     */
    private HttpMessage httpMessage;

    /**
     * Source/Target address.
     */
    private InetAddress address;

    /**
     * Source/Target port.
     */
    private int port;

    /**
     * Constructs new message.
     *
     * @param message
     *            HTTP message to be encapsulated.
     */
    public HttpUdpMessage(HttpMessage message) {
        if (message == null) {
            throw new NullPointerException("Null message given");
        }

        init(message, null, 0);
    }

    /**
     * Constructs new message.
     *
     * @param message
     *            HTTP message to be encapsulated.
     * @param address
     *            Source/Target address.
     * @param port
     *            Source/Target port.
     */
    public HttpUdpMessage(HttpMessage message, InetAddress address, int port) {
        if (message == null) {
            throw new NullPointerException("Null message given");
        }

        init(message, address, port);
    }

    /**
     * Constructs new message.
     *
     * @param message
     *            HTTP message to be encapsulated.
     * @param socketAddress
     *            Source/Target socket address.
     */
    public HttpUdpMessage(HttpMessage message, SocketAddress socketAddress) {
        if (message == null) {
            throw new NullPointerException("Null message given");
        }

        init(message, null, 0);
        setSocketAddress(socketAddress);
    }

    /**
     * Initializes the message.
     *
     * @param message
     *            HTTP message to be encapsulated.
     * @param address
     *            Source/Target address.
     * @param port
     *            Source/Target port.
     */
    private void init(HttpMessage message, InetAddress address, int port) {
        this.httpMessage = message;
        setAddress(address);
        setPort(port);
    }

    /**
     * Returns encapsulated HTTP message.
     *
     * @return Encapsulated HTTP message.
     */
    public HttpMessage getHttpMessage() {
        return this.httpMessage;
    }

    /**
     * Returns source/target address.
     *
     * @return Source/Target address.
     */
    public InetAddress getAddress() {
        return this.address;
    }

    /**
     * Sets source/target address.
     *
     * @param address
     *            Source/Target address to set.
     */
    public void setAddress(InetAddress address) {
        this.address = address;
    }

    /**
     * Returns source/target socket address.
     *
     * @return Source/Target socket address.
     */
    public SocketAddress getSocketAddress() {
        return new InetSocketAddress(this.address, this.port);
    }

    /**
     * Sets source/target socket address.
     *
     * @param socketAddress
     *            Source/Target socket address to set.
     */
    public void setSocketAddress(SocketAddress socketAddress) {
        if (socketAddress == null || !(socketAddress instanceof InetSocketAddress)) {
            throw new IllegalArgumentException("unsupported address type");
        }

        InetSocketAddress addr = (InetSocketAddress) socketAddress;
        if (addr.isUnresolved()) {
            throw new IllegalArgumentException("unresolved address");
        }

        setAddress(addr.getAddress());
        setPort(addr.getPort());
    }

    /**
     * Returns source/target port.
     *
     * @return Source/Target port.
     */
    public int getPort() {
        return this.port;
    }

    /**
     * Sets source/target port.
     *
     * @param port
     *            Source/Target port to set.
     */
    public void setPort(int port) {
        if (port < 0 || port > 0xFFFF) {
            throw new IllegalArgumentException("Port out of range:" + port);
        }

        this.port = port;
    }

    @Override
    public String toString() {
        return "[" + super.toString() + " address=" + getAddress() + " port=" + getPort() + " message="
                + getHttpMessage() + "]";
    }

}
