/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import java.io.IOException;

import java.net.DatagramPacket;

import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestFactory;
import org.apache.http.HttpResponseFactory;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.conn.DefaultResponseParser;
import org.apache.http.impl.io.HttpRequestParser;
import org.apache.http.impl.io.HttpRequestWriter;
import org.apache.http.impl.io.HttpResponseWriter;
import org.apache.http.io.HttpMessageParser;
import org.apache.http.io.HttpMessageWriter;
import org.apache.http.message.BasicLineFormatter;
import org.apache.http.message.BasicLineParser;
import org.apache.http.message.LineFormatter;
import org.apache.http.message.LineParser;
import org.apache.http.params.HttpParams;

/**
 * HTTP UDP datagrams tools.
 *
 * @author Krzysztof Kapuscik
 */
class DatagramMessageTools {

    private static byte[] HTTP_RESPONSE_PREFIX = { 'H', 'T', 'T', 'P', '/', '1', '.' };

    private DatagramMessageTools() {
        // do nothing
    }

    static DatagramPacket createDatagramPacket(HttpUdpClient client) {
        byte[] buffer = new byte[client.getMaximumDatagramSize()];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

        return packet;
    }

    static HttpUdpMessage parsePacket(HttpUdpClient client, DatagramPacket packet) throws IOException, HttpException {
        HttpParams params = client.getHttpClient().getParams();
        DatagramSessionInputBuffer buffer = new DatagramSessionInputBuffer(packet, params);

        LineParser lineParser = new BasicLineParser();

        HttpMessageParser messageParser;
        HttpMessage httpMessage;

        if (isResponse(packet)) {
            HttpResponseFactory responseFactory = new DefaultHttpResponseFactory();
            messageParser = new DefaultResponseParser(buffer, lineParser, responseFactory, params);
        } else {
            HttpRequestFactory requestFactory = new HttpUdpRequestFactory();
            messageParser = new HttpRequestParser(buffer, lineParser, requestFactory, params);
        }

        httpMessage = messageParser.parse();

        return new HttpUdpMessage(httpMessage, packet.getAddress(), packet.getPort());
    }

    static DatagramPacket buildPacket(HttpUdpClient client, HttpUdpMessage message) throws IOException, HttpException {
        HttpParams params = client.getHttpClient().getParams();
        DatagramSessionOutputBuffer buffer = new DatagramSessionOutputBuffer(params);

        LineFormatter lineFormatter = new BasicLineFormatter();
        HttpMessageWriter messageWriter;

        HttpMessage httpMessage = message.getHttpMessage();

        if (httpMessage instanceof HttpRequest) {
            messageWriter = new HttpRequestWriter(buffer, lineFormatter, params);
        } else {
            messageWriter = new HttpResponseWriter(buffer, lineFormatter, params);
        }

        messageWriter.write(httpMessage);

        buffer.flush();

        byte[] data = buffer.getData();

        return new DatagramPacket(data, data.length, message.getSocketAddress());
    }

    private static boolean isResponse(DatagramPacket packet) {
        byte[] data = packet.getData();
        int offset = packet.getOffset();
        int length = packet.getLength();

        // "HTTP/1.1".length() == 8
        if (offset + 8 >= length) {
            return false;
        }

        for (int i = 0; i < HTTP_RESPONSE_PREFIX.length; ++i) {
            if (data[offset + i] != HTTP_RESPONSE_PREFIX[i]) {
                return false;
            }
        }

        return true;
    }

}
