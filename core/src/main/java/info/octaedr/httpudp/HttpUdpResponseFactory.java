/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import org.apache.http.ReasonPhraseCatalog;

import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.EnglishReasonPhraseCatalog;

/**
 * HTTP UDP response factory.
 *
 * @author Krzysztof Kapuscik
 */
public class HttpUdpResponseFactory extends DefaultHttpResponseFactory {

    /**
     * Creates a new response factory with the default catalog. The default catalog is
     * {@link EnglishReasonPhraseCatalog}.
     */
    public HttpUdpResponseFactory() {
        super();
    }

    /**
     * Creates a new response factory with the given catalog.
     *
     * @param catalog
     *            the catalog of reason phrases
     */
    public HttpUdpResponseFactory(ReasonPhraseCatalog catalog) {
        super(catalog);
    }

}
