/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.httpudp;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpUdpClient {

    private static final int MINIMUM_DATAGRAMS_SIZE = 1 * 1024;
    private static final int MAXIMUM_DATAGRAMS_SIZE = 64 * 1024;
    private static final int DEFAULT_MAXIMUM_DATAGRAM_SIZE = 8 * 1024;

    private final HttpClient httpClient;
    private int maximumDatagramSize;

    public HttpUdpClient() {
        this(new DefaultHttpClient());
    }

    public HttpUdpClient(HttpClient httpClient) {
        if (httpClient == null) {
            throw new NullPointerException();
        }
        this.httpClient = httpClient;
        setMaximumDatagramSize(DEFAULT_MAXIMUM_DATAGRAM_SIZE);
    }

    public HttpClient getHttpClient() {
        return this.httpClient;
    }

    public int getMaximumDatagramSize() {
        return this.maximumDatagramSize;
    }

    public void setMaximumDatagramSize(int size) {
        if (size < MINIMUM_DATAGRAMS_SIZE || size > MAXIMUM_DATAGRAMS_SIZE) {
            throw new IllegalArgumentException("Size outside allowed range: " + MINIMUM_DATAGRAMS_SIZE + ".."
                    + MAXIMUM_DATAGRAMS_SIZE);
        }
        this.maximumDatagramSize = size;
    }

}
