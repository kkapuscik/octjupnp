/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jupnp.core.DataType;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.tools.XMLTools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Parser for service description documents.
 */
class ServiceDescriptionParser extends DescriptionParser {

    /** XML namespace for device description documents. */
    static final String DOCUMENT_NAMESPACE = "urn:schemas-upnp-org:service-1-0";

    private static final String ATTRIBUTE_NAME__SEND_EVENTS = "sendEvents";

    private static final String ELEMENT_NAME__SCPD = "scpd";

    private static final String ELEMENT_NAME__ACTION_LIST = "actionList";

    private static final String ELEMENT_NAME__SERVICE_STATE_TABLE = "serviceStateTable";

    private static final String ELEMENT_NAME__STATE_VARIABLE = "stateVariable";

    private static final String ELEMENT_NAME__ACTION = "action";

    private static final String ELEMENT_NAME__NAME = "name";

    private static final String ELEMENT_NAME__DATA_TYPE = "dataType";

    private static final String ELEMENT_NAME__DEFAULT_VALUE = "defaultValue";

    private static final String ELEMENT_NAME__ALLOWED_VALUE_LIST = "allowedValueList";

    private static final String ELEMENT_NAME__ALLOWED_VALUE_RANGE = "allowedValueRange";

    private static final String ELEMENT_NAME__ALLOWED_VALUE = "allowedValue";

    private static final String ELEMENT_NAME__MINIMUM = "minimum";

    private static final String ELEMENT_NAME__MAXIMUM = "maximum";

    private static final String ELEMENT_NAME__STEP = "step";

    private static final String ELEMENT_NAME__ARGUMENT_LIST = "argumentList";

    private static final String ELEMENT_NAME__ARGUMENT = "argument";

    private static final String ELEMENT_NAME__DIRECTION = "direction";

    private static final String ELEMENT_NAME__RETVAL = "retval";

    private static final String ELEMENT_NAME__RELATED_STATE_VARIABLE = "relatedStateVariable";

    private static final Collection<String> EMPTY_ALLOWED_VALUES = Collections.emptyList();

    /*
     * Private constructor to forbid creating instances.
     */
    private ServiceDescriptionParser() {
        // nothing to do
    }

    /**
     * Parses action argument description.
     *
     * @param argumentElement
     *            Root element of argument description.
     * @param stateVars
     *            Collection of service's state variables.
     *
     * @return Parsed action argument.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    private static GenericActionArgument parseActionArgument(final Element argumentElement,
            final HashMap<String, GenericStateVariable> stateVars) throws UpnpException {
        String name;
        String direction;
        boolean isReturnValue;
        GenericStateVariable stateVariable;

        /* name */
        String argumentName = XMLTools.getUniqueChildElementValue(argumentElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__NAME);
        if (argumentName != null) {
            name = argumentName.trim();
            if (name.length() == 0) {
                throw new UpnpException("Empty action argument name");
            }
        } else {
            throw new UpnpException("Missing action argument name");
        }

        /* direction */
        String directionString = XMLTools.getUniqueChildElementValue(argumentElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__DIRECTION);
        if (ActionArgument.DIRECTION_IN.equals(directionString)) {
            direction = ActionArgument.DIRECTION_IN;
        } else if (ActionArgument.DIRECTION_OUT.equals(directionString)) {
            direction = ActionArgument.DIRECTION_OUT;
        } else {
            throw new UpnpException("Invalid action argument direction: " + directionString);
        }

        /* retval */
        Element retvalElement = XMLTools.findFirstChildElement(argumentElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__RETVAL);
        isReturnValue = (retvalElement != null);

        /* relatedStateVariable */
        String stateVariableName = XMLTools.getUniqueChildElementValue(argumentElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__RELATED_STATE_VARIABLE);
        if (stateVariableName != null) {
            stateVariableName = stateVariableName.trim();
            stateVariable = stateVars.get(stateVariableName);
            if (stateVariable == null) {
                throw new UpnpException("Unknown action argument related state variable: " + stateVariableName);
            }
        } else {
            throw new UpnpException("Missing action argument related state variable: ");
        }

        /* construct action argument */
        return new GenericActionArgument(name, direction, isReturnValue, stateVariable);
    }

    /**
     * Parses action description.
     *
     * @param actionElement
     *            Root element of action description.
     * @param stateVars
     *            Collection of service's state variables.
     *
     * @return Parsed action.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    private static GenericAction parseAction(final Element actionElement,
            final HashMap<String, GenericStateVariable> stateVars) throws UpnpException {
        String name = null;

        /* name */
        String stateVarName = XMLTools
                .getUniqueChildElementValue(actionElement, DOCUMENT_NAMESPACE, ELEMENT_NAME__NAME);
        if (stateVarName != null) {
            name = stateVarName.trim();
            if (name.length() == 0) {
                throw new UpnpException("Empty action name");
            }
        } else {
            throw new UpnpException("Missing action name");
        }

        /* argumentList */
        ArrayList<GenericActionArgument> arguments = new ArrayList<GenericActionArgument>();

        Element argumentListElement = XMLTools.findUniqueChildElement(actionElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__ARGUMENT_LIST);
        if (argumentListElement != null) {
            Iterator<Element> childIterator = XMLTools.createChildElementIterator(argumentListElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__ARGUMENT);
            while (childIterator.hasNext()) {
                Element argumentElement = childIterator.next();

                GenericActionArgument argumentInfo = parseActionArgument(argumentElement, stateVars);

                for (int i = 0; i < arguments.size(); ++i) {
                    if (arguments.get(i).getName().equals(argumentInfo.getName())) {
                        throw new UpnpException("Duplicated action argument: " + argumentInfo.getName());
                    }
                }

                arguments.add(argumentInfo);
            }
        }

        GenericActionArgument[] argumentsArray = arguments.toArray(new GenericActionArgument[arguments.size()]);

        /* construct action */
        return new GenericAction(name, argumentsArray);
    }

    /**
     * Parses state variable description.
     *
     * @param stateVariableElement
     *            Root element of state variable description.
     *
     * @return Parsed state variable.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    private static GenericStateVariable parseStateVariable(final Element stateVariableElement) throws UpnpException {
        boolean sendEvents;
        String name;
        DataType dataType;
        Object defaultValue = null;
        Object rangeMinimumValue = null;
        Object rangeMaximumValue = null;
        Object rangeStepValue = null;
        ArrayList<String> allowedValues = new ArrayList<String>();

        /* sendEvents attribute */
        String stateVarSendEvents = stateVariableElement.getAttribute(ATTRIBUTE_NAME__SEND_EVENTS);
        if (stateVarSendEvents != null) {
            if (stateVarSendEvents.equalsIgnoreCase("yes")) {
                sendEvents = true;
            } else if (stateVarSendEvents.equalsIgnoreCase("no")) {
                sendEvents = false;
            } else {
                throw new UpnpException("Invalid state variable sendEvents attribute: " + stateVarSendEvents);
            }
        } else {
            sendEvents = true; // default
        }

        /* name */
        String stateVarName = XMLTools.getUniqueChildElementValue(stateVariableElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__NAME);
        if (stateVarName != null) {
            name = stateVarName.trim();
            if (name.length() == 0) {
                throw new UpnpException("Empty state variable name");
            }
        } else {
            throw new UpnpException("Missing state variable name");
        }

        /* data type */
        String stateVarDataType = XMLTools.getUniqueChildElementValue(stateVariableElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__DATA_TYPE);
        if (stateVarDataType != null) {
            stateVarDataType = stateVarDataType.trim();
            if (stateVarDataType.length() == 0) {
                throw new UpnpException("Empty state variable data type");
            }
            dataType = DataType.parseDataType(stateVarDataType);
            if (dataType == null) {
                throw new UpnpException("Unknown state variable data type: " + stateVarDataType);
            }
        } else {
            throw new UpnpException("Missing state variable data type");
        }

        /* default value */
        String stateVarDefaultValue = XMLTools.getUniqueChildElementValue(stateVariableElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__DEFAULT_VALUE);
        if (stateVarDefaultValue != null) {
            stateVarDefaultValue = stateVarDefaultValue.trim();

            defaultValue = dataType.parseValue(stateVarDefaultValue);
            if (defaultValue == null) {
                throw new UpnpException("Invalid state variable default value: " + stateVarDefaultValue);
            }
        }

        /* allowed values list */
        Element allowedValueListElement = XMLTools.findUniqueChildElement(stateVariableElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__ALLOWED_VALUE_LIST);
        if (allowedValueListElement != null) {
            if (!dataType.isValueListAllowed()) {
                throw new UpnpException("Value list specified but not allowed");
            }

            Iterator<Element> elementIterator = XMLTools.createChildElementIterator(allowedValueListElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__ALLOWED_VALUE);
            while (elementIterator.hasNext()) {
                Element valueElement = elementIterator.next();

                /* extract value */
                String value = XMLTools.getElementValue(valueElement);
                if (value == null) {
                    throw new UpnpException("Invalid allowed value element");
                } else {
                    value = value.trim();
                }

                // TODO: Check allowed values...
                /* check the value */
                if (!dataType.checkValue(value, EMPTY_ALLOWED_VALUES, null, null, null)) {
                    throw new UpnpException("Invalid allowed value");
                }

                /* add allowed value to array */
                allowedValues.add(value);
            }
        }

        /* allowed values range */
        Element allowedValueRangeElement = XMLTools.findUniqueChildElement(stateVariableElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__ALLOWED_VALUE_RANGE);
        if (allowedValueRangeElement != null) {
            if (!dataType.isValueRangeAllowed()) {
                throw new UpnpException("Value range specified but not allowed");
            }

            /* get minimum, maximum, step */
            String minimumValueString = XMLTools.getUniqueChildElementValue(allowedValueRangeElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__MINIMUM);
            String maximumValueString = XMLTools.getUniqueChildElementValue(allowedValueRangeElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__MAXIMUM);
            String stepValueString = XMLTools.getUniqueChildElementValue(allowedValueRangeElement, DOCUMENT_NAMESPACE,
                    ELEMENT_NAME__STEP);

            /* check if required elements are present */
            if (minimumValueString == null) {
                throw new UpnpException("State variable range minimum value missing");
            }
            if (maximumValueString == null) {
                throw new UpnpException("State variable range maximum value missing");
            }

            /* check the type of values */
            rangeMinimumValue = dataType.parseValue(minimumValueString);
            if (rangeMinimumValue == null) {
                throw new UpnpException("Invalid state variable range minimum value");
            }
            rangeMaximumValue = dataType.parseValue(maximumValueString);
            if (rangeMaximumValue == null) {
                throw new UpnpException("Invalid state variable range maximum value");
            }
            if (stepValueString != null) {
                rangeStepValue = dataType.parseValue(stepValueString);
                if (rangeStepValue == null) {
                    throw new UpnpException("Invalid state variable range step value");
                }
            }

            /* validate the range again */
            if (!dataType.validateRange(rangeMinimumValue, rangeMaximumValue, rangeStepValue)) {
                throw new UpnpException("Invalid state variable values range");
            }
        }

        /* convert collection of allowed values to array */
        String[] allowedValuesArray = allowedValues.toArray(new String[allowedValues.size()]);

        /* construct state variable */
        return new GenericStateVariable(sendEvents, name, dataType, defaultValue, allowedValuesArray,
                rangeMinimumValue, rangeMaximumValue, rangeStepValue);
    }

    /**
     * Parses state table description.
     *
     * @param serviceStateTableElement
     *            Root element of state table description.
     *
     * @return Collection of parsed state variables.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    private static HashMap<String, GenericStateVariable> parseStateTable(final Element serviceStateTableElement)
            throws UpnpException {
        HashMap<String, GenericStateVariable> stateVars = null;

        stateVars = new HashMap<String, GenericStateVariable>();

        /* iterate over child elements of table element */
        Iterator<Element> elementIterator = XMLTools.createChildElementIterator(serviceStateTableElement,
                DOCUMENT_NAMESPACE, ELEMENT_NAME__STATE_VARIABLE);
        while (elementIterator.hasNext()) {
            Element childElement = elementIterator.next();

            /* parse state variable element */
            GenericStateVariable stateVarInfo = parseStateVariable(childElement);

            /* checking if variable with same name is already present */
            if (stateVars.get(stateVarInfo.getName()) != null) {
                throw new UpnpException("State variable defined twice: " + stateVarInfo.getName());
            }

            /* add state variable to array */
            stateVars.put(stateVarInfo.getName(), stateVarInfo);
        }

        return stateVars;
    }

    /**
     * Parses action list description.
     *
     * @param actionListElement
     *            Root element of action list description.
     * @param stateVars
     *            Collection of service's state variables.
     *
     * @return Collection of parsed actions.
     *
     * @throws UpnpException
     *             if description was invalid.
     */
    private static HashMap<String, GenericAction> parseActions(final Element actionListElement,
            final HashMap<String, GenericStateVariable> stateVars) throws UpnpException {
        HashMap<String, GenericAction> actions = new HashMap<String, GenericAction>();

        /* iterate over child elements of table element */
        Iterator<Element> elementIterator = XMLTools.createChildElementIterator(actionListElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__ACTION);
        while (elementIterator.hasNext()) {
            Element childElement = elementIterator.next();

            /* parse action element */
            GenericAction actionInfo = parseAction(childElement, stateVars);

            /* checking if variable with same name is already present */
            if (actions.get(actionInfo.getName()) != null) {
                throw new UpnpException("Duplicated action: " + actionInfo.getName());
            }

            /* add state variable to array */
            actions.put(actionInfo.getName(), actionInfo);
        }

        return actions;
    }

    /**
     * Parses service description.
     *
     * @param serviceInfo
     *            Service info from device description.
     * @param serviceDocument
     *            XML document with service description.
     *
     * @return Service object.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    static GenericService parseXML(final GenericServiceBrief serviceInfo, final Document serviceDocument)
            throws UpnpException {
        /* check the document */
        Element scpdElement = findRootElement(serviceDocument, DOCUMENT_NAMESPACE, ELEMENT_NAME__SCPD);

        /* parse state variables */
        HashMap<String, GenericStateVariable> stateVars = null;

        /* find state variables table element */
        Element serviceStateTableElement = XMLTools.findFirstChildElement(scpdElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__SERVICE_STATE_TABLE);
        if (serviceStateTableElement != null) {
            stateVars = parseStateTable(serviceStateTableElement);
        }

        /* - service must have > 0 state variables */
        // TODO: STRICT MODE
        // if (stateVars != null && stateVars.size() > 0) {
        if (stateVars == null) {
            throw new UpnpException("State variables parsing failed");
        }

        /* parse actions */
        HashMap<String, GenericAction> actions = null;

        /* find actions table element */
        Element actionListElement = XMLTools.findFirstChildElement(scpdElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__ACTION_LIST);
        if (actionListElement != null) {
            actions = parseActions(actionListElement, stateVars);
        }
        if (actions == null) {
            throw new UpnpException("Actions parsing failed");
        }

        /* set parsed elements */
        return new GenericService(serviceInfo, stateVars.values().toArray(new GenericStateVariable[stateVars.size()]),
                actions.values().toArray(new GenericAction[actions.size()]));
    }

}
