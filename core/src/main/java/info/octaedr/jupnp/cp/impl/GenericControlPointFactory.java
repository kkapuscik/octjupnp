/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.collection.WriteSafeArrayList;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointFactory;
import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;

public class GenericControlPointFactory extends ControlPointFactory {

    /**
     * Message factory singleton.
     */
    private static ControlPointFactory theInstance;

    /**
     * Returns factory singleton.
     *
     * @return Factory singleton.
     */
    public static synchronized ControlPointFactory getInstance() {
        if (theInstance == null) {
            theInstance = new GenericControlPointFactory();
        }
        return theInstance;
    }

    private final WriteSafeArrayList<ControlPointFactoryPlugin> plugins;

    /**
     * Contructs factory.
     */
    private GenericControlPointFactory() {
        this.plugins = new WriteSafeArrayList<ControlPointFactoryPlugin>();
    }

    @Override
    public void registerPlugin(ControlPointFactoryPlugin plugin) {
        if (plugin == null) {
            throw new NullPointerException("Null plugin given");
        }

        this.plugins.add(plugin);
    }

    @Override
    public ControlPoint createControlPoint() {
        Wrapper wrapper = new Wrapper();

        for (ControlPointFactoryPlugin plugin : this.plugins) {
            wrapper.processPlugin(plugin);
        }

        return new GenericControlPoint(wrapper);
    }

}
