/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.ActionRequestArguments;
import info.octaedr.jupnp.cp.StateVariable;

import java.util.HashMap;

/**
 * Generic implementation of action request arguments.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericActionRequestArguments implements ActionRequestArguments {

    /**
     * Action for which arguments were created.
     */
    private final Action action;

    /**
     * Arguments values.
     */
    private final HashMap<String, Object> arguments;

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericActionRequestArguments(Action action) {
        this.action = action;
        this.arguments = new HashMap<String, Object>();
    }

    @Override
    public Action getAction() {
        return this.action;
    }

    @Override
    public Object getArgumentValue(String name) throws UpnpException {
        return getArgumentValueInternal(name, null);
    }

    @Override
    public void clearArgumentValue(String name) throws UpnpException {
        clearArgumentValueInternal(name, null);
    }

    @Override
    public void setArgumentValueString(String name, String valueString) throws UpnpException {
        setArgumentValueInternal(name, null, valueString);
    }

    @Override
    public void setArgumentValue(String name, Object value) throws UpnpException {
        setArgumentValueInternal(name, null, value);
    }

    @Override
    public Object getInputArgumentValue(String name) throws UpnpException {
        return getArgumentValueInternal(name, ActionArgument.DIRECTION_IN);
    }

    @Override
    public void clearInputArgumentValue(String name) throws UpnpException {
        clearArgumentValueInternal(name, ActionArgument.DIRECTION_IN);
    }

    @Override
    public void setInputArgumentValueString(String name, String valueString) throws UpnpException {
        setArgumentValueInternal(name, ActionArgument.DIRECTION_IN, valueString);
    }

    @Override
    public void setInputArgumentValue(String name, Object value) throws UpnpException {
        setArgumentValueInternal(name, ActionArgument.DIRECTION_IN, value);
    }

    @Override
    public Object getOutputArgumentValue(String name) throws UpnpException {
        return getArgumentValueInternal(name, ActionArgument.DIRECTION_OUT);
    }

    @Override
    public void clearOutputArgumentValue(String name) throws UpnpException {
        clearArgumentValueInternal(name, ActionArgument.DIRECTION_OUT);
    }

    @Override
    public void setOutputArgumentValueString(String name, String valueString) throws UpnpException {
        setArgumentValueInternal(name, ActionArgument.DIRECTION_OUT, valueString);
    }

    @Override
    public void setOutputArgumentValue(String name, Object value) throws UpnpException {
        setArgumentValueInternal(name, ActionArgument.DIRECTION_OUT, value);
    }

    /**
     * Returns argument value.
     *
     * @param name
     *            Name of the argument.
     * @param requiredDirection
     *            Required argument direction.
     *
     * @return Value of the argument, null if not set.
     *
     * @throws UpnpException
     *             if there is no argument with given name or the argument direction was different from requested.
     */
    private synchronized Object getArgumentValueInternal(String name, String requiredDirection) throws UpnpException {
        /* find argument */
        ActionArgument argument = findArgumentUnsafe(name, requiredDirection);

        /* get the value */
        return this.arguments.get(argument.getName());
    }

    /**
     * Clears argument value.
     *
     * @param name
     *            Name of the argument.
     * @param requiredDirection
     *            Required argument direction.
     *
     * @throws UpnpException
     *             if there is no argument with given name or the argument direction was different from requested.
     */
    private synchronized void clearArgumentValueInternal(String name, String requiredDirection) throws UpnpException {
        /* find argument */
        ActionArgument argument = findArgumentUnsafe(name, requiredDirection);

        /* clear the value */
        this.arguments.remove(argument.getName());
    }

    /**
     * Sets the argument value from string representation.
     *
     * @param name
     *            Name of the argument.
     * @param requiredDirection
     *            Required argument direction.
     * @param valueString
     *            String representation of the value to set.
     *
     * @throws UpnpException
     *             if there is no argument with given name or the argument direction was different from requested or the
     *             value representation was invalid.
     */
    private synchronized void setArgumentValueInternal(String name, String requiredDirection, String valueString)
            throws UpnpException {
        if (valueString == null) {
            throw new NullPointerException("Null value given");
        }

        /* find argument */
        ActionArgument argument = findArgumentUnsafe(name, requiredDirection);

        /* parse the string representation */
        Object value = argument.getDataType().parseValue(valueString);
        if (value == null) {
            throw new UpnpException("Invalid value for argument: " + name + " = " + valueString);
        }

        /* check the value */
        checkValueUnsafe(argument, value);

        /* set the value */
        this.arguments.put(argument.getName(), value);
    }

    /**
     * Sets the argument value from string representation.
     *
     * @param name
     *            Name of the argument.
     * @param requiredDirection
     *            Required argument direction.
     * @param alue
     *            Value to set.
     *
     * @throws UpnpException
     *             if there is no argument with given name or the argument direction was different from requested.
     */
    private synchronized void setArgumentValueInternal(String name, String requiredDirection, Object value)
            throws UpnpException {
        if (value == null) {
            throw new NullPointerException("Null value given");
        }

        /* find argument */
        ActionArgument argument = findArgumentUnsafe(name, requiredDirection);

        /* check the value */
        checkValueUnsafe(argument, value);

        /* set the value */
        this.arguments.put(argument.getName(), value);
    }

    /**
     * Finds argument with given name and direction.
     *
     * @param name
     *            Name of the argument.
     * @param requiredDirection
     *            Required argument direction. Null to skip checking.
     *
     * @return Argument object.
     *
     * @throws UpnpException
     *             if there is no argument with given name or the argument direction was different from requested.
     */
    private ActionArgument findArgumentUnsafe(String name, String requiredDirection) throws UpnpException {
        if (name == null) {
            throw new NullPointerException("Argument name may not be null");
        }

        /* get action argument */
        ActionArgument argument = this.action.getArgument(name);
        if (argument == null) {
            throw new UpnpException("No such argument: " + name);
        }

        /* check argument direction */
        if (requiredDirection != null) {
            if (!argument.getDirection().equals(requiredDirection)) {
                throw new UpnpException("Argument direction mismatch: " + name);
            }
        }

        return argument;
    }

    /**
     * Checks if given value matches given argument requirements.
     *
     * @param argument
     *            Argument object.
     * @param value
     *            Value object.
     *
     * @throws UpnpException
     *             if value does not match argument requirements.
     */
    private void checkValueUnsafe(final ActionArgument argument, final Object value) throws UpnpException {
        StateVariable variable = argument.getRelatedStateVariable();
        if (argument.getDataType().checkValue(value, variable.getAllowedValues(), variable.getAllowedRangeMinimum(),
                variable.getAllowedRangeMaximum(), variable.getAllowedRangeStep()) == false) {

            throw new UpnpException("Invalid value for argument: " + argument.getName() + " = " + value);
        }
    }

}
