/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.tools.XMLTools;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

class DescriptionParser {

    /** Element name - specification version. */
    private static final String ELEMENT_NAME__SPEC_VERSION = "specVersion";

    /** Element name - specification version - major. */
    private static final String ELEMENT_NAME__SPEC_VERSION_MAJOR = "major";

    /** Element name - specification version - minor. */
    private static final String ELEMENT_NAME__SPEC_VERSION_MINOR = "minor";

    /**
     * Checks specification version used by document.
     *
     * @param rootElement
     *            Root element of document.
     * @param namespaceURI
     *            Document namespace URI.
     *
     * @return True if specification version used in document is supported, false otherwise.
     * @throws UpnpException
     */
    protected static void checkSpecificationVersion(final Element rootElement, final String namespaceURI)
            throws UpnpException {
        int majorVersion = -1;
        int minorVersion = -1;

        Element specElement = XMLTools.findUniqueChildElement(rootElement, namespaceURI, ELEMENT_NAME__SPEC_VERSION);
        if (specElement != null) {
            String majorString = XMLTools.getUniqueChildElementValue(specElement, namespaceURI,
                    ELEMENT_NAME__SPEC_VERSION_MAJOR);
            String minorString = XMLTools.getUniqueChildElementValue(specElement, namespaceURI,
                    ELEMENT_NAME__SPEC_VERSION_MINOR);

            if (majorString != null && minorString != null) {
                try {
                    majorVersion = Integer.parseInt(majorString, 10);
                    minorVersion = Integer.parseInt(minorString, 10);

                    if (majorVersion == 1 && minorVersion >= 0) {
                        return;
                    }
                } catch (NumberFormatException e) {
                }
            }
        }

        throw new UpnpException("Invalid spec version " + majorVersion + "." + minorVersion);
    }

    protected static Element findRootElement(final Document deviceDocument, final String namespaceURI,
            final String rootElementName) throws UpnpException {

        /* check root element name */
        Element rootElement = deviceDocument.getDocumentElement();
        if (rootElement == null) {
            throw new UpnpException("Missing document root element");
        }

        if (!XMLTools.checkElementName(rootElement, namespaceURI, rootElementName)) {
            throw new UpnpException("Invalid document root element: " + rootElement.getNodeName());
        }

        /* check specification version */
        checkSpecificationVersion(rootElement, namespaceURI);

        return rootElement;
    }

}
