/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.core.UpnpException;

// TODO: comments
public interface ActionRequest<ArgumentsType extends ActionRequestArguments> {

    public Action getAction();

    public ArgumentsType getArguments();

    public void execute(final ActionRequestListener<ArgumentsType> listener) throws UpnpException;

    public void cancel();

    public ActionRequestStatus getStatus();

    public int getErrorCode();

    public String getErrorDescription();

    public String getExecutionErrorDescription();

}
