/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.http.client.HttpClient;

/**
 * Device description retriever.
 *
 * <p>
 * This class is synchronized by the {@link DiscoveryManager}.
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
class DescriptionRetriever {

    /** Discovery manager for which this retriever was created. */
    private final DiscoveryManager discoveryManager;

    /**
     * Wrapper instance.
     */
    private Wrapper wrapper;

    /**
     * HTTP client instance.
     */
    private HttpClient httpClient;

    /**
     * Mapping of root device UUIDs to description retrieval tasks.
     */
    private final Map<UUID, DescriptionRetrieverTask> taskMap = new HashMap<UUID, DescriptionRetrieverTask>();

    /**
     * Constructs retriever.
     *
     * @param discoveryManager
     *            Discovery manager for which this retriever was created.
     */
    public DescriptionRetriever(DiscoveryManager discoveryManager) {
        this.discoveryManager = discoveryManager;
    }

    /**
     * Starts retriever.
     *
     * @param wrapper
     *            Wrapper to use.
     * @param httpClient
     *            HTTP client to use.
     */
    public void start(Wrapper wrapper, HttpClient httpClient) {
        this.wrapper = wrapper;
        this.httpClient = httpClient;
    }

    /**
     * Stops retriever.
     *
     * <p>
     * Method cancels all existing description retrieval tasks and removes them.
     * </p>
     */
    public void stop() {
        /* cancel all tasks */
        for (DescriptionRetrieverTask task : this.taskMap.values()) {
            task.cancel();
        }

        /* clear everything */
        this.taskMap.clear();

        /* this shall no longer be used */
        this.httpClient = null;
        this.wrapper = null;
    }

    /**
     * Updates device alive status.
     *
     * @param uuid
     *            UUID of the device.
     *
     * @return True if there was a description retrieval task and status was updated, false otherwise.
     */
    public boolean updateDeviceAliveStatus(UUID uuid) {
        DescriptionRetrieverTask task = this.taskMap.get(uuid);
        if (task != null) {
            task.updateDeviceStatus(uuid);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Schedules description retrieval task.
     *
     * @param uuid
     *            UUID of the root device.
     * @param location
     *            Location of device description document.
     * @param cacheControl
     *            Cache control value.
     */
    public void scheduleRetrieval(UUID uuid, URL location, int cacheControl) {
        DescriptionRetrieverTask task;

        /* create retrieval task */
        task = new DescriptionRetrieverTask(this, this.wrapper, this.httpClient, uuid, location, cacheControl);
        /* put task to running tasks map */
        this.taskMap.put(uuid, task);
        /* start task */
        task.start();
    }

    /**
     * Cancels description retrieval task.
     *
     * @param uuid
     *            UUID of the root device.
     *
     * @return True on success (there was a task to cancel), false otherwise.
     */
    public boolean cancelRetrieval(UUID uuid) {
        DescriptionRetrieverTask task = this.taskMap.remove(uuid);
        if (task != null) {
            task.cancel();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns retrieval process result.
     *
     * @param uuid
     *            UUID of the device the retrieval was started.
     *
     * @return Result of the retrieval. Null if retrieval was canceled or replaced in meantime.
     */
    public DescriptionRetrieverResult getResult(UUID uuid) {
        DescriptionRetrieverResult result = null;

        DescriptionRetrieverTask task = this.taskMap.get(uuid);
        if (task != null) {
            result = task.getResult();
            if (result != null) {
                this.taskMap.remove(uuid);
            }
        }

        return result;
    }

    /**
     * Processes description retrieval finished event.
     *
     * @param uuid
     *            UUID of the device for which retrieval was started.
     */
    public void processDescriptionRetrievalFinished(UUID uuid) {
        this.discoveryManager.processDescriptionRetrievalFinished(uuid);
    }

}
