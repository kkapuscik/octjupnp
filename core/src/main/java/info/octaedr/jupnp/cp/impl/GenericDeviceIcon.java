/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.DeviceIcon;

import java.net.URL;

/**
 * Device icon information.
 */
public class GenericDeviceIcon implements DeviceIcon {

    /** MIME type. */
    private final String mimeType;

    /** Width in pixels. */
    private final int width;

    /** Height in pixels. */
    private final int height;

    /** Number of color bits per pixel. */
    private final int depth;

    /** Pointer to icon image. */
    private final URL url;

    /** Device to which this icon belongs. */
    private GenericDevice device;

    /**
     * Constructs new icon info.
     *
     * @param mimeType
     *            MIME type.
     * @param width
     *            Width in pixels.
     * @param height
     *            Height in pixels.
     * @param depth
     *            Number of color bits per pixel.
     * @param url
     *            Pointer to icon image.
     */
    GenericDeviceIcon(String mimeType, final int width, final int height, final int depth, final URL url) {
        /* check arguments */
        if (mimeType == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        mimeType = mimeType.trim();
        if (mimeType.length() == 0) {
            throw new IllegalArgumentException("MIME type may not be empty");
        }
        if (width <= 0 || height <= 0 || depth <= 0) {
            throw new IllegalArgumentException("Width, heigth & depth must be positive");
        }
        if (url == null) {
            throw new IllegalArgumentException("URL may not be null");
        }

        /* store data */
        this.mimeType = mimeType;
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.url = url;
    }

    public void init(Wrapper wrapper, GenericDevice ownerDevice) {
        /* check parameters */
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }
        if (ownerDevice == null) {
            throw new NullPointerException("Null device given");
        }

        /* check state */
        if (this.device != null) {
            throw new IllegalStateException("Device already set");
        }

        /* store data */
        this.device = ownerDevice;

        /* initialize sub-elements */
        /* - nothing to do */

        /* restore links if needed */
        /* - nothing to do */
    }

    @Override
    public Device getDevice() {
        return this.device;
    }

    @Override
    public String getMIMEType() {
        return this.mimeType;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getDepth() {
        return this.depth;
    }

    @Override
    public URL getURL() {
        return this.url;
    }

}
