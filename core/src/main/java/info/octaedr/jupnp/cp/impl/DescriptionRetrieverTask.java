/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.UpnpException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import info.octaedr.tools.XMLTools;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

// TODO: comments
// TODO: canceling/aborting requests
class DescriptionRetrieverTask extends Thread {

    private static final Logger logger = Logger.getLogger(DescriptionRetrieverTask.class);

    private static final long MAX_DESCRIPTION_LENGTH = 64 * 1024;

    /**
     * State of the task.
     */
    private static enum RetrievalTaskState {
        /** Downloading device description document. */
        DOWNLOADING_DEVICE_DESCRIPTION,
        /** Downloading service description document. */
        DOWNLOADING_SERVICE_DESCRIPTION,

        /** Perform final device initialization. */
        INIT_DEVICE,

        /** Finished with success. */
        SUCCEEDED,
        /** Finished with error. */
        FAILED,
        /** Finished due to cancel request. */
        CANCELED
    }

    private static class ServiceListEntry {

        public GenericDevice device;
        public GenericServiceBrief brief;

        public ServiceListEntry(GenericDevice device, GenericServiceBrief brief) {
            this.device = device;
            this.brief = brief;
        }

    }

    private final DescriptionRetriever descriptionRetriever;
    private final Wrapper wrapper;
    private final HttpClient httpClient;
    private final UUID uuid;
    private final URL location;

    // TODO: convert to validity period END
    private final int cacheControl;

    private volatile RetrievalTaskState state;
    private volatile HttpGet currentGet;

    private GenericDevice rootDevice;
    private final LinkedList<ServiceListEntry> servicesList;

    public DescriptionRetrieverTask(DescriptionRetriever descriptionRetriever, Wrapper wrapper, HttpClient httpClient,
            UUID uuid, URL location, int cacheControl) {
        this.descriptionRetriever = descriptionRetriever;
        this.wrapper = wrapper;
        this.httpClient = httpClient;
        this.uuid = uuid;
        this.location = location;
        this.cacheControl = cacheControl;

        this.servicesList = new LinkedList<ServiceListEntry>();

        this.state = RetrievalTaskState.DOWNLOADING_DEVICE_DESCRIPTION;
    }

    public void updateDeviceStatus(UUID uuid) {
        // TODO Auto-generated method stub
    }

    public void cancel() {
        setState(RetrievalTaskState.CANCELED);
        if (this.currentGet != null) {
            this.currentGet.abort();
        }
    }

    @Override
    public void run() {
        for (;;) {
            if (!process()) {
                break;
            }
        }

        this.descriptionRetriever.processDescriptionRetrievalFinished(this.uuid);
    }

    /**
     * Processes the task.
     *
     * @return True if processing shall be continued, false otherwise.
     */
    private boolean process() {
        if (logger.trace()) {
            logger.trace("Process " + this.uuid + " - enter state=" + this.state);
        }

        boolean rv = true;

        switch (this.state) {
            case DOWNLOADING_DEVICE_DESCRIPTION:
                setState(downloadDeviceDescription());
                break;

            case DOWNLOADING_SERVICE_DESCRIPTION:
                setState(downloadServiceDescription());
                break;

            case INIT_DEVICE:
                setState(initDevice());
                break;

            case SUCCEEDED:
            case FAILED:
            case CANCELED:
                rv = false;
                break;

            default:
                throw new IllegalStateException();
        }

        if (logger.trace()) {
            logger.trace("Process " + this.uuid + " - exit state=" + this.state + " rv=" + rv);
        }

        return rv;
    }

    private RetrievalTaskState initDevice() {
        this.rootDevice = wrapper.wrap(this.rootDevice);
        this.rootDevice.init(wrapper, null);

        return RetrievalTaskState.SUCCEEDED;
    }

    private synchronized void setState(RetrievalTaskState newState) {
        if (this.state != RetrievalTaskState.CANCELED) {
            this.state = newState;
        }
    }

    public synchronized DescriptionRetrieverResult getResult() {
        DescriptionRetrieverResult result = null;

        switch (this.state) {
            case FAILED:
                result = new DescriptionRetrieverResult(this.uuid, this.location);
                break;

            case SUCCEEDED:
                result = new DescriptionRetrieverResult(this.rootDevice, this.cacheControl);
                break;

            default:
                // ignore
                break;
        }

        return result;
    }

    private RetrievalTaskState downloadDeviceDescription() {
        try {
            /* download and parse the document */
            Document deviceDescription = downloadAndParse(this.location);

            if (logger.trace()) {
                logger.trace("Device description document successfully downloaded");
            }

            /* process the XML */
            this.rootDevice = DeviceDescriptionParser.parseXML(this.location, this.uuid, deviceDescription);

            /* build list of services to process */
            buildServiceDescriptionsList(this.rootDevice);

            if (logger.trace()) {
                logger.trace("Service description list was build. Size: " + this.servicesList.size());
            }

            return RetrievalTaskState.DOWNLOADING_SERVICE_DESCRIPTION;
        } catch (Exception e) {
            if (logger.trace()) {
                logger.trace("Device description document download failed. Exception: " + e);
            }
            if (logger.trace()) {
                logger.trace("Device description failed.", e);
            }
        }

        return RetrievalTaskState.FAILED;
    }

    private void buildServiceDescriptionsList(GenericDevice device) {
        /* add all services of this device */
        for (GenericServiceBrief brief : device.getGenericBriefs()) {
            this.servicesList.add(new ServiceListEntry(device, brief));
        }

        /* process all subdevices */
        for (GenericDevice subdevice : device.getGenericSubdevices()) {
            buildServiceDescriptionsList(subdevice);
        }
    }

    private RetrievalTaskState downloadServiceDescription() {
        try {
            /* nothing more to download & parse - success */
            if (this.servicesList.size() == 0) {
                return RetrievalTaskState.INIT_DEVICE;
            }

            /* get service to process */
            ServiceListEntry entry = this.servicesList.removeFirst();

            /* download and parse the document */
            Document serviceDescription = downloadAndParse(entry.brief.getSCPDURL());

            if (logger.trace()) {
                logger.trace("Service description document successfully downloaded");
            }

            /* process the XML */
            GenericService service = ServiceDescriptionParser.parseXML(entry.brief, serviceDescription);

            /* add service to device */
            entry.device.setService(entry.brief, service);

            /* continue */
            return RetrievalTaskState.DOWNLOADING_SERVICE_DESCRIPTION;
        } catch (Exception e) {
            if (logger.trace()) {
                logger.trace("Service description document download failed. Exception: " + e);
            }
            if (logger.trace()) {
                logger.trace("Service description failed.", e);
            }
        }

        return RetrievalTaskState.FAILED;
    }

    /**
     * Download the XML document and parses it.
     *
     * @param url
     *            URL of the document.
     *
     * @return Parsed document.
     *
     *         TODO: cleanup of exceptions needed
     *
     * @throws ClientProtocolException
     * @throws IOException
     * @throws UpnpException
     * @throws URISyntaxException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IllegalStateException
     */
    private Document downloadAndParse(URL url) throws ClientProtocolException, IOException, UpnpException,
            URISyntaxException, IllegalStateException, ParserConfigurationException, SAXException {
        if (logger.trace()) {
            logger.trace("Creating get request for: " + url);
        }

        /* create get request */
        this.currentGet = new HttpGet(url.toURI());

        if (logger.trace()) {
            logger.trace("Executing request");
        }

        /* execute the request */
        HttpResponse response = this.httpClient.execute(this.currentGet);

        /* checking the response */
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new UpnpException("Invalid response status code for URL=" + url + " status="
                    + response.getStatusLine().getStatusCode());
        }

        if (logger.trace()) {
            logger.trace("Getting entity from response");
        }

        /* get response entity */
        HttpEntity entity = response.getEntity();

        if (logger.trace()) {
            logger.trace("Entity length: " + entity.getContentLength());
        }

        if (entity.getContentLength() > MAX_DESCRIPTION_LENGTH) {
            throw new IOException("Entity too long: " + entity.getContentLength());
        }

        // TODO: limiting length if unknown

        Document serviceDescription = XMLTools.parseDocument(entity.getContent());
        if (serviceDescription == null) {
            throw new UpnpException("Document parsing failed");
        }

        return serviceDescription;
    }

}
