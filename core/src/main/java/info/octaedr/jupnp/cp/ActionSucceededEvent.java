/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.core.UpnpException;

/**
 * Event sent when action was executed completely and finished with success.
 *
 * @author Krzysztof Kapuscik
 *
 * @param <ArgumentsType>
 *            Type of action request arguments object.
 */
public class ActionSucceededEvent<ArgumentsType extends ActionRequestArguments> extends
        ActionRequestEvent<ArgumentsType> {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -803301165234990752L;

    /**
     * Constructs event.
     *
     * @param source
     *            Action request being source of the event.
     */
    public ActionSucceededEvent(final ActionRequest<ArgumentsType> source) {
        super(source);
    }

    /**
     * Returns value of output argument returned in action response.
     *
     * @param name
     *            Name of the argument.
     *
     * @return Value of the argument.
     *
     * @throws UpnpException
     *             if such argument is not defined for action that was executed.
     */
    public Object getOutputArgumentValue(final String name) throws UpnpException {
        return getSource().getArguments().getOutputArgumentValue(name);
    }

}
