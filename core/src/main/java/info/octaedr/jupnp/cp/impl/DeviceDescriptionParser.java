/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.DescriptionConsts;
import info.octaedr.jupnp.core.DeviceProperties;
import info.octaedr.jupnp.core.DeviceProperty;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.core.UUIDTools;
import info.octaedr.tools.XMLTools;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Parser for device description documents.
 */
class DeviceDescriptionParser extends DescriptionParser {

    private static final Logger logger = Logger.getLogger(DeviceDescriptionParser.class);

    /** XML namespace for device description documents. */
    private static final String DOCUMENT_NAMESPACE = DescriptionConsts.DEVICE_DOCUMENT_NAMESPACE;
    /** Property separator between namespace and name. */
    private static final char PROPERTY_SEPARATOR = DescriptionConsts.PROPERTY_SEPARATOR;

    /** UUID values prefix. */
    private static final String UUID_PREFIX = "uuid:";

    private static final String ELEMENT_NAME__ROOT = "root";

    private static final String ELEMENT_NAME__DEVICE = "device";

    private static final String ELEMENT_NAME__UDN = "UDN";

    private static final String ELEMENT_NAME__URL_BASE = "URLBase";

    private static final String ELEMENT_NAME__SERVICE_LIST = "serviceList";

    private static final String ELEMENT_NAME__SERVICE = "service";

    private static final String ELEMENT_NAME__SERVICE_TYPE = "serviceType";

    private static final String ELEMENT_NAME__SERVICE_ID = "serviceId";

    private static final String ELEMENT_NAME__SPCD_URL = "SCPDURL";

    private static final String ELEMENT_NAME__CONTROL_URL = "controlURL";

    private static final String ELEMENT_NAME__EVENT_SUB_URL = "eventSubURL";

    private static final String ELEMENT_NAME__ICON_LIST = "iconList";

    private static final String ELEMENT_NAME__ICON = "icon";

    private static final String ELEMENT_NAME__MIMETYPE = "mimetype";

    private static final String ELEMENT_NAME__WIDTH = "width";

    private static final String ELEMENT_NAME__HEIGHT = "height";

    private static final String ELEMENT_NAME__DEPTH = "depth";

    private static final String ELEMENT_NAME__URL = "url";

    private static final String ELEMENT_NAME__DEVICE_LIST = "deviceList";

    private static final String PROPERTY_KEY__UDN = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + ELEMENT_NAME__UDN;

    /** Table of required device properties. */
    static final String[] REQUIRED_PROPERTIES = { DeviceProperty.DEVICE_TYPE, DeviceProperty.FRIENDLY_NAME,
            DeviceProperty.MANUFACTURER, DeviceProperty.MODEL_NAME, DeviceProperty.UDN };

    private static final GenericDevice[] EMPTY_DEVICE_ARRAY = new GenericDevice[0];

    private static final GenericDeviceIcon[] EMPTY_ICON_ARRAY = new GenericDeviceIcon[0];

    private static final GenericServiceBrief[] EMPTY_BRIEFS_ARRAY = new GenericServiceBrief[0];

    /*
     * Private constructor to forbid creating instances.
     */
    private DeviceDescriptionParser() {
        // nothing to do
    }

    /**
     * Parses device properties.
     *
     * @param deviceElement
     *            Root element of device description.
     *
     * @return Parsed properties.
     */
    private static DeviceProperties parseDeviceProperties(final Element deviceElement) {
        DeviceProperties properties = new DeviceProperties();

        /* find device properties */
        Iterator<Element> propertyElementIterator = XMLTools.createChildElementIterator(deviceElement);
        while (propertyElementIterator.hasNext()) {
            Element propertyElement = propertyElementIterator.next();

            /* check property value */
            String propertyValue = XMLTools.getElementValue(propertyElement);
            if (propertyValue != null) {
                /* prepare property name */
                String namespace = propertyElement.getNamespaceURI();
                String localName = propertyElement.getLocalName();
                String propertyKey;

                if (namespace != null) {
                    propertyKey = namespace + PROPERTY_SEPARATOR + localName;
                } else {
                    propertyKey = localName;
                }

                /* process property */
                if (properties.contains(propertyKey)) {
                    /* Already defined */
                    if (logger.info()) {
                        logger.info("DeviceInfo: Property already defined: " + propertyKey);
                    }
                } else {
                    /* Add to collection */
                    properties.add(propertyKey, propertyValue);
                    if (logger.trace()) {
                        logger.trace("DeviceInfo: Property added: " + propertyKey + " = " + propertyValue);
                    }
                }
            }
        }

        return properties;
    }

    /**
     * Checks if required properties are present.
     *
     * @param properties
     *            Collection of device's properties.
     *
     * @throws UpnpException
     *             if one or more of required properties is missing.
     */
    private static final void checkRequiredProperties(final DeviceProperties properties) throws UpnpException {
        /* check required properties */
        for (int i = 0; i < REQUIRED_PROPERTIES.length; ++i) {
            String value = properties.get(REQUIRED_PROPERTIES[i]);
            if (value == null) {
                throw new UpnpException("Required device property is missing: " + REQUIRED_PROPERTIES[i]);
            }
        }
    }

    /**
     * Parses icon description.
     *
     * @param iconElement
     *            Root element of icon description.
     * @param urlbase
     *            Base URL for creating absolute URLs.
     *
     * @return Parsed icon.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    private static GenericDeviceIcon parseIcon(final Element iconElement, final URL urlbase) throws UpnpException {
        try {
            /* get values */
            String mimeType = XMLTools.getUniqueChildElementValue(iconElement, DOCUMENT_NAMESPACE,
                    ELEMENT_NAME__MIMETYPE);
            String widthString = XMLTools.getUniqueChildElementValue(iconElement, DOCUMENT_NAMESPACE,
                    ELEMENT_NAME__WIDTH);
            String heightString = XMLTools.getUniqueChildElementValue(iconElement, DOCUMENT_NAMESPACE,
                    ELEMENT_NAME__HEIGHT);
            String depthString = XMLTools.getUniqueChildElementValue(iconElement, DOCUMENT_NAMESPACE,
                    ELEMENT_NAME__DEPTH);
            String urlString = XMLTools.getUniqueChildElementValue(iconElement, DOCUMENT_NAMESPACE, ELEMENT_NAME__URL);

            /* check if required elements are present */
            if (mimeType == null || widthString == null || heightString == null || depthString == null
                    || urlString == null) {
                throw new UpnpException("Icon required data is missing");
            }

            /* parse the values */
            int width = Integer.parseInt(widthString.trim(), 10);
            int height = Integer.parseInt(heightString.trim(), 10);
            int depth = Integer.parseInt(depthString.trim(), 10);
            URL iconURL = new URL(urlbase, urlString.trim());

            /* construct icon */
            return new GenericDeviceIcon(mimeType, width, height, depth, iconURL);
        } catch (NumberFormatException e) {
            throw new UpnpException("Icon numeric data is invalid");
        } catch (MalformedURLException e) {
            throw new UpnpException("Icon URL is invalid");
        }
    }

    /**
     * Parses icon list description.
     *
     * @param deviceElement
     *            Root element of device description.
     * @param urlbase
     *            Base URL for creating absolute URLs.
     *
     * @return Parsed collection of icons.
     *
     * @throws UpnpException
     *             if description was invalid.
     */
    private static GenericDeviceIcon[] parseIconList(final Element deviceElement, final URL urlbase)
            throws UpnpException {
        /* collection of icons */
        ArrayList<GenericDeviceIcon> iconList = new ArrayList<GenericDeviceIcon>();

        /* find icon list element */
        Element iconListElement = XMLTools.findUniqueChildElement(deviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__ICON_LIST);
        if (iconListElement != null) {
            /* get iterator over children */
            Iterator<Element> iconElementIterator = XMLTools.createChildElementIterator(iconListElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__ICON);
            while (iconElementIterator.hasNext()) {
                /* check if we found icon element */
                Element iconElement = iconElementIterator.next();

                /* parse the icon */
                GenericDeviceIcon iconInfo = parseIcon(iconElement, urlbase);
                iconList.add(iconInfo);
            }
        }

        /* convert icons collection to array */
        if (iconList.size() > 0) {
            return iconList.toArray(new GenericDeviceIcon[iconList.size()]);
        } else {
            return EMPTY_ICON_ARRAY;
        }
    }

    /**
     * Prepares base URL for creating absolute URLs.
     *
     * @param rootElement
     *            Root element of device description document.
     * @param location
     *            Location of device description document
     *
     * @return Base URL for creating absolute URLs.
     *
     * @throws UpnpException
     *             if device description document is invalid.
     */
    private static URL getURLBase(final Element rootElement, final URL location) throws UpnpException {
        Element urlbaseElement = XMLTools
                .findFirstChildElement(rootElement, DOCUMENT_NAMESPACE, ELEMENT_NAME__URL_BASE);
        if (urlbaseElement != null) {
            String value = XMLTools.getElementValue(urlbaseElement);
            if (value != null) {
                try {
                    return new URL(value);
                } catch (MalformedURLException e) {
                    throw new UpnpException("Invalid URLBase value: " + value);
                }
            } else {
                throw new UpnpException("Missing URLBase value");
            }
        } else {
            return location;
        }
    }

    /**
     * Parses device description.
     *
     * @param location
     *            Location of device description document.
     * @param udn
     *            UDN of the device to be parsed.
     * @param deviceDocument
     *            XML document with device description.
     *
     * @return Parsed device description.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    static GenericDevice parseXML(final URL location, final UUID udn, final Document deviceDocument)
            throws UpnpException {
        /* get document root element */
        Element rootElement = findRootElement(deviceDocument, DOCUMENT_NAMESPACE, ELEMENT_NAME__ROOT);

        /* get base URL */
        URL baseURL = getURLBase(rootElement, location);

        Element rootDeviceElement = XMLTools.findUniqueChildElement(rootElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__DEVICE);
        if (rootDeviceElement == null) {
            throw new UpnpException("No root device element found");
        }

        GenericDevice deviceInfo = parseDevice(rootDeviceElement, location, baseURL);
        if (!deviceInfo.getUDN().equals(udn)) {
            throw new UpnpException("Root device UDN do not match");
        }

        return deviceInfo;
    }

    private static GenericDevice parseDevice(final Element deviceElement, final URL location, final URL baseURL)
            throws UpnpException {
        /* parse & check properties */
        DeviceProperties properties = parseDeviceProperties(deviceElement);
        checkRequiredProperties(properties);

        /* parse icon list */
        GenericDeviceIcon[] icons = parseIconList(deviceElement, baseURL);

        /* parse services */
        GenericServiceBrief[] services = parseServiceList(deviceElement, baseURL);

        /* parse embedded device */
        GenericDevice[] subdevices = parseDeviceList(deviceElement, location, baseURL);

        /* construct device */
        String udnProperty = properties.get(PROPERTY_KEY__UDN);
        if (udnProperty == null) {
            throw new UpnpException("Device has no UDN property");
        }
        UUID udn = UUIDTools.parseUUID(udnProperty, UUID_PREFIX);
        if (udn == null) {
            throw new UpnpException("Device has invalid UDN: " + udnProperty);
        }

        return new GenericDevice(location, udn, baseURL, properties, icons, services, subdevices);
    }

    private static GenericDevice[] parseDeviceList(final Element deviceElement, final URL location, final URL baseURL)
            throws UpnpException {
        ArrayList<GenericDevice> subdeviceList = new ArrayList<GenericDevice>();

        Element deviceListElement = XMLTools.findUniqueChildElement(deviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__DEVICE_LIST);
        if (deviceListElement != null) {
            /* get iterator over children */
            Iterator<Element> iconElementIterator = XMLTools.createChildElementIterator(deviceListElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__DEVICE);
            while (iconElementIterator.hasNext()) {
                Element subDeviceElement = iconElementIterator.next();

                /* parse the subdevice */
                GenericDevice deviceInfo = parseDevice(subDeviceElement, location, baseURL);
                subdeviceList.add(deviceInfo);
            }
        }

        if (subdeviceList.size() > 0) {
            return subdeviceList.toArray(new GenericDevice[subdeviceList.size()]);
        } else {
            return EMPTY_DEVICE_ARRAY;
        }
    }

    /**
     * Parses service entry description.
     *
     * @param serviceElement
     *            Root element of service description.
     * @param baseURL
     *            Base URL for creating absolute URLs.
     *
     * @return Parsed service entry.
     *
     * @throws UpnpException
     *             if description is invalid.
     */
    private static GenericServiceBrief parseServiceEntry(final Element serviceElement, final URL baseURL)
            throws UpnpException {
        Element serviceTypeElement = XMLTools.findFirstChildElement(serviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__SERVICE_TYPE);
        Element serviceIDElement = XMLTools.findFirstChildElement(serviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__SERVICE_ID);
        Element scpdURLElement = XMLTools.findFirstChildElement(serviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__SPCD_URL);
        Element controlURLElement = XMLTools.findFirstChildElement(serviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__CONTROL_URL);
        Element eventSubURLElement = XMLTools.findFirstChildElement(serviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__EVENT_SUB_URL);

        if (serviceTypeElement == null || serviceIDElement == null || scpdURLElement == null
                || controlURLElement == null) {
            throw new UpnpException("Required service elements missing");
        }

        String serviceType = XMLTools.getElementValue(serviceTypeElement);
        String serviceID = XMLTools.getElementValue(serviceIDElement);
        String scpdURLValue = XMLTools.getElementValue(scpdURLElement);
        String controlURLValue = XMLTools.getElementValue(controlURLElement);
        String eventSubURLValue = eventSubURLElement != null ? XMLTools.getElementValue(eventSubURLElement) : null;

        if (serviceType == null || serviceID == null || scpdURLValue == null || controlURLValue == null) {
            throw new UpnpException("Required service elements values missing");
        }

        /* resolve URLs */
        try {
            URL scpdURL = new URL(baseURL, scpdURLValue);
            URL controlURL = new URL(baseURL, controlURLValue);
            URL eventSubURL = eventSubURLValue != null ? new URL(baseURL, eventSubURLValue) : null;

            return new GenericServiceBrief(serviceType, serviceID, scpdURL, controlURL, eventSubURL);
        } catch (MalformedURLException e) {
            throw new UpnpException("Service URL is invalid" + e.getMessage());
        }
    }

    /**
     * Parses device's service list.
     *
     * @param deviceElement
     *            Device description XML element.
     * @param baseURL
     *            Base URL for relative URLs in document.
     *
     * @return Parsed service list.
     *
     * @throws UpnpException
     *             if description document is invalid.
     */
    private static GenericServiceBrief[] parseServiceList(final Element deviceElement, URL baseURL)
            throws UpnpException {
        ArrayList<GenericServiceBrief> serviceList = new ArrayList<GenericServiceBrief>();

        /* find service list element */
        Element serviceListElement = XMLTools.findUniqueChildElement(deviceElement, DOCUMENT_NAMESPACE,
                ELEMENT_NAME__SERVICE_LIST);
        if (serviceListElement != null) {
            Iterator<Element> serviceIterator = XMLTools.createChildElementIterator(serviceListElement,
                    DOCUMENT_NAMESPACE, ELEMENT_NAME__SERVICE);
            while (serviceIterator.hasNext()) {
                Element serviceElement = serviceIterator.next();

                /* parse service entry */
                GenericServiceBrief serviceInfo = parseServiceEntry(serviceElement, baseURL);

                /* add to service list */
                serviceList.add(serviceInfo);
            }
        }

        if (serviceList.size() > 0) {
            return serviceList.toArray(new GenericServiceBrief[serviceList.size()]);
        } else {
            return EMPTY_BRIEFS_ARRAY;
        }
    }

}
