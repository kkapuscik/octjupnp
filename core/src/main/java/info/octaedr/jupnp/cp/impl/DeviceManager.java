/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.cp.Device;

import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Device database manager.
 *
 * <p>
 * This class is synchronized by the {@link DiscoveryManager}.
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
class DeviceManager {

    private static final Logger logger = Logger.getLogger(DeviceManager.class);

    /**
     * Control point being owner this device manager.
     */
    private final GenericControlPoint owner;

    /**
     * Collection of known root devices.
     */
    private final Map<UUID, Device> rootDevices = new HashMap<UUID, Device>();

    /**
     * Constructs new manager.
     *
     * @param owner
     *            Control point being owner this device manager.
     */
    public DeviceManager(GenericControlPoint owner) {
        this.owner = owner;
    }

    /**
     * Returns currently known root devices.
     *
     * @return Root devices collection. The collection may only be used under synchronization.
     */
    public Collection<Device> getRootDevicesUnsafe() {
        return this.rootDevices.values();
    }

    /**
     * Starts manager.
     */
    public void startUnsafe() {
        if (logger.trace()) {
            logger.trace("Started");
        }
    }

    /**
     * Stops managers.
     *
     * <p>
     * Removes all currently known devices.
     * </p>
     */
    public void stopUnsafe() {
        /* remove all known devices */
        UUID[] uuids = this.rootDevices.keySet().toArray(new UUID[this.rootDevices.size()]);
        for (int i = 0; i < uuids.length; ++i) {
            removeRootDeviceUnsafe(uuids[i]);
        }

        if (logger.trace()) {
            logger.trace("Stopped");
        }
    }

    /**
     * Adds root device.
     *
     * @param rootDevice
     *            Root device to be added.
     * @param cacheControl
     *            Cache control value. TODO: change to validity expiration time.
     */
    public void addRootDeviceUnsafe(Device rootDevice, int cacheControl) {
        if (this.rootDevices.get(rootDevice.getUDN()) != null) {
            throw new IllegalStateException("Device already present: " + rootDevice.getUDN());
        }

        if (logger.trace()) {
            logger.trace("Root device added: " + rootDevice);
        }

        this.rootDevices.put(rootDevice.getUDN(), rootDevice);

        this.owner.processRootDeviceAddedUnsafe(rootDevice);

        // TODO: schedule validity timer
    }

    /**
     * Updates root device validity.
     *
     * @param uuid
     *            UUID of the root device.
     * @param cacheControl
     *            Cache control value. TODO: change to validity expiration time.
     *
     * @return True if device was updated, false if there was no matching device.
     */
    public boolean updateRootDeviceValidityUnsafe(UUID uuid, int cacheControl) {
        boolean knownDevice = false;

        Device rootDevice = this.rootDevices.get(uuid);
        if (rootDevice != null) {
            if (logger.trace()) {
                logger.trace("Root device updated: " + rootDevice);
            }

            // TODO reschedule validity timer

            knownDevice = true;
        }

        return knownDevice;
    }

    /**
     * Removes root device.
     *
     * @param uuid
     *            UUID of the root device.
     *
     * @return True if device was removed, false if there was no matching device.
     */
    public boolean removeRootDeviceUnsafe(UUID uuid) {
        boolean knownDevice = false;

        Device rootDevice = this.rootDevices.remove(uuid);
        if (rootDevice != null) {
            if (logger.trace()) {
                logger.trace("Root device removed: " + rootDevice);
            }

            this.owner.processRootDeviceRemovedUnsafe(rootDevice);

            // TODO: deschedule validity timer

            knownDevice = true;
        }

        return knownDevice;
    }

    /**
     * Adds entry about device that is not operable.
     *
     * <p>
     * This method is used to create an entry about device that is not operable by control points due to different
     * reasons like inability to load device description.
     * </p>
     *
     * @param uuid
     *            UUID of the device.
     * @param location
     *            URL to the device description document.
     */
    public void addUnoperableDeviceEntryUnsafe(UUID uuid, URL location) {
        // TODO Auto-generated method stub
    }

}
