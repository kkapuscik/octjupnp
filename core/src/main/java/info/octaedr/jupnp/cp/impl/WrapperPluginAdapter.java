/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

/**
 * Adapter class for wrapper plugins.
 *
 * @author Krzysztof Kapuscik
 */
public abstract class WrapperPluginAdapter implements WrapperPlugin {

    @Override
    public GenericDevice wrap(GenericDevice device) {
        /* do not wrap anything */
        return null;
    }

    @Override
    public GenericDeviceIcon wrap(GenericDeviceIcon icon) {
        /* do not wrap anything */
        return null;
    }

    @Override
    public GenericServiceBrief wrap(GenericServiceBrief service) {
        /* do not wrap anything */
        return null;
    }

    @Override
    public GenericService wrap(GenericService service) {
        /* do not wrap anything */
        return null;
    }

    @Override
    public GenericAction wrap(GenericAction action) {
        /* do not wrap anything */
        return null;
    }

    @Override
    public GenericActionArgument wrap(GenericActionArgument argument) {
        /* do not wrap anything */
        return null;
    }

    @Override
    public GenericStateVariable wrap(GenericStateVariable variable) {
        /* do not wrap anything */
        return null;
    }

}
