/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.net.URL;

/**
 * UPnP service brief description.
 *
 * <p>
 * The brief description is taken from device description document.
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
public interface ServiceBrief {

    /**
     * Returns device that owns this service.
     *
     * @return Device object.
     */
    public Device getDevice();

    /**
     * Returns service identifier.
     *
     * @return Service identifier.
     */
    public String getServiceID();

    /**
     * Returns service type.
     *
     * @return Service type.
     */
    public String getServiceType();

    /**
     * Returns URL to service description.
     *
     * @return URL to service description.
     */
    public URL getSCPDURL();

    /**
     * Returns URL for sending control actions.
     *
     * @return URL for sending control actions.
     */
    public URL getControlURL();

    /**
     * Returns URL for subscribing for events.
     *
     * @return Event subscription URL or null.
     */
    public URL getEventSubURL();

    /**
     * Checks if service is compatible with given type.
     *
     * @param requestedType
     *            Type to compare with.
     *
     * @return True if this device is compatible with given type, false otherwise.
     */
    boolean isCompatible(String requestedType);

}
