/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import java.net.URL;

import info.octaedr.jupnp.core.UpnpTools;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.ServiceBrief;
import info.octaedr.tools.ArgumentTools;

public class GenericServiceBrief implements ServiceBrief {

    private final String serviceType;

    private final String serviceID;

    private final URL scpdURL;

    private final URL controlURL;

    private final URL eventSubURL;

    private Device device;

    public GenericServiceBrief(final String serviceType, final String serviceID, final URL scpdURL,
            final URL controlURL, final URL eventSubURL) {
        ArgumentTools.checkForNull(serviceType, "service type");
        ArgumentTools.checkForNull(serviceID, "service ID");
        ArgumentTools.checkForNull(scpdURL, "SCPD URL");

        this.serviceType = serviceType;
        this.serviceID = serviceID;
        this.scpdURL = scpdURL;
        this.controlURL = controlURL;
        this.eventSubURL = eventSubURL;
    }

    public GenericServiceBrief(ServiceBrief other) {
        ArgumentTools.checkForNull(other, "other");

        this.serviceType = other.getServiceType();
        this.serviceID = other.getServiceID();
        this.scpdURL = other.getSCPDURL();
        this.controlURL = other.getControlURL();
        this.eventSubURL = other.getEventSubURL();
    }

    public void init(Wrapper wrapper, GenericDevice ownerDevice) {
        /* check parameters */
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }
        if (ownerDevice == null) {
            throw new NullPointerException("Null device given");
        }

        /* check state */
        if (this.device != null) {
            throw new IllegalStateException("Device already set");
        }

        /* store data */
        this.device = ownerDevice;

        /* initialize sub-elements */
        /* - nothing to do */

        /* restore links if needed */
        /* - nothing to do */
    }

    @Override
    public Device getDevice() {
        return this.device;
    }

    @Override
    public String getServiceID() {
        return this.serviceID;
    }

    @Override
    public String getServiceType() {
        return this.serviceType;
    }

    @Override
    public URL getSCPDURL() {
        return this.scpdURL;
    }

    @Override
    public URL getControlURL() {
        return this.controlURL;
    }

    @Override
    public URL getEventSubURL() {
        return this.eventSubURL;
    }

    @Override
    public boolean isCompatible(String otherType) {
        if (otherType == null) {
            throw new NullPointerException("Null type given");
        }

        String thisType = getServiceType();

        /* just in case */
        if (thisType == null) {
            return false;
        }

        return UpnpTools.isCompatible(thisType, otherType);
    }

}
