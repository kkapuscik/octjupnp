/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

/**
 * Interface that must be implemented by control point factory plugins.
 *
 * <p>
 * Currently there is no API in plugins as the plugins systems is strongly bound to implementation internals.
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
public interface ControlPointFactoryPlugin {

    // nothing

}
