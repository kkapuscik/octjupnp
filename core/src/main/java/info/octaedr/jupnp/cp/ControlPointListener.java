/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.util.EventListener;

/**
 * Device database.
 *
 * @author Krzysztof Kapuscik
 */
public interface ControlPointListener extends EventListener {

    /**
     * Notifies that root device was added.
     *
     * @param event
     *            Event description.
     */
    void rootDeviceAdded(ControlPointEvent event);

    /**
     * Notifies that root device was removed.
     *
     * @param event
     *            Event description.
     */
    void rootDeviceRemoved(ControlPointEvent event);

    /**
     * Notifies that device was added.
     *
     * @param event
     *            Event description.
     */
    void deviceAdded(ControlPointEvent event);

    /**
     * Notifies that device was removed.
     *
     * @param event
     *            Event description.
     */
    void deviceRemoved(ControlPointEvent event);

}
