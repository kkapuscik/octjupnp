/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.collection.ReadOnlyArrayCollection;
import info.octaedr.jupnp.core.DeviceProperties;
import info.octaedr.jupnp.core.DeviceProperty;
import info.octaedr.jupnp.core.UpnpTools;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.DeviceIcon;
import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.ServiceBrief;
import info.octaedr.tools.ArgumentTools;

import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

/**
 * Device info.
 */
public class GenericDevice implements Device {

    /** Unique device number. */
    private final UUID udn;

    /** URL base. */
    private final URL baseURL;

    /** Location of device description document. */
    private final URL location;

    /** Collection of properties. */
    private final DeviceProperties properties;

    /** Icons. */
    private final GenericDeviceIcon[] icons;

    /** Collection of services. */
    private final GenericServiceBrief[] briefs;

    /** Collection of services. */
    private final GenericService[] services;

    /** Collection of subdevices. */
    private final GenericDevice[] subdevices;

    /** Parent device. */
    private GenericDevice parentDevice;

    /**
     * Constructs new device info.
     *
     * @param location
     *            Document location.
     * @param udn
     *            Device UDN.
     * @param baseURL
     *            Base URL.
     * @param properties
     *            Device properties.
     * @param icons
     *            Device icons.
     * @param briefs
     *            Collection of service briefs.
     * @param subdevices
     *            Collection of subdevices.
     */
    GenericDevice(final URL location, final UUID udn, final URL baseURL, final DeviceProperties properties,
            final GenericDeviceIcon[] icons, final GenericServiceBrief[] briefs, final GenericDevice[] subdevices) {
        ArgumentTools.checkForNull(location, "location");
        ArgumentTools.checkForNull(udn, "udn");
        ArgumentTools.checkForNull(properties, "properties");
        ArgumentTools.checkForNull(icons, "icons");
        ArgumentTools.checkForNull(briefs, "briefs");
        ArgumentTools.checkForNull(subdevices, "subdevices");

        this.location = location;
        this.udn = udn;
        this.baseURL = baseURL;

        this.properties = properties;

        this.icons = icons;
        this.briefs = briefs;
        this.subdevices = subdevices;
        this.services = new GenericService[briefs.length];
    }

    /**
     * Constructs shallow copy of other device.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Device to be copied.
     */
    protected GenericDevice(GenericDevice other) {
        this(other.location, other.udn, other.baseURL, other.properties, other.icons, other.briefs, other.subdevices);
        System.arraycopy(other.services, 0, this.services, 0, this.services.length);
    }

    public void setService(GenericServiceBrief brief, GenericService service) {
        if (brief == null) {
            throw new NullPointerException("Null brief given");
        }
        if (service == null) {
            throw new NullPointerException("Null service given");
        }

        int index = -1;
        for (int i = 0; i < this.briefs.length; ++i) {
            if (this.briefs[i] == brief) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            throw new IllegalArgumentException("Brief does not belong to this service");
        }

        if (this.services[index] != null) {
            throw new IllegalStateException("Service " + brief.getServiceID() + " already set");
        }

        this.services[index] = service;
    }

    public final void init(final Wrapper wrapper, final GenericDevice ownerDevice) {
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }

        /* check state */
        if (this.parentDevice != null) {
            throw new IllegalStateException("Parent device already set");
        }
        for (int i = 0; i < this.services.length; ++i) {
            if (this.services[i] == null) {
                throw new NullPointerException("Service " + i + " not initialized");
            }
        }

        /* store data */
        this.parentDevice = ownerDevice;

        /* initialize sub-elements */
        /* - state variables must go first as they will be used in action arguments initialization */
        for (int i = 0; i < this.subdevices.length; ++i) {
            this.subdevices[i] = wrapper.wrap(this.subdevices[i]);
            this.subdevices[i].init(wrapper, this);
        }
        for (int i = 0; i < this.services.length; ++i) {
            this.services[i] = wrapper.wrap(this.services[i]);
            this.services[i].init(wrapper, this);
        }
        for (int i = 0; i < this.icons.length; ++i) {
            this.icons[i] = wrapper.wrap(this.icons[i]);
            this.icons[i].init(wrapper, this);
        }

        /* restore links if needed */
        /* - nothing to do */

        postInit();
    }

    protected void postInit() {
        // nothing to do
    }

    @Override
    public Device getParentDevice() {
        return this.parentDevice;
    }

    @Override
    public URL getDescriptionLocation() {
        return this.location;
    }

    @Override
    public URL getBaseURL() {
        return this.baseURL;
    }

    @Override
    public String getProperty(final String key) {
        return this.properties.get(key);
    }

    @Override
    public Iterator<String> getPropertyKeys() {
        return this.properties.getKeys();
    }

    @Override
    public UUID getUDN() {
        return this.udn;
    }

    @Override
    public Collection<DeviceIcon> getIcons() {
        return new ReadOnlyArrayCollection<DeviceIcon>(this.icons);
    }

    @Override
    public Collection<Service> getServices() {
        return new ReadOnlyArrayCollection<Service>(this.services);
    }

    @Override
    public Collection<ServiceBrief> getBriefs() {
        return new ReadOnlyArrayCollection<ServiceBrief>(this.briefs);
    }

    @Override
    public Collection<Device> getSubdevices() {
        return new ReadOnlyArrayCollection<Device>(this.subdevices);
    }

    @Override
    public String getFriendlyName() {
        return getProperty(DeviceProperty.FRIENDLY_NAME);
    }

    @Override
    public String getDeviceType() {
        return getProperty(DeviceProperty.DEVICE_TYPE);
    }

    @Override
    public boolean isCompatible(String otherType) {
        if (otherType == null) {
            throw new NullPointerException("Null type given");
        }

        String thisType = getDeviceType();

        /* just in case */
        if (thisType == null) {
            return false;
        }

        return UpnpTools.isCompatible(thisType, otherType);
    }

    @Override
    public String toString() {
        return "[" + super.toString() + " UDN=" + getUDN() + " name=" + getProperty(DeviceProperty.FRIENDLY_NAME) + "]";
    }

    public Collection<GenericServiceBrief> getGenericBriefs() {
        return new ReadOnlyArrayCollection<GenericServiceBrief>(this.briefs);
    }

    public Collection<GenericDevice> getGenericSubdevices() {
        return new ReadOnlyArrayCollection<GenericDevice>(this.subdevices);
    }

}
