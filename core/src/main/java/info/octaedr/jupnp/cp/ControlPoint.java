/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

/**
 * UPnP control point.
 *
 * @author Krzysztof Kapuscik
 */
public interface ControlPoint {

    // TODO: configuration (user agent etc.)

    /**
     * Starts control point.
     */
    public void start();

    /**
     * Stops control point.
     */
    public void stop();

    /**
     * Rescans network looking for new devices.
     */
    public void rescanNetwork();

    /**
     * Adds control point listener.
     *
     * @param listener
     *            Listener to be added.
     */
    public void addControlPointListener(ControlPointListener listener);

    /**
     * Removes control point listener.
     *
     * @param listener
     *            Listener to be removed.
     */
    public void removeControlPointListener(ControlPointListener listener);

}
