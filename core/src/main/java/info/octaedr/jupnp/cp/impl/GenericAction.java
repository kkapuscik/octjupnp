/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import java.util.Collection;

import info.octaedr.jcommons.collection.ReadOnlyArrayCollection;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.ActionRequestArguments;

public class GenericAction implements Action {

    private final String name;

    private final GenericActionArgument[] arguments;
    private final GenericActionArgument[] inputArguments;
    private final GenericActionArgument[] outputArguments;
    private GenericService service;

    public GenericAction(final String name, final GenericActionArgument[] arguments) {
        this.name = name;
        this.arguments = arguments;

        int inputCount = 0;
        int outputCount = 0;
        for (int i = 0; i < arguments.length; ++i) {
            if (arguments[i].getDirection().equals(ActionArgument.DIRECTION_IN)) {
                ++inputCount;
            } else if (arguments[i].getDirection().equals(ActionArgument.DIRECTION_OUT)) {
                ++outputCount;
            }
        }

        this.inputArguments = new GenericActionArgument[inputCount];
        this.outputArguments = new GenericActionArgument[outputCount];

        inputCount = 0;
        outputCount = 0;
        for (int i = 0; i < arguments.length; ++i) {
            if (arguments[i].getDirection().equals(ActionArgument.DIRECTION_IN)) {
                this.inputArguments[inputCount] = arguments[i];
            } else if (arguments[i].getDirection().equals(ActionArgument.DIRECTION_OUT)) {
                this.outputArguments[outputCount] = arguments[i];
            }
        }
    }

    void init(final Wrapper wrapper, final GenericService ownerService) {
        /* check parameters */
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }
        if (ownerService == null) {
            throw new NullPointerException("Null service given");
        }

        /* check state */
        if (this.service != null) {
            throw new IllegalStateException("Service already set");
        }

        /* store data */
        this.service = ownerService;

        /* initialize sub-elements */
        if (this.arguments != null) {
            for (int i = 0; i < this.arguments.length; ++i) {
                this.arguments[i] = wrapper.wrap(this.arguments[i]);
                this.arguments[i].init(wrapper, this);
            }
        }

        /* restore links if needed */
        /* - nothing to do */
    }

    @Override
    public GenericService getService() {
        if (this.service == null) {
            throw new IllegalStateException("Service not set");
        }
        return this.service;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Collection<ActionArgument> getArguments() {
        return new ReadOnlyArrayCollection<ActionArgument>(this.arguments);
    }

    @Override
    public Collection<ActionArgument> getInputArguments() {
        return new ReadOnlyArrayCollection<ActionArgument>(this.inputArguments);
    }

    @Override
    public Collection<ActionArgument> getOutputArguments() {
        return new ReadOnlyArrayCollection<ActionArgument>(this.outputArguments);
    }

    @Override
    public ActionArgument getArgument(final String argumentName) {
        if (argumentName == null) {
            throw new IllegalArgumentException("Name may not be null");
        }

        if (this.arguments != null) {
            for (int i = 0; i < this.arguments.length; ++i) {
                if (this.arguments[i].getName().equals(argumentName)) {
                    return this.arguments[i];
                }
            }
        }

        return null;
    }

    @Override
    public ActionRequest<ActionRequestArguments> createRequest() {
        return new GenericActionRequest<ActionRequestArguments>(new GenericActionRequestArguments(this));
    }

}
