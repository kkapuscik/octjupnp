/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import java.util.ArrayList;

import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;

public class Wrapper {

    private final ArrayList<WrapperPlugin> plugins;

    public Wrapper() {
        this.plugins = new ArrayList<WrapperPlugin>();
    }

    public void processPlugin(ControlPointFactoryPlugin plugin) {
        if (plugin instanceof WrapperPlugin) {
            this.plugins.add((WrapperPlugin) plugin);
        }
    }

    public GenericDevice wrap(GenericDevice device) {
        GenericDevice rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(device);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = device;
        }

        return rv;
    }

    public GenericDeviceIcon wrap(GenericDeviceIcon icon) {
        GenericDeviceIcon rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(icon);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = icon;
        }

        return rv;
    }

    public GenericServiceBrief wrap(GenericServiceBrief brief) {
        GenericServiceBrief rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(brief);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = brief;
        }

        return rv;
    }

    public GenericService wrap(GenericService service) {
        GenericService rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(service);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = service;
        }

        return rv;
    }

    public GenericAction wrap(GenericAction action) {
        GenericAction rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(action);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = action;
        }

        return rv;
    }

    public GenericActionArgument wrap(GenericActionArgument argument) {
        GenericActionArgument rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(argument);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = argument;
        }

        return rv;
    }

    public GenericStateVariable wrap(GenericStateVariable variable) {
        GenericStateVariable rv = null;

        for (WrapperPlugin plugin : plugins) {
            rv = plugin.wrap(variable);
            if (rv != null) {
                break;
            }
        }

        if (rv == null) {
            rv = variable;
        }

        return rv;
    }

}
