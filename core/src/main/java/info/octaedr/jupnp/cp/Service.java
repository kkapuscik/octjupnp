/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.util.Collection;

/**
 * UPnP service.
 *
 * @author Krzysztof Kapuscik
 */
public interface Service extends ServiceBrief {

    /**
     * Returns service state variables.
     *
     * @return Collection of state variables.
     */
    Collection<StateVariable> getStateVariables();

    /**
     * Returns service state variable by name.
     *
     * @param name
     *            Name of the state variable to find.
     *
     * @return State variable object if found, null otherwise.
     */
    StateVariable getStateVariable(String name);

    /**
     * Returns service actions.
     *
     * @return Collection of actions.
     */
    Collection<Action> getActions();

    /**
     * Returns service action by name.
     *
     * @param name
     *            Name of the action to find.
     *
     * @return Action object if found, null otherwise.
     */
    public Action getAction(String name);

}
