/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointListener;
import info.octaedr.jupnp.cp.Device;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;

/**
 * Manager for control points.
 *
 * TODO: configuration (user agent etc.)
 *
 * @author Krzysztof Kapuscik
 */
public class GenericControlPoint implements ControlPoint {

    private static final Logger logger = Logger.getLogger(GenericControlPoint.class);

    /** Wrapper instance. */
    private final Wrapper wrapper;

    /** Object for synchronizing control point activities. */
    private final ControlPointSyncState syncState;

    /** Event manager. */
    private final ControlPointEventManager eventManager;

    /** Device database. */
    private final DeviceManager deviceManager;
    /** Discovery manager. */
    private final DiscoveryManager discovery;
    /** Control manager. */
    private final ControlManager control;
    /** Eventing manager. */
    private final EventingManager eventing;

    /**
     * HTTP client instance.
     */
    private HttpClient httpClient;

    /**
     * Constructs new manager.
     *
     * @param wrapper
     *            UPnP elements wrapper instance.
     */
    GenericControlPoint(Wrapper wrapper) {
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }

        this.wrapper = wrapper;

        this.syncState = new ControlPointSyncState();

        this.eventManager = new ControlPointEventManager(this);

        this.deviceManager = new DeviceManager(this);

        this.discovery = new DiscoveryManager(this.syncState, this.deviceManager);
        this.control = new ControlManager(this.syncState);
        this.eventing = new EventingManager(this.syncState);
    }

    @Override
    public void start() {
        synchronized (this.syncState) {
            if (logger.trace()) {
                logger.trace("Starting control point");
            }

            if (!this.syncState.isStarted()) {
                this.syncState.markStarted();

                this.httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());

                this.deviceManager.startUnsafe();

                this.eventing.startUnsafe();
                this.control.startUnsafe();
                this.discovery.startUnsafe(this.wrapper, this.httpClient);

                if (logger.trace()) {
                    logger.trace("Starting control point finished");
                }
            } else {
                if (logger.trace()) {
                    logger.trace("Control point already started");
                }
            }
        }
    }

    @Override
    public void stop() {
        synchronized (this.syncState) {
            if (logger.trace()) {
                logger.trace("Stopping control point");
            }

            if (this.syncState.isStarted()) {
                this.syncState.markStopped();

                this.discovery.stopUnsafe();
                this.control.stopUnsafe();
                this.eventing.stopUnsafe();

                this.deviceManager.stopUnsafe();

                /*
                 * When HttpClient instance is no longer needed, shut down the connection manager to ensure immediate
                 * deallocation of all system resources.
                 */
                this.httpClient.getConnectionManager().shutdown();
                this.httpClient = null;

                if (logger.trace()) {
                    logger.trace("Stopping control point finished");
                }
            } else {
                if (logger.trace()) {
                    logger.trace("Control point not started");
                }
            }
        }
    }

    @Override
    public void rescanNetwork() {
        synchronized (this.syncState) {
            if (logger.trace()) {
                logger.trace("Rescan network");
            }

            if (this.syncState.isStarted()) {
                this.discovery.rescanNetworkUnsafe();
            }
        }
    }

    @Override
    public void addControlPointListener(ControlPointListener listener) {
        synchronized (this.syncState) {
            this.eventManager.addListener(listener);

            this.eventManager.notifyInitialState(listener, this.deviceManager.getRootDevicesUnsafe());
        }
    }

    @Override
    public void removeControlPointListener(ControlPointListener listener) {
        synchronized (this.syncState) {
            this.eventManager.removeListener(listener);
        }
    }

    void processRootDeviceAddedUnsafe(Device rootDevice) {
        this.eventManager.notifyRootDeviceAdded(rootDevice);
    }

    void processRootDeviceRemovedUnsafe(Device rootDevice) {
        this.eventManager.notifyRootDeviceRemoved(rootDevice);
    }

}
