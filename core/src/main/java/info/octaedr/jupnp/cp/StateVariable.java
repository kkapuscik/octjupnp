/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.core.DataType;

import java.util.Collection;

/**
 * UPnP service state variable.
 *
 * @author Krzysztof Kapuscik
 */
public interface StateVariable {

    // TODO: comments

    public Service getService();

    public boolean isSendingEvents();

    public String getName();

    public DataType getDataType();

    public Object getDefaultValue();

    /**
     * Returns collection of allowed values.
     *
     * @return Read-only collection of allowed values. Empty if there are no allowed values list defined.
     */
    public Collection<String> getAllowedValues();

    public Object getAllowedRangeMinimum();

    public Object getAllowedRangeMaximum();

    public Object getAllowedRangeStep();

}
