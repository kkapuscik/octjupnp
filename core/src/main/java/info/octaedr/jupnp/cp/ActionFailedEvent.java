/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

/**
 * Event sent when action was executed completely and finished with error.
 *
 * @author Krzysztof Kapuscik
 *
 * @param <ArgumentsType>
 *            Type of action request arguments object.
 */
public class ActionFailedEvent<ArgumentsType extends ActionRequestArguments> extends ActionRequestEvent<ArgumentsType> {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 4512733349835318683L;

    /**
     * Constructs event.
     *
     * @param request
     *            Action request being source of the event.
     */
    public ActionFailedEvent(final ActionRequest<ArgumentsType> request) {
        super(request);
    }

    /**
     * Returns error code from received response.
     *
     * @return Error code value.
     */
    public int getErrorCode() {
        return getSource().getErrorCode();
    }

    /**
     * Returns error description from received response.
     *
     * @return Error description.
     */
    public String getErrorDescription() {
        return getSource().getErrorDescription();
    }

}
