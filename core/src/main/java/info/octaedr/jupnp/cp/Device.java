/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.core.DeviceProperty;

import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

/**
 * UPnP device.
 */
public interface Device {

    /**
     * Returns parent device.
     *
     * @return Parent device of this device. Null if this device is a root device.
     */
    Device getParentDevice();

    /**
     * Returns location of the description document.
     *
     * @return URL to description document.
     */
    URL getDescriptionLocation();

    /**
     * Returns base URL for relative addresses.
     *
     * <p>
     * Note: this is not URLBase from XML. It is the URL which can be used to construct absolute URIs defined by UPnP
     * specification.
     * </p>
     *
     * @return Base URL that can be used to construct absolute addresses.
     */
    URL getBaseURL();

    /**
     * Returns device property.
     *
     * @param key
     *            Key of the property.
     *
     * @return Value of the property or null if property is not defined.
     */
    String getProperty(final String key);

    /**
     * Returns keys of available properties.
     *
     * @return Iterator over collection of keys of available properties.
     */
    Iterator<String> getPropertyKeys();

    /**
     * Returns unique device number.
     *
     * @return UDN of device.
     */
    UUID getUDN();

    /**
     * Returns device type.
     *
     * @return Value of {@link #getProperty(String)} for {@link DeviceProperty#DEVICE_TYPE}.
     */
    String getDeviceType();

    /**
     * Returns friendly name.
     *
     * @return Value of {@link #getProperty(String)} for {@link DeviceProperty#FRIENDLY_NAME}.
     */
    String getFriendlyName();

    /**
     * Checks if device is compatible with given type.
     *
     * @param requestedType
     *            Type to compare with.
     *
     * @return True if this device is compatible with given type, false otherwise.
     */
    boolean isCompatible(String requestedType);

    /**
     * Returns collection of device icons.
     *
     * @return Device icons.
     */
    Collection<DeviceIcon> getIcons();

    /**
     * Returns collection of device services briefs .
     *
     * @return Device services briefs.
     */
    Collection<ServiceBrief> getBriefs();

    /**
     * Returns collection of device services.
     *
     * @return Device services.
     */
    Collection<Service> getServices();

    /**
     * Returns collection of device subdevices.
     *
     * @return Device subdevices.
     */
    Collection<Device> getSubdevices();

}
