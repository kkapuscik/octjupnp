/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jupnp.cp.Device;

import java.net.URL;
import java.util.UUID;

public class DescriptionRetrieverResult {

    private final UUID uuid;
    private final URL location;
    private final Device rootDevice;
    private final int cacheControl;

    /**
     * Creates failed retrieval result.
     *
     * @param uuid
     *            UUID of the device.
     * @param location
     *            Device description document URL.
     */
    public DescriptionRetrieverResult(UUID uuid, URL location) {
        this.uuid = uuid;
        this.location = location;
        this.rootDevice = null;
        this.cacheControl = -1;
    }

    public DescriptionRetrieverResult(Device rootDevice, int cacheControl) {
        this.uuid = rootDevice.getUDN();
        this.location = rootDevice.getDescriptionLocation();
        this.rootDevice = rootDevice;
        this.cacheControl = cacheControl;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public URL getLocation() {
        return this.location;
    }

    public Device getRootDevice() {
        return this.rootDevice;
    }

    // TODO: convert to validity end time
    public int getCacheControl() {
        return this.cacheControl;
    }

}
