/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.event.EventListenerManager;
import info.octaedr.jcommons.event.QueuedEventDispatcher;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointEvent;
import info.octaedr.jupnp.cp.ControlPointListener;
import info.octaedr.jupnp.cp.Device;

import java.util.Collection;

/**
 * Event manager for control point.
 *
 * @author Krzysztof Kapuscik
 */
class ControlPointEventManager extends EventListenerManager<ControlPointListener, ControlPointEvent> {

    private final ControlPoint controlPoint;

    /**
     * Control point events dispatcher.
     */
    private static class Dispatcher extends QueuedEventDispatcher<ControlPointListener, ControlPointEvent> {

        @Override
        protected void notifyEvent(ControlPointListener listener, ControlPointEvent event) {
            switch (event.getType()) {
                case DEVICE_ADDED:
                    listener.deviceAdded(event);
                    break;

                case DEVICE_REMOVED:
                    listener.deviceRemoved(event);
                    break;

                case ROOT_DEVICE_ADDED:
                    listener.rootDeviceAdded(event);
                    break;

                case ROOT_DEVICE_REMOVED:
                    listener.rootDeviceRemoved(event);
                    break;

                default:
                    throw new IllegalArgumentException();
            }
        }

    }

    /**
     * Constructs new manager.
     *
     * @param controlPoint
     *            Control point for which this manager was created.
     */
    public ControlPointEventManager(ControlPoint controlPoint) {
        super(new Dispatcher());
        this.controlPoint = controlPoint;
    }

    public synchronized void notifyInitialState(ControlPointListener listener, Collection<Device> rootDevices) {
        for (Device rootDevice : rootDevices) {
            scheduleEventUnsafe(listener, new ControlPointEvent(this.controlPoint,
                    ControlPointEvent.Type.ROOT_DEVICE_ADDED, rootDevice));
            notifyInitialState(listener, rootDevice);
        }
    }

    private void notifyInitialState(ControlPointListener listener, Device device) {
        scheduleEventUnsafe(listener, new ControlPointEvent(this.controlPoint, ControlPointEvent.Type.DEVICE_ADDED,
                device));

        for (Device subdevice : device.getSubdevices()) {
            notifyInitialState(listener, subdevice);
        }
    }

    public synchronized void notifyRootDeviceAdded(Device rootDevice) {
        scheduleEvent(new ControlPointEvent(this.controlPoint, ControlPointEvent.Type.ROOT_DEVICE_ADDED, rootDevice));

        notifyDeviceAdded(rootDevice);
    }

    private void notifyDeviceAdded(Device device) {
        scheduleEvent(new ControlPointEvent(this.controlPoint, ControlPointEvent.Type.DEVICE_ADDED, device));

        for (Device subdevice : device.getSubdevices()) {
            notifyDeviceAdded(subdevice);
        }
    }

    public synchronized void notifyRootDeviceRemoved(Device rootDevice) {
        scheduleEvent(new ControlPointEvent(this.controlPoint, ControlPointEvent.Type.ROOT_DEVICE_REMOVED, rootDevice));

        notifyDeviceRemoved(rootDevice);
    }

    private void notifyDeviceRemoved(Device device) {
        scheduleEvent(new ControlPointEvent(this.controlPoint, ControlPointEvent.Type.DEVICE_REMOVED, device));

        for (Device subdevice : device.getSubdevices()) {
            notifyDeviceRemoved(subdevice);
        }
    }

}
