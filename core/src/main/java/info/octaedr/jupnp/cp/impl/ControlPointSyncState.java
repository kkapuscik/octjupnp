/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

/**
 * State of the control point and synchronization object.
 *
 * <p>
 * The object is used for all operations that changes the internal state of control point like:
 * <ul>
 * <li>Control point start and stop.
 * <li>Device addition and removal.
 * <li>Action execution request / result dispatch scheduling.
 * <li>Events subscription request / notification dispatch scheduling.
 * </ul>
 *
 * All operation that could may block or are time consuming shall be moved to separate threads and following rules must
 * be followed:
 * <ul>
 * <li>Operations shall be started and finished in synchronized blocks.
 * <li>All operations must check if control point is still running when processing an event.
 * <li>For operations that could be canceled the results must not be returned directly. Instead there shall be
 * notification about task completion (either success or failure) and in processing of such event there shall be two
 * checks: if control point is still running and if the operation was not canceled in a meantime. The cancel operation
 * must be synchronized with the completion notification event.
 * </ul>
 *
 * @author Krzysztof Kapuscik
 */
class ControlPointSyncState {

    /** Control point started flag. */
    private boolean isStarted;

    /**
     * Constructs sync. state object.
     */
    ControlPointSyncState() {
        this.isStarted = false;
    }

    /**
     * Marks the control point as started.
     *
     * @throws IllegalStateException
     *             if the control point was already started.
     */
    void markStarted() {
        if (this.isStarted) {
            throw new IllegalStateException("Control point already started");
        }
        this.isStarted = true;
    }

    /**
     * Marks the control point as stopped.
     *
     * @throws IllegalStateException
     *             if the control point was already stopped.
     */
    void markStopped() {
        if (!this.isStarted) {
            throw new IllegalStateException("Control point already started");
        }
        this.isStarted = true;
    }

    /**
     * Checks if the control point is started.
     *
     * @return True if control point is started, false otherwise.
     */
    boolean isStarted() {
        return this.isStarted;
    }

}
