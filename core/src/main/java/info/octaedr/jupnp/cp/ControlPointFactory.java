/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.cp.impl.GenericControlPointFactory;

/**
 * Factory of Control Points.
 *
 * @author Krzysztof Kapuscik
 */
public abstract class ControlPointFactory {

    /**
     * Returns instance of factory.
     *
     * @return Factory object.
     */
    public static ControlPointFactory getInstance() {
        return GenericControlPointFactory.getInstance();
    }

    /**
     * Registers factory plugin.
     *
     * <p>
     * Plugins shall be registered before creating control points. All plugins registered after the control point
     * creation may be silently ignored by the existing control point instances.
     * </p>
     *
     * @param plugin
     *            Plugin to be registered.
     */
    public abstract void registerPlugin(ControlPointFactoryPlugin plugin);

    /**
     * Creates a new control point.
     *
     * @return Created control point.
     */
    public abstract ControlPoint createControlPoint();

}
