/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import java.net.URL;
import java.util.UUID;

import org.apache.http.client.HttpClient;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.ssdp.SsdpClient;
import info.octaedr.jupnp.cp.Device;

/**
 * Manager of discovery part of UPnP Control Point implementation.
 *
 * <p>
 * Tasks:
 * <ul>
 * <li>Sending SSDP search requests and receiving SSDP alive/byebye messages.
 * <li>Retrieving description documents for discovered devices.
 * <li>Holding database of discovered devices.
 * </ul>
 * </p>
 *
 * @author Krzysztof Kapuscik
 */
class DiscoveryManager {

    /** Logger for printing debug messages. */
    private static final Logger logger = Logger.getLogger(DiscoveryManager.class);

    /** Synchronization & state object. */
    private final ControlPointSyncState syncState;

    /** SSDP client configuration. */
    private final SsdpClient ssdpClient;

    /** Description retriever. */
    private final DescriptionRetriever descriptionRetriever;
    /** SSDP activity monitor. */
    private final RootDevicesSsdpMonitor ssdpMonitor;

    /** Device manager to be used. */
    private final DeviceManager deviceManager;

    /**
     * Constructs discovery manager.
     *
     * @param syncState
     *            Synchronization & state object.
     * @param deviceManager
     *            Device manager.
     */
    public DiscoveryManager(ControlPointSyncState syncState, DeviceManager deviceManager) {
        this.syncState = syncState;
        this.deviceManager = deviceManager;

        this.descriptionRetriever = new DescriptionRetriever(this);

        this.ssdpClient = new SsdpClient();
        this.ssdpMonitor = new RootDevicesSsdpMonitor(this.ssdpClient);

        this.ssdpMonitor.setDiscoveryManager(this);
    }

    /**
     * Starts discovery manager.
     *
     * <p>
     * This method performs start of all internal elements and sends initial search for devices already present in
     * network.
     * </p>
     *
     * @param wrapper
     *            Wrapper instance.
     * @param httpClient
     *            HTTP client instance.
     */
    public void startUnsafe(Wrapper wrapper, HttpClient httpClient) {
        /* start all elements */
        this.descriptionRetriever.start(wrapper, httpClient);
        this.ssdpMonitor.start();

        /* send initial search request to find devices already in network */
        this.ssdpMonitor.sendSearch();

        if (logger.trace()) {
            logger.trace("Discovery manager started: " + this);
        }
    }

    /**
     * Stops discovery manager.
     *
     * <p>
     * This method performs stop of all internal elements, removes all known devices and cancels all discovery actions
     * in progress.
     * </p>
     */
    public void stopUnsafe() {
        this.ssdpMonitor.stop();
        this.descriptionRetriever.stop();

        if (logger.trace()) {
            logger.trace("Discovery manager stopped: " + this);
        }
    }

    /**
     * Rescans network looking for new devices.
     *
     * <p>
     * If discovery manager is not started does nothing.
     * </p>
     */
    public void rescanNetworkUnsafe() {
        synchronized (this.syncState) {
            if (this.syncState.isStarted()) {
                this.ssdpMonitor.sendSearch();
            }
        }
    }

    /**
     * Processes alive notification.
     *
     * TODO: change to listener/observer
     *
     * @param uuid
     *            UUID of the device.
     * @param location
     *            Location of device description document.
     * @param cacheControl
     *            Cache control value.
     */
    public void processAliveNotify(UUID uuid, URL location, int cacheControl) {
        if (logger.trace()) {
            logger.trace("Process alive notify: uuid=" + uuid + " locationURL=" + location + " cacheControl="
                    + cacheControl);
        }

        synchronized (this.syncState) {
            if (this.syncState.isStarted()) {
                if (this.deviceManager.updateRootDeviceValidityUnsafe(uuid, cacheControl)) {
                    /* device was already known */

                    if (logger.trace()) {
                        logger.trace("Process alive notify: device status updated");
                    }
                } else if (this.descriptionRetriever.updateDeviceAliveStatus(uuid)) {
                    /* device was during description retrieval */

                    if (logger.trace()) {
                        logger.trace("Process alive notify: device status updated during description retrieval");
                    }
                } else {
                    /* schedule device description retrieval */
                    this.descriptionRetriever.scheduleRetrieval(uuid, location, cacheControl);

                    if (logger.trace()) {
                        logger.trace("Process alive notify: device description retrieval scheduled");
                    }
                }
            } else {
                if (logger.trace()) {
                    logger.trace("Process alive notify: ignoring (manager stopped)");
                }
            }
        }
    }

    /**
     * Processes bye bye notification.
     *
     * TODO: change to listener/observer
     *
     * @param uuid
     *            UUID of the device.
     */
    public void processByeByeNotify(UUID uuid) {
        if (logger.trace()) {
            logger.trace("Process bye bye notify: uuid=" + uuid);
        }

        synchronized (this.syncState) {
            if (this.syncState.isStarted()) {
                if (this.deviceManager.removeRootDeviceUnsafe(uuid)) {
                    /* device was already known */

                    if (logger.trace()) {
                        logger.trace("Process bye bye notify: device removed");
                    }
                } else if (this.descriptionRetriever.cancelRetrieval(uuid)) {
                    /* device was during description retrieval */

                    if (logger.trace()) {
                        logger.trace("Process bye bye notify: device description retrieval canceled");
                    }
                } else {
                    if (logger.trace()) {
                        logger.trace("Process bye bye notify: ignoring (unknown device)");
                    }
                }
            } else {
                if (logger.trace()) {
                    logger.trace("Process bye bye notify: ignoring (manager stopped)");
                }
            }
        }
    }

    /**
     * Processes description retrieval finished event.
     *
     * TODO: change to listener/observer
     *
     * @param uuid
     *            UUID of the device for which retrieval was started.
     */
    public void processDescriptionRetrievalFinished(UUID uuid) {
        if (logger.trace()) {
            logger.trace("Process description retrieval finished: uuid=" + uuid);
        }

        synchronized (this.syncState) {
            if (this.syncState.isStarted()) {
                /*
                 * get result - it may be not returned if task was canceled in meantime
                 */
                DescriptionRetrieverResult result = this.descriptionRetriever.getResult(uuid);
                if (result != null) {
                    Device rootDevice = result.getRootDevice();
                    if (rootDevice != null) {
                        if (logger.trace()) {
                            logger.trace("Process description retrieval finished with success: " + rootDevice);
                        }

                        this.deviceManager.addRootDeviceUnsafe(rootDevice, result.getCacheControl());
                    } else {
                        if (logger.trace()) {
                            logger.trace("Process description retrieval finished with failure");
                        }

                        this.deviceManager.addUnoperableDeviceEntryUnsafe(result.getUuid(), result.getLocation());
                    }
                }
            } else {
                if (logger.trace()) {
                    logger.trace("Process description retrieval finished: ignoring (manager stopped)");
                }
            }
        }
    }

}
