/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jupnp.core.DataType;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.StateVariable;

public class GenericActionArgument implements ActionArgument {

    private final String name;

    private final String direction;

    private final boolean isReturnValue;

    private GenericStateVariable relatedStateVariable;

    private GenericAction action;

    public GenericActionArgument(final String name, final String direction, final boolean isReturnValue,
            final GenericStateVariable relatedStateVariable) {
        this.name = name;
        this.direction = direction;
        this.isReturnValue = isReturnValue;
        this.relatedStateVariable = relatedStateVariable;
    }

    public void init(final Wrapper wrapper, final GenericAction ownerAction) {
        /* check parameters */
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }
        if (ownerAction == null) {
            throw new NullPointerException("Null action given");
        }

        /* check state */
        if (this.action != null) {
            throw new IllegalStateException("Action already set");
        }

        /* store data */
        this.action = ownerAction;

        /* initialize sub-elements */
        /* - nothing to do */

        /* restore links if needed */
        /* - find (possibly wrapped) state variable */
        this.relatedStateVariable = this.action.getService().getStateVariable(this.relatedStateVariable.getName());
        if (this.relatedStateVariable == null) {
            throw new IllegalStateException("Cannot restore state variable");
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDirection() {
        return this.direction;
    }

    @Override
    public boolean isReturnValue() {
        return this.isReturnValue;
    }

    @Override
    public StateVariable getRelatedStateVariable() {
        return this.relatedStateVariable;
    }

    @Override
    public DataType getDataType() {
        return this.relatedStateVariable.getDataType();
    }

    @Override
    public Action getAction() {
        return this.action;
    }

}
