/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.net.URL;

/**
 * UPnP device icon.
 */
public interface DeviceIcon {

    /**
     * Returns device to which this icon belongs.
     *
     * @return Device object.
     */
    public Device getDevice();

    /**
     * Returns icon MIME type.
     *
     * @return MIME type.
     */
    public String getMIMEType();

    /**
     * Returns width of icon in pixels.
     *
     * @return Width in pixels
     */
    public int getWidth();

    /**
     * Returns height of icon in pixels.
     *
     * @return Height in pixels
     */
    public int getHeight();

    /**
     * Returns depth of icon in bits.
     *
     * @return Number of color bits per pixel.
     */
    public int getDepth();

    /**
     * Returns pointer to icon image.
     *
     * @return URL to icon image.
     */
    public URL getURL();

}
