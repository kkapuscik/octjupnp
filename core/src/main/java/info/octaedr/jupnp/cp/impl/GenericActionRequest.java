/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.DataType;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.core.soap11.SoapBody;
import info.octaedr.jupnp.core.soap11.SoapBodyElement;
import info.octaedr.jupnp.core.soap11.SoapClient;
import info.octaedr.jupnp.core.soap11.SoapClientFactory;
import info.octaedr.jupnp.core.soap11.SoapElement;
import info.octaedr.jupnp.core.soap11.SoapEnvelope;
import info.octaedr.jupnp.core.soap11.SoapException;
import info.octaedr.jupnp.core.soap11.SoapFactory;
import info.octaedr.jupnp.core.soap11.SoapFault;
import info.octaedr.jupnp.core.soap11.SoapFaultDetail;
import info.octaedr.jupnp.core.soap11.SoapFaultDetailEntry;
import info.octaedr.jupnp.core.soap11.SoapMessage;
import info.octaedr.jupnp.core.soap11.SoapName;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.ActionExecutionFailedEvent;
import info.octaedr.jupnp.cp.ActionFailedEvent;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.ActionRequestArguments;
import info.octaedr.jupnp.cp.ActionRequestListener;
import info.octaedr.jupnp.cp.ActionRequestStatus;
import info.octaedr.jupnp.cp.ActionSucceededEvent;
import info.octaedr.jupnp.cp.Service;

import java.net.URL;
import java.util.Collection;
import java.util.Iterator;

public class GenericActionRequest<ArgumentsType extends ActionRequestArguments> implements ActionRequest<ArgumentsType> {

    private static final Logger logger = Logger.getLogger(GenericActionRequest.class);

    private static final String ENCODING_STYLE = "http://schemas.xmlsoap.org/soap/encoding/";

    private static final String UPNP_FAULT_CODE = "Client";

    private static final String UPNP_FAULT_STRING = "UPnPError";

    private ActionRequestStatus status = ActionRequestStatus.INITIAL;

    private int errorCode;

    private String errorDescription;

    private String executionErrorDescription;

    private final ArgumentsType arguments;

    public GenericActionRequest(final ArgumentsType arguments) {
        if (arguments == null) {
            throw new NullPointerException("Null arguments given");
        }

        this.arguments = arguments;
    }

    @Override
    public ArgumentsType getArguments() {
        return this.arguments;
    }

    @Override
    public Action getAction() {
        return this.arguments.getAction();
    }

    @Override
    public void execute(final ActionRequestListener<ArgumentsType> listener) throws UpnpException {
        checkInputArguments();

        setStatus(ActionRequestStatus.EXECUTING);

        new Thread() {
            @Override
            public void run() {
                try {
                    Service service = GenericActionRequest.this.getAction().getService();
                    URL controlURL = service.getControlURL();

                    // TODO: Optimize - this stuff probably can be created just once
                    SoapClient soapClient = SoapClientFactory.getInstance().createClient();

                    /* prepare the message */
                    SoapMessage requestMessage = createRequestDocument();

                    /* execute the request */
                    SoapMessage responseMessage = soapClient.call(requestMessage, controlURL);

                    /* dispose the client to release resources */
                    soapClient.dispose();

                    /* parse the response */
                    parseResponseDocument(responseMessage);
                } catch (SoapException e) {
                    setExecutionFailed("SOAP exception: " + e.getMessage());
                } catch (UpnpException e) {
                    setExecutionFailed("UPnP exception: " + e.getMessage());
                } catch (Throwable t) {
                    setExecutionFailed("UPnP exception: " + t.getMessage());
                }

                if (listener != null) {
                    GenericActionRequest.this.notifyListener(listener);
                }
            }
        }.start();
    }

    private void notifyListener(ActionRequestListener<ArgumentsType> listener) {
        switch (GenericActionRequest.this.status) {
            case SUCCEEDED:
                listener.actionSucceeded(new ActionSucceededEvent<ArgumentsType>(this));
                break;
            case FAILED:
                listener.actionFailed(new ActionFailedEvent<ArgumentsType>(this));
                break;
            case EXECUTION_FAILED:
                listener.actionExecutionFailed(new ActionExecutionFailedEvent<ArgumentsType>(this));
                break;
            default:
                throw new IllegalStateException("Illegal state after execution");
        }
    }

    @Override
    public void cancel() {
        // TODO Auto-generated method stub
    }

    @Override
    public ActionRequestStatus getStatus() {
        return this.status;
    }

    @Override
    public int getErrorCode() {
        if (this.status != ActionRequestStatus.FAILED) {
            throw new IllegalStateException("Action request not in failed state");
        }
        return this.errorCode;
    }

    @Override
    public String getErrorDescription() {
        if (this.status != ActionRequestStatus.FAILED) {
            throw new IllegalStateException("Action request not in failed state");
        }
        return this.errorDescription;
    }

    @Override
    public String getExecutionErrorDescription() {
        if (this.status != ActionRequestStatus.EXECUTION_FAILED) {
            throw new IllegalStateException("Action request not in execution failed state");
        }
        return this.executionErrorDescription;
    }

    private void checkInputArguments() throws UpnpException {
        for (ActionArgument argument : getAction().getInputArguments()) {
            Object value = this.arguments.getInputArgumentValue(argument.getName());
            if (value == null) {
                throw new UpnpException("Input argument missing: " + argument.getName());
            }
        }
    }

    private SoapMessage createRequestDocument() throws SoapException, UpnpException {
        Action action = getAction();
        String actionName = action.getName();
        Service service = action.getService();
        String serviceType = service.getServiceType();

        SoapFactory soapFactory = SoapFactory.getInstance();

        /* prepare message */
        SoapMessage requestMessage = soapFactory.createMessage();

        /* add required UPnP headers */
        requestMessage.setSoapAction('"' + serviceType + '#' + actionName + '"');

        /* get envelope */
        SoapEnvelope envelope = requestMessage.getSoapPart().getEnvelope(true);
        envelope.setEncodingStyle(ENCODING_STYLE);

        /* prepare message body */
        SoapBody body = requestMessage.getSoapPart().getEnvelope().getBody(true);

        /* prepare action element */
        SoapName bodyName = soapFactory.createName(serviceType, "u", actionName);
        SoapBodyElement bodyElement = body.addBodyElement(bodyName);

        /* prepare argument elements */
        for (ActionArgument argument : action.getInputArguments()) {
            /* get required data */
            String name = argument.getName();
            DataType dataType = argument.getDataType();
            Object value = this.arguments.getInputArgumentValue(name);
            if (value == null) {
                throw new UpnpException("Input argument missing: " + argument.getName());
            }

            /* convert value to string representation */
            String valueString = dataType.valueToString(value);

            /* create single argument element */
            SoapName argumentName = soapFactory.createName(name);
            SoapElement argumentElement = bodyElement.addChildElement(argumentName);
            argumentElement.addTextNode(valueString);
        }

        return requestMessage;
    }

    private void parseResponseDocument(SoapMessage responseMessage) throws SoapException, UpnpException {
        /* check envelope */
        SoapEnvelope responseEnvelope = responseMessage.getSoapPart().getEnvelope();
        if (!ENCODING_STYLE.equals(responseEnvelope.getEncodingStyle())) {
            throw new UpnpException("Invalid encoding style in SOAP envelope");
        }

        /* parse body */
        if (!responseEnvelope.getBody().hasFault()) {
            parseResponseSuccessBody(responseEnvelope);
        } else {
            parseResponseFaultBody(responseEnvelope);
        }
    }

    private void parseResponseSuccessBody(final SoapEnvelope responseEnvelope) throws UpnpException, SoapException {

        SoapFactory soapFactory = SoapFactory.getInstance();

        SoapBody responseBody = responseEnvelope.getBody();

        /* prepare useful data */
        Action action = getAction();
        String actionName = action.getName();
        Service service = action.getService();
        String serviceType = service.getServiceType();

        /* find response element */
        SoapName actionElementName = soapFactory.createName(serviceType, null, actionName + "Response");
        Collection<SoapBodyElement> bodyElements = responseBody.getBodyElements(actionElementName);
        if (bodyElements.size() < 1) {
            throw new UpnpException("Cannot find response element in SOAP body");
        } else if (bodyElements.size() > 1) {
            throw new UpnpException("Too many response elements in SOAP body");
        }

        /* process response element */
        SoapBodyElement responseElement = bodyElements.iterator().next();

        /* parse arguments */
        for (ActionArgument argument : action.getOutputArguments()) {
            // TODO: namespace etc.
            SoapName argumentName = soapFactory.createName(argument.getName());

            boolean argumentElementFound = false;

            for (SoapElement element : responseElement.getChildElements(argumentName)) {
                if (argumentElementFound) {
                    throw new UpnpException("Illegal multiple output argument values");
                } else {
                    argumentElementFound = true;
                }

                String valueString = element.getTextContent();

                if (logger.info()) {
                    logger.info("ActionRequest ELEMENT: " + argument.getName() + " = " + valueString);
                }

                this.arguments.setOutputArgumentValueString(argument.getName(), valueString);
            }

            if (!argumentElementFound) {
                throw new UpnpException("Output argument missing in response: " + argument.getName());
            }
        }

        setSucceeded();
    }

    private void parseResponseFaultBody(final SoapEnvelope responseEnvelope) throws UpnpException, SoapException {
        /* useful objects */
        SoapBody responseBody = responseEnvelope.getBody();

        /* get failure info */
        SoapFault responseFault = responseBody.getFault();
        String soapPrefix = responseFault.getXmlElement().getPrefix();
        String expectedCode;

        if (soapPrefix != null) {
            expectedCode = soapPrefix + ":" + UPNP_FAULT_CODE;
        } else {
            expectedCode = UPNP_FAULT_CODE;
        }

        if (!expectedCode.equalsIgnoreCase(responseFault.getFaultCode())) {
            throw new UpnpException("Invalid fault code: " + responseFault.getFaultCode());
        }

        if (!UPNP_FAULT_STRING.equalsIgnoreCase(responseFault.getFaultString())) {
            throw new UpnpException("Invalid fault string: " + responseFault.getFaultString());
        }

        boolean upnpErrorDetailsFound = false;
        Integer upnpErrorCode = null;
        String upnpErrorDescription = null;

        // FIXME - parsing missing
        SoapFaultDetail detail = responseFault.getDetail();
        if (detail != null) {
            // TODO: collection
            Iterator<SoapFaultDetailEntry> detailIterator = detail.getDetailEntries();
            while (detailIterator.hasNext()) {
                SoapFaultDetailEntry entry = detailIterator.next();

                if (entry.getLocalName().equals("UPnPError") && entry.getNamespaceURI() == null) {
                    if (upnpErrorDetailsFound) {
                        throw new UpnpException("Multiple error details found");
                    } else {
                        upnpErrorDetailsFound = true;
                    }

                    // Iterator<?> children = entry.getChildElements();
                    // while (children.hasNext()) {
                    // Node node = (Node) children.next();
                    //
                    // if (logger.trace()) {
                    // logger.trace("NODE: " + node + " " + node.getValue());
                    // }
                    // }
                }
            }
        }

        if (!upnpErrorDetailsFound) {
            throw new UpnpException("No error details found");
        }
        if (upnpErrorCode == null || upnpErrorDescription == null) {
            throw new UpnpException("UPnP eror code or description is missing");
        }

        // TODO
        // setFailed(upnpErrorCode.intValue(), upnpErrorDescription);
    }

    // TODO
    // private void setFailed(final int errorCode, final String errorDescription) {
    // this.errorCode = errorCode;
    // this.errorDescription = errorDescription;
    // setStatus(ActionRequestStatus.FAILED);
    // }

    private void setSucceeded() {
        setStatus(ActionRequestStatus.SUCCEEDED);
    }

    private void setExecutionFailed(String errorDescription) {
        this.executionErrorDescription = errorDescription;
        setStatus(ActionRequestStatus.EXECUTION_FAILED);
    }

    private synchronized void setStatus(final ActionRequestStatus newStatus) {
        if (this.status.compareTo(newStatus) > 0) {
            throw new IllegalStateException("Illegal state transition: " + this.status + " => " + newStatus);
        }
        if (this.status.compareTo(ActionRequestStatus.EXECUTING) > 0) {
            throw new IllegalStateException("Illegal state transition: " + this.status + " => " + newStatus);
        }
        if (this.status == ActionRequestStatus.INITIAL && newStatus != ActionRequestStatus.EXECUTING) {
            throw new IllegalStateException("Illegal state transition: " + this.status + " => " + newStatus);
        }
        this.status = newStatus;
    }

}
