/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.util.Collection;

/**
 * UPnP service action.
 *
 * @author Krzysztof Kapuscik
 */
public interface Action {

    /**
     * Returns service to which this action belongs.
     *
     * @return Service object.
     */
    Service getService();

    /**
     * Returns name of the action.
     *
     * @return Action name.
     */
    String getName();

    /**
     * Returns all arguments.
     *
     * @return Collection of action arguments.
     */
    Collection<ActionArgument> getArguments();

    /**
     * Returns all input arguments.
     *
     * @return Collection of action arguments.
     */
    Collection<ActionArgument> getInputArguments();

    /**
     * Returns all output arguments.
     *
     * @return Collection of action arguments.
     */
    Collection<ActionArgument> getOutputArguments();

    /**
     * Returns action argument by name
     *
     * @param name
     *            Name of the argument.
     *
     * @return Argument object or null if not found.
     */
    ActionArgument getArgument(String name);

    /**
     * Creates action execution request.
     *
     * @return Request object.
     */
    ActionRequest<ActionRequestArguments> createRequest();

}
