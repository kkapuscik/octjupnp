/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.util.EventObject;

/**
 * Control point event.
 *
 * @author Krzysztof Kapuscik
 */
public class ControlPointEvent extends EventObject {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 5343914428072924988L;

    /** Event type. */
    public static enum Type {
        /** Root device added. */
        ROOT_DEVICE_ADDED,
        /** Root device removed. */
        ROOT_DEVICE_REMOVED,
        /** Device added. */
        DEVICE_ADDED,
        /** Device removed */
        DEVICE_REMOVED
    }

    /** Type of the event. */
    private final Type type;
    /** Device about which is this event. */
    private final Device device;

    /**
     * Constructs new event.
     *
     * @param source
     *            Control point being source of this event.
     * @param type
     *            Type of the event.
     * @param device
     *            Device about which is this event.
     */
    public ControlPointEvent(ControlPoint source, Type type, Device device) {
        super(source);

        if (type == null) {
            throw new NullPointerException("Null type given");
        }
        if (device == null) {
            throw new NullPointerException("Null device given");
        }

        this.type = type;
        this.device = device;
    }

    @Override
    public ControlPoint getSource() {
        return (ControlPoint) super.getSource();
    }

    /**
     * Returns type of the event.
     *
     * @return Event type.
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Device about which is this event.
     *
     * @return Device object.
     */
    public Device getDevice() {
        return this.device;
    }

}
