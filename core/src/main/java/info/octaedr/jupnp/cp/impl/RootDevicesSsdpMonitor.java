/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.ssdp.AliveNotifyMessage;
import info.octaedr.jupnp.core.ssdp.ByeByeNotifyMessage;
import info.octaedr.jupnp.core.ssdp.MultiSsdpMonitor;
import info.octaedr.jupnp.core.ssdp.SearchResponseMessage;
import info.octaedr.jupnp.core.ssdp.SsdpClient;

/**
 * SSDP activity monitor
 *
 * @author Krzysztof Kapuscik
 */
class RootDevicesSsdpMonitor extends MultiSsdpMonitor {

    /** Logger for printing debug messages. */
    private static final Logger logger = Logger.getLogger(RootDevicesSsdpMonitor.class);

    /**
     * Notify target - "upnp:rootdevice".
     */
    private static final String NOTIFY_TARGET_UPNP_ROOT_DEVICE = "upnp:rootdevice";

    /**
     * Search target - "ssdp:all".
     */
    public static final String SEARCH_TARGET_SSDP_ALL = "ssdp:all";

    /**
     * Search target - "upnp:rootdevice".
     */
    public static final String SEARCH_TARGET_UPNP_ROOTDEVICE = "upnp:rootdevice";

    /**
     * Discovery manager.
     */
    private DiscoveryManager discoveryManager;

    /**
     * Constructs monitor using default network interface.
     *
     * @param ssdpClient
     *            SSDP client instance.
     */
    public RootDevicesSsdpMonitor(SsdpClient ssdpClient) {
        super(ssdpClient);
    }

    // /**
    // * Constructs monitor using given network interface.
    // *
    // * @param ssdpClient
    // * SSDP client instance.
    // * @param ifaceAddress
    // * Address of the network interface to use.
    // */
    // public RootDevicesSsdpMonitor(SsdpClient ssdpClient, InetAddress ifaceAddress) {
    // super(ssdpClient, ifaceAddress);
    // }

    /**
     * Sets discovery manager for which events shall be sent.
     *
     * @param discoveryManager
     *            Discovery manager instance.
     */
    public void setDiscoveryManager(DiscoveryManager discoveryManager) {
        if (discoveryManager == null) {
            throw new NullPointerException("Null discovery manager given");
        }

        if (this.discoveryManager != null) {
            throw new IllegalStateException("Discovery manager already set");
        }

        this.discoveryManager = discoveryManager;
    }

    /**
     * Starts monitoring.
     */
    @Override
    public void start() {
        if (this.discoveryManager == null) {
            throw new IllegalStateException("Discovery manager not set");
        }

        super.start();
    }

    /**
     * Sends search request for root devices with default MX.
     */
    public void sendSearch() {
        sendSearch(SEARCH_TARGET_UPNP_ROOTDEVICE);
    }

    /**
     * Sends search request for root devices with given MX.
     *
     * @param mx
     *            Request MX (delay) value.
     */
    public void sendSearch(int mx) {
        sendSearch(mx, SEARCH_TARGET_UPNP_ROOTDEVICE);
    }

    @Override
    protected void processAliveNotify(AliveNotifyMessage message) {
        if (logger.trace()) {
            logger.trace("processAliveNotify() - alive message received");
        }

        do {
            try {
                if (!NOTIFY_TARGET_UPNP_ROOT_DEVICE.equals(message.getNotifyTarget())) {
                    break;
                }

                this.discoveryManager.processAliveNotify(message.getUSN().getUUID(), message.getLocation(),
                        message.getCacheControl());
            } catch (Exception e) {
                if (logger.trace()) {
                    logger.trace("processAliveNotify() - exception catched: " + e);
                }
            }
        } while (false);
    }

    @Override
    protected void processByeByeNotify(ByeByeNotifyMessage message) {
        if (logger.trace()) {
            logger.trace("processPacket() - byebye message received");
        }

        do {
            try {
                this.discoveryManager.processByeByeNotify(message.getUSN().getUUID());
            } catch (Exception e) {
                if (logger.trace()) {
                    logger.trace("processAliveNotify() - exception catched: " + e);
                }
            }
        } while (false);
    }

    @Override
    protected void processSearchResponse(SearchResponseMessage message) {
        if (logger.trace()) {
            logger.trace("processPacket() - search response received");
        }

        do {
            try {
                if (!NOTIFY_TARGET_UPNP_ROOT_DEVICE.equals(message.getSearchTarget())) {
                    break;
                }

                this.discoveryManager.processAliveNotify(message.getUSN().getUUID(), message.getLocation(),
                        message.getCacheControl());
            } catch (Exception e) {
                if (logger.trace()) {
                    logger.trace("processAliveNotify() - exception catched: " + e);
                }
            }
        } while (false);
    }

}
