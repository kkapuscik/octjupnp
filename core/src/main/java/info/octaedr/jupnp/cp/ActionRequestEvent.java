/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.util.EventObject;

/**
 * Event describing action execution status.
 *
 * @author Krzysztof Kapuscik
 *
 * @param <ArgumentsType>
 *            Type of action request arguments object.
 */
public abstract class ActionRequestEvent<ArgumentsType extends ActionRequestArguments> extends EventObject {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -8807301433267754784L;

    /**
     * Constructs event.
     *
     * @param request
     *            Action request being source of the event.
     */
    protected ActionRequestEvent(final ActionRequest<ArgumentsType> request) {
        super(request);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ActionRequest<ArgumentsType> getSource() {
        return (ActionRequest<ArgumentsType>) super.getSource();
    }

}
