/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import info.octaedr.jcommons.collection.ReadOnlyArrayCollection;
import info.octaedr.jupnp.core.DataType;
import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.StateVariable;

import java.util.Collection;

public class GenericStateVariable implements StateVariable {

    private final String name;

    private final boolean sendEvents;

    private final DataType dataType;

    private final Object defaultValue;

    private final String[] allowedValues;

    private final Object allowedRangeMinimum;

    private final Object allowedRangeMaximum;

    private final Object allowedRangeStep;

    private Service service;

    public GenericStateVariable(final boolean sendEvents, final String name, final DataType dataType,
            final Object defaultValue, final String[] allowedValues, final Object allowedRangeMinimum,
            final Object allowedRangeMaximum, final Object allowedRangeStep) {
        this.name = name;
        this.dataType = dataType;
        this.sendEvents = sendEvents;
        this.defaultValue = defaultValue;
        this.allowedValues = allowedValues;
        this.allowedRangeMinimum = allowedRangeMinimum;
        this.allowedRangeMaximum = allowedRangeMaximum;
        this.allowedRangeStep = allowedRangeStep;
    }

    public void init(final Wrapper wrapper, final GenericService ownerService) {
        /* check parameters */
        if (wrapper == null) {
            throw new NullPointerException("Null wrapper given");
        }
        if (ownerService == null) {
            throw new NullPointerException("Null service given");
        }

        /* check state */
        if (this.service != null) {
            throw new IllegalStateException("Service already set");
        }

        /* store data */
        this.service = ownerService;

        /* initialize sub-elements */
        /* - nothing to do */

        /* restore links if needed */
        /* - nothing to do */
    }

    @Override
    public Service getService() {
        if (this.service == null) {
            throw new IllegalStateException("Service not set");
        }
        return this.service;
    }

    @Override
    public boolean isSendingEvents() {
        return this.sendEvents;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public DataType getDataType() {
        return this.dataType;
    }

    @Override
    public Object getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public Collection<String> getAllowedValues() {
        if (this.allowedValues != null) {
            return new ReadOnlyArrayCollection<String>(this.allowedValues);
        } else {
            return null;
        }
    }

    @Override
    public Object getAllowedRangeMinimum() {
        return this.allowedRangeMinimum;
    }

    @Override
    public Object getAllowedRangeMaximum() {
        return this.allowedRangeMaximum;
    }

    @Override
    public Object getAllowedRangeStep() {
        return this.allowedRangeStep;
    }

}
