/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

/**
 * Event describing details of action execution failure.
 *
 * <p>
 * Action execution failure occurs when requested action could not be executed due to some error. This does not include
 * situation when target service responded with error - in that case {@link ActionFailedEvent}..
 * </p>
 *
 * @author Krzysztof Kapuscik
 *
 * @param <ArgumentsType>
 *            Type of action request arguments object.
 */
public class ActionExecutionFailedEvent<ArgumentsType extends ActionRequestArguments> extends
        ActionRequestEvent<ArgumentsType> {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 229005617110253740L;

    /**
     * Constructs new event.
     *
     * @param source
     *            Action request being source of the event.
     */
    public ActionExecutionFailedEvent(final ActionRequest<ArgumentsType> source) {
        super(source);
    }

    /**
     * Returns failure error description.
     *
     * @return Error description string.
     */
    public String getErrorDescription() {
        return getSource().getExecutionErrorDescription();
    }

}
