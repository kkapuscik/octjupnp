/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.core.DataType;

/**
 * UPnP service action argument.
 *
 * @author Krzysztof Kapuscik
 */
public interface ActionArgument {

    // TODO: convert to ENUM

    /**
     * Argument direction - input.
     */
    public static final String DIRECTION_IN = "in";

    /**
     * Argument direction - output.
     */
    public static final String DIRECTION_OUT = "out";

    /**
     * Returns name of the argument.
     *
     * @return Argument name.
     */
    public String getName();

    /**
     * Returns direction of the argument.
     *
     * @return Direction description: {@link #DIRECTION_IN} or {@link #DIRECTION_OUT}.
     */
    public String getDirection();

    /**
     * Returns information if this argument is marked as return value.
     *
     * @return True if argument is marked as a return value, false otherwise.
     */
    public boolean isReturnValue();

    /**
     * Returns state variable to which this argument is related.
     *
     * @return State variable object.
     */
    public StateVariable getRelatedStateVariable();

    /**
     * Returns data type of the argument.
     *
     * @return Data type description object.
     */
    public DataType getDataType();

    /**
     * Returns action to which this argument belongs.
     *
     * @return Action object.
     */
    public Action getAction();

}
