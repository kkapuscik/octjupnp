/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

/**
 * Action request status.
 *
 * @author Krzysztof Kapuscik
 */
public enum ActionRequestStatus {

    /** Request is not yet sent. */
    INITIAL,
    /** Request is in progress. */
    EXECUTING,
    /** Request succeeded (action finished with success). */
    SUCCEEDED,
    /** Request failed (action finished with failure). */
    FAILED,
    /** Request execution failed. */
    EXECUTION_FAILED

}
