/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import info.octaedr.jupnp.core.UpnpException;

/**
 * Collection of action request arguments.
 *
 * @author Krzysztof Kapuscik
 */
public interface ActionRequestArguments {

    /**
     * Returns action for which this arguments were created.
     *
     * @return Action object.
     */
    Action getAction();

    /**
     * Returns value of argument.
     *
     * @param name
     *            Name of the argument.
     *
     * @return Value of the argument or null if it is not set.
     *
     * @throws UpnpException
     *             if argument name is invalid.
     */
    public Object getArgumentValue(final String name) throws UpnpException;

    /**
     * Clears value of the argument.
     *
     * @param name
     *            Name of the argument.
     *
     * @throws UpnpException
     *             if argument name is invalid.
     */
    public void clearArgumentValue(final String name) throws UpnpException;

    /**
     * Sets value of the argument from string representation.
     *
     * @param name
     *            Name of the argument.
     * @param valueString
     *            String representation of the value to set. The representation will be parsed, verified and then set as
     *            an object.
     *
     * @throws UpnpException
     *             if argument name or value is invalid.
     */
    public void setArgumentValueString(final String name, final String valueString) throws UpnpException;

    /**
     * Sets value of the argument.
     *
     * @param name
     *            Name of the argument.
     * @param value
     *            Value to set.
     *
     * @throws UpnpException
     *             if argument name or value is invalid.
     */
    public void setArgumentValue(final String name, final Object value) throws UpnpException;

    /**
     * Returns value of <strong>input</strong> argument.
     *
     * @param name
     *            Name of the argument.
     *
     * @return Value of the argument or null if it is not set.
     *
     * @throws UpnpException
     *             if argument name is invalid.
     */
    public Object getInputArgumentValue(final String name) throws UpnpException;

    /**
     * Clears value of the <strong>input</strong> argument.
     *
     * @param name
     *            Name of the argument.
     *
     * @throws UpnpException
     *             if argument name is invalid.
     */
    public void clearInputArgumentValue(final String name) throws UpnpException;

    /**
     * Sets value of the <strong>input</strong> argument from string representation.
     *
     * @param name
     *            Name of the argument.
     * @param valueString
     *            String representation of the value to set. The representation will be parsed, verified and then set as
     *            an object.
     *
     * @throws UpnpException
     *             if argument name or value is invalid.
     */
    public void setInputArgumentValueString(final String name, final String valueString) throws UpnpException;

    /**
     * Sets value of the <strong>input</strong> argument.
     *
     * @param name
     *            Name of the argument.
     * @param value
     *            Value to set.
     *
     * @throws UpnpException
     *             if argument name or value is invalid.
     */
    public void setInputArgumentValue(final String name, final Object value) throws UpnpException;

    /**
     * Returns value of <strong>output</strong> argument.
     *
     * @param name
     *            Name of the argument.
     *
     * @return Value of the argument or null if it is not set.
     *
     * @throws UpnpException
     *             if argument name is invalid.
     */
    public Object getOutputArgumentValue(final String name) throws UpnpException;

    /**
     * Clears value of the <strong>output</strong> argument.
     *
     * @param name
     *            Name of the argument.
     *
     * @throws UpnpException
     *             if argument name is invalid.
     */
    public void clearOutputArgumentValue(final String name) throws UpnpException;

    /**
     * Sets value of the <strong>output</strong> argument from string representation.
     *
     * @param name
     *            Name of the argument.
     * @param valueString
     *            String representation of the value to set. The representation will be parsed, verified and then set as
     *            an object.
     *
     * @throws UpnpException
     *             if argument name or value is invalid.
     */
    public void setOutputArgumentValueString(final String name, final String valueString) throws UpnpException;

    /**
     * Sets value of the <strong>output</strong> argument.
     *
     * @param name
     *            Name of the argument.
     * @param value
     *            Value to set.
     *
     * @throws UpnpException
     *             if argument name or value is invalid.
     */
    public void setOutputArgumentValue(final String name, final Object value) throws UpnpException;

}
