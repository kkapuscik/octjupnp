/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp;

import java.util.EventListener;

/**
 * Listener for action request events.
 *
 * @author Krzysztof Kapuscik
 *
 * @param <ArgumentsType>
 *            Type of action request arguments object.
 */
public interface ActionRequestListener<ArgumentsType extends ActionRequestArguments> extends EventListener {

    /**
     * Called when action was executed completely and finished with success.
     *
     * @param event
     *            Event description object.
     */
    void actionSucceeded(ActionSucceededEvent<ArgumentsType> event);

    /**
     * Called when action was executed completely and finished with error.
     *
     * @param event
     *            Event description object.
     */
    void actionFailed(ActionFailedEvent<ArgumentsType> event);

    /**
     * Called when action was execution failed.
     *
     * @param event
     *            Event description object.
     */
    void actionExecutionFailed(ActionExecutionFailedEvent<ArgumentsType> event);

}
