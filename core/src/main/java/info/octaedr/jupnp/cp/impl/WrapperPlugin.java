/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

public interface WrapperPlugin {

    public GenericDevice wrap(GenericDevice device);

    public GenericDeviceIcon wrap(GenericDeviceIcon icon);

    public GenericServiceBrief wrap(GenericServiceBrief service);

    public GenericService wrap(GenericService service);

    public GenericAction wrap(GenericAction action);

    public GenericActionArgument wrap(GenericActionArgument argument);

    public GenericStateVariable wrap(GenericStateVariable variable);

}
