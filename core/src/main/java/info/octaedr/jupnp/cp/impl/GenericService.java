/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.impl;

import java.util.Collection;

import info.octaedr.jcommons.collection.ReadOnlyArrayCollection;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.ServiceBrief;
import info.octaedr.jupnp.cp.StateVariable;

public class GenericService extends GenericServiceBrief implements Service {

    private final GenericStateVariable[] stateVariables;

    private final GenericAction[] actions;

    public GenericService(final ServiceBrief brief, final GenericStateVariable[] stateVariables,
            final GenericAction[] actions) {
        super(brief);

        this.stateVariables = stateVariables;
        this.actions = actions;
    }

    /**
     * Constructs shallow copy of other service.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Service to be copied.
     */
    protected GenericService(GenericService other) {
        this(other, other.stateVariables, other.actions);
    }

    @Override
    public void init(final Wrapper wrapper, final GenericDevice ownerDevice) {
        super.init(wrapper, ownerDevice);

        /* initialize sub-elements */
        /* - state variables must go first as they will be used in action arguments initialization */
        if (this.stateVariables != null) {
            for (int i = 0; i < this.stateVariables.length; ++i) {
                this.stateVariables[i] = wrapper.wrap(this.stateVariables[i]);
                this.stateVariables[i].init(wrapper, this);
            }
        }
        if (this.actions != null) {
            for (int i = 0; i < this.actions.length; ++i) {
                this.actions[i] = wrapper.wrap(this.actions[i]);
                this.actions[i].init(wrapper, this);
            }
        }

        /* restore links if needed */
        /* - nothing to do */
    }

    @Override
    public Collection<StateVariable> getStateVariables() {
        return new ReadOnlyArrayCollection<StateVariable>(this.stateVariables);
    }

    @Override
    public GenericStateVariable getStateVariable(final String name) {
        if (this.stateVariables != null) {
            for (int i = 0; i < this.stateVariables.length; ++i) {
                if (this.stateVariables[i].getName().equals(name)) {
                    return this.stateVariables[i];
                }
            }
        }
        return null;
    }

    @Override
    public Collection<Action> getActions() {
        return new ReadOnlyArrayCollection<Action>(this.actions);
    }

    @Override
    public Action getAction(final String name) {
        if (this.actions != null) {
            for (int i = 0; i < this.actions.length; ++i) {
                if (this.actions[i].getName().equals(name)) {
                    return actions[i];
                }
            }
        }
        return null;
    }

}
