/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Hexadecimal digits representing octets. Treats each nibble as a hex digit and encodes as a separate Byte. (1 octet is
 * encoded as 2.) No limit on size.
 *
 * The type used for values is TODO -define type-.
 */
class BinHexDataType extends UnsupportedSimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "bin.hex";

    public BinHexDataType() {
        super(DATA_TYPE_NAME);
    }

}
