/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class DefaultSoapText extends DefaultSoapNode implements SoapText {

    public DefaultSoapText(Node xmlNode) {
        super(xmlNode);
    }

    @Override
    public Text getXmlText() {
        return (Text) getXmlNode();
    }

}
