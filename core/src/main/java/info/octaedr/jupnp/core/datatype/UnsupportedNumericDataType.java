/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

abstract class UnsupportedNumericDataType extends NumericDataType {

    protected UnsupportedNumericDataType(String name) {
        super(name);
    }

    @Override
    protected boolean checkValue(Object value, Object allowedRangeMinimum, Object allowedRangeMaximum,
            Object allowedRangeStep) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean validateRange(Object minimum, Object maximum, Object step) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class<?> getValueClass() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object parseValue(String valueString) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String valueToString(Object value) {
        throw new UnsupportedOperationException();
    }

}
