/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Floating point number. Mantissa (left of the decimal) and/or exponent may have a leading sign. Mantissa and/or
 * exponent may have leading zeros. Decimal character in mantissa is a period, i.e., whole digits in mantissa separated
 * from fractional digits by period. Mantissa separated from exponent by E. (No currency symbol.) (No grouping of digits
 * in the mantissa, e.g., no commas.)
 *
 * The type used for values is TODO -define type-.
 */
class FloatDataType extends UnsupportedNumericDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "float";

    public FloatDataType() {
        super(DATA_TYPE_NAME);
    }

}
