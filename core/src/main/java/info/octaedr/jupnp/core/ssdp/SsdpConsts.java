/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;

/**
 * SSDP protocol constants.
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpConsts {

    /** Consts singleton. */
    private static SsdpConsts theInstance;

    /**
     * Returns constants singleton.
     *
     * @return Constants singleton
     */
    public static synchronized SsdpConsts getInstance() {
        if (theInstance == null) {
            theInstance = new SsdpConsts();
        }
        return theInstance;
    }

    /** SSDP host. */
    private static final String SSDP_HOST = "239.255.255.250";
    /** SSDP port. */
    private static final int SSDP_PORT = 1900;

    /** SSDP HTTP host (HOST header value). */
    private static final String SSDP_HTTP_HOST = "239.255.255.250:1900";

    /** SSDP host address. */
    private InetAddress ssdpHostAddress;
    /** SSDP host socket address. */
    private SocketAddress ssdpSocketAddress;

    /**
     * Constructs constants.
     */
    private SsdpConsts() {
        try {
            this.ssdpHostAddress = InetAddress.getByName(SSDP_HOST);
            this.ssdpSocketAddress = new InetSocketAddress(this.ssdpHostAddress, SSDP_PORT);
        } catch (UnknownHostException e) {
            throw new IllegalStateException("Cannot create valid SSDP consts");
        }
    }

    /**
     * Returns SSDP host.
     *
     * @return SSDP host in IP address string form.
     */
    public String getSsdpHost() {
        return SSDP_HOST;
    }

    /**
     * Returns SSDP port.
     *
     * @return SSDP port.
     */
    public int getSsdpPort() {
        return SSDP_PORT;
    }

    /**
     * Returns value of HTTP HOST header for SSDP.
     *
     * @return HOST header value.
     */
    public String getSddpHttpHost() {
        return SSDP_HTTP_HOST;
    }

    /**
     * Returns SSDP host address.
     *
     * @return SSDP host address.
     */
    public InetAddress getSsdpHostAddress() {
        return this.ssdpHostAddress;
    }

    /**
     * Returns SSDP socket address.
     *
     * @return SSDP socket address.
     */
    public SocketAddress getSsdpSocketAddress() {
        return this.ssdpSocketAddress;
    }
}
