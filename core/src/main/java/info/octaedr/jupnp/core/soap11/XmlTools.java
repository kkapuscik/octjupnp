/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

class XmlTools {

    private static final String EMPTY_STRING = "";

    public static Element findSingleElement(Node baseNode, String namespaceURI, String localName) throws SoapException {
        Element foundElement = null;

        for (Node childNode = baseNode.getFirstChild(); childNode != null; childNode = childNode.getNextSibling()) {

            if (childNode instanceof Element) {
                Element childElement = (Element) childNode;

                if (elementCompareName(childElement, namespaceURI, localName)) {
                    if (foundElement == null) {
                        foundElement = childElement;
                    } else {
                        throw new SoapException("Duplicate element: " + namespaceURI + ":" + localName);
                    }
                }
            }
        }

        return foundElement;
    }

    public static boolean elementCompareName(Element element, String namespaceURI, String localName) {
        String elementNamespaceURI = element.getNamespaceURI();
        String elementLocalName = element.getLocalName();

        return safeStringEquals(elementNamespaceURI, namespaceURI) && safeStringEquals(elementLocalName, localName);
    }

    private static boolean safeStringEquals(String string1, String string2) {
        if (string1 == null) {
            string1 = EMPTY_STRING;
        }
        if (string2 == null) {
            string2 = EMPTY_STRING;
        }

        return string1.equals(string2);
    }

    public static Element findSingleElement(Node baseNode, SoapName name) throws SoapException {
        return findSingleElement(baseNode, name.getNamespaceURI(), name.getLocalName());
    }

    public static Collection<Element> getChildElements(final Node node, final SoapName name) {
        ArrayList<Element> elements = new ArrayList<Element>();

        for (Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child instanceof Element) {
                Element childElement = (Element) child;

                if (elementCompareName(childElement, name.getNamespaceURI(), name.getLocalName())) {
                    elements.add(childElement);
                }
            }
        }

        return elements;
    }

}
