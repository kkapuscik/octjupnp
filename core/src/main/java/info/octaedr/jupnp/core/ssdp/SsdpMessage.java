/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.Header;
import org.apache.http.HttpMessage;

import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.jupnp.core.USN;

public abstract class SsdpMessage {

    protected static final String HEADER_NAME_USN = "USN";
    protected static final String HEADER_NAME_NT = "NT";
    protected static final String HEADER_NAME_ST = "ST";
    protected static final String HEADER_NAME_MX = "MX";
    protected static final String HEADER_NAME_LOCATION = "LOCATION";
    protected static final String HEADER_NAME_CACHE_CONTROL = "CACHE-CONTROL";

    protected static final String MAX_AGE_VALUE_PREFIX = "max-age=";

    private final HttpUdpMessage message;

    public SsdpMessage(HttpUdpMessage message) {
        super();

        if (message == null) {
            throw new NullPointerException("Null message given");
        }

        this.message = message;
    }

    public HttpUdpMessage getMessage() {
        return this.message;
    }

    public HttpMessage getHttpMessage() {
        return this.message.getHttpMessage();
    }

    protected String getHttpHeaderValue(String headerName, String defaultValue) {
        String value = defaultValue;
        Header header = getHttpMessage().getFirstHeader(headerName);
        if (header != null) {
            value = header.getValue();
        }
        return value;
    }

    protected int getHttpHeaderIntValue(String headerName, int defaultValue) {
        int value = defaultValue;
        Header header = getHttpMessage().getFirstHeader(headerName);
        if (header != null && header.getValue() != null) {
            try {
                value = Integer.parseInt(header.getValue(), 10);
            } catch (NumberFormatException e) {
                // ignore
            }
        }
        return value;
    }

    public abstract boolean isValid();

    @Override
    public String toString() {
        return "[" + super.toString() + " httpUdpMessage=" + getMessage() + "]";
    }

    protected URL parseLocation(String value) {
        try {
            return new URL(value);
        } catch (MalformedURLException e) {
            // ignore
        }
        return null;
    }

    protected int parseCacheControl(String value) {
        try {
            if (value != null && value.startsWith(MAX_AGE_VALUE_PREFIX)) {
                value = value.substring(MAX_AGE_VALUE_PREFIX.length());
                int intValue = Integer.parseInt(value, 10);
                if (intValue >= 0) {
                    return intValue;
                }
            }
        } catch (NumberFormatException e) {
            // ignore
        }

        return -1;
    }

    protected USN parseUSN(String value) {
        return USN.parseUSN(value);
    }

}
