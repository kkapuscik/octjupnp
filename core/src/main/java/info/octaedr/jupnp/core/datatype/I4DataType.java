/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * 4 Byte int. Same format as int. Must be between -2147483648 and 2147483647.
 *
 * The type used for values is Integer.
 */
class I4DataType extends IxDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "i4";
    private static final long MIN_VALUE = Integer.MIN_VALUE;
    private static final long MAX_VALUE = Integer.MAX_VALUE;

    /** Constructs data type info. */
    public I4DataType() {
        super(DATA_TYPE_NAME, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public Class<?> getValueClass() {
        return Integer.class;
    }

    @Override
    protected long convertObject(final Object valueObject) {
        if (valueObject instanceof Integer) {
            return (Integer) valueObject;
        }
        return CONVERT_FAILED;
    }

    @Override
    protected Object createObject(long value) {
        return new Integer((int) value);
    }

}
