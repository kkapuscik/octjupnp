/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * SOAP envelope element.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapEnvelope extends SoapElement {

    /**
     * Returns body object.
     *
     * @return Body object if exists, null otherwise.
     *
     * @throws SoapException
     *             if the SOAP XML document is invalid or element could not be created.
     */
    SoapBody getBody() throws SoapException;

    /**
     * Returns body object.
     *
     * @param create
     *            Create body object flag. If true and the body object not existed then new body object is created and
     *            attached to envelope.
     *
     * @return Body object.
     *
     * @throws SoapException
     *             if the SOAP XML document is invalid or element could not be created.
     */
    SoapBody getBody(boolean create) throws SoapException;

    /**
     * Returns header object.
     *
     * @return Header object if exists, null otherwise.
     *
     * @throws SoapException
     *             if the SOAP XML document is invalid or element could not be created.
     */
    SoapHeader getHeader() throws SoapException;

    /**
     * Returns header object.
     *
     * @param create
     *            Create header object flag. If true and the header object not existed then new header object is created
     *            and attached to envelope.
     *
     * @return Header object.
     *
     * @throws SoapException
     *             if the SOAP XML document is invalid or element could not be created.
     */
    SoapHeader getHeader(boolean create) throws SoapException;

    void setEncodingStyle(String encodingStyle);

    Object getEncodingStyle();

}
