/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.httpudp.HttpUdpMessage;

/**
 * SSDP search request message.
 *
 * <pre>
 * M-SEARCH * HTTP/1.1
 * HOST: 239.255.255.250:1900
 * MAN: "ssdp:discover"
 * MX: seconds to delay response
 * ST: search target
 * </pre>
 *
 * @author Krzysztof Kapuscik
 */
public class SearchRequestMessage extends SsdpMessage {

    SearchRequestMessage(HttpUdpMessage message) {
        super(message);
    }

    public String getSearchTarget() {
        return getHttpHeaderValue(HEADER_NAME_ST, null);
    }

    public int getMX() {
        return getHttpHeaderIntValue(HEADER_NAME_MX, -1);
    }

    @Override
    public boolean isValid() {
        return getSearchTarget() != null && getMX() > 0;
    }

}
