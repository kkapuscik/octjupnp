/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.jcommons.collection.WriteSafeHashSet;
import info.octaedr.jcommons.logging.Logger;

import java.io.IOException;
import java.net.DatagramSocket;
import java.util.Collection;

/**
 * SSDP sockets base.
 *
 * @author Krzysztof Kapuscik
 */
abstract class SsdpSocket {

    /** Logger object for printing debug messages. */
    private static final Logger logger = Logger.getLogger(SsdpSocket.class);

    /**
     * Manager for SsdpSocket listeners.
     */
    private class SsdpSocketListenerManager extends WriteSafeHashSet<SsdpSocketListener> {

        /**
         * Notifies all listeners about new message received.
         *
         * @param message
         *            Received message.
         */
        public void notifyEventReceived(SsdpMessage message) {
            Collection<SsdpSocketListener> listeners = getContents();
            if (listeners.size() > 0) {
                SsdpSocketEvent event = new SsdpSocketEvent(SsdpSocket.this, SsdpSocketEvent.Type.MESSAGE_RECEIVED,
                        message);
                for (SsdpSocketListener listener : listeners) {
                    listener.messageReceived(event);
                }
            }
        }

    }

    /** Manager for SsdpSocketListeners. */
    private final SsdpSocketListenerManager ssdpSocketListeners;
    /** SSDP client. */
    private final SsdpClient ssdpClient;
    /** Messages factory. */
    protected final SsdpMessageFactory messageFactory;

    /** Messages queue. */
    private final MessageQueue sendQueue;

    /** Current executor. */
    private SsdpSocketExecutor executor = null;

    /**
     * Constructs socket.
     *
     * @param ssdpClient
     *            SSDP client.
     */
    protected SsdpSocket(SsdpClient ssdpClient) {
        super();

        if (ssdpClient == null) {
            throw new NullPointerException("Null SSDP client");
        }

        this.ssdpSocketListeners = new SsdpSocketListenerManager();
        this.ssdpClient = ssdpClient;
        this.sendQueue = new MessageQueue();
        this.messageFactory = new SsdpMessageFactory(this.ssdpClient.getConfig());

        if (logger.trace()) {
            logger.trace("Socket created: " + this);
        }
    }

    /**
     * Adds SsdpSocket events listener.
     *
     * @param listener
     *            Listener to be added.
     */
    public void addSsdpSocketListener(SsdpSocketListener listener) {
        if (logger.trace()) {
            logger.trace("addSsdpSocketListener() socket=" + this + " listener=" + listener);
        }

        this.ssdpSocketListeners.add(listener);
    }

    /**
     * Removes SsdpSocket events listener.
     *
     * @param listener
     *            Listener to be removed.
     */
    public void removeSsdpSocketListener(SsdpSocketListener listener) {
        if (logger.trace()) {
            logger.trace("removeSsdpSocketListener() socket=" + this + " listener=" + listener);
        }

        this.ssdpSocketListeners.remove(listener);
    }

    /**
     * Starts the socket operations.
     */
    public void start() {
        if (logger.trace()) {
            logger.trace("start() socket=" + this);
        }

        synchronized (this) {
            if (this.executor != null) {
                return;
            }

            SsdpSocketExecutorHelper helper = new SsdpSocketExecutorHelper() {

                @Override
                public DatagramSocket createPeerSocket() throws IOException {
                    return SsdpSocket.this.createPeerSocket();
                }

                @Override
                public void notifyEventReceived(SsdpMessage ssdpMessage) {
                    if (logger.trace()) {
                        logger.trace("notifyEventReceived() socket=" + SsdpSocket.this + " ssdpMessage=" + ssdpMessage);
                    }

                    SsdpSocket.this.ssdpSocketListeners.notifyEventReceived(ssdpMessage);
                }

                @Override
                public String toString() {
                    return "[SsdpSocketExecutor socket=" + SsdpSocket.this + "]";
                }
            };

            this.executor = new SsdpSocketExecutor(helper, this.ssdpClient, this.messageFactory, this.sendQueue);
            this.executor.start();
        }

        if (logger.trace()) {
            logger.trace("start() socket=" + this + " - finished");
        }
    }

    /**
     * Stops the socket operations.
     */
    public void stop() {
        if (logger.trace()) {
            logger.trace("stop() socket=" + this);
        }

        SsdpSocketExecutor tmpExecutor = null;

        synchronized (this) {
            tmpExecutor = this.executor;
            this.executor = null;
        }

        if (tmpExecutor != null) {
            tmpExecutor.stop();
        }

        if (logger.trace()) {
            logger.trace("stop() socket=" + this + " - finished");
        }
    }

    /**
     * Schedules message to be sent by the socket.
     *
     * @param message
     *            Message to schedule.
     */
    public void scheduleMessageToSend(SsdpMessage message) {
        if (logger.trace()) {
            logger.trace("scheduleMessageToSend() message=" + this);
        }

        this.sendQueue.scheduleMessage(message);
    }

    /**
     * Constructs the peer socket to be used.
     *
     * @return Constructed socket.
     *
     * @throws IOException
     *             if socket could not be created
     */
    protected abstract DatagramSocket createPeerSocket() throws IOException;

}
