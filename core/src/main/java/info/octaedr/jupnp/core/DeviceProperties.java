/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Collection of device properties.
 */
public class DeviceProperties {

    /** Storage for properties. */
    private final Map<String, String> storage = new HashMap<String, String>();

    /**
     * Constructs new properties collection.
     */
    public DeviceProperties() {
        // nothing to do
    }

    /**
     * Returns property with given key.
     *
     * @param key
     *            Key of the property to get.
     *
     * @return Value of the property or null if such property was not found.
     */
    public String get(final String key) {
        if (key == null) {
            throw new IllegalArgumentException("Key may not be null");
        }

        return this.storage.get(key);
    }

    /**
     * Returns iterator over keys of available properties.
     *
     * @return Iterator over keys.
     */
    public Iterator<String> getKeys() {
        return this.storage.keySet().iterator();
    }

    /**
     * Checks if given property is available.
     *
     * @param key
     *            Key of the property to check.
     *
     * @return True if property is available, false otherwise.
     */
    public boolean contains(final String key) {
        return this.storage.containsKey(key);
    }

    /**
     * Adds new property to collection.
     *
     * @param key
     *            Key of the property.
     * @param value
     *            Value of the property.
     */
    public void add(final String key, final String value) {
        if (key == null) {
            throw new IllegalArgumentException("Key may not be null");
        }
        if (value == null) {
            throw new IllegalArgumentException("Value may not be null");
        }

        this.storage.put(key, value);
    }

}
