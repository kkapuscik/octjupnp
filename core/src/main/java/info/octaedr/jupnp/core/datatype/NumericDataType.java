/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import info.octaedr.jupnp.core.DataType;

/**
 * Numerical data type info base class.
 */
abstract class NumericDataType extends DataType {

    /**
     * Constructs new info.
     *
     * @param name
     *            Name of the data type.
     */
    protected NumericDataType(final String name) {
        super(name);
    }

    @Override
    public final boolean isValueListAllowed() {
        return false;
    }

    @Override
    public final boolean isValueRangeAllowed() {
        return true;
    }

    @Override
    protected abstract boolean checkValue(final Object value, final Object allowedRangeMinimum,
            final Object allowedRangeMaximum, final Object allowedRangeStep);

    @Override
    public abstract boolean validateRange(final Object minimum, final Object maximum, final Object step);

    public String valueToStringLong(long value) {
        return Long.toString(value, 10);
    }

    protected boolean checkValueLong(final long value, final long allowedRangeMinimum, final long allowedRangeMaximum,
            final long allowedRangeStep) {
        if (value < allowedRangeMinimum) {
            return false;
        }
        if (value > allowedRangeMaximum) {
            return false;
        }
        // TODO: check the step?
        return true;
    }

}
