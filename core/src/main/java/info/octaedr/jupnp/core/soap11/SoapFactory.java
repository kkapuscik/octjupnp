/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * Factory of SOAP objects.
 *
 * @author Krzysztof Kapuscik
 */
public abstract class SoapFactory {

    /**
     * Returns instance of factory.
     *
     * @return Factory object.
     */
    public static SoapFactory getInstance() {
        return DefaultSoapFactory.getInstance();
    }

    /**
     * Creates a new message object with the default elements.
     *
     * @return Created SOAP message.
     */
    public abstract SoapMessage createMessage();

    /**
     * Creates a name object.
     *
     * @param namespaceURI
     *            Namespace URI. Could be null.
     * @param prefix
     *            Namespace prefix. Could be null.
     * @param localName
     *            Local name.
     *
     * @return Created name object.
     */
    public abstract SoapName createName(String namespaceURI, String prefix, String localName);

    public abstract SoapName createName(String localName);

}
