/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.net.URL;

import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.jupnp.core.USN;

/**
 * SSDP search response message.
 *
 * <pre>
 * HTTP/1.1 200 OK
 * CACHE-CONTROL: max-age = seconds until advertisement expires
 * DATE: when response was generated
 * EXT:
 * LOCATION: URL for UPnP description for root device
 * SERVER: OS/version UPnP/1.0 product/version
 * ST: search target
 * USN: advertisement UUID
 * </pre>
 *
 * @author Krzysztof Kapuscik
 */
public class SearchResponseMessage extends SsdpMessage {

    SearchResponseMessage(HttpUdpMessage message) {
        super(message);
    }

    public String getSearchTarget() {
        return getHttpHeaderValue(HEADER_NAME_ST, null);
    }

    public USN getUSN() {
        return parseUSN(getHttpHeaderValue(HEADER_NAME_USN, null));
    }

    public URL getLocation() {
        return parseLocation(getHttpHeaderValue(HEADER_NAME_LOCATION, null));
    }

    public int getCacheControl() {
        return parseCacheControl(getHttpHeaderValue(HEADER_NAME_CACHE_CONTROL, null));
    }

    @Override
    public boolean isValid() {
        return getSearchTarget() != null && getUSN() != null && getLocation() != null && getCacheControl() > 0;
    }

}
