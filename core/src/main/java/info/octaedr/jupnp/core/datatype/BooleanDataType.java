/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Boolean data type.
 *
 * <p>
 * The proffered representations are "0" for false or "1" for true. The values "true", "yes", "false", or "no" may also
 * be used but are not recommended.
 * </p>
 *
 * The type used for values is Boolean.
 */
class BooleanDataType extends SimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "boolean";

    /**
     * Constructs new data type info.
     */
    public BooleanDataType() {
        super(DATA_TYPE_NAME);
    }

    @Override
    public Class<?> getValueClass() {
        return Boolean.class;
    }

    @Override
    public boolean checkValue(Object value) {
        if (value instanceof Boolean) {
            return true;
        }
        return false;
    }

    @Override
    public Object parseValue(String valueString) {
        if (valueString.equals("1") || valueString.equalsIgnoreCase("yes") || valueString.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        } else if (valueString.equals("0") || valueString.equalsIgnoreCase("no")
                || valueString.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }

        return null;
    }

    @Override
    public String valueToString(Object value) {
        return ((Boolean) value) ? "1" : "0";
    }

}
