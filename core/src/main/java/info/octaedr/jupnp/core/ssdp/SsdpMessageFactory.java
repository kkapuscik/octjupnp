/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.tools.DateFormatTools;

import java.net.InetAddress;
import java.net.URI;
import java.util.Date;

import org.apache.http.Header;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.message.BasicHttpResponse;

/**
 * Factory of SSDP messages.
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpMessageFactory {

    /** SSDP configuration. */
    // private final SsdpConfig config;
    /** HTTP user agent header value. */
    private String versionString;

    /**
     * Constructs new factory.
     *
     * @param config
     *            SSDP configuration to be used.
     */
    public SsdpMessageFactory(SsdpConfig config) {
        if (config == null) {
            throw new NullPointerException("Null config");
        }
        // this.config = config;
        this.versionString = config.getSystemVersion() + ' ' + config.getUpnpVersion() + ' ' + config.getLibVersion();
    }

    /**
     * Creates search request message to SSDP multicast group.
     *
     * @param address
     *            Message target address.
     * @param port
     *            Message target port.
     * @param mx
     *            Maximum response delay value in seconds. Must be positive.
     * @param searchTarget
     *            Search target.
     *
     * @return Created message.
     */
    public SearchRequestMessage createSearchRequest(int mx, String searchTarget) {
        SsdpConsts consts = SsdpConsts.getInstance();
        return createSearchRequest(consts.getSsdpHostAddress(), consts.getSsdpPort(), mx, searchTarget);
    }

    /**
     * Creates search request message to sender of given request.
     *
     * @param request
     *            Search request message.
     * @param address
     *            Message target address.
     * @param port
     *            Message target port.
     * @param location
     *            URI to description document.
     * @param cacheControl
     *            Seconds until advertisement expires. Must be positive.
     * @param searchTarget
     *            Search target (from request).
     * @param usn
     *            Advertisement UUID.
     *
     * @return Created message.
     */
    public SearchResponseMessage createSearchRequest(SearchRequestMessage request, URI location, int cacheControl,
            String searchTarget, String usn) {
        return createSearchResponse(request.getMessage().getAddress(), request.getMessage().getPort(), location,
                cacheControl, searchTarget, usn);
    }

    /**
     * Creates alive notify message.
     *
     * @param location
     *            URI to description document.
     * @param cacheControl
     *            Seconds until advertisement expires. Must be positive.
     * @param notifyTarget
     *            Notify target.
     * @param usn
     *            Advertisement UUID.
     *
     * @return Created message.
     */
    public AliveNotifyMessage createAliveNotifyMessage(URI location, int cacheControl, String notifyTarget, String usn) {
        SsdpConsts consts = SsdpConsts.getInstance();
        return createAliveNotifyMessage(consts.getSsdpHostAddress(), consts.getSsdpPort(), location, cacheControl,
                notifyTarget, usn);
    }

    /**
     * Creates bye bye notify message.
     *
     * @param notifyTarget
     *            Notify target.
     * @param usn
     *            Advertisement UUID.
     *
     * @return Created message.
     */
    public ByeByeNotifyMessage createByeByeNotifyMessage(String notifyTarget, String usn) {
        SsdpConsts consts = SsdpConsts.getInstance();
        return createByeByeNotifyMessage(consts.getSsdpHostAddress(), consts.getSsdpPort(), notifyTarget, usn);
    }

    /**
     * Creates search request message.
     *
     * @param address
     *            Message target address.
     * @param port
     *            Message target port.
     * @param mx
     *            Maximum response delay value in seconds. Must be positive.
     * @param searchTarget
     *            Search target.
     *
     * @return Created message.
     */
    public SearchRequestMessage createSearchRequest(InetAddress address, int port, int mx, String searchTarget) {
        if (mx <= 0) {
            throw new IllegalArgumentException("Invalid MX: " + mx);
        }
        if (searchTarget == null || searchTarget.length() == 0) {
            throw new IllegalArgumentException("Invalid search target: " + searchTarget);
        }

        HttpMessage httpMessage = new BasicHttpRequest("M-SEARCH", "*", HttpVersion.HTTP_1_1);
        httpMessage.setHeader("HOST", address.getHostAddress() + ":" + port); // TODO
        httpMessage.setHeader("MAN", "\"ssdp:discover\"");
        httpMessage.setHeader("MX", Integer.toString(mx));
        httpMessage.setHeader("ST", searchTarget);

        httpMessage.setHeader("USER-AGENT", this.versionString);

        HttpUdpMessage httpUdpMessage = new HttpUdpMessage(httpMessage, address, port);

        return new SearchRequestMessage(httpUdpMessage);
    }

    /**
     * Creates search response message.
     *
     * @param address
     *            Message target address.
     * @param port
     *            Message target port.
     * @param location
     *            URI to description document.
     * @param cacheControl
     *            Seconds until advertisement expires. Must be positive.
     * @param searchTarget
     *            Search target (from request).
     * @param usn
     *            Advertisement UUID.
     *
     * @return Created message.
     */
    public SearchResponseMessage createSearchResponse(InetAddress address, int port, URI location, int cacheControl,
            String searchTarget, String usn) {

        if (location == null || !location.isAbsolute()) {
            throw new IllegalArgumentException("Invalid location: " + location);
        }
        if (cacheControl <= 0) {
            throw new IllegalArgumentException("Invalid cache control: " + cacheControl);
        }
        if (searchTarget == null || searchTarget.length() == 0) {
            throw new IllegalArgumentException("Invalid search target: " + searchTarget);
        }
        if (usn == null || usn.length() == 0) {
            throw new IllegalArgumentException("Invalid USN: " + usn);
        }

        HttpMessage httpMessage = new BasicHttpResponse(HttpVersion.HTTP_1_1, 200, "OK");
        httpMessage.setHeader("CACHE-CONTROL", "max-age=" + cacheControl);
        httpMessage.setHeader("DATE", DateFormatTools.dateToString(new Date()));
        httpMessage.setHeader("EXT", null);
        httpMessage.setHeader("LOCATION", location.toString());
        httpMessage.setHeader("SERVER", this.versionString);
        httpMessage.setHeader("ST", searchTarget);
        httpMessage.setHeader("USN", usn);

        HttpUdpMessage httpUdpMessage = new HttpUdpMessage(httpMessage, address, port);

        return new SearchResponseMessage(httpUdpMessage);
    }

    /**
     * Creates alive notify message.
     *
     * @param address
     *            Message target address.
     * @param port
     *            Message target port.
     * @param location
     *            URI to description document.
     * @param cacheControl
     *            Seconds until advertisement expires. Must be positive.
     * @param notifyTarget
     *            Notify target.
     * @param usn
     *            Advertisement UUID.
     *
     * @return Created message.
     */
    public AliveNotifyMessage createAliveNotifyMessage(InetAddress address, int port, URI location, int cacheControl,
            String notifyTarget, String usn) {

        if (location == null || !location.isAbsolute()) {
            throw new IllegalArgumentException("Invalid location: " + location);
        }
        if (cacheControl <= 0) {
            throw new IllegalArgumentException("Invalid cache control: " + cacheControl);
        }
        if (notifyTarget == null || notifyTarget.length() == 0) {
            throw new IllegalArgumentException("Invalid notify target: " + notifyTarget);
        }
        if (usn == null || usn.length() == 0) {
            throw new IllegalArgumentException("Invalid USN: " + usn);
        }

        HttpMessage httpMessage = new BasicHttpRequest("NOTIFY", "*", HttpVersion.HTTP_1_1);
        httpMessage.setHeader("HOST", address.getHostAddress() + ":" + port); // TODO
        httpMessage.setHeader("CACHE-CONTROL", "max-age=" + cacheControl);
        httpMessage.setHeader("LOCATION", location.toString());
        httpMessage.setHeader("NT", notifyTarget);
        httpMessage.setHeader("NTS", "ssdp:alive");
        httpMessage.setHeader("SERVER", this.versionString);
        httpMessage.setHeader("USN", usn);

        HttpUdpMessage httpUdpMessage = new HttpUdpMessage(httpMessage, address, port);

        return new AliveNotifyMessage(httpUdpMessage);
    }

    /**
     * Creates bye bye notify message.
     *
     * @param address
     *            Message target address.
     * @param port
     *            Message target port.
     * @param notifyTarget
     *            Notify target.
     * @param usn
     *            Advertisement UUID.
     *
     * @return Created message.
     */
    public ByeByeNotifyMessage createByeByeNotifyMessage(InetAddress address, int port, String notifyTarget, String usn) {

        if (notifyTarget == null || notifyTarget.length() == 0) {
            throw new IllegalArgumentException("Invalid notify target: " + notifyTarget);
        }
        if (usn == null || usn.length() == 0) {
            throw new IllegalArgumentException("Invalid USN: " + usn);
        }

        HttpMessage httpMessage = new BasicHttpRequest("NOTIFY", "*", HttpVersion.HTTP_1_1);
        httpMessage.setHeader("HOST", address.getHostAddress() + ":" + port); // TODO
        httpMessage.setHeader("NT", notifyTarget);
        httpMessage.setHeader("NTS", "ssdp:byebye");
        httpMessage.setHeader("USN", usn);

        HttpUdpMessage httpUdpMessage = new HttpUdpMessage(httpMessage, address, port);

        return new ByeByeNotifyMessage(httpUdpMessage);
    }

    /**
     * Parses HTTP UDP message and returns SSDP message for it.
     *
     * @param httpUdpMessage
     *            Message to be parsed.
     *
     * @return Subclass of SSDP message that is represented by given HTTP UDP message. Null if given HTTP UDP message
     *         does not represent any known SSDP message.
     */
    public SsdpMessage parseMessage(HttpUdpMessage httpUdpMessage) {
        SsdpMessage ssdpMessage = null;
        HttpMessage httpMessage = httpUdpMessage.getHttpMessage();

        if (httpMessage instanceof HttpRequest) {
            HttpRequest httpRequest = (HttpRequest) httpMessage;
            String method = httpRequest.getRequestLine().getMethod();

            if ("M-SEARCH".equals(method)) {
                ssdpMessage = new SearchRequestMessage(httpUdpMessage);
            } else if ("NOTIFY".equals(method)) {
                Header ntsHeader = httpRequest.getFirstHeader("NTS");
                if (ntsHeader != null) {
                    String ntsValue = ntsHeader.getValue();
                    if ("ssdp:alive".equals(ntsValue)) {
                        ssdpMessage = new AliveNotifyMessage(httpUdpMessage);
                    } else if ("ssdp:byebye".equals(ntsValue)) {
                        ssdpMessage = new ByeByeNotifyMessage(httpUdpMessage);
                    }
                }
            }
        } else if (httpMessage instanceof HttpResponse) {
            HttpResponse httpResponse = (HttpResponse) httpMessage;

            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                Header extHeader = httpResponse.getFirstHeader("EXT");
                if (extHeader != null) {
                    ssdpMessage = new SearchResponseMessage(httpUdpMessage);
                }
            }
        }

        if (ssdpMessage != null) {
            if (!ssdpMessage.isValid()) {
                ssdpMessage = null;
            }
        } else {
            return null;
        }

        return ssdpMessage;
    }

}
