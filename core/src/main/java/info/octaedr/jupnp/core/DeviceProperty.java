/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

/**
 * Property of a device.
 *
 * TODO: convert it to real object and not the String wrapper.
 */
public class DeviceProperty {

    /** XML namespace for device description documents. */
    private static final String DOCUMENT_NAMESPACE = DescriptionConsts.DEVICE_DOCUMENT_NAMESPACE;
    /** Property separator between namespace and name. */
    private static final char PROPERTY_SEPARATOR = DescriptionConsts.PROPERTY_SEPARATOR;

    /** UPnP device type. */
    public static final String DEVICE_TYPE = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "deviceType";

    /** Short description for end user. */
    public static final String FRIENDLY_NAME = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "friendlyName";

    /** Manufacturer's name. */
    public static final String MANUFACTURER = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "manufacturer";

    /** Web site for Manufacturer. */
    public static final String MANUFACTURER_URL = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "manufacturerURL";

    /** Long description for end user. */
    public static final String MODEL_DESCRIPTION = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "modelDescription";

    /** Model name. */
    public static final String MODEL_NAME = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "modelName";

    /** Model number. */
    public static final String MODEL_NUMBER = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "modelNumber";

    /** Web site for model. */
    public static final String MODEL_URL = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "modelURL";

    /** Serial number. */
    public static final String SERIAL_NUMBER = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "serialNumber";

    /** Unique device name. */
    public static final String UDN = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "UDN";

    /** Universal Product Code. */
    public static final String UPC = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "UPC";

    /** URI to presentation for device. */
    public static final String PRESENTATION_URL = DOCUMENT_NAMESPACE + PROPERTY_SEPARATOR + "presentationURL";

    public static String getPropertyLocalName(final String key) {
        int index = key.indexOf(PROPERTY_SEPARATOR);
        if (index != -1) {
            return key.substring(index + 1);
        } else {
            return key;
        }
    }

}
