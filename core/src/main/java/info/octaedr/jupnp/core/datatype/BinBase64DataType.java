/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * MIME-style Base64 encoded binary BLOB. Takes 3 Bytes, splits them into 4 parts, and maps each 6 bit piece to an
 * octet. (3 octets are encoded as 4.) No limit on size.
 *
 * The type used for values is TODO -define type-.
 */
class BinBase64DataType extends UnsupportedSimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "bin.base64";

    public BinBase64DataType() {
        super(DATA_TYPE_NAME);
    }

}
