/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import java.util.Collection;

/**
 * Unicode string. No limit on length.
 *
 * The type used for values is String.
 */
class StringDataType extends UnicodeStringDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "string";

    public StringDataType() {
        super(DATA_TYPE_NAME);
    }

    @Override
    public Object parseValue(final String valueString) {
        return valueString;
    }

    @Override
    public String valueToString(Object value) {
        return (String) value;
    }

    @Override
    protected boolean checkValue(final Object value, Collection<String> allowedValues) {
        if (value instanceof String) {
            String valueStr = (String) value;

            if (allowedValues.isEmpty()) {
                return true;
            }

            for (String allowed : allowedValues) {
                if (allowed.equalsIgnoreCase(valueStr)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Class<?> getValueClass() {
        return String.class;
    }

}
