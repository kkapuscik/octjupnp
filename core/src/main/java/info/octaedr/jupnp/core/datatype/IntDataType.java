/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Fixed point, integer number. May have leading sign. May have leading zeros. (No currency symbol.) (No grouping of
 * digits to the left of the decimal, e.g., no commas.).
 *
 * The type used for values is TODO -define type-.
 */
class IntDataType extends UnsupportedNumericDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "int";

    public IntDataType() {
        super(DATA_TYPE_NAME);
    }

}
