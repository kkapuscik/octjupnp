/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * SOAP constants.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapConstants {

    /**
     * The namespace URI for the SOAP 1.1 envelope.
     */
    public static final String ENVELOPE_NAMESPACE_URI = "http://schemas.xmlsoap.org/soap/envelope/";

    /**
     * The namespace URI for the SOAP 1.1 encoding.
     *
     * <p>
     * An attribute named <code>encodingStyle</code> in the <code>URI_NS_SOAP_ENVELOPE</code> namespace and set to the
     * value <code>URI_NS_SOAP_ENCODING</code> can be added to an element to indicate that it is encoded using the rules
     * in section 5 of the SOAP 1.1 specification.
     * </p>
     */
    public static final String ENCODING_NAMESPACE_URI = "http://schemas.xmlsoap.org/soap/encoding/";

    public static final String ELEMENT_NAME_ENVELOPE = "Envelope";
    public static final String ELEMENT_NAME_HEADER = "Header";
    public static final String ELEMENT_NAME_BODY = "Body";
    public static final String ELEMENT_NAME_FAULT = "Fault";
    public static final String ELEMENT_NAME_FAULT_CODE = "faultcode";
    public static final String ELEMENT_NAME_FAULT_STRING = "faultstring";
    public static final String ELEMENT_NAME_FAULT_ACTOR = "faultactor";
    public static final String ELEMENT_NAME_DEFAULT = "detail";

    public static final String ATTRIBUTE_NAME_ENCODING_STYLE = "encodingStyle";

    public static final String SOAPACTION_HEADER_NAME = "SOAPACTION";

}
