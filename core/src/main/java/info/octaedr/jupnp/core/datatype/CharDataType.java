/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import java.util.Collection;

/**
 * Unicode string. One character long.
 *
 * TODO: Should this be String or Char? The type used for values is TODO -define type-.
 */
class CharDataType extends UnicodeStringDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "char";

    public CharDataType() {
        super(DATA_TYPE_NAME);
    }

    @Override
    public Object parseValue(String valueString) {
        if (valueString.length() == 1) {
            return valueString;
        } else {
            return null;
        }
    }

    @Override
    public String valueToString(Object value) {
        return (String) value;
    }

    @Override
    protected boolean checkValue(Object value, Collection<String> allowedValues) {
        if (value instanceof String) {
            String valueStr = (String) value;

            if (valueStr.length() != 1) {
                return false;
            }

            if (allowedValues.isEmpty()) {
                return true;
            }

            for (String allowed : allowedValues) {
                if (allowed.equalsIgnoreCase(valueStr)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Class<?> getValueClass() {
        // TODO Auto-generated method stub
        return null;
    }

}
