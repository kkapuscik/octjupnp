/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.jupnp.core.USN;

/**
 * SSDP byebye notify message.
 *
 * <pre>
 * NOTIFY * HTTP/1.1
 * HOST: 239.255.255.250:1900
 * NT: search target
 * NTS: ssdp:byebye
 * USN: uuid:advertisement UUID
 * </pre>
 *
 * @author Krzysztof Kapuscik
 */
public class ByeByeNotifyMessage extends SsdpMessage {

    ByeByeNotifyMessage(HttpUdpMessage message) {
        super(message);
    }

    public String getNotifyTarget() {
        return getHttpHeaderValue(HEADER_NAME_NT, null);
    }

    public USN getUSN() {
        return parseUSN(getHttpHeaderValue(HEADER_NAME_USN, null));
    }

    @Override
    public boolean isValid() {
        return getNotifyTarget() != null && getUSN() != null;
    }

}
