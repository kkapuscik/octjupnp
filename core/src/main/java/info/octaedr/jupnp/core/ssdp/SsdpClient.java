/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.httpudp.HttpUdpClient;

/**
 * SSDP client.
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpClient {

    /** Default TTL value for SSDP messages. */
    static final int DEFAULT_TTL = 4;

    /** HTTP UDP client. */
    private final HttpUdpClient httpUdpClient;
    /** SSDP configuration. */
    private final SsdpConfig ssdpConfig;

    public SsdpClient() {
        this(new HttpUdpClient(), new SsdpConfig());
    }

    public SsdpClient(HttpUdpClient httpUdpClient, SsdpConfig ssdpConfig) {
        if (httpUdpClient == null) {
            throw new NullPointerException("Null HTTP UDP client given");
        }
        if (ssdpConfig == null) {
            throw new NullPointerException("Null SSDP config given");
        }

        this.httpUdpClient = httpUdpClient;
        this.ssdpConfig = ssdpConfig;
    }

    public HttpUdpClient getHttpUdpClient() {
        return this.httpUdpClient;
    }

    public SsdpConfig getConfig() {
        return this.ssdpConfig;
    }

}
