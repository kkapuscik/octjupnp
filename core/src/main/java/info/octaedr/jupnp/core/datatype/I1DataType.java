/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * 1 Byte int. Same format as int.
 *
 * The type used for values is Byte.
 */
class I1DataType extends IxDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "i1";

    private static final long MIN_VALUE = Byte.MIN_VALUE;

    private static final long MAX_VALUE = Byte.MAX_VALUE;

    public I1DataType() {
        super(DATA_TYPE_NAME, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public Class<?> getValueClass() {
        return Byte.class;
    }

    @Override
    protected long convertObject(final Object valueObject) {
        if (valueObject instanceof Byte) {
            return (Byte) valueObject;
        }
        return CONVERT_FAILED;
    }

    @Override
    protected Object createObject(long value) {
        return new Byte((byte) value);
    }

}
