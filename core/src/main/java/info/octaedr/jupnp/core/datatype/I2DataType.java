/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * 2 Byte int. Same format as int.
 *
 * The type used for values is Short.
 */
class I2DataType extends IxDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "i2";
    private static final long MIN_VALUE = Short.MIN_VALUE;
    private static final long MAX_VALUE = Short.MAX_VALUE;

    public I2DataType() {
        super(DATA_TYPE_NAME, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public Class<?> getValueClass() {
        return Short.class;
    }

    @Override
    protected long convertObject(final Object valueObject) {
        if (valueObject instanceof Short) {
            return (Short) valueObject;
        }
        return CONVERT_FAILED;
    }

    @Override
    protected Object createObject(long value) {
        return new Short((short) value);
    }

}
