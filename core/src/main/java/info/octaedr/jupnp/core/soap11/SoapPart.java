/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Document;

/**
 * SOAP part of the message.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapPart extends SoapNode {

    /**
     * Returns SOAP part XML document.
     *
     * @return Document object.
     */
    Document getXmlDocument();

    /**
     * Returns the SOAP envelope object associated with this SOAP part object.
     *
     * @return SOAP envelope or null if not available.
     *
     * @throws SoapException
     *             if the SOAP XML document is invalid or element could not be created.
     */
    SoapEnvelope getEnvelope() throws SoapException;

    /**
     * Returns the SOAP envelope object associated with this SOAP part object.
     *
     * @param create
     *            Create flag. If true and envelope object is not present then it is created.
     *
     * @return SOAP envelope or null if not available.
     *
     * @throws SoapException
     *             if the SOAP XML document is invalid or element could not be created.
     */
    SoapEnvelope getEnvelope(boolean create) throws SoapException;

    /**
     * Checks if SOAP part is valid.
     *
     * @return True if SOAP part is valid, false otherwise.
     */
    boolean isValid();

}
