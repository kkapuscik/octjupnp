/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import java.util.Collection;

import info.octaedr.jupnp.core.DataType;

/**
 * Unicode strings data type info base class.
 */
abstract class UnicodeStringDataType extends DataType {

    /**
     * Constructs new info.
     *
     * @param name
     *            Name of the data type.
     */
    protected UnicodeStringDataType(final String name) {
        super(name);
    }

    @Override
    public final boolean isValueListAllowed() {
        return true;
    }

    @Override
    public final boolean isValueRangeAllowed() {
        return false;
    }

    @Override
    protected abstract boolean checkValue(Object value, Collection<String> allowedValues);

}
