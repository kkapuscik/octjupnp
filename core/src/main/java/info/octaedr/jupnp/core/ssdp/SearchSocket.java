/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;

public class SearchSocket extends SsdpSocket {

    private final InetAddress ifaceAddress;

    public SearchSocket(SsdpClient ssdpClient) {
        super(ssdpClient);
        this.ifaceAddress = null;
    }

    public SearchSocket(SsdpClient ssdpClient, InetAddress ifaceAddress) {
        super(ssdpClient);
        if (ifaceAddress == null) {
            throw new NullPointerException("Null interface address given");
        }
        this.ifaceAddress = ifaceAddress;
    }

    @Override
    protected DatagramSocket createPeerSocket() throws IOException {
        // DatagramSocket peerSocket;
        MulticastSocket peerSocket;

        peerSocket = new MulticastSocket(null);
        peerSocket.setReuseAddress(false);
        peerSocket.setTimeToLive(SsdpClient.DEFAULT_TTL);

        peerSocket.setLoopbackMode(false);

        if (this.ifaceAddress != null) {
            peerSocket.setInterface(this.ifaceAddress);
        }

        // peerSocket.bind(new InetSocketAddress(0));
        peerSocket.bind(new InetSocketAddress(this.ifaceAddress, 0));

        return peerSocket;
    }

    public void scheduleSearchMessage(int mx, String searchTarget) {
        SsdpMessage searchMessage = this.messageFactory.createSearchRequest(mx, searchTarget);
        scheduleMessageToSend(searchMessage);
    }

}
