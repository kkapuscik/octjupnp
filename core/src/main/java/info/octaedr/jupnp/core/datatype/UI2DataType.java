/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Unsigned 2 Byte int. Same format as int without leading sign.
 *
 * The type used for values is Integer.
 */
class UI2DataType extends IxDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "ui2";
    private static final long MIN_VALUE = 0x0000;
    private static final long MAX_VALUE = 0xFFFF;

    public UI2DataType() {
        super(DATA_TYPE_NAME, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public Class<?> getValueClass() {
        return Integer.class;
    }

    @Override
    protected long convertObject(final Object valueObject) {
        if (valueObject instanceof Integer) {
            return (Integer) valueObject;
        }
        return CONVERT_FAILED;
    }

    @Override
    protected Object createObject(long value) {
        return new Integer((int) value);
    }

}
