/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.net.URL;

public interface SoapClient {

    SoapMessage call(SoapMessage request, URL target) throws SoapException;

    void dispose();

}
