/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import info.octaedr.jupnp.core.DataType;

/**
 * Data type info base class for data types without lists and ranges.
 */
abstract class SimpleDataType extends DataType {

    /**
     * Constructs new info.
     *
     * @param name
     *            Name of the data type.
     */
    protected SimpleDataType(final String name) {
        super(name);
    }

    @Override
    public final boolean isValueListAllowed() {
        return false;
    }

    @Override
    public final boolean isValueRangeAllowed() {
        return false;
    }

}
