/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

abstract class UnsupportedSimpleDataType extends SimpleDataType {

    protected UnsupportedSimpleDataType(String name) {
        super(name);
    }

    @Override
    public boolean checkValue(Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object parseValue(String valueString) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String valueToString(Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class<?> getValueClass() {
        throw new UnsupportedOperationException();
    }

}
