/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Universal Resource Identifier.
 *
 * The type used for values is URI.
 */
class URIDataType extends SimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "uri";

    /**
     * Constructs new data type info.
     */
    URIDataType() {
        super(DATA_TYPE_NAME);
    }

    @Override
    public boolean checkValue(Object value) {
        if (value instanceof URI) {
            return true;
        }
        return false;
    }

    @Override
    public Object parseValue(String valueString) {
        try {
            return new URI(valueString);
        } catch (URISyntaxException e) {
            return null;
        }
    }

    @Override
    public String valueToString(Object value) {
        return ((URI) value).toString();
    }

    @Override
    public Class<?> getValueClass() {
        return URI.class;
    }

}
