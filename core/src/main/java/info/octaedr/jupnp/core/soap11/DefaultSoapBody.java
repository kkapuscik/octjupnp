/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

public class DefaultSoapBody extends DefaultSoapElement implements SoapBody {

    public DefaultSoapBody(Element element) {
        super(element);
    }

    @Override
    public SoapBodyElement addBodyElement(SoapName name) throws DOMException, SoapException {
        Element newElement = addChildElementInternal(name);
        return new DefaultSoapBodyElement(newElement);
    }

    @Override
    public boolean hasFault() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public SoapFault getFault() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<SoapBodyElement> getBodyElements(SoapName name) {
        Collection<Element> elements = XmlTools.getChildElements(getXmlNode(), name);
        ArrayList<SoapBodyElement> soapBodyElements = new ArrayList<SoapBodyElement>();
        for (Element element : elements) {
            soapBodyElements.add(new DefaultSoapBodyElement(element));
        }
        return soapBodyElements;
    }

}
