/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.util.EventObject;

/**
 * SSDP socket event.
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpSocketEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    /**
     * Type of the event.
     */
    public static enum Type {
        /** Message received event. */
        MESSAGE_RECEIVED
    }

    /** Type of the event. */
    private final Type type;
    /** The received message. */
    private final SsdpMessage message;

    /**
     * Constructs new event.
     *
     * @param source
     *            Socket being source of the event.
     * @param type
     *            Type of the event.
     * @param message
     *            Message data object.
     */
    public SsdpSocketEvent(SsdpSocket source, Type type, SsdpMessage message) {
        super(source);

        if (type == null) {
            throw new NullPointerException("Null type given");
        }
        if (message == null) {
            throw new NullPointerException("NUll message given");
        }

        this.type = type;
        this.message = message;
    }

    @Override
    public SsdpSocket getSource() {
        return (SsdpSocket) super.getSource();
    }

    /**
     * Returns type of the event.
     *
     * @return Type of the event.
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Message data object.
     *
     * @return Message data object (e.g. the message received).
     */
    public SsdpMessage getMessage() {
        return this.message;
    }

}
