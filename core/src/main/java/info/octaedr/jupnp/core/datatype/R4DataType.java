/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * 4 Byte float. Same format as float. Must be between 3.40282347E+38 to 1.17549435E-38.
 *
 * The type used for values is Float.
 */
class R4DataType extends NumericDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "r4";

    public R4DataType() {
        super(DATA_TYPE_NAME);
    }

    @Override
    public Class<?> getValueClass() {
        return Float.class;
    }

    @Override
    public Object parseValue(String valueString) {
        // TODO: Check if java parser is UPnP compatible
        try {
            return Float.parseFloat(valueString);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public String valueToString(Object value) {
        // TODO: Check if java formatter is UPnP compatible
        return ((Float) value).toString();
    }

    @Override
    public boolean validateRange(Object minimum, Object maximum, Object step) {
        Float rangeMin = (Float) minimum;
        Float rangeMax = (Float) maximum;
        if (rangeMin > rangeMax) {
            return false;
        }

        if (step != null) {
            Float rangeStep = (Float) step;
            Float rangeLen = rangeMax - rangeMin;

            if (rangeStep > rangeLen) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected boolean checkValue(Object valueObject, Object allowedRangeMinimum, Object allowedRangeMaximum,
            Object allowedRangeStep) {
        float value = (Float) valueObject;
        float rangeMin;
        float rangeMax;

        if (allowedRangeMinimum != null) {
            rangeMin = (Float) allowedRangeMinimum;
        } else {
            rangeMin = Float.MIN_VALUE;
        }

        if (allowedRangeMaximum != null) {
            rangeMax = (Float) allowedRangeMaximum;
        } else {
            rangeMax = Float.MAX_VALUE;
        }

        if (value < rangeMin || value > rangeMax) {
            return false;
        }

        return true;
    }

}
