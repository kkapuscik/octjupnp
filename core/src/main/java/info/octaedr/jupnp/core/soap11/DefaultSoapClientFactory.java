/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * Default implementation of SoapClientFactory.
 *
 * @author Krzysztof Kapuscik
 */
class DefaultSoapClientFactory extends SoapClientFactory {

    /**
     * Message factory singleton.
     */
    private static SoapClientFactory theInstance;

    /**
     * Returns factory singleton.
     *
     * @return Factory singleton.
     */
    public static synchronized SoapClientFactory getInstance() {
        if (theInstance == null) {
            theInstance = new DefaultSoapClientFactory();
        }
        return theInstance;
    }

    /**
     * Constructs the factory
     */
    private DefaultSoapClientFactory() {
        // nothing to do
    }

    @Override
    public SoapClient createClient() {
        return new DefaultSoapClient();
    }

}
