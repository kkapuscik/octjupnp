/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.net.URL;

import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.jupnp.core.USN;

/**
 * SSDP alive notify message.
 *
 * <pre>
 * NOTIFY * HTTP/1.1
 * HOST: 239.255.255.250:1900
 * CACHE-CONTROL: max-age = seconds until advertisement expires
 * LOCATION: URL for UPnP description for root device
 * NT: search target
 * NTS: ssdp:alive
 * SERVER: OS/version UPnP/1.0 product/version
 * USN: advertisement UUID
 * </pre>
 *
 * @author Krzysztof Kapuscik
 */
public class AliveNotifyMessage extends SsdpMessage {

    AliveNotifyMessage(HttpUdpMessage message) {
        super(message);
    }

    public String getNotifyTarget() {
        return getHttpHeaderValue(HEADER_NAME_NT, null);
    }

    public USN getUSN() {
        return parseUSN(getHttpHeaderValue(HEADER_NAME_USN, null));
    }

    public URL getLocation() {
        return parseLocation(getHttpHeaderValue(HEADER_NAME_LOCATION, null));
    }

    public int getCacheControl() {
        return parseCacheControl(getHttpHeaderValue(HEADER_NAME_CACHE_CONTROL, null));
    }

    @Override
    public boolean isValid() {
        return getNotifyTarget() != null && getUSN() != null && getLocation() != null && getCacheControl() > 0;
    }

}
