/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

class DefaultSoapElement extends DefaultSoapNode implements SoapElement {

    public DefaultSoapElement(Element element) {
        super(element);
    }

    @Override
    public Element getXmlElement() {
        return (Element) getXmlNode();
    }

    @Override
    public SoapElement addChildElement(SoapName name) throws DOMException, SoapException {
        Element newElement = addChildElementInternal(name);
        return new DefaultSoapElement(newElement);
    }

    @Override
    public SoapText addTextNode(String value) throws SoapException {
        Text textNode = getDocument().createTextNode(value);
        getXmlNode().appendChild(textNode);
        return new DefaultSoapText(textNode);
    }

    @Override
    public Collection<SoapElement> getChildElements(SoapName name) {
        Collection<Element> elements = XmlTools.getChildElements(getXmlNode(), name);
        ArrayList<SoapElement> soapElements = new ArrayList<SoapElement>();
        for (Element element : elements) {
            soapElements.add(new DefaultSoapElement(element));
        }
        return soapElements;
    }

    @Override
    public String getTextContent() {
        return getXmlNode().getTextContent();
    }

}
