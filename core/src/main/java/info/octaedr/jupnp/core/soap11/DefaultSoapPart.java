/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Default implementation of SoapPart.
 *
 * @author Krzysztof Kapuscik
 */
class DefaultSoapPart extends DefaultSoapNode implements SoapPart {

    /**
     * Constructs new SOAP part object.
     *
     * @param documentBuilder
     *            Document build to be used to build document.
     */
    public DefaultSoapPart(final Document document) {
        super(document);
    }

    @Override
    public Document getXmlDocument() {
        return (Document) getXmlNode();
    }

    @Override
    public SoapEnvelope getEnvelope() throws SoapException {
        return getEnvelope(false);
    }

    @Override
    public SoapEnvelope getEnvelope(boolean create) throws SoapException {
        // TODO: the prefix
        Element docElement = findOrCreateSingletonChild(new DefaultSoapName(SoapConstants.ENVELOPE_NAMESPACE_URI, "s",
                SoapConstants.ELEMENT_NAME_ENVELOPE), create);

        if (docElement != null) {
            return new DefaultSoapEnvelope(docElement);
        } else {
            return null;
        }
    }

    @Override
    public boolean isValid() {
        // TODO isValid()
        return true;
    }

}
