/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import info.octaedr.jupnp.core.UUIDTools;

import java.util.UUID;

/**
 * Universally Unique ID. Hexadecimal digits representing octets. Optional embedded hyphens are ignored.
 *
 * The type used for values is UUID.
 */
class UUIDDataType extends SimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "uuid";

    /**
     * Constructs UUID data type info.
     */
    public UUIDDataType() {
        super(DATA_TYPE_NAME);
    }

    @Override
    public boolean checkValue(Object value) {
        if (value instanceof UUID) {
            return true;
        }
        return false;
    }

    @Override
    public Object parseValue(final String valueString) {
        return UUIDTools.parseUUID(valueString);
    }

    @Override
    public String valueToString(Object value) {
        return ((UUID) value).toString();
    }

    @Override
    public Class<?> getValueClass() {
        return UUID.class;
    }

}
