/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.util.Collection;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

/**
 * SOAP XML element.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapElement extends SoapNode {

    /**
     * Returns XML element this SOAP element is associated with.
     *
     * @return XML element object.
     */
    Element getXmlElement();

    SoapElement addChildElement(SoapName name) throws DOMException, SoapException;

    SoapText addTextNode(String value) throws SoapException;

    Collection<SoapElement> getChildElements(SoapName name);

    String getTextContent();

}
