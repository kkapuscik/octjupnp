/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

import info.octaedr.jupnp.core.datatype.DataTypeFactory;

import java.util.Collection;

/**
 * Data type information.
 */
public abstract class DataType {

    /** Name of the data type. */
    private final String name;

    /**
     * Constructs new data type.
     *
     * @param name
     *            Name of the data type.
     */
    protected DataType(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name may not be null");
        }

        this.name = name;
    }

    /**
     * Returns data type name.
     *
     * @return Name of the data type.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns class of values of this data type.
     *
     * @return Class that will be used for representing values.
     */
    public abstract Class<?> getValueClass();

    /**
     * Checks if value is valid for the data type.
     *
     * @param value
     *            Value to be checked.
     * @param allowedRangeMinimum
     *            Minimum allowed value.
     * @param allowedRangeMaximum
     *            Maximum allowed value
     * @param allowedRangeStep
     *            Step defined in allowed range. May be null.
     * @param allowedValues
     *            Collection of allowed values (empty if there are none defined).
     *
     * @return True if value is valid, false otherwise.
     */
    public boolean checkValue(final Object value, final Collection<String> allowedValues,
            final Object allowedRangeMinimum, final Object allowedRangeMaximum, final Object allowedRangeStep) {
        if (isValueListAllowed()) {
            return checkValue(value, allowedValues);
        } else if (isValueRangeAllowed()) {
            return checkValue(value, allowedRangeMinimum, allowedRangeMaximum, allowedRangeStep);
        } else {
            return checkValue(value);
        }
    }

    /**
     * Checks if value list is allowed for this data type.
     *
     * @return True if value list is allowed, false otherwise.
     */
    public abstract boolean isValueListAllowed();

    /**
     * Checks if value range is allowed for this data type.
     *
     * @return True if value range is allowed, false otherwise.
     */
    public abstract boolean isValueRangeAllowed();

    /**
     * Parses the string representation of value.
     *
     * @param valueString
     *            Value to be parsed.
     *
     * @return Parsed value or null if string representation was invalid.
     */
    public abstract Object parseValue(final String valueString);

    /**
     * Checks if range is valid for the data type.
     *
     * @param minimum
     *            Minimum value.
     * @param maximum
     *            Maximum Value.
     * @param step
     *            Step value. May be null if not specified.
     *
     * @return True if given range is valid, false otherwise.
     */
    public boolean validateRange(final Object minimum, final Object maximum, final Object step) {
        if (isValueRangeAllowed()) {
            throw new RuntimeException("Fatal error - not implemented");
        }
        // ranges not allowed
        return false;
    }

    /**
     * Converts value to string representation.
     *
     * @param value
     *            The value to be converted.
     *
     * @return String representation of the value.
     */
    public abstract String valueToString(final Object value);

    /*----------------------------------------------------------------------*/

    /**
     * Checks if value is valid for the data type.
     *
     * <strong>This method MUST be overridden by data types that does not support ranges & value lists.</strong>
     *
     * @param value
     *            Value to be checked.
     *
     * @return True if value is valid, false otherwise.
     */
    protected boolean checkValue(final Object value) {
        return false;
    }

    /**
     * Checks if value is valid for the data type.
     *
     * <strong>This method MUST be overridden by data types that support value ranges.</strong>
     *
     * @param value
     *            Value to be checked.
     * @param allowedRangeMinimum
     *            Minimum allowed value.
     * @param allowedRangeMaximum
     *            Maximum allowed value
     * @param allowedRangeStep
     *            Step defined in allowed range. May be null.
     *
     * @return True if value is valid, false otherwise.
     */
    protected boolean checkValue(final Object value, final Object allowedRangeMinimum,
            final Object allowedRangeMaximum, final Object allowedRangeStep) {
        return false;
    }

    /**
     * Checks if value is valid for the data type.
     *
     * <strong>This method MUST be overridden by data types that support value lists.</strong>
     *
     * @param value
     *            Value to be checked.
     * @param allowedValues
     *            Collection of allowed values (empty if there are none defined).
     *
     * @return True if value is valid, false otherwise.
     */
    protected boolean checkValue(final Object value, final Collection<String> allowedValues) {
        return false;
    }

    /*----------------------------------------------------------------------*/

    /**
     * Parses data type.
     *
     * @param dataTypeName
     *            Name of the data type.
     *
     * @return Data type information object or null if given data type is unknown.
     */
    public static DataType parseDataType(final String dataTypeName) {
        return DataTypeFactory.fromString(dataTypeName);
    }

}
