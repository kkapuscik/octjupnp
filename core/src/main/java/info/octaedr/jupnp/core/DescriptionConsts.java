/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

public interface DescriptionConsts {

    /** XML namespace for device description documents. */
    static final String DEVICE_DOCUMENT_NAMESPACE = "urn:schemas-upnp-org:device-1-0";

    /** Property separator between namespace and name. */
    static final char PROPERTY_SEPARATOR = ' ';

}
