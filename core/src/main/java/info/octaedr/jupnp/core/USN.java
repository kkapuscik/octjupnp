/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

import java.util.UUID;

/**
 * USN (Unique Service Name).
 *
 * @author Krzysztof Kapuscik
 */
public final class USN {

    /** USN type. */
    public static enum Type {
        /** USN with device UUID. */
        DEVICE,
        /** USN with root device. */
        ROOT_DEVICE,
        /** USN with device type. */
        DEVICE_TYPE,
        /** USN with service type. */
        SERVICE_TYPE
    }

    /** Type of the USN. */
    private final Type type;

    /** Device UUID. */
    private final UUID uuid;

    /** USN type string (the part after "::" separator except root device). */
    private final String typeString;

    /**
     * Constructs new USN.
     *
     * @param type
     *            Type of USN to create. {@link Type#DEVICE} or {@link Type#ROOT_DEVICE} allowed.
     * @param uuid
     *            UUID of device.
     */
    private USN(final Type type, final UUID uuid) {
        if (type != Type.DEVICE && type != Type.ROOT_DEVICE) {
            throw new IllegalArgumentException("Illegal type specified");
        }

        this.type = type;
        this.uuid = uuid;
        this.typeString = null;
    }

    /**
     * Constructs new USN.
     *
     * @param type
     *            Type of USN to create. {@link Type#DEVICE_TYPE} or {@link Type#SERVICE_TYPE} allowed.
     * @param uuid
     *            UUID of device.
     * @param typeString
     *            Type string (device type or service type).
     */
    private USN(final Type type, final UUID uuid, final String typeString) {
        if (type != Type.DEVICE_TYPE && type != Type.SERVICE_TYPE) {
            throw new IllegalArgumentException("Illegal type specified");
        }
        this.type = type;
        this.uuid = uuid;
        this.typeString = typeString;
    }

    /**
     * Returns type of USN.
     *
     * @return Type of USN.
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Returns device UUID.
     *
     * @return Device UUID.
     */
    public UUID getUUID() {
        return this.uuid;
    }

    /**
     * Returns USN type string (service type or device type).
     *
     * @return Type string or null if not defined for USN type.
     */
    public String getTypeString() {
        return this.typeString;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("uuid:");
        builder.append(this.uuid.toString());

        switch (this.type) {
            case DEVICE:
                break;
            case ROOT_DEVICE:
                builder.append("::upnp:rootdevice");
                break;
            case DEVICE_TYPE:
            case SERVICE_TYPE:
                builder.append("::");
                builder.append(this.typeString);
                break;
            default:
                // should never happen
                throw new IllegalStateException("Illegal USN type");
        }

        return builder.toString();
    }

    /**
     * Parses USN string.
     *
     * @param usnString
     *            String to be parsed.
     *
     * @return Parsed USN object on success or null on error.
     */
    public static USN parseUSN(final String usnString) {
        if (usnString == null) {
            throw new NullPointerException("USN string may not be null");
        }

        String[] parts = usnString.split(":");

        UUID uuid = null;
        int version = -1;

        if (parts.length != 2 && parts.length != 5 && parts.length != 8) {
            return null;
        }

        /* check UUID prefix */
        if (!parts[0].equals("uuid")) {
            return null;
        }

        /* check UUID */
        uuid = UUIDTools.parseUUID(parts[1]);
        if (uuid == null) {
            return null;
        }

        if (parts.length == 2) {
            /* USN := uuid:device-UUID */
            return new USN(Type.DEVICE, uuid);
        } else {
            /* third part must be empty ('::' separator) */
            if (parts[2].length() != 0) {
                return null;
            }

            if (parts.length == 5) {
                /* USN := uuid:device-UUID::upnp:rootdevice */

                /* the rest must be upnp:rootdevice */
                if (parts[3].equals("upnp") && parts[4].equals("rootdevice")) {
                    return new USN(Type.ROOT_DEVICE, uuid);
                }
            } else if (parts.length == 8) {
                /* uuid:device-UUID::urn:schemas-upnp-org:device:deviceType:ver */
                /* uuid:device-UUID::urn:domain-name:device:deviceType:ver */
                /* uuid:device-UUID::urn:schemas-upnp-org:service:serviceType:ver */
                /* uuid:device-UUID::urn:domain-name:service:serviceType:ver */

                String typeString = usnString.substring(usnString.indexOf("::") + 2);

                /* fourth part must be 'urn' */
                if (!parts[3].equals("urn")) {
                    return null;
                }

                /* domain name and type must not be empty */
                if (parts[4].length() == 0 || parts[6].length() == 0) {
                    return null;
                }

                /* version must be valid */
                try {
                    version = Integer.parseInt(parts[7], 10);
                    if (version <= 0) {
                        return null;
                    }
                } catch (NumberFormatException e) {
                    return null;
                }

                if (parts[5].equals("device")) {
                    return new USN(Type.DEVICE_TYPE, uuid, typeString);
                } else if (parts[5].equals("service")) {
                    return new USN(Type.SERVICE_TYPE, uuid, typeString);
                }
            }
        }

        return null;
    }

}
