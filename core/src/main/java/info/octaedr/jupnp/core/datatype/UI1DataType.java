/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Unsigned 1 Byte int. Same format as int without leading sign.
 *
 * The type used for values is Short.
 */
class UI1DataType extends IxDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "ui1";
    private static final long MIN_VALUE = 0x00;
    private static final long MAX_VALUE = 0xFF;

    public UI1DataType() {
        super(DATA_TYPE_NAME, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public Class<?> getValueClass() {
        return Short.class;
    }

    @Override
    protected long convertObject(final Object valueObject) {
        if (valueObject instanceof Short) {
            return (Short) valueObject;
        }
        return CONVERT_FAILED;
    }

    @Override
    protected Object createObject(long value) {
        return new Short((short) value);
    }

}
