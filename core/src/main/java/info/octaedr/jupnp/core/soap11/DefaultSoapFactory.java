/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Default implementation of SoapFactory.
 *
 * @author Krzysztof Kapuscik
 */
class DefaultSoapFactory extends SoapFactory {

    /**
     * Message factory singleton.
     */
    private static SoapFactory theInstance;

    /**
     * Returns factory singleton.
     *
     * @return Factory singleton.
     */
    public static synchronized SoapFactory getInstance() {
        if (theInstance == null) {
            theInstance = new DefaultSoapFactory();
        }
        return theInstance;
    }

    private final DocumentBuilderFactory documentBuilderFactory;
    private final DocumentBuilder documentBuilder;

    /**
     * Constructs new factory.
     */
    public DefaultSoapFactory() {
        this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
        this.documentBuilderFactory.setNamespaceAware(true);
        try {
            this.documentBuilder = this.documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("Cannot create document builder", e);
        }
    }

    @Override
    public SoapMessage createMessage() {
        return new DefaultSoapMessage(this.documentBuilder);
    }

    @Override
    public SoapName createName(String namespaceURI, String prefix, String localName) {
        return new DefaultSoapName(namespaceURI, prefix, localName);
    }

    @Override
    public SoapName createName(String localName) {
        return new DefaultSoapName(localName);
    }

}
