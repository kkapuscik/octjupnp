/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.jcommons.logging.Logger;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * SSDP Monitor that performs monitoring on multiple network interfaces.
 *
 * @author Krzysztof Kapuscik
 */
public class MultiSsdpMonitor extends AbstractSsdpMonitor {

    // TODO: interfaces list changes monitoring

    /** Logger object for printing debug messages. */
    private static final Logger logger = Logger.getLogger(MultiSsdpMonitor.class);

    /**
     * Child SSDP monitor that is bound to a network interface.
     */
    private class SubMonitor extends SsdpMonitor {

        /**
         * Constructs monitor using given network interface.
         *
         * @param ssdpClient
         *            SSDP client instance.
         * @param ifaceAddress
         *            Address of the network interface to use.
         */
        public SubMonitor(SsdpClient ssdpClient, InetAddress ifaceAddress) {
            super(ssdpClient, ifaceAddress);
        }

        @Override
        protected void processPacket(SsdpSocketEvent event, boolean isMulticast) {
            MultiSsdpMonitor.this.processPacket(event, isMulticast);
        }

    }

    /**
     * Array of child monitors.
     */
    private final ArrayList<SubMonitor> subMonitors;

    /**
     * Constructs monitor using default network interface.
     *
     * @param ssdpClient
     *            SSDP client instance.
     */
    public MultiSsdpMonitor(SsdpClient ssdpClient) {
        this.subMonitors = new ArrayList<SubMonitor>();

        if (logger.trace()) {
            logger.trace("Creating multi monitor");
        }

        try {
            Enumeration<NetworkInterface> ifacesEnum = NetworkInterface.getNetworkInterfaces();
            while (ifacesEnum.hasMoreElements()) {
                NetworkInterface iface = ifacesEnum.nextElement();

                if (logger.trace()) {
                    logger.trace("Processing interface: " + iface.getName() + " " + iface.getDisplayName());
                }

                Enumeration<InetAddress> ifaceAddrEnum = iface.getInetAddresses();
                while (ifaceAddrEnum.hasMoreElements()) {
                    InetAddress ifaceAddr = ifaceAddrEnum.nextElement();

                    if (logger.trace()) {
                        logger.trace("Processing interface address: " + ifaceAddr.toString());
                    }

                    if (ifaceAddr instanceof Inet4Address) {
                        this.subMonitors.add(new SubMonitor(ssdpClient, ifaceAddr));
                    } else {
                        if (logger.trace()) {
                            logger.trace("Interface address ignored: " + ifaceAddr.toString());
                        }
                    }
                }
            }
        } catch (SocketException e) {
            if (logger.trace()) {
                logger.trace("Exception during multi monitor creation", e);
            }
        }

        if (logger.trace()) {
            logger.trace("Multi monitor created");
        }
    }

    @Override
    public void start() {
        for (SubMonitor monitor : this.subMonitors) {
            monitor.start();
        }
    }

    @Override
    public void stop() {
        for (SubMonitor monitor : this.subMonitors) {
            monitor.stop();
        }
    }

    @Override
    public void sendSearch(int mx, String target) {
        for (SubMonitor monitor : this.subMonitors) {
            monitor.sendSearch(mx, target);
        }
    }

}
