/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Same as r8 but no more than 14 digits to the left of the decimal point and no more than 4 to the right.
 *
 * The type used for values is TODO -define type-.
 */
class Fixed144DataType extends UnsupportedNumericDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "fixed.14.4";

    public Fixed144DataType() {
        super(DATA_TYPE_NAME);
    }

}
