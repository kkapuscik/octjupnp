/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

import info.octaedr.jupnp.core.DataType;

/**
 * DataTypes factory.
 */
public class DataTypeFactory {

    /** Table of known data types. */
    private static final DataType[] DATA_TYPES = { new UI1DataType(), new UI2DataType(), new UI4DataType(),
            new I1DataType(), new I2DataType(), new I4DataType(), new IntDataType(), new R4DataType(),
            new R8DataType(), new NumberDataType(), new Fixed144DataType(), new FloatDataType(), new CharDataType(),
            new StringDataType(), new DateDataType(), new DateTimeDataType(), new DateTimeTzDataType(),
            new TimeDataType(), new TimeTzDataType(), new BooleanDataType(), new BinBase64DataType(),
            new BinHexDataType(), new URIDataType(), new UUIDDataType(), };

    /**
     * Returns data type object for given name.
     *
     * @param dataTypeName
     *            Name of the data type.
     *
     * @return Data type object on success, null if given data type name is unknown.
     */
    public static DataType fromString(final String dataTypeName) {
        for (int i = 0; i < DATA_TYPES.length; ++i) {
            if (DATA_TYPES[i].getName().equals(dataTypeName)) {
                return DATA_TYPES[i];
            }
        }
        return null;
    }

}
