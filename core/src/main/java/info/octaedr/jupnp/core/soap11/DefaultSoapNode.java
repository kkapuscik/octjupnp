/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

class DefaultSoapNode implements SoapNode {

    private final Node xmlNode;

    public DefaultSoapNode(Node xmlNode) {
        if (xmlNode == null) {
            throw new NullPointerException("Null node given");
        }

        this.xmlNode = xmlNode;
    }

    @Override
    public Node getXmlNode() {
        return this.xmlNode;
    }

    @Override
    public String getLocalName() {
        return this.xmlNode.getLocalName();
    }

    @Override
    public String getPrefix() {
        return this.xmlNode.getPrefix();
    }

    @Override
    public String getNamespaceURI() {
        return this.xmlNode.getNamespaceURI();
    }

    protected Document getDocument() throws SoapException {
        Node currentNode = getXmlNode();
        while (currentNode.getParentNode() != null) {
            currentNode = currentNode.getParentNode();
        }
        if (currentNode instanceof Document) {
            return (Document) currentNode;
        } else {
            throw new SoapException("Cannot find document node");
        }
    }

    protected Element addChildElementInternal(SoapName name) throws DOMException, SoapException {
        Element newElement = getDocument().createElementNS(name.getNamespaceURI(), name.getQualifiedName());
        getXmlNode().appendChild(newElement);
        return newElement;
    }

    protected Element findOrCreateSingletonChild(SoapName name, boolean create) throws DOMException, SoapException {
        Element theElement = XmlTools.findSingleElement(getXmlNode(), name);
        if (theElement == null && create) {
            theElement = addChildElementInternal(name);
        }

        return theElement;
    }

}
