/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Unsigned 4 Byte int. Same format as int without leading sign.
 *
 * The type used for values is Long.
 */
class UI4DataType extends IxDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "ui4";
    private static final long MIN_VALUE = 0x00000000L;
    private static final long MAX_VALUE = 0xFFFFFFFFL;

    public UI4DataType() {
        super(DATA_TYPE_NAME, MIN_VALUE, MAX_VALUE);
    }

    @Override
    public Class<?> getValueClass() {
        return Long.class;
    }

    @Override
    protected long convertObject(final Object valueObject) {
        if (valueObject instanceof Long) {
            return (Long) valueObject;
        }
        return CONVERT_FAILED;
    }

    @Override
    protected Object createObject(long value) {
        return new Long(value);
    }

}
