/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.util.Iterator;

import org.w3c.dom.Element;

class DefaultSoapElementIterator implements Iterator<SoapElement> {

    private final Iterator<Element> elementIterator;

    public DefaultSoapElementIterator(Iterator<Element> elementIterator) {
        this.elementIterator = elementIterator;
    }

    @Override
    public boolean hasNext() {
        return this.elementIterator.hasNext();
    }

    @Override
    public SoapElement next() {
        return new DefaultSoapElement(this.elementIterator.next());
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
