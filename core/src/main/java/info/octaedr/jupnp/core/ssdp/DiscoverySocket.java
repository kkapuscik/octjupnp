/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.io.IOException;
import java.net.BindException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;

public class DiscoverySocket extends SsdpSocket {

    private final InetAddress ifaceAddress;
    private boolean bindToMulticast = true;

    public DiscoverySocket(SsdpClient ssdpClient) {
        super(ssdpClient);
        this.ifaceAddress = null;
    }

    public DiscoverySocket(SsdpClient ssdpClient, InetAddress ifaceAddress) {
        super(ssdpClient);
        if (ifaceAddress == null) {
            throw new NullPointerException("Null interface address given");
        }
        this.ifaceAddress = ifaceAddress;
    }

    @Override
    protected DatagramSocket createPeerSocket() throws IOException {
        SsdpConsts ssdpConsts = SsdpConsts.getInstance();

        if (this.bindToMulticast) {
            try {
                return createPeerSocket(ssdpConsts, ssdpConsts.getSsdpSocketAddress());
            } catch (BindException e) {
                // ignore
                this.bindToMulticast = false;
            }
        }

        // peerSocket.bind(new InetSocketAddress(this.ifaceAddress, ssdpConsts.getSsdpPort()));

        return createPeerSocket(ssdpConsts, new InetSocketAddress(ssdpConsts.getSsdpPort()));
    }

    private DatagramSocket createPeerSocket(SsdpConsts ssdpConsts, SocketAddress bindAddress) throws IOException {
        MulticastSocket peerSocket;

        peerSocket = new MulticastSocket(null);
        peerSocket.setTimeToLive(SsdpClient.DEFAULT_TTL);
        peerSocket.setLoopbackMode(false);

        if (this.ifaceAddress != null) {
            peerSocket.setInterface(this.ifaceAddress);
        }
        peerSocket.bind(bindAddress);

        peerSocket.joinGroup(ssdpConsts.getSsdpHostAddress());

        return peerSocket;
    }

}
