/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

/**
 * SSDP configuration
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpConfig {

    private String systemVersion;
    private String upnpVersion;
    private String libVersion;

    public SsdpConfig() {
        setSystemVersion("Linux/3.0");
        setUpnpVersion("UPnP/1.0");
        setLibVersion("OCTjupnp/1.0");
    }

    public String getSystemVersion() {
        return this.systemVersion;
    }

    public void setSystemVersion(String osVersion) {
        this.systemVersion = osVersion;
    }

    public String getUpnpVersion() {
        return this.upnpVersion;
    }

    public void setUpnpVersion(String upnpVersion) {
        this.upnpVersion = upnpVersion;
    }

    public String getLibVersion() {
        return this.libVersion;
    }

    public void setLibVersion(String libVersion) {
        this.libVersion = libVersion;
    }

}
