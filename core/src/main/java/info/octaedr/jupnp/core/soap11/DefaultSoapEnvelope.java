/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Element;

public class DefaultSoapEnvelope extends DefaultSoapElement implements SoapEnvelope {

    public DefaultSoapEnvelope(Element element) {
        super(element);
    }

    @Override
    public SoapBody getBody() throws SoapException {
        return getBody(false);
    }

    @Override
    public SoapBody getBody(boolean create) throws SoapException {
        // TODO: the prefix
        Element docElement = findOrCreateSingletonChild(new DefaultSoapName(SoapConstants.ENVELOPE_NAMESPACE_URI, "s",
                SoapConstants.ELEMENT_NAME_BODY), create);

        if (docElement != null) {
            return new DefaultSoapBody(docElement);
        } else {
            return null;
        }
    }

    @Override
    public SoapHeader getHeader() throws SoapException {
        return getHeader(false);
    }

    @Override
    public SoapHeader getHeader(boolean create) throws SoapException {
        // TODO: the prefix
        Element docElement = findOrCreateSingletonChild(new DefaultSoapName(SoapConstants.ENVELOPE_NAMESPACE_URI, "s",
                SoapConstants.ELEMENT_NAME_HEADER), create);

        if (docElement != null) {
            return new DefaultSoapHeader(docElement);
        } else {
            return null;
        }

    }

    @Override
    public void setEncodingStyle(String encodingStyle) {
        // TODO: the prefix

        if (encodingStyle != null) {
            getXmlElement().setAttributeNS(SoapConstants.ENVELOPE_NAMESPACE_URI,
                    "s:" + SoapConstants.ATTRIBUTE_NAME_ENCODING_STYLE, encodingStyle);
        } else {
            getXmlElement().removeAttributeNS(SoapConstants.ENVELOPE_NAMESPACE_URI,
                    "s:" + SoapConstants.ATTRIBUTE_NAME_ENCODING_STYLE);
        }
    }

    @Override
    public Object getEncodingStyle() {
        return getXmlElement().getAttributeNS(SoapConstants.ENVELOPE_NAMESPACE_URI,
                SoapConstants.ATTRIBUTE_NAME_ENCODING_STYLE);
    }

}
