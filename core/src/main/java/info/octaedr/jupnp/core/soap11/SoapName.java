/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * A representation of an XML name.
 *
 * <p>
 * This interface provides methods for getting the local and namespace-qualified names and also for getting the prefix
 * associated with the namespace for the name. It is also possible to get the URI of the namespace.
 * </p>
 *
 * <p>
 * For the XML element: &lt;example:element xmlns:example="http://www.example.xxx/xmlexample"&gt; the values are as
 * follow:
 * <ul>
 * <li>Local name: "element"
 * <li>Prefix: "example"
 * <li>Qualified name: "example:element"
 * <li>URI: "http://www.example.xxx/xmlexample"
 * </ul>
 */
public interface SoapName {

    /**
     * Gets the local name part of the XML name that this Name object represents.
     *
     * @return Local name part.
     */
    String getLocalName();

    /**
     * Returns the prefix that was specified when this Name object was initialized.
     *
     * @return Qualified name prefix.
     */
    String getPrefix();

    /**
     * Gets the namespace-qualified name of the XML name that this Name object represents.
     *
     * @return Qualified name.
     */
    String getQualifiedName();

    /**
     * Returns the URI of the namespace for the XML name that this Name object represents.
     *
     * @return Namespace URI.
     */
    String getNamespaceURI();

}
