/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * 8 Byte float. Same format as float. Must be between -1.79769313486232E308 and -4.94065645841247E-324 for negative
 * values, and between 4.94065645841247E-324 and 1.79769313486232E308 for positive values, i.e., IEEE 64-bit (8-Byte)
 * double.
 *
 * The type used for values is Double.
 */
class R8DataType extends NumericDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "r8";

    /**
     * Constructs new data type info.
     */
    public R8DataType() {
        super(DATA_TYPE_NAME);
    }

    /**
     * Constructs new data type info with given name.
     *
     * @param dataTypeName
     *            Data type name.
     */
    protected R8DataType(final String dataTypeName) {
        super(dataTypeName);
    }

    @Override
    public Class<?> getValueClass() {
        return Double.class;
    }

    @Override
    public Object parseValue(String valueString) {
        // TODO: Check if java parser is UPnP compatible
        try {
            return Double.parseDouble(valueString);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public String valueToString(Object value) {
        // TODO: Check if java formatter is UPnP compatible
        return ((Double) value).toString();
    }

    @Override
    public boolean validateRange(Object minimum, Object maximum, Object step) {
        Double rangeMin = (Double) minimum;
        Double rangeMax = (Double) maximum;
        if (rangeMin > rangeMax) {
            return false;
        }

        if (step != null) {
            Double rangeStep = (Double) step;
            Double rangeLen = rangeMax - rangeMin;

            if (rangeStep > rangeLen) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected boolean checkValue(Object valueObject, Object allowedRangeMinimum, Object allowedRangeMaximum,
            Object allowedRangeStep) {
        double value = (Double) valueObject;
        double rangeMin;
        double rangeMax;

        if (allowedRangeMinimum != null) {
            rangeMin = (Double) allowedRangeMinimum;
        } else {
            rangeMin = Double.MIN_VALUE;
        }

        if (allowedRangeMaximum != null) {
            rangeMax = (Double) allowedRangeMaximum;
        } else {
            rangeMax = Double.MAX_VALUE;
        }

        if (value < rangeMin || value > rangeMax) {
            return false;
        }

        return true;
    }

}
