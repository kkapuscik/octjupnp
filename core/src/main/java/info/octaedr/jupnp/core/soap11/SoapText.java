/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Text;

/**
 * SOAP XML text node.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapText extends SoapNode {

    /**
     * Returns XML text node this SOAP text node is associated with.
     *
     * @return XML text node object.
     */
    Text getXmlText();

}
