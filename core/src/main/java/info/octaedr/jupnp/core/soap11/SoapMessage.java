/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * SOAP message.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapMessage {

    /**
     * Returns SOAPACTION header value.
     *
     * @return Value of the header or null if not available.
     */
    String getSoapAction();

    /**
     * Sets SOAPACTION header value.
     *
     * @param soapAction
     *            Value to set or null to clear the value.
     */
    void setSoapAction(String soapAction);

    /**
     * Returns SOAP part of the message.
     *
     * @return SOAP part of the message.
     */
    SoapPart getSoapPart();

    /**
     * Checks if SOAP message is valid.
     *
     * @return True if SOAP message is valid, false otherwise.
     */
    boolean isValid();

}
