/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

class DefaultSoapName implements SoapName {

    private final String namespaceURI;
    private final String prefix;
    private final String localName;
    private final String qualifiedName;

    public DefaultSoapName(String namespaceURI, String prefix, String localName) {
        if (localName == null) {
            throw new NullPointerException("Null local name");
        }

        this.namespaceURI = namespaceURI;
        this.prefix = prefix;
        this.localName = localName;
        this.qualifiedName = (this.prefix != null ? (this.prefix + ":" + this.localName) : this.localName);
    }

    public DefaultSoapName(String localName) {
        this(null, null, localName);
    }

    @Override
    public String getLocalName() {
        return this.localName;
    }

    @Override
    public String getPrefix() {
        return this.prefix;
    }

    @Override
    public String getQualifiedName() {
        return this.qualifiedName;
    }

    @Override
    public String getNamespaceURI() {
        return this.namespaceURI;
    }

}
