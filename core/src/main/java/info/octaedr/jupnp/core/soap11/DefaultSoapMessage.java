/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;

/**
 * Default implementation of SoapMessage.
 *
 * @author Krzysztof Kapuscik
 */
class DefaultSoapMessage implements SoapMessage {

    /** SOAP part (SOAP XML document). */
    private final DefaultSoapPart soapPart;
    private String soapAction;

    /**
     * Constructs empty message.
     *
     * @param documentBuilder
     *            Document builder to be used to build soap part of message.
     */
    public DefaultSoapMessage(DocumentBuilder documentBuilder) {
        if (documentBuilder == null) {
            throw new NullPointerException("Null document builder");
        }
        this.soapPart = new DefaultSoapPart(documentBuilder.newDocument());
    }

    /**
     * Constructs message from response document.
     *
     * @param responseDocument
     *            Document with response from SOAP server.
     */
    public DefaultSoapMessage(Document responseDocument) {
        if (responseDocument == null) {
            throw new NullPointerException("Null response document");
        }
        this.soapPart = new DefaultSoapPart(responseDocument);
    }

    @Override
    public SoapPart getSoapPart() {
        return this.soapPart;
    }

    @Override
    public boolean isValid() {
        // TODO isValid()
        return true;
    }

    @Override
    public String getSoapAction() {
        return this.soapAction;
    }

    @Override
    public void setSoapAction(String soapAction) {
        this.soapAction = soapAction;
    }

}
