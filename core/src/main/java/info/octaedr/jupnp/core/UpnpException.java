/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

/**
 * UPnP exceptions base class.
 */
public class UpnpException extends Exception {

    /**
     * Serial version UID (generated).
     */
    private static final long serialVersionUID = 4117023791835053020L;

    /**
     * Constructs exception without detail message.
     */
    public UpnpException() {
        super();
    }

    /**
     * Constructs exception with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public UpnpException(final String message) {
        super(message);
    }

}
