/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import info.octaedr.jcommons.logging.Logger;

/**
 * SSDP activity monitor
 *
 * @author Krzysztof Kapuscik
 */
public abstract class AbstractSsdpMonitor {

    /** Logger object for printing debug messages. */
    private static final Logger logger = Logger.getLogger(AbstractSsdpMonitor.class);

    /**
     * Default MX (search response delay) value.
     */
    public static final int DEFAULT_MX = 5;

    /**
     * Constructs monitor.
     */
    protected AbstractSsdpMonitor() {
        // nothing to do
    }

    /**
     * Starts monitoring.
     */
    public abstract void start();

    /**
     * Stops monitoring.
     */
    public abstract void stop();

    /**
     * Sends search request with default MX.
     *
     * @param target
     *            Search target.
     */
    public void sendSearch(String target) {
        sendSearch(DEFAULT_MX, target);
    }

    /**
     * Sends search request with given MX.
     *
     * @param mx
     *            Request MX (delay) value.
     * @param target
     *            Search target.
     */
    public abstract void sendSearch(int mx, String target);

    /**
     * Processes received event.
     *
     * @param event
     *            Event to process.
     * @param isMulticast
     *            True if packet is coming from discovery socket, false if it is from search socket.
     */
    protected void processPacket(SsdpSocketEvent event, boolean isMulticast) {
        if (logger.trace()) {
            logger.trace("processPacket(event=" + event + "," + isMulticast + ")");
        }

        if (event.getType() == SsdpSocketEvent.Type.MESSAGE_RECEIVED) {
            SsdpMessage message = event.getMessage();

            if (message instanceof AliveNotifyMessage) {
                processAliveNotify((AliveNotifyMessage) message);
            } else if (message instanceof ByeByeNotifyMessage) {
                processByeByeNotify((ByeByeNotifyMessage) message);
            } else if (message instanceof SearchResponseMessage) {
                processSearchResponse((SearchResponseMessage) message);
            } else if (message instanceof SearchRequestMessage) {
                processSearchRequest((SearchRequestMessage) message);
            }
        }
    }

    /**
     * Performs processing of received alive notify.
     *
     * @param message
     *            Message to be processed.
     */
    protected void processAliveNotify(AliveNotifyMessage message) {
        if (logger.trace()) {
            logger.trace("processAliveNotify(event=" + message + ") - ignored");
        }
    }

    /**
     * Performs processing of received byebye notify.
     *
     * @param message
     *            Message to be processed.
     */
    protected void processByeByeNotify(ByeByeNotifyMessage message) {
        if (logger.trace()) {
            logger.trace("processByeByeNotify(event=" + message + ") - ignored");
        }
    }

    /**
     * Performs processing of received search response.
     *
     * @param message
     *            Message to be processed.
     */
    protected void processSearchResponse(SearchResponseMessage message) {
        if (logger.trace()) {
            logger.trace("processSearchResponse(event=" + message + ") - ignored");
        }
    }

    /**
     * Performs processing of received search request.
     *
     * @param message
     *            Message to be processed.
     */
    protected void processSearchRequest(SearchRequestMessage message) {
        if (logger.trace()) {
            logger.trace("processSearchRequest(event=" + message + ") - ignored");
        }
    }

}
