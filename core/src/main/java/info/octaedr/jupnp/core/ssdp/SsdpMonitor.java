/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.net.InetAddress;

import info.octaedr.jcommons.logging.Logger;

/**
 * SSDP activity monitor
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpMonitor extends AbstractSsdpMonitor {

    /** Logger object for printing debug messages. */
    private static final Logger logger = Logger.getLogger(SsdpMonitor.class);

    /**
     * Discovery socket.
     *
     * Discovery socket is used for receiving alive/byebye notifications.
     */
    private final DiscoverySocket discoverySocket;

    /**
     * Search socket.
     *
     * Search socket is used for sending search requests and receiving responses.
     */
    private final SearchSocket searchSocket;

    /**
     * Constructs monitor using default network interface.
     *
     * @param ssdpClient
     *            SSDP client instance.
     */
    public SsdpMonitor(SsdpClient ssdpClient) {
        this(null, ssdpClient);
    }

    /**
     * Constructs monitor using given network interface.
     *
     * @param ssdpClient
     *            SSDP client instance.
     * @param ifaceAddress
     *            Address of the network interface to use.
     */
    public SsdpMonitor(SsdpClient ssdpClient, InetAddress ifaceAddress) {
        this(ifaceAddress, ssdpClient);
    }

    /**
     * Constructs monitor.
     *
     * @param ifaceAddress
     *            Address of the network interface to use. Nul for default interface.
     * @param ssdpClient
     *            SSDP client instance.
     */
    private SsdpMonitor(InetAddress ifaceAddress, SsdpClient ssdpClient) {
        super();

        this.discoverySocket = createDiscoverySocket(ssdpClient, ifaceAddress);
        this.searchSocket = createSearchSocket(ssdpClient, ifaceAddress);

        SsdpSocketListener multicastListener = new SsdpSocketListener() {
            @Override
            public void messageReceived(SsdpSocketEvent event) {
                SsdpMonitor.this.processPacket(event, true);
            }
        };

        SsdpSocketListener unicastListener = new SsdpSocketListener() {
            @Override
            public void messageReceived(SsdpSocketEvent event) {
                SsdpMonitor.this.processPacket(event, false);
            }
        };

        this.discoverySocket.addSsdpSocketListener(multicastListener);
        this.searchSocket.addSsdpSocketListener(unicastListener);

        if (logger.trace()) {
            logger.trace("Created SSDP Monitor: " + this);
        }
    }

    /**
     * Constructs search socket.
     *
     * @param ssdpClient
     *            SSDP client instance.
     * @param ifaceAddress
     *            Address of the network interface to use. Nul for default interface.
     *
     * @return Created socket.
     */
    private SearchSocket createSearchSocket(SsdpClient ssdpClient, InetAddress ifaceAddress) {
        if (ifaceAddress != null) {
            return new SearchSocket(ssdpClient, ifaceAddress);
        } else {
            return new SearchSocket(ssdpClient);
        }
    }

    /**
     * Constructs discovery socket.
     *
     * @param ssdpClient
     *            SSDP client instance.
     * @param ifaceAddress
     *            Address of the network interface to use. Nul for default interface.
     *
     * @return Created socket.
     */
    private DiscoverySocket createDiscoverySocket(SsdpClient ssdpClient, InetAddress ifaceAddress) {
        if (ifaceAddress != null) {
            return new DiscoverySocket(ssdpClient, ifaceAddress);
        } else {
            return new DiscoverySocket(ssdpClient);
        }
    }

    @Override
    public void start() {
        this.discoverySocket.start();
        this.searchSocket.start();
    }

    @Override
    public void stop() {
        this.discoverySocket.stop();
        this.searchSocket.stop();
    }

    @Override
    public void sendSearch(int mx, String target) {
        this.searchSocket.scheduleSearchMessage(mx, target);
    }

}
