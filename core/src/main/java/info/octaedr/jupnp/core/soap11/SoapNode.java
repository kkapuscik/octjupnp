/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Node;

/**
 * SOAP XML node.
 *
 * @author Krzysztof Kapuscik
 */
public interface SoapNode {

    /**
     * Returns XML node this SOAP node is associated with.
     *
     * @return XML node object.
     */
    Node getXmlNode();

    /**
     * Returns the local part of the qualified name of this node.
     *
     * @return Local name of the node.
     *
     * @see Node#getLocalName()
     */
    String getLocalName();

    /**
     * Return the namespace prefix of this node.
     *
     * @return The namespace prefix of this node, or <code>null</code> if it is unspecified.
     *
     * @see Node#getPrefix()
     */
    public String getPrefix();

    /**
     * Return the namespace URI of this node.
     *
     * @return Namespace URI of the node or <code>null</code> if it is unspecified.
     *
     * @see Node#getNamespaceURI()
     */
    public String getNamespaceURI();

}
