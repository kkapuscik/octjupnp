/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import java.util.Collection;

import org.w3c.dom.DOMException;

public interface SoapBody extends SoapElement {

    SoapBodyElement addBodyElement(SoapName name) throws DOMException, SoapException;

    boolean hasFault();

    SoapFault getFault();

    Collection<SoapBodyElement> getBodyElements(SoapName name);

}
