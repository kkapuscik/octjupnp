/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.util.LinkedList;

/**
 * List of messages.
 *
 * @author Krzysztof Kapuscik
 */
class MessageQueue {

    /** List of message to send. */
    private final LinkedList<SsdpMessage> messagesToSend;

    /**
     * Constructs empty queue.
     */
    public MessageQueue() {
        this.messagesToSend = new LinkedList<SsdpMessage>();
    }

    /**
     * Wakes all threads waiting for new message to send.
     */
    public synchronized void wakeGetters() {
        notifyAll();
    }

    /**
     * Returns next message to process.
     *
     * <p>
     * This function will block until there is a message to return. The wait could be broken by thread
     * InterruptedException or by calling wake method.
     * </p>
     *
     * @return Next message if available, null otherwise.
     */
    public synchronized SsdpMessage getNextMessage() {
        if (this.messagesToSend.size() == 0) {
            try {
                wait();
            } catch (final InterruptedException e) {
                // ignore
            }
        }

        if (this.messagesToSend.size() > 0) {
            return this.messagesToSend.removeFirst();
        } else {
            return null;
        }
    }

    /**
     * Schedules message to be processed.
     *
     * @param message
     *            Message to be scheduled.
     */
    public void scheduleMessage(final SsdpMessage message) {
        addToQueue(message, true);
    }

    /**
     * Reschedules message to be sent.
     *
     * <p>
     * This method is normally called when sending the message failed due to socket problems.
     * </p>
     *
     * @param message
     *            Message to be rescheduled.
     */
    public void rescheduleMessage(final SsdpMessage message) {
        addToQueue(message, false);
    }

    /**
     * Adds message to queue.
     *
     * <p>
     * Method wakes single getter waiting in {@link #getNextMessage()} if there is any.
     * </p>
     *
     * @param message
     *            Message to add.
     * @param atEnd
     *            If true message is added at end of queue. If false message is added at start of queue.
     */
    private synchronized void addToQueue(final SsdpMessage message, final boolean atEnd) {
        if (message == null) {
            throw new NullPointerException("Null message given");
        }

        if (atEnd) {
            this.messagesToSend.addLast(message);
        } else {
            this.messagesToSend.addFirst(message);
        }

        if (this.messagesToSend.size() == 1) {
            notify();
        }
    }

}
