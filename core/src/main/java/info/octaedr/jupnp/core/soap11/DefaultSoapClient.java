/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import info.octaedr.jcommons.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import info.octaedr.tools.XMLTools;

import java.io.StringWriter;
import java.net.URL;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

class DefaultSoapClient implements SoapClient {

    private static final Logger logger = Logger.getLogger(DefaultSoapClient.class);

    private DefaultHttpClient httpClient;

    private final Transformer xmlTransformer;

    public DefaultSoapClient() {
        try {
            this.xmlTransformer = TransformerFactory.newInstance().newTransformer();
            this.xmlTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
        } catch (TransformerException e) {
            throw new IllegalStateException("Cannot create XML transformer", e);
        }

        // this.httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());
        this.httpClient = new DefaultHttpClient();
    }

    @Override
    public SoapMessage call(SoapMessage soapRequest, URL target) throws SoapException {
        HttpPost httpPost = null;
        HttpResponse httpResponse = null;

        try {
            if (logger.trace()) {
                logger.trace("Creating POST request for: " + target);
            }

            if (!soapRequest.isValid()) {
                throw new SoapException("Invalid request message");
            }

            /* prepare request */
            String serializedSoapRequest = serializeMessage(soapRequest);

            /* prepare request entity */
            StringEntity requestEntity = new StringEntity(serializedSoapRequest, "text/xml", HTTP.UTF_8);

            /* create get request */
            httpPost = new HttpPost(target.toURI());
            httpPost.setEntity(requestEntity);
            if (soapRequest.getSoapAction() != null) {
                httpPost.addHeader(SoapConstants.SOAPACTION_HEADER_NAME, soapRequest.getSoapAction());
            }

            if (logger.trace()) {
                logger.trace("Executing request");
            }

            /* execute the request */
            httpResponse = this.httpClient.execute(httpPost);

            /* checking the response */
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                if (logger.trace()) {
                    logger.trace("Server responded with success");
                }
            } else if (httpResponse.getStatusLine().getStatusCode() == 500) {
                if (logger.trace()) {
                    logger.trace("Server responded with failure");
                }
            } else {
                throw new SoapException("Invalid response status code for URL=" + target + " status="
                        + httpResponse.getStatusLine().getStatusCode());
            }

            if (logger.trace()) {
                logger.trace("Getting entity from response");
            }

            /* get response entity */
            HttpEntity entity = httpResponse.getEntity();

            if (logger.trace()) {
                logger.trace("Entity length: " + entity.getContentLength());
            }

            /* parse response document */
            Document responseDocument = XMLTools.parseDocument(entity.getContent());
            if (responseDocument == null) {
                throw new SoapException("Cannot parse response document");
            }

            /* build response message */
            SoapMessage message = new DefaultSoapMessage(responseDocument);

            return message;
        } catch (SoapException e) {
            if (logger.trace()) {
                logger.trace("POST failed with exception: " + e);
            }

            /* rethrow */
            throw e;
        } catch (Exception e) {
            if (logger.trace()) {
                logger.trace("POST failed with exception: " + e);
            }

            throw new SoapException("Exception occured during SOAP call", e);
        } finally {
            if (httpPost != null) {
                httpPost.abort();
            }
        }
    }

    private String serializeMessage(SoapMessage request) throws TransformerException {
        /* get source document */
        Document document = request.getSoapPart().getXmlDocument();

        /* initialize source */
        DOMSource source = new DOMSource(document);

        // TODO: produce UTF-8 bytes directly

        /* initialize target */
        StreamResult result = new StreamResult(new StringWriter());

        /* transport from XML document to string representation */
        this.xmlTransformer.transform(source, result);

        /* get result */
        String xmlString = result.getWriter().toString();

        if (logger.trace()) {
            logger.trace("serialized message: " + request);
        }

        return xmlString;
    }

    @Override
    public void dispose() {
        /*
         * When HttpClient instance is no longer needed, shut down the connection manager to ensure immediate
         * deallocation of all system resources.
         */
        this.httpClient.getConnectionManager().shutdown();
        this.httpClient = null;
    }

}
