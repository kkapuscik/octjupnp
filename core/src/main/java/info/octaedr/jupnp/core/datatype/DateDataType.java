/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Date in a subset of ISO 8601 format without time data.
 *
 * The type used for values is TODO -define type-.
 */
class DateDataType extends UnsupportedSimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "time";

    public DateDataType() {
        super(DATA_TYPE_NAME);
    }

}
