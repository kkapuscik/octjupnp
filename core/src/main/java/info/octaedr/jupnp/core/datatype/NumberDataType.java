/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Same as r8.
 *
 * The type used for values is TODO -define type-.
 */
class NumberDataType extends R8DataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "number";

    /**
     * Constructs number data type.
     */
    public NumberDataType() {
        super(DATA_TYPE_NAME);
    }

}
