/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.io.IOException;
import java.net.DatagramSocket;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.httpudp.HttpUdpSocket;
import org.apache.http.HttpException;

/**
 * The executor of SsdpSocket send/receive functionality.
 *
 * TODO: refactoring - change to nio or something so only one thread (or no threads at all) are needed.
 *
 * @author Krzysztof Kapuscik
 */
class SsdpSocketExecutor {

    /** Logger object for printing debug messages. */
    private static final Logger logger = Logger.getLogger(SsdpSocketExecutor.class);

    /** The delay to wait after the unsuccessful socket (re)creation. */
    private static final long SOCKET_CREATE_ERROR_DELAY = 5 * 1000;

    /** Helper for communication with SSDP socket. */
    private final SsdpSocketExecutorHelper helper;
    /** SSDP client instance. */
    private final SsdpClient ssdpClient;
    /** Factory to be used for construction of SSDP messages. */
    private final SsdpMessageFactory messageFactory;
    /** Queue with messages to send. */
    private final MessageQueue sendQueue;

    /** Stop requested flag. */
    private volatile boolean stopRequested;
    /** Socket for seding messages. */
    private volatile HttpUdpSocket httpUdpSocket;
    /** Thread for receiving messages. */
    private Thread receiveThread;
    /** Thread for sending message. */
    private Thread sendThread;

    /**
     * Constructs the executor.
     *
     * @param helper
     *            Helper for communication with SSDP socket.
     * @param ssdpClient
     *            SSDP client instance.
     * @param messageFactory
     *            Factory to be used for construction of SSDP messages.
     * @param sendQueue
     *            Queue with messages to send.
     */
    public SsdpSocketExecutor(SsdpSocketExecutorHelper helper, SsdpClient ssdpClient,
            SsdpMessageFactory messageFactory, MessageQueue sendQueue) {
        this.helper = helper;
        this.ssdpClient = ssdpClient;
        this.messageFactory = messageFactory;
        this.sendQueue = sendQueue;

        this.stopRequested = false;

        if (logger.trace()) {
            logger.trace("Created executor for helper=" + this.helper);
        }
    }

    /**
     * Starts the executor.
     */
    public void start() {
        if (logger.trace()) {
            logger.trace("Starting executor for helper=" + this.helper);
        }

        /* create thread for receiving packets. */
        this.receiveThread = new Thread(new Runnable() {
            @Override
            public void run() {
                SsdpSocketExecutor.this.receiveLoop();
            }
        });
        /* create thread for sending packets */
        this.sendThread = new Thread(new Runnable() {
            @Override
            public void run() {
                SsdpSocketExecutor.this.sendLoop();
            }
        });

        /* start threads */
        this.receiveThread.start();
        this.sendThread.start();

        if (logger.trace()) {
            logger.trace("Executor started for helper=" + this.helper);
        }
    }

    /**
     * Stops the executor.
     */
    public void stop() {
        if (logger.trace()) {
            logger.trace("Stopping executor for helper=" + this.helper);
        }

        while (this.receiveThread != null) {
            try {
                this.stopRequested = true;

                /* wake up sleepers */
                synchronized (this) {
                    notifyAll();
                }

                this.sendQueue.wakeGetters();
                closeSocket();

                this.receiveThread.join();
                this.sendThread.join();

                this.receiveThread = null;
                this.sendThread = null;

                this.stopRequested = false;
            } catch (InterruptedException e) {
                // ignore
            }
        }

        if (logger.trace()) {
            logger.trace("Executor stopped for helper=" + this.helper);
        }
    }

    /**
     * Returns the socket for sending and receiving packets.
     *
     * <p>
     * If the socket does not exists creates new socket.
     * </p>
     *
     * @return Socket or null if socket is not available.
     */
    private synchronized HttpUdpSocket getSocket() {
        if (this.httpUdpSocket == null) {
            try {
                DatagramSocket peerSocket = this.helper.createPeerSocket();
                HttpUdpSocket newSocket = new HttpUdpSocket(this.ssdpClient.getHttpUdpClient(), peerSocket);

                this.httpUdpSocket = newSocket;

                if (logger.trace()) {
                    logger.trace("Executor socket created for helper=" + this.helper + " socket=" + this.httpUdpSocket);
                }
            } catch (IOException e) {
                if (logger.info()) {
                    logger.info("Executor socket creation failed for helper=" + this.helper, e);
                }

                try {
                    wait(SOCKET_CREATE_ERROR_DELAY);
                } catch (InterruptedException e1) {
                    // ignore
                }
            }
        }

        return this.httpUdpSocket;
    }

    /**
     * Closes the socket used for sending and receiving packets.
     */
    private synchronized void closeSocket() {
        if (this.httpUdpSocket != null) {
            if (logger.trace()) {
                logger.trace("Executor socket closed for helper=" + this.helper + " socket=" + this.httpUdpSocket);
            }
            this.httpUdpSocket.close();
            this.httpUdpSocket = null;
        }
    }

    /**
     * Receives packets until stop is requested.
     */
    private void receiveLoop() {
        while (!this.stopRequested) {
            try {
                try {
                    /* get/create the socket */
                    HttpUdpSocket socket = getSocket();

                    if (socket != null) {
                        /* receive message */
                        HttpUdpMessage httpUdpMessage = socket.receiveMessage();

                        /* parse received message */
                        SsdpMessage ssdpMessage = this.messageFactory.parseMessage(httpUdpMessage);
                        if (ssdpMessage != null) {
                            /* send event with received messages */
                            this.helper.notifyEventReceived(ssdpMessage);
                        }
                    }
                } catch (IOException e) {
                    /* ignore "socket closed" exception on exit */
                    if (!this.stopRequested) {
                        if (logger.info()) {
                            logger.info("Executor receive IO error for helper=" + this.helper, e);
                        }
                    }

                    closeSocket();
                } catch (HttpException e) {
                    if (logger.info()) {
                        logger.info("Executor receive HTTP error for helper=" + this.helper, e);
                    }
                }
            } catch (Throwable t) {
                if (logger.info()) {
                    logger.info("Fatal receive exception catched", t);
                }
            }
        }
    }

    /**
     * Sends packets from the send queue until stop is requested.
     */
    private void sendLoop() {
        while (!this.stopRequested) {
            try {
                /* get next message to send */
                SsdpMessage message = this.sendQueue.getNextMessage();
                if (message != null) {
                    try {
                        /* get/create the socket */
                        HttpUdpSocket socket = getSocket();

                        if (socket != null) {
                            /* send the message */
                            socket.sendMessage(message.getMessage());

                            /* clear message so it will not be rescheduled */
                            message = null;
                        }
                    } catch (IOException e) {
                        /* ignore "socket closed" exception on exit */
                        if (!this.stopRequested) {
                            if (logger.info()) {
                                logger.info("Executor send IO error for helper=" + this.helper, e);
                            }
                        }

                        closeSocket();
                    } catch (HttpException e) {
                        if (logger.info()) {
                            logger.info("Executor send HTTP error for helper=" + this.helper, e);
                        }
                    }
                }

                /* on error */
                if (message != null) {
                    this.sendQueue.rescheduleMessage(message);
                }

                /* free resources */
                message = null;
            } catch (Throwable t) {
                if (logger.info()) {
                    logger.info("Fatal send exception catched", t);
                }
            }
        }
    }

}
