/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

/**
 * UPnP related utilities.
 *
 * @author Krzysztof Kapuscik
 */
public class UpnpTools {

    /**
     * Checks if element is compatible with requested type.
     *
     * @param elementType
     *            Type of the element to check.
     * @param requestedType
     *            Requested type.
     *
     * @return True if element is compatible with requested type, false otherwise.
     */
    public static boolean isCompatible(String elementType, String requestedType) {
        /* check if there is a version in both types */
        int thisIndex = elementType.lastIndexOf(':');
        int otherIndex = requestedType.lastIndexOf(':');
        if (thisIndex > 0 && otherIndex > 0 && thisIndex == otherIndex) {
            String thisTypeBase = elementType.substring(0, thisIndex);
            String otherTypeBase = requestedType.substring(0, otherIndex);

            if (thisTypeBase.equals(otherTypeBase)) {
                String thisVersionStr = elementType.substring(thisIndex + 1);
                String otherVersionStr = requestedType.substring(otherIndex + 1);

                try {
                    int thisVersion = Integer.parseInt(thisVersionStr);
                    int otherVersion = Integer.parseInt(otherVersionStr);

                    if (thisVersion > 0 && otherVersion > 0 && thisVersion >= otherVersion) {
                        return true;
                    }
                } catch (NumberFormatException e) {
                    // ignore, fallback to string compare
                }
            }
        }

        return elementType.equals(requestedType);
    }

}
