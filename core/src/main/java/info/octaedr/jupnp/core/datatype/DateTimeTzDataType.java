/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * Date in ISO 8601 format with optional time and optional time zone.
 *
 * The type used for values is TODO -define type-.
 */
class DateTimeTzDataType extends UnsupportedSimpleDataType {

    /** Data type name. */
    private static final String DATA_TYPE_NAME = "dateTime.tz";

    public DateTimeTzDataType() {
        super(DATA_TYPE_NAME);
    }

}
