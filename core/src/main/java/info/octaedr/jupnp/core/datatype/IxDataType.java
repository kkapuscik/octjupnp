/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.datatype;

/**
 * X-bytes integer data type base class.
 */
abstract class IxDataType extends NumericDataType {

    /** Conversion failed constant. */
    protected static final long CONVERT_FAILED = Long.MIN_VALUE;

    /** Minimum allowed value. */
    private final long minValue;
    /** Maximum allowed value. */
    private final long maxValue;

    /**
     * Constructs new data type info.
     *
     * @param dataTypeName
     *            Name of the data type.
     * @param minValue
     *            Minimum allowed value.
     * @param maxValue
     *            Maximum allowed value.
     */
    IxDataType(final String dataTypeName, final long minValue, final long maxValue) {
        super(dataTypeName);
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    /**
     * Checks if value is valid and withing given range.
     *
     * @param value
     *            Value to check.
     * @param rangeMin
     *            Value range minimum.
     * @param rangeMax
     *            Value range maximum.
     *
     * @return True if value is valid, false otherwise.
     */
    private boolean checkValue(final long value, final long rangeMin, final long rangeMax) {
        if (value < rangeMin) {
            return false;
        }
        if (value > rangeMax) {
            return false;
        }

        return true;
    }

    @Override
    protected boolean checkValue(final Object valueObject, final Object allowedRangeMinimum,
            final Object allowedRangeMaximum, final Object allowedRangeStep) {
        long value = convertObject(valueObject);
        if (value == CONVERT_FAILED) {
            return false;
        }

        /* check minimum */
        long rangeMin;
        if (allowedRangeMinimum != null) {
            rangeMin = convertObject(allowedRangeMinimum);
            if (rangeMin == CONVERT_FAILED) {
                return false;
            }
        } else {
            rangeMin = this.minValue;
        }

        /* check maximum */
        long rangeMax;
        if (allowedRangeMaximum != null) {
            rangeMax = convertObject(allowedRangeMaximum);
            if (rangeMax == CONVERT_FAILED) {
                return false;
            }
        } else {
            rangeMax = this.maxValue;
        }

        return checkValue(value, rangeMin, rangeMax);
    }

    @Override
    public boolean validateRange(final Object minimum, final Object maximum, final Object step) {
        long rangeMin = convertObject(minimum);
        long rangeMax = convertObject(maximum);

        if (rangeMin == CONVERT_FAILED || rangeMax == CONVERT_FAILED || rangeMin > rangeMax) {
            return false;
        }

        if (step != null) {
            long rangeLen = rangeMax - rangeMin;

            long rangeStep = convertObject(step);
            if (rangeStep == CONVERT_FAILED || rangeStep > rangeLen) {
                return false;
            }
        }

        return true;
    }

    @Override
    public String valueToString(final Object valueObject) {
        long value = convertObject(valueObject);
        return Long.toString(value, 10);
    }

    @Override
    public Object parseValue(String valueString) {
        try {
            long value = Long.parseLong(valueString, 10);
            if (checkValue(value, this.minValue, this.maxValue)) {
                return createObject(value);
            }
        } catch (NumberFormatException e) {
        }
        return null;
    }

    /**
     * Constructs object for given value.
     *
     * @param value
     *            Value for which the object should be created.
     *
     * @return Created object.
     */
    protected abstract Object createObject(long value);

    /**
     * Converts object to value.
     *
     * @param valueObject
     *            Object to be converted.
     *
     * @return Value or {@link #CONVERT_FAILED}.
     */
    protected abstract long convertObject(Object valueObject);

}
