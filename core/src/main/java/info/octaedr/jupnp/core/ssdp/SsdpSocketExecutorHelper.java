/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.ssdp;

import java.io.IOException;
import java.net.DatagramSocket;

interface SsdpSocketExecutorHelper {

    DatagramSocket createPeerSocket() throws IOException;

    void notifyEventReceived(SsdpMessage ssdpMessage);

}
