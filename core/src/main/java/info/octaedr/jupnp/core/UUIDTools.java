/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core;

import java.util.UUID;

/**
 * UUID related tools.
 */
public class UUIDTools {

    /**
     * Parses the UUID string representation.
     *
     * @param uuidString
     *            String representation of the UUID.
     *
     * @return Parsed UUID on success or null on error.
     */
    public static UUID parseUUID(final String uuidString) {
        return parseUUID(uuidString, null);
    }

    /**
     * Parses the UUID string representation.
     *
     * @param uuidString
     *            String representation of the UUID.
     * @param prefix
     *            Prefix that uuidString should have. Null if no prefix checking should be done.
     *
     * @return Parsed UUID on success or null on error.
     */
    public static UUID parseUUID(final String uuidString, final String prefix) {
        if (uuidString == null) {
            throw new IllegalArgumentException("Value may not be null");
        }

        // because of stupid java API we cannot simply use UUID.fromString
        // even more we cannot make our own UUID class because the standard
        // version is defined as final.. there is no sun
        long mostSigBits = 0;
        long leastSigBits = 0;

        int len = uuidString.length();
        int digits = 0;
        int i;

        if (prefix != null) {
            if (!uuidString.startsWith(prefix)) {
                return null;
            }
            i = prefix.length();
        } else {
            i = 0;
        }

        for (; i < len; ++i) {
            int v = -1;

            char c = uuidString.charAt(i);
            switch (c) {
                case '0':
                    v = 0x00;
                    break;
                case '1':
                    v = 0x01;
                    break;
                case '2':
                    v = 0x02;
                    break;
                case '3':
                    v = 0x03;
                    break;
                case '4':
                    v = 0x04;
                    break;
                case '5':
                    v = 0x05;
                    break;
                case '6':
                    v = 0x06;
                    break;
                case '7':
                    v = 0x07;
                    break;
                case '8':
                    v = 0x08;
                    break;
                case '9':
                    v = 0x09;
                    break;
                case 'a':
                case 'A':
                    v = 0x0A;
                    break;
                case 'b':
                case 'B':
                    v = 0x0B;
                    break;
                case 'c':
                case 'C':
                    v = 0x0C;
                    break;
                case 'd':
                case 'D':
                    v = 0x0D;
                    break;
                case 'e':
                case 'E':
                    v = 0x0E;
                    break;
                case 'f':
                case 'F':
                    v = 0x0F;
                    break;
                case '-':
                    // ignore
                    break;
                default:
                    // illegal character
                    return null;
            }

            if (v >= 0) {
                ++digits;
                if (digits > 32) {
                    break;
                }
                if (digits <= 16) {
                    mostSigBits <<= 4;
                    mostSigBits |= v;
                } else {
                    leastSigBits <<= 4;
                    leastSigBits |= v;
                }
            }
        }

        if (digits == 32) {
            return new UUID(mostSigBits, leastSigBits);
        }

        return null;
    }

}
