/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * Factory class for constructing SOAP clients.
 *
 * @author Krzysztof Kapuscik
 */
public abstract class SoapClientFactory {

    /**
     * Returns instance of factory.
     *
     * @return Factory object.
     */
    public static SoapClientFactory getInstance() {
        return DefaultSoapClientFactory.getInstance();
    }

    /**
     * Creates a client.
     *
     * @return Created client.
     */
    public abstract SoapClient createClient();

}
