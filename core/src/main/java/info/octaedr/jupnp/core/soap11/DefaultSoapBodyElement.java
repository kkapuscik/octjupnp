/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

import org.w3c.dom.Element;

public class DefaultSoapBodyElement extends DefaultSoapElement implements SoapBodyElement {

    public DefaultSoapBodyElement(Element element) {
        super(element);
    }

}
