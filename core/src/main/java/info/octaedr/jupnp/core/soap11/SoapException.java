/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.core.soap11;

/**
 * Soap exception.
 *
 * @author Krzysztof Kapuscik
 */
public class SoapException extends Exception {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 1522225681771189925L;

    /**
     * Constructs a new exception with <code>null</code> as its detail message.
     */
    public SoapException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message
     *            The detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
     *            method.
     */
    public SoapException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message
     *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param cause
     *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt>
     *            value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public SoapException(String message, Throwable cause) {
        super(message, cause);
    }

}
