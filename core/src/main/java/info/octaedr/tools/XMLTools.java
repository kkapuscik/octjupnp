/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.tools;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.UpnpException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Misc. tools for XML processing.
 */
public class XMLTools {

    /** Logger for printing debug messages. */
    private static final Logger logger = Logger.getLogger(XMLTools.class);

    /**
     * Parses given document.
     *
     * @param input
     *            Input stream with document to be parsed.
     *
     * @return Parsed document or null on error.
     *
     * @throws ParserConfigurationException
     *             if a DocumentBuilder cannot be created which satisfies the configuration requested.
     * @throws IOException
     *             if any IO errors occur.
     * @throws SAXException
     *             if any parse errors occur.
     */
    public static Document parseDocument(final InputStream input) throws ParserConfigurationException, SAXException,
            IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);

        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new InputSource(input));

        if (logger.info()) {
            logger.info("XMLParser: " + document.getDocumentElement().toString());
        }

        return document;
    }

    /**
     * Parses given document.
     *
     * @param documentText
     *            Text with document to be parsed.
     *
     * @return Parsed document or null on error.
     */
    public static Document parseDocument(final String documentText) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);

            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new InputSource(new StringReader(documentText)));

            if (logger.info()) {
                logger.info("XMLParser: " + document.getDocumentElement().toString());
            }

            return document;
        } catch (ParserConfigurationException e) {
            if (logger.info()) {
                logger.info("XMLParser failed : ", e);
            }
        } catch (SAXException e) {
            if (logger.info()) {
                logger.info("XMLParser failed : ", e);
            }
        } catch (IOException e) {
            if (logger.info()) {
                logger.info("XMLParser failed : ", e);
            }
        }

        return null;
    }

    /**
     * Returns element's value.
     *
     * @param element
     *            Element to get value for.
     *
     * @return Element's value if it was possible to prepare it, null otherwise.
     */
    public static String getElementValue(final Element element) {
        Node node;
        Node textNode = null;

        int nodeCount = 0;
        for (node = element.getFirstChild(); node != null; node = node.getNextSibling()) {
            if (node instanceof Text) {
                if (++nodeCount == 1) {
                    textNode = node;
                }
            } else if (node instanceof Element) {
                return null;
            }
        }

        if (nodeCount == 0) {
            return "";
        } else if (nodeCount == 1) {
            if (textNode == null) {
                throw new IllegalStateException();
            }
            return textNode.getNodeValue();
        }

        StringBuilder builder = new StringBuilder();
        for (node = element.getFirstChild(); node != null; node = node.getNextSibling()) {
            if (node instanceof Text) {
                builder.append(node.getNodeValue());
            }
        }

        return builder.toString();
    }

    /**
     * Checks if element has specified name.
     *
     * @param element
     *            Element to be checked.
     * @param namespaceURI
     *            Wanted namespace URI.
     * @param localName
     *            Wanted local name.
     *
     * @return True if element has specified name, false otherwise.
     */
    public static boolean checkElementName(final Element element, final String namespaceURI, final String localName) {
        String elementNamespaceURI = element.getNamespaceURI();
        String elementLocalName = element.getLocalName();

        if (namespaceURI != null) {
            if (!namespaceURI.equals(elementNamespaceURI)) {
                return false;
            }
        } else {
            if (elementNamespaceURI != null) {
                return false;
            }
        }

        if (localName != null) {
            if (!localName.equals(elementLocalName)) {
                return false;
            }
        } else {
            if (elementLocalName != null) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if node is element and has specified name.
     *
     * @param node
     *            Node to be checked.
     * @param namespaceURI
     *            Wanted namespace URI.
     * @param localName
     *            Wanted local name.
     *
     * @return True if node is an element and has specified name, false otherwise.
     */
    public static boolean checkElementName(final Node node, final String namespaceURI, final String localName) {
        if (!(node instanceof Element)) {
            return false;
        }
        return checkElementName((Element) node, namespaceURI, localName);
    }

    /**
     * Finds first child element with given name.
     *
     * <p>
     * This function is non-recursive.
     * </p>
     *
     * @param parentElement
     *            Parent element to search in.
     * @param namespaceURI
     *            Namespace URI of the element to find.
     * @param localName
     *            Local name of the element to find.
     *
     * @return Element node if element was found, null otherwise.
     */
    public static Element findFirstChildElement(final Element parentElement, final String namespaceURI,
            final String localName) {
        Node childNode;

        for (childNode = parentElement.getFirstChild(); childNode != null; childNode = childNode.getNextSibling()) {
            if (childNode instanceof Element) {
                Element childElement = (Element) childNode;
                if (checkElementName(childElement, namespaceURI, localName)) {
                    return childElement;
                }
            }
        }

        return null;
    }

    /**
     * Creates iterator over child elements.
     *
     * @param element
     *            Parent element to iterate children for.
     *
     * @return Iterator over child elements.
     */
    public static Iterator<Element> createChildElementIterator(final Element element) {
        return new ChildElementIterator(element);
    }

    public static Iterator<Element> createChildElementIterator(final Element element, final String namespaceURI,
            final String localName) {
        return new NameFiltererdChildElementIterator(element, namespaceURI, localName);
    }

    public static Element findUniqueChildElement(final Element parentElement, final String namespaceURI,
            final String localName) throws UpnpException {
        Element foundElement = null;

        Iterator<Element> iterator = createChildElementIterator(parentElement, namespaceURI, localName);
        while (iterator.hasNext()) {
            Element childElement = iterator.next();
            if (foundElement == null) {
                foundElement = childElement;
            } else {
                throw new UpnpException("Element is not unique: " + namespaceURI + ":" + localName);
            }
        }

        return foundElement;
    }

    public static String getUniqueChildElementValue(final Element parentElement, final String namespaceURI,
            final String localName) throws UpnpException {
        Element uniqueElement = findUniqueChildElement(parentElement, namespaceURI, localName);
        if (uniqueElement != null) {
            return getElementValue(uniqueElement);
        } else {
            return null;
        }
    }
}

/**
 * Iterator over child elements.
 */
class ChildElementIterator implements Iterator<Element> {

    /** Parent element. */
    private Element parentElement = null;
    /** Next element to be returned. */
    private Element nextElement = null;

    /**
     * Constructs new iterator over child elements.
     *
     * @param parentElement
     *            Parent of iterated child elements.
     */
    public ChildElementIterator(final Element parentElement) {
        this.parentElement = parentElement;
    }

    private final Element computeNext(final Node start) {
        for (Node node = start; node != null; node = node.getNextSibling()) {
            if (node instanceof Element) {
                Element element = (Element) node;
                if (checkElement(element)) {
                    return element;
                }
            }
        }
        return null;
    }

    protected boolean checkElement(final Element element) {
        return true;
    }

    @Override
    public boolean hasNext() {
        /* lazy initialization */
        if (this.parentElement != null) {
            this.nextElement = computeNext(this.parentElement.getFirstChild());
            this.parentElement = null;
        }

        return this.nextElement != null;
    }

    @Override
    public Element next() {
        /* hasNext must be first operation (lazy initialization)! */
        if (!hasNext()) {
            throw new NoSuchElementException();
        }

        Element result = this.nextElement;
        this.nextElement = computeNext(result.getNextSibling());
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}

/**
 * Iterator over child elements with name filtering.
 */
class NameFiltererdChildElementIterator extends ChildElementIterator {

    private final String namespaceURI;
    private final String localName;

    public NameFiltererdChildElementIterator(final Element parentElement, final String namespaceURI,
            final String localName) {
        super(parentElement);
        this.namespaceURI = namespaceURI;
        this.localName = localName;
    }

    @Override
    protected boolean checkElement(final Element element) {
        return super.checkElement(element) && XMLTools.checkElementName(element, this.namespaceURI, this.localName);
    }
}
