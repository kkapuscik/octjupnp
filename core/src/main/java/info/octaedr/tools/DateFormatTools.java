/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.tools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateFormatTools {

    private static SimpleDateFormat httpDateFormat;

    private static synchronized DateFormat getDateFormat() {
        if (httpDateFormat == null) {
            httpDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss 'GMT'");
            httpDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        }
        return httpDateFormat;
    }

    public static String dateToString(final Date date) {
        DateFormat df = getDateFormat();
        return df.format(date);
    }

}
