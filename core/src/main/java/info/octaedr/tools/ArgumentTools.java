/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.tools;

/**
 * Method arguments utilities.
 *
 * @author Krzysztof Kapuscik
 */
public class ArgumentTools {

    /**
     * Checks if parameter is null.
     *
     * @param argument
     *            Argument to be checked.
     * @param name
     *            Name of the argument.
     *
     * @throws NullPointerException
     *             if any null was detected.
     */
    public static void checkForNull(Object argument, String name) {
        if (argument == null) {
            throw new NullPointerException("Null " + name);
        }
    }

    /**
     * Checks if array is null or there is any null element inside.
     *
     * @param array
     *            Array to be checked.
     * @param name
     *            Name of the array to check.
     *
     * @throws NullPointerException
     *             if any null was detected.
     */
    public static void checkForNull(Object[] array, String name) {
        if (array == null) {
            throw new NullPointerException("Null " + name + " array");
        }
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == null) {
                throw new NullPointerException("Null " + name + " array element " + i);
            }
        }
    }

}
