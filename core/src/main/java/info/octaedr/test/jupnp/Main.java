/*------------------------------------------------------------------------------
- This file is part of OCTjupnp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.test.jupnp;

import info.octaedr.jcommons.logging.LogManager;
import info.octaedr.httpudp.HttpUdpClient;
import info.octaedr.httpudp.HttpUdpMessage;
import info.octaedr.httpudp.HttpUdpSocket;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointFactory;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpRequest;

@SuppressWarnings("javadoc")
public class Main {

    // private static final int SSDP_PORT = 1900;

    public static void main(String[] args) {
        try {
            LogManager.init("info/octaedr/jupnp/logging.properties");

            ControlPointFactory factory = ControlPointFactory.getInstance();
            ControlPoint controlPoint = factory.createControlPoint();
            controlPoint.start();

            for (;;) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void main2(String[] args) {
        // SsdpClient ssdpClient = new SsdpClient();
        // SsdpMonitor monitor = new SsdpMonitor(ssdpClient);
        //
        // monitor.start();
        // monitor.sendSearch();
        //
        // for (;;) {
        // try {
        // Thread.sleep(10000);
        // } catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // }
    }

    public static void main1(String[] args) {
        try {
            DatagramSocket socket = new DatagramSocket();

            // MulticastSocket socket = new MulticastSocket(SSDP_PORT);
            // socket.joinGroup(InetAddress.getByName("239.255.255.250"));
            // socket.setLoopbackMode(true);
            // socket.setTimeToLive(4);

            HttpUdpClient httpUdpClient = new HttpUdpClient();
            HttpUdpSocket httpUdpSocket = new HttpUdpSocket(httpUdpClient, socket);

            /*
             * M-SEARCH * HTTP/1.1 HOST: 239.255.255.250:1900 MAN: "ssdp:discover" MX: seconds to delay response ST:
             * search target USER-AGENT: OS/version UPnP/1.1 product/version
             */

            try {
                HttpRequest request = new BasicHttpRequest("M-SEARCH", "*", HttpVersion.HTTP_1_1);
                request.setHeader("HOST", "239.255.255.250:1900");
                request.setHeader("MAN", "\"ssdp:discover\"");
                request.setHeader("MX", "5");
                request.setHeader("ST", "ssdp:all");
                request.setHeader("USER-AGENT", "Linux/3.0 UPnP/1.0 OCTjupnp/1.0");

                HttpUdpMessage message = new HttpUdpMessage(request, InetAddress.getByName("239.255.255.250"), 1900);

                httpUdpSocket.sendMessage(message);

                System.out.println("Sent: method=" + request.getRequestLine().getMethod());
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (;;) {
                try {
                    HttpUdpMessage message = httpUdpSocket.receiveMessage();

                    if (message instanceof HttpRequest) {
                        HttpRequest request = (HttpRequest) message;
                        System.out.println("Request: method=" + request.getRequestLine().getMethod());
                    } else if (message instanceof HttpResponse) {
                        HttpResponse response = (HttpResponse) message;
                        System.out.println("Response: status=" + response.getStatusLine().getStatusCode());
                    } else {
                        System.out.println("Invalid: status=" + message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
