/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.MainFrame;
import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jupnp.app.genericcp.data.ActionArgumentInfoNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteActionNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteDeviceNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteIconNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteServiceNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteStateVariableNode;
import info.octaedr.jupnp.app.genericcp.engine.GenericCPEngine;
import info.octaedr.jupnp.core.DeviceProperty;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.DeviceIcon;
import info.octaedr.jupnp.cp.Service;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * Main frame of GenericCP application.
 */
public class GenericCPFrame extends MainFrame {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 1010331641960231760L;

    private final GenericCPEngine engine;

    private SwingAction actionAppExit;

    private SwingAction actionAppAbout;

    private SwingAction actionRescanNetwork;

    private SwingAction actionShowIcon;

    private SwingAction actionExecuteAction;

    private SwingAction actionShowServiceDescription;

    private SwingAction actionShowDeviceDescription;

    private SwingAction actionShowDevicePresentationPage;

    private ElementTree elementTree;

    private PropertyTable propertyTable;

    private EventTable eventTable;

    private JPopupMenu popupMenuIcon;

    private JPopupMenu popupMenuAction;

    private JPopupMenu popupMenuDevice;

    private JPopupMenu popupMenuService;

    public GenericCPFrame(final GenericCPEngine engine, final AboutData aboutData) {
        super(aboutData);

        this.engine = engine;

        createActions();
        createMenu();
        createPopupMenus();
        createComponents();
        setupListeners();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(final WindowEvent e) {
                engine.terminate();
            }
        });
    }

    private void createPopupMenus() {
        this.popupMenuIcon = new JPopupMenu("Icon Menu");
        this.popupMenuIcon.add(this.actionShowIcon);

        this.popupMenuAction = new JPopupMenu("Action Menu");
        this.popupMenuAction.add(this.actionExecuteAction);

        this.popupMenuService = new JPopupMenu("Service Menu");
        this.popupMenuService.add(this.actionShowServiceDescription);

        this.popupMenuDevice = new JPopupMenu("Device Menu");
        this.popupMenuDevice.add(this.actionShowDeviceDescription);
        this.popupMenuDevice.add(this.actionShowDevicePresentationPage);
    }

    private void setupListeners() {
        this.elementTree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(final TreeSelectionEvent e) {
                TreeNode selectedNode = null;

                final TreePath path = e.getPath();
                if (path != null) {
                    if (path.getLastPathComponent() instanceof TreeNode) {
                        selectedNode = (TreeNode) path.getLastPathComponent();
                    }
                }

                GenericCPFrame.this.engine.setPropertyNode(selectedNode);
            }

        });

        this.elementTree.addPopupTriggerListener(new PopupTriggerListener() {
            @Override
            public void popupTriggered(final PopupTriggerEvent event) {
                final Object selectedObject = event.getSelectedObject();

                if (selectedObject instanceof RemoteActionNode) {

                    final RemoteActionNode treeNode = (RemoteActionNode) selectedObject;
                    final info.octaedr.jupnp.cp.Action remoteAction = treeNode.getRemoteAction();
                    triggerActionPopupMenu(event.getComponent(), event.getX(), event.getY(),
                            remoteAction);

                } else if (selectedObject instanceof ActionArgumentInfoNode) {

                    final ActionArgumentInfoNode treeNode = (ActionArgumentInfoNode) selectedObject;
                    final Action remoteAction = treeNode.getActionArgumentInfo().getAction();
                    triggerActionPopupMenu(event.getComponent(), event.getX(), event.getY(),
                            remoteAction);

                } else if (selectedObject instanceof RemoteIconNode) {

                    final RemoteIconNode treeNode = (RemoteIconNode) selectedObject;
                    final DeviceIcon iconInfo = treeNode.getIconInfo();
                    triggerIconPopupMenu(event.getComponent(), event.getX(), event.getY(), iconInfo);

                } else if (selectedObject instanceof RemoteDeviceNode) {

                    final RemoteDeviceNode treeNode = (RemoteDeviceNode) selectedObject;
                    final Device remoteDevice = treeNode.getRemoteDevice();
                    triggerDevicePopupMenu(event.getComponent(), event.getX(), event.getY(),
                            remoteDevice);

                } else if (selectedObject instanceof RemoteServiceNode) {

                    final RemoteServiceNode treeNode = (RemoteServiceNode) selectedObject;
                    final Service serviceInfo = treeNode.getServiceInfo();
                    triggerServicePopupMenu(event.getComponent(), event.getX(), event.getY(),
                            serviceInfo);

                } else if (selectedObject instanceof RemoteStateVariableNode) {
                    // TODO
                }
            }

        });
    }

    private void triggerDevicePopupMenu(final Component component, final int x, final int y,
            final Device RemoteDevice) {
        /* init */
        this.actionShowDeviceDescription.setUserObject(RemoteDevice);
        this.actionShowDevicePresentationPage.setUserObject(RemoteDevice);

        /* set status */
        this.actionShowDevicePresentationPage.setEnabled(RemoteDevice
                .getProperty(DeviceProperty.PRESENTATION_URL) != null);

        /* show popup */
        this.popupMenuDevice.show(component, x, y);
    }

    private void triggerServicePopupMenu(final Component component, final int x, final int y,
            final Service serviceInfo) {
        /* init */
        this.actionShowServiceDescription.setUserObject(serviceInfo);

        /* show popup */
        this.popupMenuService.show(component, x, y);
    }

    private void triggerIconPopupMenu(final Component component, final int x, final int y,
            final DeviceIcon iconInfo) {
        this.actionShowIcon.setUserObject(iconInfo);
        this.popupMenuIcon.show(component, x, y);
    }

    private void triggerActionPopupMenu(final Component component, final int x, final int y,
            final info.octaedr.jupnp.cp.Action remoteAction) {
        this.actionExecuteAction.setUserObject(remoteAction);
        this.popupMenuAction.show(component, x, y);
    }

    private void createComponents() {
        /* create functional components */
        this.elementTree = new ElementTree(this.engine.getElementTreeModel());
        this.propertyTable = new PropertyTable(this.engine.getPropertyTableModel());
        this.eventTable = new EventTable(this.engine.getEventTableModel());

        /* build data split pane */
        final JSplitPane dataSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(
                this.propertyTable), new JScrollPane(this.eventTable));
        dataSplitPane.setDividerLocation(0.70);
        dataSplitPane.setResizeWeight(0.5);

        /* build main split pane */
        final JSplitPane mainSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                new JScrollPane(this.elementTree), dataSplitPane);
        mainSplitPane.setDividerLocation(0.3);
        mainSplitPane.setResizeWeight(0.5);
        getContentPane().add(mainSplitPane);
    }

    private void createActions() {
        this.actionAppExit = new SwingAction();
        this.actionAppExit.setName("Exit");

        this.actionAppAbout = new SwingAction();
        this.actionAppAbout.setName("About");

        this.actionRescanNetwork = new SwingAction();
        this.actionRescanNetwork.setName("Rescan Network");

        this.actionShowIcon = new SwingAction();
        this.actionShowIcon.setName("Show Icon");

        this.actionExecuteAction = new SwingAction();
        this.actionExecuteAction.setName("Execute Action");

        this.actionShowServiceDescription = new SwingAction();
        this.actionShowServiceDescription.setName("Show Description");

        this.actionShowDeviceDescription = new SwingAction();
        this.actionShowDeviceDescription.setName("Show Description");

        this.actionShowDevicePresentationPage = new SwingAction();
        this.actionShowDevicePresentationPage.setName("Show Presentation Page");

        this.actionAppExit.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                GenericCPFrame.this.dispose();
            }
        });

        this.actionAppAbout.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                GenericCPFrame.this.showAboutDialog();
            }
        });

        this.actionRescanNetwork.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                GenericCPFrame.this.engine.rescanNetwork();
            }
        });

        this.actionShowIcon.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final DeviceIcon iconInfo = (DeviceIcon) GenericCPFrame.this.actionShowIcon
                        .getUserObject();
                final IconDialog iconDialog = new IconDialog(GenericCPFrame.this, iconInfo);
                iconDialog.setVisible(true);
            }
        });

        this.actionExecuteAction.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final Action remoteAction = (Action) GenericCPFrame.this.actionExecuteAction
                        .getUserObject();

                final ExecuteActionDialog executeActionDialog = new ExecuteActionDialog(
                        GenericCPFrame.this, remoteAction);
                executeActionDialog.setVisible(true);
            }
        });

        this.actionShowServiceDescription.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final Service serviceInfo = (Service) GenericCPFrame.this.actionShowServiceDescription
                        .getUserObject();
                final DescriptionDialog descriptionDialog = new DescriptionDialog(
                        GenericCPFrame.this, serviceInfo);
                descriptionDialog.setVisible(true);
            }
        });

        this.actionShowDeviceDescription.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final Device RemoteDevice = (Device) GenericCPFrame.this.actionShowDeviceDescription
                        .getUserObject();
                final DescriptionDialog descriptionDialog = new DescriptionDialog(
                        GenericCPFrame.this, RemoteDevice);
                descriptionDialog.setVisible(true);
            }
        });

        this.actionShowDevicePresentationPage.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final Device RemoteDevice = (Device) GenericCPFrame.this.actionShowDevicePresentationPage
                        .getUserObject();
                final PresentationPageDialog dialog = new PresentationPageDialog(
                        GenericCPFrame.this, RemoteDevice);
                dialog.setVisible(true);
            }
        });

    }

    private void createMenu() {
        /* create file menu */
        final JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(this.actionRescanNetwork);
        fileMenu.addSeparator();
        fileMenu.add(this.actionAppExit);

        /* create help menu */
        final JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        helpMenu.add(this.actionAppAbout);

        /* build menu bar */
        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        /* set menu bar */
        setJMenuBar(menuBar);
    }

}
