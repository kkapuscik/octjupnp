/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Base class for element tree nodes.
 */
public abstract class ElementTreeNode extends DefaultMutableTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -20294093333294283L;

    /** Array of properties. */
    private PropertyArray propertyArray;
    /** Flag indicating that properties have been prepared. */
    private boolean propertiesReady;

    /**
     * Constructs new node.
     *
     * @param userObject
     *            User defined context object.
     */
    public ElementTreeNode(final Object userObject) {
        super(userObject);
        this.propertiesReady = false;
    }

    /**
     * Returns collection of properties describing the object.
     *
     * @return Collection of properties describing the object or null if there are no properties available.
     */
    public PropertyArray getProperties() {
        if (!this.propertiesReady) {
            this.propertyArray = buildProperties();
            this.propertiesReady = true;
        }

        return this.propertyArray;
    }

    /**
     * Builds collection of properties describing the object.
     *
     * @return Created collection of properties or null if there are no properties available.
     */
    protected abstract PropertyArray buildProperties();

}
