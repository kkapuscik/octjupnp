/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;

public class EventTable extends JXTable {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 7471755330385637671L;

    EventTable(final TableModel model) {
        super(model);
    }

}
