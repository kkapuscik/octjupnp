/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp;

import info.octaedr.jcommons.logging.LogManager;
import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.app.genericcp.engine.GenericCPEngine;
import info.octaedr.jupnp.app.genericcp.ui.GenericCPFrame;
import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.AboutPerson;

/**
 * Generic control point application class.
 */
public class GenericCP {

    private static Logger logger = Logger.getLogger(GenericCP.class);

    /**
     * Main function.
     *
     * @param args
     *            Execution arguments.
     */
    public static void main(String[] args) {
        try {
            LogManager.init();

            /* prepare about data */
            AboutData aboutData = new AboutData("GenericCP", "0.1");
            aboutData.addAuthor(new AboutPerson("Krzysztof Kapuscik", "Author", "k.kapuscik@gmail.com"));

            /* create engine */
            GenericCPEngine engine = new GenericCPEngine();

            /* create main frame */
            GenericCPFrame frame = new GenericCPFrame(engine, aboutData);

            /* show main frame */
            frame.setVisible(true);
        } catch (Throwable t) {
            if (logger.severe()) {
                logger.severe("Exception catched: " + t);
            }
        }
    }

}
