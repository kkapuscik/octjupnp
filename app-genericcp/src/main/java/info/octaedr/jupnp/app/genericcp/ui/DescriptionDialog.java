/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.Service;

public class DescriptionDialog extends JDialog {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 148457842743024944L;

    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 300;

    public DescriptionDialog(final JFrame frame, Service serviceInfo) {
        super(frame, "Service Description", false);

        initDialog(frame, serviceInfo.getSCPDURL());
    }

    public DescriptionDialog(final JFrame frame, Device RemoteDevice) {
        super(frame, "Device Description", false);

        initDialog(frame, RemoteDevice.getDescriptionLocation());
    }

    private void initDialog(final JFrame frame, final URL descriptionLocation) {
        final JTextArea textArea = new JTextArea();
        textArea.setEditable(false);

        getContentPane().add(new JScrollPane(textArea), BorderLayout.CENTER);

        /* set size & position */
        setLocationRelativeTo(frame);
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        // TODO: use EventQueue!!

        /* load document */
        new Thread() {
            @Override
            public void run() {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(descriptionLocation.openStream(),
                            "UTF-8"));
                    for (;;) {
                        String line = reader.readLine();
                        if (line == null) {
                            break;
                        }

                        textArea.append(line);
                        textArea.append("\n");
                    }
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(DescriptionDialog.this, "Cannot load description document:\n"
                            + descriptionLocation.toExternalForm(), "Description Dialog", JOptionPane.WARNING_MESSAGE);
                }
            }
        }.start();
    }

}
