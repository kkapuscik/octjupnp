/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import info.octaedr.jupnp.cp.DeviceIcon;

/**
 * Icon information node.
 */
public class RemoteIconNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -6397716713387239502L;

    /**
     * Constructs new node.
     *
     * @param iconInfo
     *            Icon to be represented.
     */
    public RemoteIconNode(final DeviceIcon iconInfo) {
        super(iconInfo);
    }

    /**
     * Returns icon being represented.
     *
     * @return Icon given to constructor.
     */
    public DeviceIcon getIconInfo() {
        return (DeviceIcon) getUserObject();
    }

    @Override
    public String toString() {
        DeviceIcon iconInfo = getIconInfo();

        return iconInfo.getMIMEType() + " " + iconInfo.getWidth() + "x" + iconInfo.getHeight() + "x"
                + iconInfo.getDepth();
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();

        DeviceIcon iconInfo = getIconInfo();

        propertyArray.addProperty("MIME type", iconInfo.getMIMEType());
        propertyArray.addProperty("Width", iconInfo.getWidth());
        propertyArray.addProperty("Height", iconInfo.getHeight());
        propertyArray.addProperty("Depth", iconInfo.getDepth());
        propertyArray.addProperty("URL", iconInfo.getURL());

        return propertyArray;
    }

}
