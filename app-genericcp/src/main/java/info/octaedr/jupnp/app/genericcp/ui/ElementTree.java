/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.event.EventListenerList;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.jdesktop.swingx.JXTree;

public class ElementTree extends JXTree {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 9019609308427330470L;

    private EventListenerList popupTriggerListeners = new EventListenerList();

    public ElementTree(final TreeModel model) {
        super(model);
        expandAll();

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                processPopup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                processPopup(e);
            }

            private void processPopup(MouseEvent e) {
                if (!e.isPopupTrigger()) {
                    return;
                }

                TreePath path = ElementTree.this.getPathForLocation(e.getX(), e.getY());
                if (path != null && path.getPathCount() > 0) {
                    ElementTree.this.setSelectionPath(path);
                    TreeNode lastComponent = (TreeNode) path.getLastPathComponent();
                    firePopupTriggerEvent(e.getX(), e.getY(), lastComponent);
                }
            }
        });
    }

    public void addPopupTriggerListener(final PopupTriggerListener listener) {
        this.popupTriggerListeners.add(PopupTriggerListener.class, listener);
    }

    public void removePopupTriggerListener(final PopupTriggerListener listener) {
        this.popupTriggerListeners.remove(PopupTriggerListener.class, listener);
    }

    protected void firePopupTriggerEvent(final int x, final int y, final TreeNode selectedNode) {
        // Guaranteed to return a non-null array
        Object[] listeners = this.popupTriggerListeners.getListenerList();

        // Process the listeners last to first, notifying
        // those that are interested in this event
        PopupTriggerEvent event = new PopupTriggerEvent(this, x, y, selectedNode);
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == PopupTriggerListener.class) {
                ((PopupTriggerListener) listeners[i + 1]).popupTriggered(event);
            }
        }
    }

}
