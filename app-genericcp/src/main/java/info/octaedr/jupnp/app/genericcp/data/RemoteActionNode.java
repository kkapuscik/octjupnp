/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import info.octaedr.jupnp.cp.Action;

public class RemoteActionNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 3415015405154422687L;

    public RemoteActionNode(final Action remoteAction) {
        super(remoteAction);
    }

    public Action getRemoteAction() {
        return (Action) getUserObject();
    }

    @Override
    public String toString() {
        return getRemoteAction().getName();
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();

        Action remoteAction = getRemoteAction();

        propertyArray.addProperty("Name", remoteAction.getName());
        propertyArray.addProperty("Total Arguments", remoteAction.getArguments().size());
        propertyArray.addProperty("Input Arguments", remoteAction.getInputArguments().size());
        propertyArray.addProperty("Output Arguments", remoteAction.getOutputArguments().size());

        return propertyArray;
    }

}
