/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import info.octaedr.jupnp.cp.Service;

/**
 * Service information node.
 */
public class RemoteServiceNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -2453128909967071959L;

    /**
     * Constructs new node.
     *
     * @param serviceInfo
     *            Service to be represented.
     */
    public RemoteServiceNode(final Service serviceInfo) {
        super(serviceInfo);
    }

    /**
     * Returns service being represented.
     *
     * @return Service given to constructor.
     */
    public Service getServiceInfo() {
        return (Service) getUserObject();
    }

    @Override
    public String toString() {
        return getServiceInfo().getServiceID();
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();

        Service serviceInfo = getServiceInfo();
        propertyArray.addProperty("Service ID", serviceInfo.getServiceID());
        propertyArray.addProperty("Service Type", serviceInfo.getServiceType());
        propertyArray.addProperty("Description URL", serviceInfo.getSCPDURL());
        propertyArray.addProperty("Control URL", serviceInfo.getControlURL());
        propertyArray.addProperty("Event URL", serviceInfo.getEventSubURL());
        propertyArray.addProperty("Actions Count", serviceInfo.getActions().size());
        propertyArray.addProperty("State Vvariables Count", serviceInfo.getStateVariables().size());

        return propertyArray;
    }

}
