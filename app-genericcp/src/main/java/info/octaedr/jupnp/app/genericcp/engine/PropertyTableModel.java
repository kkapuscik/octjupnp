/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.engine;

import javax.swing.table.AbstractTableModel;

import info.octaedr.jupnp.app.genericcp.data.PropertyArray;

public class PropertyTableModel extends AbstractTableModel {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 8096445628246378269L;

    private static final String[] COLUMN_NAMES = { "Name", "Value" };

    private PropertyArray properties;

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public int getRowCount() {
        if (this.properties != null) {
            return this.properties.getSize();
        } else {
            return 0;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.properties != null) {
            switch (columnIndex) {
                case 0:
                    return this.properties.getKey(rowIndex);
                case 1:
                    return this.properties.getValue(rowIndex);
            }
        }
        return null;
    }

    public void setPropertiesData(final PropertyArray properties) {
        this.properties = properties;
        fireTableDataChanged();
    }

}
