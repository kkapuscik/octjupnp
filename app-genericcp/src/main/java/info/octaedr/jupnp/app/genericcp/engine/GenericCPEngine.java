/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.engine;

import java.awt.EventQueue;

import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import info.octaedr.jupnp.app.genericcp.data.ElementTreeNode;
import info.octaedr.jupnp.app.genericcp.data.PropertyArray;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointEvent;
import info.octaedr.jupnp.cp.ControlPointFactory;
import info.octaedr.jupnp.cp.ControlPointListener;
import info.octaedr.jupnp.cp.Device;

public class GenericCPEngine {

    private final ControlPoint controlPoint;
    private final ElementTreeModel elementTreeModel;
    private final PropertyTableModel propertyTableModel;

    public GenericCPEngine() {
        this.controlPoint = createControlPoint();
        this.elementTreeModel = new ElementTreeModel();
        this.propertyTableModel = new PropertyTableModel();

        this.controlPoint.start();
    }

    private ControlPoint createControlPoint() {
        ControlPoint cp = ControlPointFactory.getInstance().createControlPoint();

        cp.addControlPointListener(new ControlPointListener() {

            @Override
            public void rootDeviceAdded(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        GenericCPEngine.this.deviceAdded(event);
                    }
                });
            }

            @Override
            public void rootDeviceRemoved(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        GenericCPEngine.this.deviceRemoved(event);
                    }
                });
            }

            @Override
            public void deviceAdded(ControlPointEvent event) {
                // ignore
            }

            @Override
            public void deviceRemoved(ControlPointEvent event) {
                // ignore
            }

        });

        return cp;
    }

    private void deviceAdded(final ControlPointEvent event) {
        Device device = event.getDevice();
        this.elementTreeModel.deviceAdded(device);
    }

    private void deviceRemoved(final ControlPointEvent event) {
        Device device = event.getDevice();
        this.elementTreeModel.deviceRemoved(device);
    }

    public void terminate() {
        this.controlPoint.stop();
    }

    public TreeModel getElementTreeModel() {
        return this.elementTreeModel;
    }

    public TableModel getPropertyTableModel() {
        return this.propertyTableModel;
    }

    public TableModel getEventTableModel() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setPropertyNode(final TreeNode selectedNode) {
        PropertyArray properties = null;

        if (selectedNode instanceof ElementTreeNode) {
            ElementTreeNode elementNode = (ElementTreeNode) selectedNode;
            properties = elementNode.getProperties();
        }

        this.propertyTableModel.setPropertiesData(properties);
    }

    public void rescanNetwork() {
        this.controlPoint.rescanNetwork();
    }

}
