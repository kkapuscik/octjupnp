/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JDialog;

import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.ImagePainter;
import info.octaedr.jupnp.cp.DeviceIcon;

public class IconDialog extends JDialog {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -3112273081731544712L;

    private static final int DEFAULT_WIDTH = 200;
    private static final int DEFAULT_HEIGHT = 200;

    public IconDialog(Frame frame, DeviceIcon iconInfo) {
        super(frame, "Icon", false);

        // TODO: property components?

        /* create contents */
        ImagePainter painter = new ImagePainter();
        JXPanel imagePanel = new JXPanel();
        imagePanel.setBackgroundPainter(painter);

        try {
            painter.setImage(ImageIO.read(iconInfo.getURL()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        imagePanel.setPreferredSize(new Dimension(iconInfo.getWidth(), iconInfo.getHeight()));
        getContentPane().add(imagePanel, BorderLayout.CENTER);

        /* set size & position */
        setLocationRelativeTo(frame);
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

}
