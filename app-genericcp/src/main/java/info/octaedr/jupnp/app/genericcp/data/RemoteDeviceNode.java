/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import java.util.Iterator;

import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.core.DeviceProperty;

/**
 * Device information node.
 */
public class RemoteDeviceNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -6296114312007616563L;

    /**
     * Constructs new node.
     *
     * @param RemoteDevice
     *            Device to be represented.
     */
    public RemoteDeviceNode(final Device RemoteDevice) {
        super(RemoteDevice);
    }

    /**
     * Returns device being represented.
     *
     * @return Device given to constructor.
     */
    public Device getRemoteDevice() {
        return (Device) getUserObject();
    }

    @Override
    public String toString() {
        return getRemoteDevice().getProperty(DeviceProperty.FRIENDLY_NAME);
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();

        Device RemoteDevice = getRemoteDevice();

        propertyArray.addProperty("Descrition URL", RemoteDevice.getDescriptionLocation());
        propertyArray.addProperty("Base URL", RemoteDevice.getBaseURL());
        propertyArray.addProperty("Service Count", RemoteDevice.getServices().size());
        Iterator<String> keysIterator = RemoteDevice.getPropertyKeys();
        while (keysIterator.hasNext()) {
            String key = keysIterator.next();
            String localKey = DeviceProperty.getPropertyLocalName(key);
            String value = RemoteDevice.getProperty(key);

            propertyArray.addProperty(localKey, value);
        }

        return propertyArray;
    }

}
