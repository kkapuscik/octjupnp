/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.StateVariable;

public class ActionArgumentInfoNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 6873770848937922181L;

    public ActionArgumentInfoNode(final ActionArgument argumentInfo) {
        super(argumentInfo);
    }

    public ActionArgument getActionArgumentInfo() {
        return (ActionArgument) getUserObject();
    }

    @Override
    public String toString() {
        return getActionArgumentInfo().getName();
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();

        ActionArgument argumentInfo = getActionArgumentInfo();
        StateVariable stateVariable = argumentInfo.getRelatedStateVariable();

        propertyArray.addProperty("Name", argumentInfo.getName());
        propertyArray.addProperty("Direction", argumentInfo.getDirection());
        propertyArray.addProperty("Is Return Value", argumentInfo.isReturnValue());
        propertyArray.addProperty("Related State Variable", stateVariable.getName());
        propertyArray.addProperty("Data type", stateVariable.getDataType().getName());
        if (stateVariable.getAllowedRangeMinimum() != null) {
            propertyArray.addProperty("Allowed minimum", stateVariable.getAllowedRangeMinimum().toString());
        }
        if (stateVariable.getAllowedRangeMaximum() != null) {
            propertyArray.addProperty("Allowed maximum", stateVariable.getAllowedRangeMaximum().toString());
        }
        if (stateVariable.getAllowedRangeStep() != null) {
            propertyArray.addProperty("Allowed step", stateVariable.getAllowedRangeStep().toString());
        }
        for (String allowedValue : stateVariable.getAllowedValues()) {
            propertyArray.addProperty("Allowed value", allowedValue);
        }

        return propertyArray;
    }

}
