/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

/**
 * Node used for grouping other elements.
 */
public class GroupNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 1767489154842485896L;

    /**
     * Constructs new node.
     *
     * @param groupName
     *            Name of the group.
     */
    public GroupNode(final String groupName) {
        super(groupName);
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();
        propertyArray.addProperty("Child Count", getChildCount());
        return propertyArray;
    }

}
