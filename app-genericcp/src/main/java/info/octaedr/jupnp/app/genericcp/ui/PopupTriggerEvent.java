/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import java.awt.Component;
import java.util.EventObject;

public class PopupTriggerEvent extends EventObject {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -762625007656997631L;

    private final int x;
    private final int y;
    private final Object selectedObject;

    public PopupTriggerEvent(final Component source, final int x, final int y, final Object selectedObject) {
        super(source);

        this.x = x;
        this.y = y;
        this.selectedObject = selectedObject;
    }

    public Component getComponent() {
        return (Component) getSource();
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Object getSelectedObject() {
        return this.selectedObject;
    }

}
