/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.jdesktop.swingx.JXEditorPane;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.core.DeviceProperty;

public class PresentationPageDialog extends JDialog {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 3807374224064223827L;

    private static final int DEFAULT_WIDTH = 200;
    private static final int DEFAULT_HEIGHT = 200;

    private final JXEditorPane webPagePane;
    private final JTextField addressField;

    public PresentationPageDialog(Frame frame, Device RemoteDevice) {
        super(frame, "Presentation Page", false);

        // TODO: property components?

        /* create contents */
        this.addressField = new JTextField();
        this.addressField.setEditable(false);

        this.webPagePane = new JXEditorPane();
        this.webPagePane.setEditable(false);
        this.webPagePane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent event) {
                if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    setURL(event.getURL().toExternalForm());
                }
            }
        });

        /* make layout */
        getContentPane().add(this.addressField, BorderLayout.NORTH);
        getContentPane().add(new JScrollPane(this.webPagePane), BorderLayout.CENTER);

        /* set size & position */
        setLocationRelativeTo(frame);
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        setURL(RemoteDevice.getProperty(DeviceProperty.PRESENTATION_URL));
    }

    private void setURL(String url) {
        try {
            this.addressField.setText(url);
            this.webPagePane.setPage(url);
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(PresentationPageDialog.this, "Cannot load presentation page:\n" + url,
                    "Presentation Page Dialog", JOptionPane.WARNING_MESSAGE);
        }
    }

}
