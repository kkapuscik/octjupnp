/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import info.octaedr.jupnp.cp.StateVariable;

public class RemoteStateVariableNode extends ElementTreeNode {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 5755693383450444931L;

    public RemoteStateVariableNode(final StateVariable stateVariableInfo) {
        super(stateVariableInfo);
    }

    public StateVariable getStateVariableInfo() {
        return (StateVariable) getUserObject();
    }

    @Override
    public String toString() {
        return getStateVariableInfo().getName();
    }

    @Override
    protected PropertyArray buildProperties() {
        PropertyArray propertyArray = new PropertyArray();

        StateVariable stateVarInfo = getStateVariableInfo();
        propertyArray.addProperty("Name", stateVarInfo.getName());
        propertyArray.addProperty("Datatype", stateVarInfo.getDataType().getName());
        if (stateVarInfo.getDefaultValue() != null) {
            propertyArray.addProperty("Default Value", stateVarInfo.getDefaultValue().toString());
        }
        for (String allowedValue : stateVarInfo.getAllowedValues()) {
            propertyArray.addProperty("Allowed Value ", allowedValue);
        }
        if (stateVarInfo.getAllowedRangeMinimum() != null) {
            propertyArray.addProperty("Allowed Range Minimum", stateVarInfo.getAllowedRangeMinimum().toString());
        }
        if (stateVarInfo.getAllowedRangeMaximum() != null) {
            propertyArray.addProperty("Allowed Range Maximum", stateVarInfo.getAllowedRangeMaximum().toString());
        }
        if (stateVarInfo.getAllowedRangeStep() != null) {
            propertyArray.addProperty("Allowed Range Step", stateVarInfo.getAllowedRangeStep().toString());
        }

        return propertyArray;
    }

}
