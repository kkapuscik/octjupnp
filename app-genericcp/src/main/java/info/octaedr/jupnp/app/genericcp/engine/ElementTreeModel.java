/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.engine;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import info.octaedr.jupnp.app.genericcp.data.ActionArgumentInfoNode;
import info.octaedr.jupnp.app.genericcp.data.GroupNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteActionNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteDeviceNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteIconNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteServiceNode;
import info.octaedr.jupnp.app.genericcp.data.RemoteStateVariableNode;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.DeviceIcon;
import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.StateVariable;

public class ElementTreeModel extends DefaultTreeModel {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 1558113672117255949L;

    public ElementTreeModel() {
        super(new GroupNode("UPnP Devices"));
    }

    public void deviceAdded(final Device remoteDevice) {
        MutableTreeNode rootNode = getRootNode();
        RemoteDeviceNode deviceNode = findRemoteDeviceNode(remoteDevice);
        if (deviceNode == null) {
            deviceNode = new RemoteDeviceNode(remoteDevice);
            appendNodeInto(deviceNode, rootNode);

            buildRemoteDeviceTree(remoteDevice, deviceNode);
        }
    }

    public void deviceRemoved(final Device remoteDevice) {
        RemoteDeviceNode node = findRemoteDeviceNode(remoteDevice);
        if (node != null) {
            removeNodeFromParent(node);
        }
    }

    private RemoteDeviceNode findRemoteDeviceNode(final Device remoteDevice) {
        MutableTreeNode rootNode = getRootNode();
        int childCount = rootNode.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            RemoteDeviceNode child = (RemoteDeviceNode) rootNode.getChildAt(i);
            if (child.getRemoteDevice().equals(remoteDevice)) {
                return child;
            }
        }
        return null;
    }

    private MutableTreeNode getRootNode() {
        return (MutableTreeNode) getRoot();
    }

    private void buildRemoteDeviceTree(final Device remoteDevice, final RemoteDeviceNode deviceNode) {
        if (remoteDevice.getServices().size() > 0) {
            GroupNode serviceGroupNode = new GroupNode("Services");
            appendNodeInto(serviceGroupNode, deviceNode);

            for (Service service : remoteDevice.getServices()) {
                RemoteServiceNode serviceNode = new RemoteServiceNode(service);
                appendNodeInto(serviceNode, serviceGroupNode);

                buildServiceInfoTree(service, serviceNode);
            }
        }

        if (remoteDevice.getSubdevices().size() > 0) {
            GroupNode subdeviceGroupNode = new GroupNode("Subdevices");
            appendNodeInto(subdeviceGroupNode, deviceNode);

            for (Device subdevice : remoteDevice.getSubdevices()) {
                RemoteDeviceNode subdeviceNode = new RemoteDeviceNode(subdevice);
                appendNodeInto(subdeviceNode, subdeviceGroupNode);

                buildRemoteDeviceTree(subdevice, subdeviceNode);
            }
        }

        if (remoteDevice.getIcons().size() > 0) {
            GroupNode iconGroupNode = new GroupNode("Icons");
            appendNodeInto(iconGroupNode, deviceNode);

            for (DeviceIcon icon : remoteDevice.getIcons()) {
                RemoteIconNode iconNode = new RemoteIconNode(icon);
                appendNodeInto(iconNode, iconGroupNode);
            }
        }
    }

    private void buildServiceInfoTree(final Service serviceInfo, final RemoteServiceNode serviceNode) {
        /* add actions node */
        if (serviceInfo.getActions().size() > 0) {
            GroupNode actionsNode = new GroupNode("Actions");
            appendNodeInto(actionsNode, serviceNode);

            for (Action remoteAction : serviceInfo.getActions()) {
                RemoteActionNode RemoteActionNode = new RemoteActionNode(remoteAction);
                appendNodeInto(RemoteActionNode, actionsNode);

                for (ActionArgument argument : remoteAction.getArguments()) {
                    ActionArgumentInfoNode argumentInfoNode = new ActionArgumentInfoNode(argument);
                    appendNodeInto(argumentInfoNode, RemoteActionNode);
                }
            }
        }

        /* add state variables node */
        if (serviceInfo.getStateVariables().size() > 0) {
            GroupNode stateVarsNode = new GroupNode("State variables");
            appendNodeInto(stateVarsNode, serviceNode);

            for (StateVariable stateVariable : serviceInfo.getStateVariables()) {
                RemoteStateVariableNode stateVarInfoNode = new RemoteStateVariableNode(stateVariable);
                appendNodeInto(stateVarInfoNode, stateVarsNode);
            }
        }
    }

    protected void appendNodeInto(final MutableTreeNode newChild, final MutableTreeNode parent) {
        insertNodeInto(newChild, parent, parent.getChildCount());
    }

}
