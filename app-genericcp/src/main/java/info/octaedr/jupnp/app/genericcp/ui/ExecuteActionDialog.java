/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.ui;

import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionArgument;
import info.octaedr.jupnp.cp.ActionExecutionFailedEvent;
import info.octaedr.jupnp.cp.ActionFailedEvent;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.ActionRequestArguments;
import info.octaedr.jupnp.cp.ActionRequestListener;
import info.octaedr.jupnp.cp.ActionSucceededEvent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class ExecuteActionDialog extends JDialog {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -3408506968382794240L;

    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 300;

    private final Action actionInfo;

    private ActionRequest<ActionRequestArguments> actionRequest;
    private SwingAction actionExecute;
    private SwingAction actionCancel;

    private JTextArea statusTextArea;

    private final Map<String, JTextArea> argumentComponents = new HashMap<String, JTextArea>();

    public ExecuteActionDialog(final Frame frame, final Action actionInfo) {
        super(frame, "Execute Action: " + actionInfo.getName(), false);

        this.actionInfo = actionInfo;

        createActions();
        createComponents();

        /* create contents */
        // JXImagePanel imagePanel = new JXImagePanel(iconInfo.getURL());
        // imagePanel.setPreferredSize(new Dimension(iconInfo.getWidth(), iconInfo.getHeight()));
        // getContentPane().add(imagePanel, BorderLayout.CENTER);

        /* set size & position */
        setLocationRelativeTo(frame);
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        setStatus("Idle");
    }

    private void setStatus(final String message) {
        this.statusTextArea.setText(message);
    }

    private void createActions() {
        this.actionExecute = new SwingAction();
        this.actionExecute.setName("Execute");

        this.actionCancel = new SwingAction();
        this.actionCancel.setName("Cancel");
        this.actionCancel.setEnabled(false);

        this.actionExecute.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                executeAction();
            }
        });

        this.actionCancel.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                cancelAction();
            }
        });
    }

    private void clearAction() {
        this.actionRequest = null;
        this.actionExecute.setEnabled(true);
        this.actionCancel.setEnabled(false);
    }

    private void cancelAction() {
        if (this.actionRequest != null) {
            this.actionRequest.cancel();
            clearAction();
        }
    }

    private void executeAction() {
        try {
            this.actionRequest = this.actionInfo.createRequest();

            for (final ActionArgument argument : this.actionInfo.getInputArguments()) {
                final JTextArea textArea = this.argumentComponents.get(argument.getName());
                final String value = textArea.getText();
                this.actionRequest.getArguments().setInputArgumentValueString(argument.getName(),
                        value);
            }

            this.actionRequest.execute(new ActionRequestListener<ActionRequestArguments>() {

                @Override
                public void actionSucceeded(final ActionSucceededEvent<ActionRequestArguments> event) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            setStatus("Execution successful");
                            processActionSuccess(event);
                        }
                    });
                }

                @Override
                public void actionFailed(final ActionFailedEvent<ActionRequestArguments> event) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            setStatus("Action failed: " + event.getErrorCode() + " "
                                    + event.getErrorDescription());
                            clearAction();
                        }
                    });
                }

                @Override
                public void actionExecutionFailed(
                        final ActionExecutionFailedEvent<ActionRequestArguments> event) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            setStatus("Execution failed: " + event.getErrorDescription());
                            clearAction();
                        }
                    });
                }
            });

            this.actionExecute.setEnabled(false);
            this.actionCancel.setEnabled(true);
            setStatus("Executing action");
        } catch (final UpnpException e) {
            setStatus("Failed to execute action: " + e.getMessage());
            e.printStackTrace();
            this.actionRequest = null;
        }
    }

    private void processActionSuccess(final ActionSucceededEvent<ActionRequestArguments> event) {
        for (final ActionArgument argument : this.actionInfo.getOutputArguments()) {
            final JTextArea textArea = this.argumentComponents.get(argument.getName());

            String value = null;
            try {
                value = event.getOutputArgumentValue(argument.getName()).toString();
            } catch (final UpnpException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            textArea.setText(value);
        }

        clearAction();
    }

    private void createComponents() {
        getContentPane().setLayout(new BorderLayout(5, 5));
        getContentPane().add(createStatusComponents(), BorderLayout.NORTH);
        getContentPane().add(createArgumentComponents(), BorderLayout.CENTER);
        getContentPane().add(createExecuteComponents(), BorderLayout.SOUTH);
    }

    private Component createStatusComponents() {
        this.statusTextArea = new JTextArea();
        this.statusTextArea.setEditable(false);
        this.statusTextArea.setBackground(Color.LIGHT_GRAY);

        final JPanel panel = new JPanel(new GridLayout(0, 2, 5, 5));
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.add(new JLabel("Status"));
        panel.add(this.statusTextArea);

        return panel;
    }

    private Component createArgumentComponents() {
        final int argCount = this.actionInfo.getArguments().size();

        if (argCount == 0) {
            final JPanel panel = new JPanel(new BorderLayout());
            panel.setBorder(new EmptyBorder(5, 5, 5, 5));
            panel.add(new JLabel("There are no arguments for this action"), BorderLayout.CENTER);
            return panel;
        }

        final JPanel panel = new JPanel(new GridLayout(0, 2, 5, 5));
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));

        /* input arguments */
        for (final ActionArgument argument : this.actionInfo.getInputArguments()) {
            final String name = argument.getName() + " [" + argument.getDataType().getName() + "]";
            final JTextArea valueTextArea = new JTextArea();

            panel.add(new JLabel(name));
            panel.add(new JScrollPane(valueTextArea));

            this.argumentComponents.put(argument.getName(), valueTextArea);
        }

        /* output arguments */
        for (final ActionArgument argument : this.actionInfo.getOutputArguments()) {
            final String name = argument.getName() + " [" + argument.getDataType().getName() + "]";
            final JTextArea valueTextArea = new JTextArea();
            valueTextArea.setEditable(false);
            valueTextArea.setBackground(Color.LIGHT_GRAY);

            panel.add(new JLabel(name));
            panel.add(new JScrollPane(valueTextArea));

            this.argumentComponents.put(argument.getName(), valueTextArea);
        }

        return panel;
    }

    private Component createExecuteComponents() {
        JButton buttonExecute;
        JButton buttonCancel;

        buttonExecute = new JButton(this.actionExecute);
        buttonCancel = new JButton(this.actionCancel);

        final JPanel panel = new JPanel(new GridLayout(0, 2, 5, 5));
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.add(buttonExecute);
        panel.add(buttonCancel);

        return panel;
    }

}
