/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-genericcp project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.genericcp.data;

import java.net.URL;
import java.util.ArrayList;

/**
 * Array of element's properties.
 */
public class PropertyArray {

    /** Collection of properties. */
    private final ArrayList<String> properties = new ArrayList<String>();

    /**
     * Constructs new array.
     */
    public PropertyArray() {
        // nothing to do
    }

    /**
     * Adds new property.
     *
     * @param key
     *            Key of the property.
     * @param value
     *            Value of the property. May be null.
     */
    public void addProperty(final String key, String value) {
        if (key == null) {
            throw new IllegalArgumentException("Key may not be null");
        }
        if (value == null) {
            return;
        }

        this.properties.add(key);
        this.properties.add(value);
    }

    /**
     * Adds new URL property.
     *
     * @param key
     *            Key of the property.
     * @param url
     *            URL being the property value. May be null.
     */
    public void addProperty(final String key, final URL url) {
        addProperty(key, url != null ? url.toExternalForm() : "");
    }

    /**
     * Adds new integer property.
     *
     * @param key
     *            Key of the property.
     * @param url
     *            Value of the property.
     */
    public void addProperty(final String key, final int value) {
        addProperty(key, Integer.toString(value));
    }

    /**
     * Adds new boolean property.
     *
     * @param key
     *            Key of the property.
     * @param url
     *            Value of the property.
     */
    public void addProperty(final String key, final boolean value) {
        addProperty(key, value ? "true" : "false");
    }

    /**
     * Returns number of properties in collection.
     *
     * @return Number of properties.
     */
    public int getSize() {
        return this.properties.size() / 2;
    }

    /**
     * Returns key of property.
     *
     * @param index
     *            Index of the property.
     *
     * @return Key of the property.
     */
    public String getKey(int index) {
        return this.properties.get(index * 2 + 0);
    }

    /**
     * Returns value of property.
     *
     * @param index
     *            Index of the property.
     *
     * @return String representation of property's value.
     */
    public String getValue(int index) {
        return this.properties.get(index * 2 + 1);
    }

}
