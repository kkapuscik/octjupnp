/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Time strings formatter.
 *
 * @author Krzysztof Kapuscik
 */
public class TimeFormatter {

    /** Date formatt. */
    private static DateFormat dateFormat;

    /**
     * Returns time string.
     *
     * @param timeMillis
     *            Timestamp in milliseconds from epoch.
     *
     * @return Formatted time string.
     */
    public synchronized static String getTime(long timeMillis) {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        }

        return dateFormat.format(new Date(timeMillis));
    }

}
