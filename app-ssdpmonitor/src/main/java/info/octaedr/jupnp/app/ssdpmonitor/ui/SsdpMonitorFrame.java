/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.ui;

import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.MainFrame;
import info.octaedr.jswing.Menu;
import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jswing.ToolBar;
import info.octaedr.jupnp.app.ssdpmonitor.engine.SsdpMonitorEngine;
import info.octaedr.jupnp.app.ssdpmonitor.resources.IconSet;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Main frame (application window) of SSDP monitor.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings({ "serial", "javadoc" })
public class SsdpMonitorFrame extends MainFrame {

    private final SsdpMonitorEngine engine;

    private SwingAction actionNewCapture;

    private SwingAction actionExitApplication;

    private SwingAction actionCapturePackets;

    private SwingAction actionCaptureMulticasts;

    private SwingAction actionSearchAllDevices;

    private SwingAction actionSearchRootDevices;

    private SwingAction actionSearchMediaServer1;

    private SwingAction actionSearchMediaRenderer1;

    private SwingAction actionSearchInternetGateway1;

    private SwingAction actionSelectFilter;

    private SwingAction actionShowAbout;

    private JTable packetTable;

    private JTextArea packetDetailsTextArea;

    private JTable deviceTable;

    private IconSet iconSet;

    private final FilterDialog filterDialog;

    /**
     * Constructs new frame.
     *
     * @param engine
     *            Monitor engine instance.
     * @param aboutData
     *            Application information.
     */
    public SsdpMonitorFrame(final SsdpMonitorEngine engine, final AboutData aboutData) {
        super(aboutData);

        this.engine = engine;

        initResources();

        this.filterDialog = new FilterDialog(this, this.engine);

        createActions();
        createComponents();

        connectModels();
        connectActions();

        initState();
    }

    /**
     * Initializes resource used by frame.
     */
    private void initResources() {
        this.iconSet = new IconSet("info.octaedr.jupnp.app.ssdpmonitor.resources.icons.fatcow");

        setIconImage(this.iconSet.getSmallImage(IconSet.SSDP_MONITOR));
    }

    /**
     * Initializes the state of UI elements.
     */
    private void initState() {
        this.engine.setMulticastEnabled(true);
        this.engine.setCaptureEnabled(true);
        updateState();
    }

    /**
     * Updates state of capture related UI elements.
     */
    private void updateState() {
        final boolean isCaptureStarted = this.engine.isCaptureStarted();
        final boolean isMulticastEnabled = this.engine.isMulticastEnabled();

        this.actionCapturePackets.setSelected(isCaptureStarted);
        this.actionCaptureMulticasts.setSelected(isMulticastEnabled);

        this.actionSearchAllDevices.setEnabled(isCaptureStarted);
        this.actionSearchRootDevices.setEnabled(isCaptureStarted);
        this.actionSearchMediaServer1.setEnabled(isCaptureStarted);
        this.actionSearchMediaRenderer1.setEnabled(isCaptureStarted);
        this.actionSearchInternetGateway1.setEnabled(isCaptureStarted);
    }

    /**
     * Connects actions with elements to be executed.
     */
    private void connectActions() {
        this.actionExitApplication.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.closeRequested();
            }
        });

        this.actionNewCapture.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine.newCapture();
            }
        });

        this.actionShowAbout.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.showAboutDialog();
            }
        });

        this.actionSearchAllDevices.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine.sendSearch(SsdpMonitorEngine.SEARCH_TARGET_SSDP_ALL);
            }
        });

        this.actionSearchRootDevices.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine
                        .sendSearch(SsdpMonitorEngine.SEARCH_TARGET_UPNP_ROOTDEVICE);
            }
        });

        this.actionSearchMediaServer1.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine
                        .sendSearch(SsdpMonitorEngine.SEARCH_TARGET_MEDIA_SERVER_1);
            }
        });

        this.actionSearchMediaRenderer1.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine
                        .sendSearch(SsdpMonitorEngine.SEARCH_TARGET_MEDIA_RENDERER_1);
            }
        });

        this.actionSearchInternetGateway1.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine
                        .sendSearch(SsdpMonitorEngine.SEARCH_TARGET_INTERNET_GATEWAY_1);
            }
        });

        this.packetTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                final int index = SsdpMonitorFrame.this.packetTable.getSelectedRow();

                SsdpMonitorFrame.this.engine.setSelectedPacket(index);
            }
        });

        this.actionCapturePackets.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine
                        .setCaptureEnabled(SsdpMonitorFrame.this.actionCapturePackets.getSelected());
                SsdpMonitorFrame.this.updateState();
            }
        });

        this.actionCaptureMulticasts.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.engine
                        .setMulticastEnabled(SsdpMonitorFrame.this.actionCaptureMulticasts
                                .getSelected());
                SsdpMonitorFrame.this.updateState();
            }
        });

        this.actionSelectFilter.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                SsdpMonitorFrame.this.selectFilterRequested();
            }
        });
    }

    protected void selectFilterRequested() {
        this.filterDialog.setVisible(true);
    }

    /**
     * Processes close request.
     */
    protected void closeRequested() {
        /* stop the capture to free resources */
        this.engine.setCaptureEnabled(false);

        /* dispose the frame (stop the application) */
        dispose();
    }

    /**
     * Constructs swing actions.
     */
    private void createActions() {
        this.actionNewCapture = new SwingAction();
        this.actionNewCapture.setName("New Capture");
        this.actionNewCapture.setMnemonicKey(KeyEvent.VK_N);
        this.actionNewCapture.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                InputEvent.CTRL_MASK));
        this.actionNewCapture.setShortDescription("Starts new capture process");
        this.actionNewCapture.setSmallIcon(this.iconSet.getSmallIcon(IconSet.CLEAR_CAPTURE));

        this.actionExitApplication = new SwingAction();
        this.actionExitApplication.setName("Exit");
        this.actionExitApplication.setMnemonicKey(KeyEvent.VK_X);
        this.actionExitApplication.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
                InputEvent.ALT_MASK));
        this.actionExitApplication.setShortDescription("Exists the application");
        this.actionExitApplication.setSmallIcon(this.iconSet.getSmallIcon(IconSet.EXIT));

        this.actionCapturePackets = new SwingAction();
        this.actionCapturePackets.setName("Capture Packets");
        this.actionCapturePackets.setMnemonicKey(KeyEvent.VK_C);
        this.actionCapturePackets.setShortDescription("Toggles capture process status");
        this.actionCapturePackets.setSmallIcon(this.iconSet.getSmallIcon(IconSet.START_CAPTURE));
        this.actionCapturePackets.setSelected(Boolean.FALSE);

        this.actionCaptureMulticasts = new SwingAction();
        this.actionCaptureMulticasts.setName("Capture Multicasts");
        this.actionCaptureMulticasts.setMnemonicKey(KeyEvent.VK_M);
        this.actionCaptureMulticasts.setShortDescription("Toggles capturing multicast packets");
        this.actionCaptureMulticasts.setSmallIcon(this.iconSet.getSmallIcon(IconSet.MULTICAST));
        this.actionCaptureMulticasts.setSelected(Boolean.FALSE);

        this.actionSearchAllDevices = new SwingAction();
        this.actionSearchAllDevices.setName("Search All Devices");
        this.actionSearchAllDevices.setMnemonicKey(KeyEvent.VK_A);
        this.actionSearchAllDevices
                .setShortDescription("Searches network for new devices of any type");
        this.actionSearchAllDevices.setSmallIcon(this.iconSet.getSmallIcon(IconSet.SEARCH_ALL));

        this.actionSearchRootDevices = new SwingAction();
        this.actionSearchRootDevices.setName("Search UPnP Root Devices");
        this.actionSearchRootDevices.setMnemonicKey(KeyEvent.VK_R);
        this.actionSearchRootDevices
                .setShortDescription("Searches network for new UPnP root devices");

        this.actionSearchMediaServer1 = new SwingAction();
        this.actionSearchMediaServer1.setName("Search MediaServer:1");
        this.actionSearchMediaServer1.setMnemonicKey(KeyEvent.VK_M);
        this.actionSearchMediaServer1
                .setShortDescription("Searches network for new UPnP media server devices");

        this.actionSearchMediaRenderer1 = new SwingAction();
        this.actionSearchMediaRenderer1.setName("Search MediaRenderer:1");
        this.actionSearchMediaRenderer1.setMnemonicKey(KeyEvent.VK_M);
        this.actionSearchMediaRenderer1
                .setShortDescription("Searches network for new UPnP media renderer devices");

        this.actionSearchInternetGateway1 = new SwingAction();
        this.actionSearchInternetGateway1.setName("Search InternetGatewayDevice:1");
        this.actionSearchInternetGateway1.setMnemonicKey(KeyEvent.VK_M);
        this.actionSearchInternetGateway1
                .setShortDescription("Searches network for new UPnP media renderer devices");

        this.actionSelectFilter = new SwingAction();
        this.actionSelectFilter.setName("Select Filter");
        this.actionSelectFilter.setMnemonicKey(KeyEvent.VK_F);
        this.actionSelectFilter.setShortDescription("Selects packets filter");
        this.actionSelectFilter.setSmallIcon(this.iconSet.getSmallIcon(IconSet.SELECT_FILTER));

        this.actionShowAbout = new SwingAction();
        this.actionShowAbout.setName("About...");
        this.actionShowAbout.setMnemonicKey(KeyEvent.VK_A);
        this.actionShowAbout.setShortDescription("Shows information about the application");
        this.actionShowAbout.setSmallIcon(this.iconSet.getSmallIcon(IconSet.ABOUT));
    }

    /**
     * Creates components.
     */
    private void createComponents() {
        createMenuBar();
        final JComponent toolBarPanel = createToolBars();
        final JComponent contentPanel = createContentComponents();

        final JPanel contentPanePanel = new JPanel(new BorderLayout());
        contentPanePanel.add(toolBarPanel, BorderLayout.NORTH);
        contentPanePanel.add(contentPanel, BorderLayout.CENTER);

        getContentPane().add(contentPanePanel);
    }

    /**
     * Creates content pane elements.
     *
     * @return
     */
    private JComponent createContentComponents() {
        final Font monospaceFont = new Font("Monospaced", Font.PLAIN, 14);

        this.packetTable = new JTable();
        this.packetTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.packetTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.packetTable.setRowSelectionAllowed(true);
        this.packetTable.setFont(monospaceFont);

        this.packetDetailsTextArea = new JTextArea();
        this.packetDetailsTextArea.setEditable(false);
        this.packetDetailsTextArea.setFont(monospaceFont);

        this.deviceTable = new JTable();
        this.deviceTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.deviceTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.deviceTable.setRowSelectionAllowed(true);
        this.deviceTable.setFont(monospaceFont);

        final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.BOTTOM);
        tabbedPane.addTab("Packet Details", this.iconSet.getSmallIcon(IconSet.PACKET_DETAILS),
                new JScrollPane(this.packetDetailsTextArea));
        tabbedPane.addTab("Devices", this.iconSet.getSmallIcon(IconSet.DEVICES), new JScrollPane(
                this.deviceTable));

        final JSplitPane mainSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        mainSplitPane.setResizeWeight(0.5);
        mainSplitPane.add(new JScrollPane(this.packetTable));
        mainSplitPane.add(tabbedPane);

        return mainSplitPane;
    }

    /**
     * Connects models to UI elements.
     */
    private void connectModels() {
        this.packetTable.setModel(this.engine.getPacketTableModel());
        this.packetTable.getColumnModel().getColumn(0).setPreferredWidth(70);
        this.packetTable.getColumnModel().getColumn(1).setPreferredWidth(130);
        this.packetTable.getColumnModel().getColumn(2).setPreferredWidth(200);
        this.packetTable.getColumnModel().getColumn(3).setPreferredWidth(150);
        this.packetTable.getColumnModel().getColumn(4).setPreferredWidth(800);

        this.deviceTable.setModel(this.engine.getDeviceTableModel());
        this.deviceTable.getColumnModel().getColumn(0).setPreferredWidth(150);
        this.deviceTable.getColumnModel().getColumn(1).setPreferredWidth(130);
        this.deviceTable.getColumnModel().getColumn(2).setPreferredWidth(100);
        this.deviceTable.getColumnModel().getColumn(3).setPreferredWidth(320);
        this.deviceTable.getColumnModel().getColumn(4).setPreferredWidth(500);

        this.packetDetailsTextArea.setDocument(this.engine.getPacketDetailsDocument());
    }

    /**
     * Creates menu bar.
     */
    private void createMenuBar() {
        final Menu menuFile = new Menu("File");
        menuFile.setMnemonic(KeyEvent.VK_F);
        menuFile.add(this.actionNewCapture);
        menuFile.addSeparator();
        menuFile.add(this.actionExitApplication);

        final Menu menuCapture = new Menu("Capture");
        menuCapture.setMnemonic(KeyEvent.VK_C);
        menuCapture.add(this.actionCapturePackets);
        menuCapture.addSeparator();
        menuCapture.add(this.actionCaptureMulticasts);

        final Menu menuSearch = new Menu("Search");
        menuSearch.setMnemonic(KeyEvent.VK_S);
        menuSearch.add(this.actionSearchAllDevices);
        menuSearch.add(this.actionSearchRootDevices);
        menuSearch.addSeparator();
        menuSearch.add(this.actionSearchMediaServer1);
        menuSearch.add(this.actionSearchMediaRenderer1);
        menuSearch.add(this.actionSearchInternetGateway1);

        final Menu menuFilter = new Menu("Filter");
        menuFilter.add(this.actionSelectFilter);

        final Menu menuHelp = new Menu("Help");
        menuHelp.setMnemonic(KeyEvent.VK_H);
        menuHelp.add(this.actionShowAbout);

        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuFile);
        menuBar.add(menuCapture);
        menuBar.add(menuSearch);
        menuBar.add(menuFilter);
        menuBar.add(menuHelp);

        setJMenuBar(menuBar);
    }

    /**
     * Creates tool bars.
     *
     * @return Component holding the toolbars.
     */
    private JComponent createToolBars() {
        final ToolBar mainToolBar = new ToolBar("Main");
        mainToolBar.add(this.actionNewCapture);
        mainToolBar.addSeparator();
        mainToolBar.add(this.actionCapturePackets);
        mainToolBar.add(this.actionCaptureMulticasts);
        mainToolBar.addSeparator();
        mainToolBar.add(this.actionSelectFilter);
        mainToolBar.addSeparator();
        mainToolBar.add(this.actionSearchAllDevices);

        final JPanel toolBarPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        toolBarPanel.add(mainToolBar);

        return toolBarPanel;
    }

}
