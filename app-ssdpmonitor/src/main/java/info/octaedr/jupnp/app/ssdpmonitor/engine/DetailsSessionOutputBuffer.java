/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.impl.io.AbstractSessionOutputBuffer;
import org.apache.http.params.HttpParams;

/**
 * HTTP client session output buffer storing data in byte array.
 *
 * @author Krzysztof Kapuscik
 */
class DetailsSessionOutputBuffer extends AbstractSessionOutputBuffer {

    /** Byte array to store the data. */
    private final ByteArrayOutputStream outStream;

    /**
     * Constructs new buffer.
     *
     * @param params
     *            Paramters for controlling output.
     */
    public DetailsSessionOutputBuffer(final HttpParams params) {
        super();

        this.outStream = new ByteArrayOutputStream();

        init(this.outStream, 8 * 1024, params);
    }

    /**
     * Returns stored packet data.
     *
     * @return Array with packet data.
     *
     * @throws IOException
     *             if there were problems with the buffer.
     */
    public byte[] getData() throws IOException {
        flush();
        return this.outStream.toByteArray();
    }

}
