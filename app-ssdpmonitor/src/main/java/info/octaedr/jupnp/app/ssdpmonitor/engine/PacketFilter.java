/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jupnp.core.ssdp.SsdpMessage;

/**
 * Packet filter.
 *
 * @author Krzysztof Kapuscik
 */
public interface PacketFilter {

    /**
     * Checks if packet shall be filtered.
     *
     * @param message
     *            Message to be processed.
     *
     * @return True if the packet shall be filtered (skipped), false if it was accepted.
     */
    boolean checkPacket(SsdpMessage message);

}
