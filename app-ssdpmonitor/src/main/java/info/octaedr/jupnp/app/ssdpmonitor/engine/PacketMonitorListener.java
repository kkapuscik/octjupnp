/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jupnp.core.ssdp.SsdpMessage;

/**
 * PacketMonitor events listener.
 *
 * @author Krzysztof Kapuscik
 */
public interface PacketMonitorListener {

    /**
     * Called when new SSDP message was received.
     *
     * @param message
     *            The received message. Shall not be modified by listener.
     */
    void messageReceived(SsdpMessage message);

}
