/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jcommons.collection.WriteSafeArrayList;
import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.core.ssdp.MultiSsdpMonitor;
import info.octaedr.jupnp.core.ssdp.SsdpClient;
import info.octaedr.jupnp.core.ssdp.SsdpMessage;
import info.octaedr.jupnp.core.ssdp.SsdpSocketEvent;

import java.awt.EventQueue;

/**
 * Monitor for SSDP packets.
 *
 * @author Krzysztof Kapuscik
 */
public class PacketMonitor extends MultiSsdpMonitor {

    /** Logger object for printing debug messages. */
    private static final Logger logger = Logger.getLogger(PacketMonitor.class);

    /** Monitor started flag. */
    private boolean isStarted = false;

    /** Multicast packets processing enabled. */
    private boolean multicastEnabled = true;

    /** Monitor events listeners. */
    private final WriteSafeArrayList<PacketMonitorListener> listeners;

    /** Packets filter. */
    private PacketFilter filter;

    /**
     * Constructs new monitor.
     *
     * @param ssdpClient
     *            SSDP client instance.
     */
    public PacketMonitor(final SsdpClient ssdpClient) {
        super(ssdpClient);
        this.listeners = new WriteSafeArrayList<PacketMonitorListener>();
    }

    /**
     * Returns currently set packets filter.
     *
     * @return Packet filter instance or null if not set.
     */
    public PacketFilter getFilter() {
        return this.filter;
    }

    /**
     * Sets packets filter.
     *
     * @param filter
     *            Packet filter to be used. Null to clear the currently used filter.
     */
    public void setFilter(final PacketFilter filter) {
        this.filter = filter;
    }

    /**
     * Adds monitor listener to be called on events.
     *
     * @param listener
     *            Listener to add.
     */
    public void addPacketMonitorListener(final PacketMonitorListener listener) {
        this.listeners.add(listener);
    }

    /**
     * Adds monitor listener to be called on events.
     *
     * @param listener
     *            Listener to add.
     */
    public void removePacketMonitorListener(final PacketMonitorListener listener) {
        this.listeners.remove(listener);
    }

    /**
     * Checks state of packet monitor.
     *
     * @return True if packet monitor is started, false otherwise.
     */
    public boolean isStarted() {
        return this.isStarted;
    }

    @Override
    public void start() {
        if (this.isStarted) {
            return;
        }

        this.isStarted = true;
        super.start();
    }

    @Override
    public void stop() {
        if (!this.isStarted) {
            return;
        }

        super.stop();
        this.isStarted = false;
    }

    @Override
    public void sendSearch(final String target) {
        if (!this.isStarted) {
            return;
        }

        super.sendSearch(target);
    }

    @Override
    public void sendSearch(final int mx, final String target) {
        if (!this.isStarted) {
            return;
        }

        super.sendSearch(mx, target);
    }

    /**
     * Sets multicasts capture status.
     *
     * @param enable
     *            True to enable multicasts packets capture, false to disable.
     */
    public void setMulticastEnabled(final boolean enable) {
        this.multicastEnabled = enable;
    }

    /**
     * Checks multicasts capture status.
     *
     * @return True if multicasts packets capture is enabled, false otherwise.
     */
    public boolean isMulticastEnabled() {
        return this.multicastEnabled;
    }

    @Override
    protected void processPacket(final SsdpSocketEvent event, final boolean isMulticast) {
        if (logger.trace()) {
            logger.trace("processPacket(event=" + event + "," + isMulticast + ")");
        }

        if (event.getType() == SsdpSocketEvent.Type.MESSAGE_RECEIVED) {
            final SsdpMessage message = event.getMessage();

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (!isMulticast || PacketMonitor.this.multicastEnabled) {
                        PacketMonitor.this.processMessage(message);
                    }
                }
            });
        }
    }

    /**
     * Processes received message.
     *
     * @param message
     *            Message to be processed.
     */
    private void processMessage(final SsdpMessage message) {
        if (this.isStarted) {
            if (this.filter != null) {
                if (this.filter.checkPacket(message)) {
                    /* filtered out */
                    return;
                }
            }

            for (final PacketMonitorListener listener : this.listeners.getContents()) {
                listener.messageReceived(message);
            }
        }
        /* do not call super class, more processing is not needed */
    }

}
