/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jupnp.core.ssdp.SsdpClient;
import info.octaedr.jupnp.core.ssdp.SsdpMessage;

import java.io.IOException;

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.io.HttpRequestWriter;
import org.apache.http.impl.io.HttpResponseWriter;
import org.apache.http.io.HttpMessageWriter;
import org.apache.http.message.BasicLineFormatter;
import org.apache.http.message.LineFormatter;
import org.apache.http.params.HttpParams;

/**
 * Document with packet details.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
class PacketDetailsDocument extends PlainDocument {

    /** SSDP client instance. */
    private final SsdpClient client;

    /**
     * Constructs document.
     *
     * @param client
     *            SSDP client instance.
     */
    public PacketDetailsDocument(final SsdpClient client) {
        this.client = client;
    }

    /**
     * Sets packet for which details are presented.
     *
     * @param message
     *            Message (the packet) to set or null to clear the one set previously.
     */
    public void setMessage(final SsdpMessage message) {
        /* remove everything */
        try {
            remove(0, getLength());

            if (message != null) {
                final String packetText = buildPacketString(this.client.getHttpUdpClient()
                        .getHttpClient(), message.getHttpMessage());

                insertString(0, packetText, null);
            }
        } catch (final BadLocationException e) {
            // ignore
        } catch (final IOException e) {
            // ignore
        } catch (final HttpException e) {
            // ignore
        }
    }

    /**
     * Builds string with packet contents.
     *
     * @param client
     *            HTTP client instnace.
     * @param httpMessage
     *            HTTP message for which the representation is to be made.
     *
     * @return Created representation.
     *
     * @throws IOException
     *             if there were problems with storing representation.
     * @throws HttpException
     *             if there ere HTTP related problem.
     */
    static String buildPacketString(final HttpClient client, final HttpMessage httpMessage)
            throws IOException, HttpException {
        final HttpParams params = client.getParams();
        final DetailsSessionOutputBuffer buffer = new DetailsSessionOutputBuffer(params);

        final LineFormatter lineFormatter = new BasicLineFormatter();
        HttpMessageWriter messageWriter;

        if (httpMessage instanceof HttpRequest) {
            messageWriter = new HttpRequestWriter(buffer, lineFormatter, params);
        } else {
            messageWriter = new HttpResponseWriter(buffer, lineFormatter, params);
        }

        messageWriter.write(httpMessage);

        return new String(buffer.getData());
    }

}
