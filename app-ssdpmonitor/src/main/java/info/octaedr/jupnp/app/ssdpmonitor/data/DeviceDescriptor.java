/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.data;

import info.octaedr.jupnp.core.USN;

import java.net.InetAddress;
import java.util.UUID;

/**
 * Descriptor of discovered device.
 *
 * @author Krzysztof Kapuscik
 */
public class DeviceDescriptor {

    /** Address of the device. */
    private final String address;
    /** Type of the device. */
    private final String deviceType;
    /** Device unique identifier. */
    private final UUID deviceUUID;
    /** Device unique identifier (string representation). */
    private final String uuidString;

    /** Last notify time (string representation). */
    private String lastNotifyString;

    /** Last notify time (seconds since epoch). */
    private long lastNotify;
    /** Value of cache control (in milliseconds). */
    private int cacheControl;

    /**
     * Constructs new device descriptor.
     *
     * @param inetAddress
     *            Address of the device.
     * @param usn
     *            USN with device type and UUID.
     * @param cacheControl
     *            Value of cache control (device validity time).
     */
    public DeviceDescriptor(InetAddress inetAddress, USN usn, int cacheControl) {
        this.address = inetAddress.getHostAddress();
        this.deviceType = usn.getTypeString();
        this.deviceUUID = usn.getUUID();
        this.uuidString = this.deviceUUID.toString();

        updateLastNotify(cacheControl);
    }

    /**
     * Returns device address (string representation).
     *
     * @return Device address.
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * Returns device type.
     *
     * @return Device type.
     */
    public String getDeviceType() {
        return this.deviceType;
    }

    /**
     * Returns device unique identifier (string representation).
     *
     * @return Device UUID as string.
     */
    public String getDeviceUUID() {
        return this.uuidString;
    }

    /**
     * Returns device cache control (validity delay).
     *
     * @return Value of cache control.
     */
    public int getCacheControl() {
        return this.cacheControl;
    }

    /**
     * Returns device unique identifier.
     *
     * @return Device UUID.
     */
    public UUID getUUID() {
        return this.deviceUUID;
    }

    /**
     * Returns time in milliseconds since epoch when device validity ends.
     *
     * @return End of validity time.
     */
    public long getValidityEnd() {
        if (this.cacheControl > 0) {
            return this.lastNotify + (this.cacheControl * 1000);
        } else {
            return Long.MAX_VALUE;
        }
    }

    /**
     * Returns string representation of time of last notify.
     *
     * @return Last notify time as string.
     */
    public String getLastNotify() {
        return this.lastNotifyString;
    }

    /**
     * Updates last notify time.
     *
     * @param cacheControl
     *            Updated value of cache control.
     */
    public void updateLastNotify(int cacheControl) {
        this.lastNotify = System.currentTimeMillis();
        this.lastNotifyString = TimeFormatter.getTime(this.lastNotify);
        this.cacheControl = cacheControl;
    }

}
