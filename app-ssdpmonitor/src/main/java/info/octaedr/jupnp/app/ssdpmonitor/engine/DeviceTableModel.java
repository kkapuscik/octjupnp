/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jswing.ColumnDescriptor;
import info.octaedr.jswing.TableModelBase;
import info.octaedr.jupnp.app.ssdpmonitor.data.DeviceDescriptor;
import info.octaedr.jupnp.core.USN;
import info.octaedr.jupnp.core.USN.Type;
import info.octaedr.jupnp.core.ssdp.AliveNotifyMessage;
import info.octaedr.jupnp.core.ssdp.ByeByeNotifyMessage;
import info.octaedr.jupnp.core.ssdp.SearchResponseMessage;
import info.octaedr.jupnp.core.ssdp.SsdpMessage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.UUID;

import javax.swing.Timer;

/**
 * Table model with alive devices.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
class DeviceTableModel extends TableModelBase implements PacketMonitorListener {

    /** Validity timer repeat time (in milliseconds). */
    private static final int VALIDITY_TIMER_DELAY = 15 * 1000;

    /**
     * Table columns.
     */
    private static ColumnDescriptor[] COLUMNS = {
        /** Device address. */
        new ColumnDescriptor("Address", String.class),
        /** Last notify time. */
        new ColumnDescriptor("Last Notify", String.class),
        /** Cache control value. */
        new ColumnDescriptor("Cache control", Integer.class),
        /** Device UUID. */
        new ColumnDescriptor("Device UUID", String.class),
        /** Device type. */
        new ColumnDescriptor("Device Type", String.class) };

    /**
     * Alive devices in network.
     */
    private final ArrayList<DeviceDescriptor> devices;

    /**
     * Validity checking timer.
     */
    private final Timer validityTimer;

    /**
     * Constructs the model;
     */
    public DeviceTableModel() {
        super(COLUMNS);

        this.devices = new ArrayList<DeviceDescriptor>();

        this.validityTimer = new Timer(VALIDITY_TIMER_DELAY, new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                DeviceTableModel.this.checkValidity();
            }
        });
        this.validityTimer.setRepeats(true);
        this.validityTimer.start();
    }

    /**
     * Removes all elements from the model.
     */
    public void clear() {
        /* Remove messages. */
        this.devices.clear();

        /* notify change */
        fireTableDataChanged();
    }

    @Override
    public void messageReceived(final SsdpMessage message) {

        if (message instanceof AliveNotifyMessage) {
            final AliveNotifyMessage aliveMessage = (AliveNotifyMessage) message;

            if (aliveMessage.getUSN().getType() == Type.DEVICE_TYPE) {
                processAliveMessage(aliveMessage.getMessage().getAddress(), aliveMessage.getUSN(),
                        aliveMessage.getCacheControl());
            }

        } else if (message instanceof ByeByeNotifyMessage) {
            final ByeByeNotifyMessage byebyeMessage = (ByeByeNotifyMessage) message;

            if (byebyeMessage.getUSN().getType() == Type.DEVICE_TYPE) {
                processByeByeMessage(byebyeMessage.getUSN());
            }
        } else if (message instanceof SearchResponseMessage) {
            final SearchResponseMessage responseMessage = (SearchResponseMessage) message;

            if (responseMessage.getUSN().getType() == Type.DEVICE_TYPE) {
                processAliveMessage(responseMessage.getMessage().getAddress(),
                        responseMessage.getUSN(), responseMessage.getCacheControl());
            }
        }
    }

    /**
     * Processes device alive message.
     *
     * @param inetAddress
     *            Packet (message) source.
     * @param usn
     *            USN from the message.
     * @param cacheControl
     *            Cache control value.
     */
    private void processAliveMessage(final InetAddress inetAddress, final USN usn,
            final int cacheControl) {
        final int index = findDeviceDescriptor(usn.getUUID());
        if (index < 0) {
            /* unknown device - create new entry */
            final DeviceDescriptor descriptor = new DeviceDescriptor(inetAddress, usn, cacheControl);

            /* store the device */
            this.devices.add(descriptor);

            /* notify about change */
            final int newRow = this.devices.size() - 1;
            fireTableRowsInserted(newRow, newRow);

        } else {
            /* device already known - update time */
            final DeviceDescriptor descriptor = this.devices.get(index);
            descriptor.updateLastNotify(cacheControl);

            fireTableRowsUpdated(index, index);
        }
    }

    /**
     * Processes device alive message.
     *
     * @param inetAddress
     *            Packet (message) source.
     * @param usn
     *            USN from the message.
     * @param cacheControl
     *            Cache control value.
     */
    private void processByeByeMessage(final USN usn) {
        removeDevice(usn.getUUID());
    }

    /**
     * Checks validity of the devices.
     */
    protected void checkValidity() {
        /* no removed devices */
        ArrayList<UUID> removedDevices = null;

        /* get current time */
        final long currentTime = System.currentTimeMillis();

        /* check all devices */
        for (final DeviceDescriptor device : this.devices) {
            /* check if device is no longer valid */
            if (currentTime > device.getValidityEnd()) {
                /* create collection when needed */
                if (removedDevices == null) {
                    removedDevices = new ArrayList<UUID>();
                }

                /* add to list of devices to be removed */
                removedDevices.add(device.getUUID());
            }
        }

        /* remove devices (if any) */
        if (removedDevices != null) {
            for (final UUID uuid : removedDevices) {
                removeDevice(uuid);
            }
        }
    }

    /**
     * Removes device with given UUID (if exists).
     *
     * @param uuid
     *            UUID of the device to remove.
     */
    private void removeDevice(final UUID uuid) {
        final int index = findDeviceDescriptor(uuid);
        if (index >= 0) {
            this.devices.remove(index);

            fireTableRowsDeleted(index, index);
        }
    }

    /**
     * Finds index of descriptor of device with given UUID.
     *
     * @param uuid
     *            Device UUID.
     *
     * @return Index of descriptor or -1 if not available.
     */
    private int findDeviceDescriptor(final UUID uuid) {
        for (int i = 0; i < this.devices.size(); ++i) {
            final DeviceDescriptor device = this.devices.get(i);
            if (device.getUUID().equals(uuid)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int getRowCount() {
        return this.devices.size();
    }

    @Override
    public Object getValueAt(final int row, final int column) {
        if ((column < 0) || (column >= getColumnCount())) {
            return EMPTY_STRING;
        }
        if ((row < 0) || (row >= getRowCount())) {
            return EMPTY_STRING;
        }

        final DeviceDescriptor descriptor = this.devices.get(row);

        switch (column) {
        case 0:
            return descriptor.getAddress();
        case 1:
            return descriptor.getLastNotify();
        case 2:
            return descriptor.getCacheControl();
        case 3:
            return descriptor.getDeviceUUID();
        case 4:
            return descriptor.getDeviceType();
        default:
            return EMPTY_STRING;
        }
    }

}
