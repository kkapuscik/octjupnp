/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.ui;

import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jupnp.app.ssdpmonitor.engine.IpPacketFilter;
import info.octaedr.jupnp.app.ssdpmonitor.engine.PacketFilter;
import info.octaedr.jupnp.app.ssdpmonitor.engine.SsdpMonitorEngine;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Dialog for configuring packet filter.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings({ "serial", "javadoc" })
public class FilterDialog extends JDialog {

    /** Monitor engine to be used. */
    private final SsdpMonitorEngine engine;

    private ButtonGroup filterButtonGroup;
    private JRadioButton noFilterButton;
    private JRadioButton ipFilterButton;

    private JTextField ipTextField;
    private SwingAction actionOK;
    private SwingAction actionCancel;
    private SwingAction actionNoFilter;
    private SwingAction actionIpFilter;

    /** First setVisible call is going to be processed flag. */
    private boolean firstShow = true;

    /**
     * Constructs new dialog.
     *
     * @param owner
     *            Frame owning this dialog.
     * @param engine
     *            Monitor engine to be used.
     */
    public FilterDialog(final Frame owner, final SsdpMonitorEngine engine) {
        super(owner);
        this.engine = engine;

        setSize(400, 230);

        createActions();
        createComponents();
        connectActions();
        initState();
    }

    @Override
    public void setVisible(final boolean b) {
        if (b) {
            if (this.firstShow) {
                this.firstShow = false;
                setLocationRelativeTo(getOwner());
            }

            final PacketFilter filter = this.engine.getFilter();
            if (filter instanceof IpPacketFilter) {
                final InetAddress address = ((IpPacketFilter) filter).getAddress();

                this.ipTextField.setText(address.getHostAddress());

                this.actionIpFilter.setSelected(true);
            } else {
                this.actionNoFilter.setSelected(true);
            }
        }

        super.setVisible(b);
    }

    /**
     * Initializes dialog state.
     */
    private void initState() {
        this.actionNoFilter.setSelected(true);
        this.actionIpFilter.setSelected(false);
    }

    /**
     * Constructs dialog actions.
     */
    private void createActions() {
        this.actionOK = new SwingAction();
        this.actionOK.setName("OK");
        this.actionOK.setShortDescription("Accepts filter settings");

        this.actionCancel = new SwingAction();
        this.actionCancel.setName("Cancel");
        this.actionCancel.setShortDescription("Cancels filter settings");
        this.actionCancel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));

        this.actionNoFilter = new SwingAction();
        this.actionNoFilter.setName("No Filter");
        this.actionNoFilter.setShortDescription("Packets filtering is disabled");

        this.actionIpFilter = new SwingAction();
        this.actionIpFilter.setName("Source IP Filter");
        this.actionIpFilter.setShortDescription("Packets filtering using packet source IP address");
    }

    /**
     * Connects executors to actions.
     */
    private void connectActions() {
        this.rootPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                FilterDialog.this.actionCancel.actionPerformed(e);
            }
        }, this.actionCancel.getAccelerator(), JComponent.WHEN_IN_FOCUSED_WINDOW);

        this.filterButtonGroup = new ButtonGroup();
        this.filterButtonGroup.add(this.noFilterButton);
        this.filterButtonGroup.add(this.ipFilterButton);

        this.ipTextField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(final DocumentEvent e) {
                FilterDialog.this.actionIpFilter.setSelected(true);
            }

            @Override
            public void insertUpdate(final DocumentEvent e) {
                FilterDialog.this.actionIpFilter.setSelected(true);
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                FilterDialog.this.actionIpFilter.setSelected(true);
            }
        });

        this.actionOK.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                FilterDialog.this.processOKRequest();
            }
        });

        this.actionCancel.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                FilterDialog.this.setVisible(false);
            }
        });
    }

    /**
     * Processes dialog OK action request.
     */
    private void processOKRequest() {
        if (this.actionNoFilter.isSelected()) {
            this.engine.setFilter(null);

            setVisible(false);
        } else if (this.actionIpFilter.isSelected()) {
            InetAddress filterAddress = null;
            do {
                final String ipText = this.ipTextField.getText();

                if ((ipText == null) || (ipText.length() == 0)) {
                    break;
                }

                try {
                    filterAddress = InetAddress.getByName(ipText);
                } catch (final UnknownHostException e) {
                    // ignore
                }

            } while (false);

            if (filterAddress != null) {
                this.engine.setFilter(new IpPacketFilter(filterAddress));

                setVisible(false);
            } else {
                JOptionPane.showMessageDialog(this,
                        "The specified IP is invalid.\n\nPlease correct it and then retry.");
            }
        }
    }

    /**
     * Constructs dialog components.
     */
    private void createComponents() {
        final Component noFilterComponent = createNoFilterComponents();
        final Component ipFilterComponent = createIpFitlerComponents();

        final JPanel filtersPanel = new JPanel();
        filtersPanel.setLayout(new BoxLayout(filtersPanel, BoxLayout.Y_AXIS));
        filtersPanel.add(noFilterComponent);
        filtersPanel.add(ipFilterComponent);

        final JButton okButton = new JButton(this.actionOK);
        final JButton cancelButton = new JButton(this.actionCancel);

        final JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 0, 50, 5));
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        final JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout(5, 5));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        mainPanel.add(filtersPanel, BorderLayout.NORTH);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        setContentPane(mainPanel);
    }

    /**
     * Constructs "No filter" settings components.
     *
     * @return Settings root component.
     */
    private Component createNoFilterComponents() {
        this.noFilterButton = new JRadioButton(this.actionNoFilter);

        final JPanel noFilterPanel = new JPanel();
        noFilterPanel.setLayout(new BorderLayout());
        noFilterPanel.setBorder(new TitledBorder("No Filter"));

        noFilterPanel.add(this.noFilterButton, BorderLayout.NORTH);

        return noFilterPanel;
    }

    /**
     * Constructs "IP filter" settings components.
     *
     * @return Settings root component.
     */
    private Component createIpFitlerComponents() {
        this.ipFilterButton = new JRadioButton(this.actionIpFilter);

        this.ipTextField = new JTextField();

        final JPanel ipFilterPanel = new JPanel();
        ipFilterPanel.setLayout(new BorderLayout());
        ipFilterPanel.setBorder(new TitledBorder("IP Filter"));

        ipFilterPanel.add(this.ipFilterButton, BorderLayout.NORTH);
        ipFilterPanel.add(this.ipTextField, BorderLayout.SOUTH);

        return ipFilterPanel;
    }

}
