/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jupnp.core.ssdp.SsdpClient;
import info.octaedr.jupnp.core.ssdp.SsdpMessage;

import javax.swing.table.TableModel;
import javax.swing.text.Document;

/**
 * Engine (document) class for SSDP monitor.
 *
 * @author Krzysztof Kapuscik
 */
public class SsdpMonitorEngine {

    /** Search target - "ssdp:all". */
    public static final String SEARCH_TARGET_SSDP_ALL = "ssdp:all";

    /** Search target - "upnp:rootdevice". */
    public static final String SEARCH_TARGET_UPNP_ROOTDEVICE = "upnp:rootdevice";

    /** Search target - InternetGatewayDevice:1. */
    public static final String SEARCH_TARGET_MEDIA_SERVER_1 = "urn:schemas-upnp-org:device:MediaServer:1";

    /** Search target - InternetGatewayDevice:1. */
    public static final String SEARCH_TARGET_MEDIA_RENDERER_1 = "urn:schemas-upnp-org:device:MediaRenderer:1";

    /** Search target - InternetGatewayDevice:1. */
    public static final String SEARCH_TARGET_INTERNET_GATEWAY_1 = "urn:schemas-upnp-org:device:InternetGatewayDevice:1";

    /** Table with captured packets. */
    private final PacketTableModel packetTableModel;
    /** Table with alive devices. */
    private final DeviceTableModel deviceTableModel;
    /** Packet monitor. */
    private final PacketMonitor packetMonitor;
    /** Packet details document. */
    private final PacketDetailsDocument packetDetailsDocument;

    /**
     * Constructs engine.
     */
    public SsdpMonitorEngine() {
        SsdpClient client = new SsdpClient();

        this.packetTableModel = new PacketTableModel();

        this.deviceTableModel = new DeviceTableModel();

        this.packetMonitor = new PacketMonitor(client);
        this.packetMonitor.addPacketMonitorListener(this.packetTableModel);
        this.packetMonitor.addPacketMonitorListener(this.deviceTableModel);

        this.packetDetailsDocument = new PacketDetailsDocument(client);
    }

    /**
     * Returns model for packet table.
     *
     * @return Model for packed table.
     */
    public TableModel getPacketTableModel() {
        return this.packetTableModel;
    }

    /**
     * Returns model for device table.
     *
     * @return Model for device table.
     */
    public TableModel getDeviceTableModel() {
        return this.deviceTableModel;
    }

    /**
     * Sends search request for root devices.
     *
     * @param searchTarget
     *            Search target.
     */
    public void sendSearch(String searchTarget) {
        this.packetMonitor.sendSearch(searchTarget);
    }

    /**
     * Clears all packets captured so far.
     */
    public void newCapture() {
        this.packetTableModel.clear();
        this.deviceTableModel.clear();
    }

    /**
     * Sets packet that shall be considered as selected.
     *
     * @param index
     *            Index of the selected packet.
     */
    public void setSelectedPacket(int index) {
        SsdpMessage message;

        if (index >= 0) {
            message = this.packetTableModel.getMessage(index);
        } else {
            message = null;
        }

        this.packetDetailsDocument.setMessage(message);
    }

    /**
     * Returns packet details document.
     *
     * @return Packet details document.
     */
    public Document getPacketDetailsDocument() {
        return this.packetDetailsDocument;
    }

    /**
     * Checks state of capture process.
     *
     * @return True if capture process is started, false otherwise.
     */
    public boolean isCaptureStarted() {
        return this.packetMonitor.isStarted();
    }

    /**
     * Sets state of capture process.
     *
     * @param enable
     *            True to enable capture (start capture process), false to disable.
     */
    public void setCaptureEnabled(boolean enable) {
        if (enable) {
            this.packetMonitor.start();
        } else {
            this.packetMonitor.stop();
        }
    }

    /**
     * Sets multicasts capture status.
     *
     * @param enable
     *            True to enable multicasts packets capture, false to disable.
     */
    public void setMulticastEnabled(boolean enable) {
        this.packetMonitor.setMulticastEnabled(enable);
    }

    /**
     * Checks multicasts capture status.
     *
     * @return True if multicasts packets capture is enabled, false otherwise.
     */
    public boolean isMulticastEnabled() {
        return this.packetMonitor.isMulticastEnabled();
    }

    /**
     * Returns currently set packets filter.
     *
     * @return Packet filter instance or null if not set.
     */
    public PacketFilter getFilter() {
        return this.packetMonitor.getFilter();
    }

    /**
     * Sets packets filter.
     *
     * @param filter
     *            Packet filter to be used. Null to clear the currently used filter.
     */
    public void setFilter(PacketFilter filter) {
        this.packetMonitor.setFilter(filter);
    }

}
