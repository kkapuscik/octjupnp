/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import java.net.InetAddress;

import info.octaedr.jupnp.core.ssdp.SsdpMessage;

/**
 * Filter that checks the packet source address.
 *
 * @author Krzysztof Kapuscik
 */
public class IpPacketFilter implements PacketFilter {

    /** Source address of packets that shall be accepted. */
    private final InetAddress address;

    /**
     * Constructs the filter.
     *
     * @param address
     *            Source address of packets that shall be accepted.
     */
    public IpPacketFilter(InetAddress address) {
        if (address == null) {
            throw new NullPointerException();
        }

        this.address = address;
    }

    /**
     * Returns filter address.
     *
     * @return Address given during filter construction.
     */
    public InetAddress getAddress() {
        return this.address;
    }

    @Override
    public boolean checkPacket(SsdpMessage message) {
        if (this.address.equals(message.getMessage().getAddress())) {
            return false;
        }

        return true;
    }

}
