/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.resources;

import info.octaedr.jswing.resources.ResourceProvider;

import java.awt.Image;

import javax.swing.Icon;

/**
 * Application icon set.
 *
 * @author Krzysztof Kapuscik
 */
public class IconSet extends ResourceProvider {

    @SuppressWarnings("javadoc")
    public static final String ABOUT = "about";
    @SuppressWarnings("javadoc")
    public static final String CLEAR_CAPTURE = "clear-capture";
    @SuppressWarnings("javadoc")
    public static final String DOWNLOAD = "download";
    @SuppressWarnings("javadoc")
    public static final String EXIT = "exit";
    @SuppressWarnings("javadoc")
    public static final String MULTICAST = "multicast";
    @SuppressWarnings("javadoc")
    public static final String START_CAPTURE = "start-capture";
    @SuppressWarnings("javadoc")
    public static final String SEARCH_ALL = "search-all";
    @SuppressWarnings("javadoc")
    public static final String SELECT_FILTER = "select-filter";
    @SuppressWarnings("javadoc")
    public static final String DEVICES = "devices";
    @SuppressWarnings("javadoc")
    public static final String PACKET_DETAILS = "packet-details";
    @SuppressWarnings("javadoc")
    public static final String SSDP_MONITOR = "ssdp-monitor";

    /**
     * Constructs icon set.
     *
     * @param pkgName
     *            Name of the package.
     */
    public IconSet(final String pkgName) {
        super(pkgName);
    }

    /**
     * Returns small (16x16) icon for given key.
     *
     * @param key
     *            Icon key.
     *
     * @return Icon object or null if not found.
     */
    public Icon getSmallIcon(final String key) {
        return getIcon(key, "small", "png");
    }

    /**
     * Returns small (16x16) image for given key.
     *
     * @param key
     *            Image key.
     *
     * @return Image object or null if not found.
     */
    public Image getSmallImage(final String key) {
        return getImage(key, "small", "png");
    }

}
