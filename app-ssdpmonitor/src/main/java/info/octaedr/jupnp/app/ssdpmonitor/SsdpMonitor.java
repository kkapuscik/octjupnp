/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor;

import info.octaedr.jcommons.logging.LogManager;
import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.AboutPerson;
import info.octaedr.jupnp.app.ssdpmonitor.engine.SsdpMonitorEngine;
import info.octaedr.jupnp.app.ssdpmonitor.ui.SsdpMonitorFrame;

/**
 * DeviceSpy application class.
 */
public class SsdpMonitor {

    /** Logger for printing debug messages. */
    private static Logger logger = Logger.getLogger(SsdpMonitor.class);

    /**
     * Main function.
     *
     * @param args
     *            Execution arguments.
     */
    public static void main(final String[] args) {
        try {
            LogManager.init();

            /* prepare about data */
            final AboutData aboutData = new AboutData("SSDP Monitor", "0.3");
            aboutData.setShortDescription("Utility for monitoring SSDP protocol messages");
            aboutData.setCopyrightStatement("2008-2012 OCTaedr Software");
            aboutData.addAuthor(new AboutPerson("Krzysztof Kapuscik", "Author",
                    "k.kapuscik@gmail.com"));
            aboutData.addCredit(new AboutPerson("Farm-Fresh Web Icons", "Icon Set", null,
                    "http://www.fatcow.com/free-icons"));

            /* create engine */
            final SsdpMonitorEngine appEngine = new SsdpMonitorEngine();

            /* create main frame */
            final SsdpMonitorFrame appFrame = new SsdpMonitorFrame(appEngine, aboutData);

            /* show main frame */
            appFrame.setVisible(true);
        } catch (final Throwable t) {
            if (logger.severe()) {
                logger.severe("Exception catched", t);
            }
        }
    }

}
