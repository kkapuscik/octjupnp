/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.data;

import info.octaedr.jupnp.core.ssdp.AliveNotifyMessage;
import info.octaedr.jupnp.core.ssdp.ByeByeNotifyMessage;
import info.octaedr.jupnp.core.ssdp.SearchRequestMessage;
import info.octaedr.jupnp.core.ssdp.SearchResponseMessage;
import info.octaedr.jupnp.core.ssdp.SsdpMessage;

/** Packet descriptor. */
public class PacketDescriptor {

    /** The SSDP message described. */
    public final SsdpMessage message;
    /** Type. */
    public final String type;
    /** Short description. */
    public final String shortDescription;
    /** Packet receive time. */
    public final String time;
    /** Packet address. */
    public final String address;

    /**
     * Constructs new descriptor.
     *
     * @param message
     *            Message to be described.
     */
    public PacketDescriptor(SsdpMessage message) {
        this.message = message;
        this.type = getType(message);
        this.shortDescription = getShortDescription(message);
        this.time = TimeFormatter.getTime(System.currentTimeMillis());
        this.address = getAddress(message);
    }

    /**
     * Returns packet type.
     *
     * @param message
     *            Message to process.
     *
     * @return Packet type.
     */
    private static String getType(SsdpMessage message) {
        if (message instanceof AliveNotifyMessage) {
            return "Alive Notify";
        } else if (message instanceof ByeByeNotifyMessage) {
            return "ByeBye Notify";
        } else if (message instanceof SearchRequestMessage) {
            return "Search Request";
        } else if (message instanceof SearchResponseMessage) {
            return "Search Response";
        } else {
            return "Unknown";
        }
    }

    /**
     * Returns packet short descriptor.
     *
     * @param message
     *            Message to process.
     *
     * @return Packet short descriptor.
     */
    private static String getShortDescription(SsdpMessage message) {
        if (message instanceof AliveNotifyMessage) {
            return ((AliveNotifyMessage) message).getUSN().toString();
        } else if (message instanceof ByeByeNotifyMessage) {
            return ((ByeByeNotifyMessage) message).getUSN().toString();
        } else if (message instanceof SearchRequestMessage) {
            return ((SearchRequestMessage) message).getSearchTarget();
        } else if (message instanceof SearchResponseMessage) {
            return ((SearchResponseMessage) message).getUSN().toString();
        } else {
            return "Unknown";
        }
    }

    /**
     * Returns packet address.
     *
     * @param message
     *            Message to process.
     *
     * @return Packet address.
     */
    private static String getAddress(SsdpMessage message) {
        return message.getMessage().getAddress().getHostAddress() + ":" + message.getMessage().getPort();
    }

}
