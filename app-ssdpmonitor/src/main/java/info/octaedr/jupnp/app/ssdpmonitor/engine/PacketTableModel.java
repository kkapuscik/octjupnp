/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-ssdpmonitor project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.ssdpmonitor.engine;

import info.octaedr.jswing.ColumnDescriptor;
import info.octaedr.jswing.TableModelBase;
import info.octaedr.jupnp.app.ssdpmonitor.data.PacketDescriptor;
import info.octaedr.jupnp.core.ssdp.SsdpMessage;

import java.util.ArrayList;

/**
 * Table model with SSDP packets.
 *
 * @author Krzysztof Kapuscik
 */
@SuppressWarnings("serial")
class PacketTableModel extends TableModelBase implements PacketMonitorListener {

    /**
     * Table columns.
     */
    private static ColumnDescriptor[] COLUMNS = {
        /** Packet number. */
        new ColumnDescriptor("No.", Integer.class),
        /** Packet receive time. */
        new ColumnDescriptor("Time", String.class),
        /** Packet source address. */
        new ColumnDescriptor("Source Address", String.class),
        /** Packet (message) type. */
        new ColumnDescriptor("Packet Type", String.class),
        /** Short packet description. */
        new ColumnDescriptor("Packet Information", String.class) };

    /**
     * Messages stored in model.
     */
    private final ArrayList<PacketDescriptor> packets;

    /**
     * Constructs the model;
     */
    public PacketTableModel() {
        super(COLUMNS);

        this.packets = new ArrayList<PacketDescriptor>();
    }

    /**
     * Removes all elements from the model.
     */
    public void clear() {
        /* Remove messages. */
        this.packets.clear();

        /* notify change */
        fireTableDataChanged();
    }

    @Override
    public void messageReceived(final SsdpMessage message) {
        /* store the message */
        this.packets.add(new PacketDescriptor(message));

        /* notify about change */
        final int newRow = this.packets.size() - 1;
        fireTableRowsInserted(newRow, newRow);
    }

    @Override
    public int getRowCount() {
        return this.packets.size();
    }

    @Override
    public Object getValueAt(final int row, final int column) {
        if ((column < 0) || (column >= getColumnCount())) {
            return EMPTY_STRING;
        }
        if ((row < 0) || (row >= getRowCount())) {
            return EMPTY_STRING;
        }

        if (column == 0) {
            return row + 1;
        }

        final PacketDescriptor descriptor = this.packets.get(row);

        switch (column) {
        case 1:
            return descriptor.time;
        case 2:
            return descriptor.address;
        case 3:
            return descriptor.type;
        case 4:
            return descriptor.shortDescription;
        default:
            return EMPTY_STRING;
        }
    }

    /**
     * Returns message stored at given index.
     *
     * @param row
     *            Row in model.
     *
     * @return Stored message or null if not available.
     */
    public SsdpMessage getMessage(final int row) {
        if ((row < 0) || (row >= getRowCount())) {
            return null;
        }
        return this.packets.get(row).message;
    }

}
