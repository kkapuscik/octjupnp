/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-tools project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.avplayer.ui;

import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.MainFrame;
import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jupnp.app.avplayer.engine.AVPlayerEngine;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JSplitPane;

/**
 * Main frame of AVPlayer.
 */
public class AVPlayerFrame extends MainFrame {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 2428464068926888704L;

    private final AVPlayerEngine playerEngine;
    private final AboutData aboutData;

    private SwingAction actionAppExit;
    private SwingAction actionAppAbout;
    private SwingAction actionRescanNetwork;

    private BrowserView browserView;
    private PlayerView playerView;
    private PropertiesView propertiesView;

    public AVPlayerFrame(final AVPlayerEngine spyEngine, final AboutData aboutData) {
        super(aboutData);

        this.playerEngine = spyEngine;
        this.aboutData = aboutData;

        createActions();
        createMenu();
        createComponents();
        setupListeners();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(final WindowEvent e) {
                spyEngine.terminate();
            }
        });
    }

    private void setupListeners() {
    }

    private void createComponents() {
        /* create functional components */
        this.browserView = new BrowserView(this.playerEngine.getBrowserModel());
        this.playerView = new PlayerView();
        this.propertiesView = new PropertiesView();

        /* create layout components */
        final JSplitPane dataSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.playerView,
                this.propertiesView);
        dataSplitPane.setDividerLocation(0.5);
        dataSplitPane.setResizeWeight(0.5);

        final JSplitPane mainSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                this.browserView, dataSplitPane);
        mainSplitPane.setDividerLocation(0.5);
        mainSplitPane.setResizeWeight(0.5);

        getContentPane().add(mainSplitPane, BorderLayout.CENTER);
    }

    @SuppressWarnings("serial")
    private void createActions() {
        this.actionAppExit = new SwingAction();
        this.actionAppExit.setName("Exit");

        this.actionAppAbout = new SwingAction();
        this.actionAppAbout.setName("About");

        this.actionRescanNetwork = new SwingAction();
        this.actionRescanNetwork.setName("Rescan Network");

        this.actionAppExit.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                AVPlayerFrame.this.dispose();
            }
        });

        this.actionAppAbout.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                AVPlayerFrame.this.showAboutDialog();
            }
        });

        this.actionRescanNetwork.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                AVPlayerFrame.this.playerEngine.rescanNetwork();
            }
        });
    }

    private void createMenu() {
        /* create file menu */
        final JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(this.actionRescanNetwork);
        fileMenu.addSeparator();
        fileMenu.add(this.actionAppExit);

        /* create help menu */
        final JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        helpMenu.add(this.actionAppAbout);

        /* build menu bar */
        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        /* set menu bar */
        setJMenuBar(menuBar);
    }

}
