/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-tools project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.avplayer.engine;

import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointEvent;
import info.octaedr.jupnp.cp.ControlPointFactory;
import info.octaedr.jupnp.cp.ControlPointListener;

import java.awt.EventQueue;

import javax.swing.ListModel;

/**
 * AVPlayer engine class.
 */
public class AVPlayerEngine {

    private final ControlPoint controlPoint;

    private final BrowserModel browserModel;

    public AVPlayerEngine() {
        this.browserModel = new BrowserModel();

        this.controlPoint = createControlPoint();
        this.controlPoint.start();
    }

    private ControlPoint createControlPoint() {
        ControlPoint cp = ControlPointFactory.getInstance().createControlPoint();

        cp.addControlPointListener(new ControlPointListener() {

            @Override
            public void deviceAdded(final ControlPointEvent event) {
                // ignore
            }

            @Override
            public void deviceRemoved(final ControlPointEvent event) {
                // ignore
            }

            @Override
            public void rootDeviceAdded(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        AVPlayerEngine.this.deviceAdded(event);
                    }
                });
            }

            @Override
            public void rootDeviceRemoved(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        AVPlayerEngine.this.deviceRemoved(event);
                    }
                });
            }

        });

        return cp;
    }

    private void deviceAdded(final ControlPointEvent event) {
        // RemoteDevice device = event.getRemoteDevice();
        // this.elementTreeModel.deviceAdded(device);
    }

    private void deviceRemoved(final ControlPointEvent event) {
        // RemoteDevice device = event.getRemoteDevice();
        // this.elementTreeModel.deviceRemoved(device);
    }

    public void terminate() {
        this.controlPoint.stop();
    }

    public void rescanNetwork() {
        this.controlPoint.rescanNetwork();
    }

    public ListModel getBrowserModel() {
        return this.browserModel;
    }

}
