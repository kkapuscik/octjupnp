/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-tools project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.avplayer.ui;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.JXPanel;

public class BrowserView extends JXPanel {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 6414974800258072133L;

    private JXList itemsList;

    public BrowserView(final ListModel model) {
        super(new BorderLayout());
        setBorder(new TitledBorder("Browser"));

        createComponents(model);
    }

    private void createComponents(ListModel model) {
        this.itemsList = new JXList(model);

        add(new JScrollPane(this.itemsList), BorderLayout.CENTER);
    }

}
