/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-tools project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.avplayer.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXPanel;

public class PlayerView extends JXPanel {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 4457506907667867590L;

    /**
     * Creates a new instance of JXPanel
     */
    public PlayerView() {
        super(new BorderLayout());
        setBorder(new TitledBorder("Player"));

        // temporary
        final TestPlayer testPlayer = new TestPlayer();
        JButton testButton = new JButton("Test");
        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser("/home/data");
                int rv = fc.showOpenDialog(PlayerView.this);
                if (rv == JFileChooser.APPROVE_OPTION) {
                    testPlayer.setup(fc.getSelectedFile().getAbsolutePath());
                }
            }
        });

        add(testButton, BorderLayout.NORTH);
        add(testPlayer, BorderLayout.CENTER);

    }

}
