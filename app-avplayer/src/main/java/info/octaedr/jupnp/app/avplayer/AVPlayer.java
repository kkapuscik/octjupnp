/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-tools project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.avplayer;

import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.AboutPerson;
import info.octaedr.jupnp.app.avplayer.engine.AVPlayerEngine;
import info.octaedr.jupnp.app.avplayer.ui.AVPlayerFrame;

/**
 * AVPlayer application class.
 */
public class AVPlayer {

    /**
     * Main function.
     *
     * @param args
     *            Execution arguments.
     */
    public static void main(final String[] args) {
        /* prepare about data */
        final AboutData aboutData = new AboutData("AVPlayer", "0.1");
        aboutData
                .addAuthor(new AboutPerson("Krzysztof Kapuscik", "Author", "k.kapuscik@gmail.com"));

        /* create engine */
        final AVPlayerEngine playerEngine = new AVPlayerEngine();

        /* create main frame */
        final AVPlayerFrame playerFrame = new AVPlayerFrame(playerEngine, aboutData);

        /* show main frame */
        playerFrame.setVisible(true);
    }

}
