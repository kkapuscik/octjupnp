/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-tools project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.avplayer.ui;

import org.jdesktop.swingx.JXTable;

public class PropertiesView extends JXTable {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -8170037454370085921L;

}
