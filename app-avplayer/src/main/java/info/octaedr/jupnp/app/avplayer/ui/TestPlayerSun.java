/*
 * @(#)SimplePlayerApplet.java  1.2 01/03/13
 *
 * Copyright (c) 1996-2001 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Sun grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Sun.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear
 * facility. Licensee represents and warrants that it will not use or
 * redistribute the Software for such purposes.
 */

package info.octaedr.jupnp.app.avplayer.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.media.CachingControl;
import javax.media.CachingControlEvent;
import javax.media.Controller;
import javax.media.ControllerClosedEvent;
import javax.media.ControllerErrorEvent;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.EndOfMediaEvent;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.media.RealizeCompleteEvent;
import javax.media.Time;

import org.jdesktop.swingx.JXPanel;

/**
 * This is a Java Applet that demonstrates how to create a simple media player with a media event
 * listener. It will play the media clip right away and continuously loop.
 *
 * <!-- Sample HTML <applet code=SimplePlayerApplet width=320 height=300> <param name=file
 * value="sun.avi"> </applet> -->
 */
public class TestPlayerSun extends JXPanel implements ControllerListener {

    // media Player
    Player player = null;

    // component in which video is playing
    Component visualComponent = null;

    // controls gain, position, start, stop
    Component controlComponent = null;

    // displays progress during download
    Component progressBar = null;

    boolean firstTime = true;

    long CachingSize = 0L;

    int controlPanelHeight = 0;

    int videoWidth = 0;

    int videoHeight = 0;

    /**
     * Read the applet file parameter and create the media player.
     */
    public void setup(final String mediaFile) {
        // $ System.out.println("Applet.init() is called");
        setLayout(new BorderLayout());
        setBackground(Color.white);

        // URL for our media file
        MediaLocator mrl = null;

        try {
            // Create a media locator from the file name
            if ((mrl = new MediaLocator(mediaFile)) == null) {
                Fatal("Can't build URL for " + mediaFile);
            }

            /*
             * try { JMFSecurity.enablePrivilege.invoke(JMFSecurity.privilegeManager,
             * JMFSecurity.writePropArgs);
             * JMFSecurity.enablePrivilege.invoke(JMFSecurity.privilegeManager,
             * JMFSecurity.readPropArgs);
             * JMFSecurity.enablePrivilege.invoke(JMFSecurity.privilegeManager,
             * JMFSecurity.connectArgs); } catch (Exception e) {}
             */

            // Create an instance of a player for this media
            this.player = Manager.createPlayer(mrl);
            // Add ourselves as a listener for a player's events
            this.player.addControllerListener(this);

        } catch (final NoPlayerException e) {
            System.out.println(e);
            Fatal("Could not create player for " + mrl);
        } catch (final MalformedURLException e) {
            Fatal("Invalid media file URL!");
        } catch (final IOException e) {
            Fatal("IO exception creating player for " + mrl);
        }

        // This applet assumes that its start() calls
        // player.start(). This causes the player to become
        // realized. Once realized, the applet will get
        // the visual and control panel components and add
        // them to the Applet. These components are not added
        // during init() because they are long operations that
        // would make us appear unresposive to the user.
    }

    /**
     * Start media file playback. This function is called the first time that the Applet runs and
     * every time the user re-enters the page.
     */

    public void start() {
        // $ System.out.println("Applet.start() is called");
        // Call start() to prefetch and start the player.
        if (this.player != null) {
            this.player.start();
        }
    }

    /**
     * Stop media file playback and release resource before leaving the page.
     */
    public void stop() {
        // $ System.out.println("Applet.stop() is called");
        if (this.player != null) {
            this.player.stop();
            this.player.deallocate();
        }
    }

    public void destroy() {
        // $ System.out.println("Applet.destroy() is called");
        this.player.close();
    }

    /**
     * This controllerUpdate function must be defined in order to implement a ControllerListener
     * interface. This function will be called whenever there is a media event
     */
    @Override
    public synchronized void controllerUpdate(final ControllerEvent event) {
        // If we're getting messages from a dead player,
        // just leave
        if (this.player == null) {
            return;
        }

        // When the player is Realized, get the visual
        // and control components and add them to the Applet
        if (event instanceof RealizeCompleteEvent) {
            if (this.progressBar != null) {
                remove(this.progressBar);
                this.progressBar = null;
            }

            int width = 320;
            int height = 0;
            if (this.controlComponent == null) {
                if ((this.controlComponent = this.player.getControlPanelComponent()) != null) {

                    this.controlPanelHeight = this.controlComponent.getPreferredSize().height;
                    add(this.controlComponent, BorderLayout.NORTH);
                    height += this.controlPanelHeight;
                }
            }
            if (this.visualComponent == null) {
                if ((this.visualComponent = this.player.getVisualComponent()) != null) {
                    add(this.visualComponent, BorderLayout.CENTER);
                    final Dimension videoSize = this.visualComponent.getPreferredSize();
                    this.videoWidth = videoSize.width;
                    this.videoHeight = videoSize.height;
                    width = this.videoWidth;
                    height += this.videoHeight;
                    this.visualComponent.setBounds(0, 0, this.videoWidth, this.videoHeight);
                }
            }

            if (this.controlComponent != null) {
                this.controlComponent
                .setBounds(0, this.videoHeight, width, this.controlPanelHeight);
                this.controlComponent.invalidate();
            }

        } else if (event instanceof CachingControlEvent) {
            if (this.player.getState() > Controller.Realizing) {
                return;
            }
            // Put a progress bar up when downloading starts,
            // take it down when downloading ends.
            final CachingControlEvent e = (CachingControlEvent) event;
            final CachingControl cc = e.getCachingControl();

            // Add the bar if not already there ...
            if (this.progressBar == null) {
                if ((this.progressBar = cc.getControlComponent()) != null) {
                    add(this.progressBar, BorderLayout.SOUTH);
                    validate();
                }
            }
        } else if (event instanceof EndOfMediaEvent) {
            // We've reached the end of the media; rewind and
            // start over
            this.player.setMediaTime(new Time(0));
            this.player.start();
        } else if (event instanceof ControllerErrorEvent) {
            // Tell TypicalPlayerApplet.start() to call it a day
            this.player = null;
            Fatal(((ControllerErrorEvent) event).getMessage());
        } else if (event instanceof ControllerClosedEvent) {
            removeAll();
        }
    }

    void Fatal(final String s) {
        // Applications will make various choices about what
        // to do here. We print a message
        System.err.println("FATAL ERROR: " + s);
    }
}
