/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

import java.util.EventObject;

/**
 * Defines an event that encapsulates changes to a list model.
 *
 * @param <ElementType>
 *            Type of the elements in model.
 */
public class ListModelEvent<ElementType> extends EventObject {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -971971177539474816L;

    /**
     * Event type.
     */
    public enum Type {
        /** Identifies one or more changes in the lists contents. */
        CONTENTS_CHANGED,
        /** Identifies the addition of one or more contiguous items to the list */
        INTERVAL_ADDED,
        /** Identifies the removal of one or more contiguous items from the list */
        INTERVAL_REMOVED
    }

    /** Event type. */
    private final Type type;

    /** Lower end of the new interval. */
    private final int index0;

    /** Upper end of the new interval. */
    private final int index1;

    /**
     * Constructs a event object.
     *
     * @param source
     *            Model being source of this event.
     * @param type
     *            Type of event.
     * @param index0
     *            Lower end of the interval (inclusive).
     * @param index1
     *            Upper end of the interval (inclusive).
     */
    public ListModelEvent(ListModel<ElementType> source, Type type, int index0, int index1) {
        super(source);

        if (type == null) {
            throw new NullPointerException("Null type given");
        }
        if (index0 > index1) {
            throw new IllegalArgumentException("Invalid interval: " + index0 + " > " + index1);
        }

        this.type = type;
        this.index0 = Math.min(index0, index1);
        this.index1 = Math.max(index0, index1);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ListModel<ElementType> getSource() {
        return (ListModel<ElementType>) super.getSource();
    }

    /**
     * Returns the event type.
     *
     * @return Event type.
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Returns the lower index of the range.
     *
     * <p>
     * For a single element, this value is the same as that returned by {@link #getIndex1}.
     * </p>
     *
     * @return An int representing the lower index value.
     */
    public int getIndex0() {
        return this.index0;
    }

    /**
     * Returns the upper index of the range.
     *
     * <p>
     * For a single element, this value is the same as that returned by {@link #getIndex0}.
     * </p>
     *
     * @return An int representing the upper index value.
     */
    public int getIndex1() {
        return this.index1;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[type=" + this.type + ",index0=" + this.index0 + ",index1=" + this.index1 + "]";
    }
}
