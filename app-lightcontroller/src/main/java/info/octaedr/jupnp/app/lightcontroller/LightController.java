/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller;

import info.octaedr.jcommons.logging.LogManager;
import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.AboutPerson;
import info.octaedr.jupnp.app.lightcontroller.engine.LightControllerEngine;
import info.octaedr.jupnp.app.lightcontroller.ui.LightControllerFrame;

/**
 * Light controller application.
 *
 * @author Krzysztof Kapuscik
 */
public class LightController {

    private static Logger logger = Logger.getLogger(LightController.class);

    /**
     * Main function.
     *
     * @param args
     *            Execution arguments.
     */
    public static void main(final String[] args) {
        try {
            LogManager.init();

            /* prepare about data */
            final AboutData aboutData = new AboutData("LightController", "0.1");
            aboutData.addAuthor(new AboutPerson("Krzysztof Kapuscik", "Author",
                    "k.kapuscik@gmail.com"));

            /* create engine */
            final LightControllerEngine engine = new LightControllerEngine();

            /* create main frame */
            final LightControllerFrame mainFrame = new LightControllerFrame(engine, aboutData);

            /* show main frame */
            mainFrame.setVisible(true);
        } catch (final Throwable t) {
            if (logger.severe()) {
                logger.severe("Exception catched: " + t);
            }
        }
    }

}
