/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.ui;

import info.octaedr.jswing.AboutData;
import info.octaedr.jswing.MainFrame;
import info.octaedr.jswing.SwingAction;
import info.octaedr.jswing.SwingActionExecutor;
import info.octaedr.jupnp.app.lightcontroller.engine.LightControllerEngine;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;

/**
 * Main frame of DeviceSpy.
 */
public class LightControllerFrame extends MainFrame {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -7585534461810921572L;

    private final LightControllerEngine engine;

    private SwingAction actionAppExit;

    private SwingAction actionAppAbout;

    private SwingAction actionRescanNetwork;

    private SwingAction actionShowIcon;

    private SwingAction actionExecuteAction;

    private SwingAction actionShowServiceDescription;

    private SwingAction actionShowDeviceDescription;

    private SwingAction actionShowDevicePresentationPage;

    private DeviceListPanel deviceList;

    //
    // private ElementTree elementTree;
    //
    // private PropertyTable propertyTable;
    //
    // private EventTable eventTable;

    private JPopupMenu popupMenuIcon;

    private JPopupMenu popupMenuAction;

    private JPopupMenu popupMenuDevice;

    private JPopupMenu popupMenuService;

    public LightControllerFrame(final LightControllerEngine engine, final AboutData aboutData) {
        super(aboutData);

        this.engine = engine;

        createActions();
        createMenu();
        createPopupMenus();
        createComponents();
        setupListeners();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(final WindowEvent e) {
                engine.terminate();
            }
        });

        engine.start();
    }

    private void createPopupMenus() {
        this.popupMenuIcon = new JPopupMenu("Icon Menu");
        this.popupMenuIcon.add(this.actionShowIcon);

        this.popupMenuAction = new JPopupMenu("Action Menu");
        this.popupMenuAction.add(this.actionExecuteAction);

        this.popupMenuService = new JPopupMenu("Service Menu");
        this.popupMenuService.add(this.actionShowServiceDescription);

        this.popupMenuDevice = new JPopupMenu("Device Menu");
        this.popupMenuDevice.add(this.actionShowDeviceDescription);
        this.popupMenuDevice.add(this.actionShowDevicePresentationPage);
    }

    private void setupListeners() {
        // this.elementTree.addTreeSelectionListener(new TreeSelectionListener() {
        //
        // public void valueChanged(TreeSelectionEvent e) {
        // TreeNode selectedNode = null;
        //
        // TreePath path = e.getPath();
        // if (path != null) {
        // if (path.getLastPathComponent() instanceof TreeNode) {
        // selectedNode = (TreeNode) path.getLastPathComponent();
        // }
        // }
        //
        // LightControllerFrame.this.engine.setPropertyNode(selectedNode);
        // }
        //
        // });
        //
        // this.elementTree.addPopupTriggerListener(new PopupTriggerListener() {
        // public void popupTriggered(PopupTriggerEvent event) {
        // Object selectedObject = event.getSelectedObject();
        //
        // if (selectedObject instanceof RemoteActionNode) {
        //
        // RemoteActionNode treeNode = (RemoteActionNode) selectedObject;
        // info.octaedr.jupnp.cp.Action remoteAction = treeNode.getRemoteAction();
        // triggerActionPopupMenu(event.getComponent(), event.getX(), event.getY(), remoteAction);
        //
        // } else if (selectedObject instanceof ActionArgumentInfoNode) {
        //
        // ActionArgumentInfoNode treeNode = (ActionArgumentInfoNode) selectedObject;
        // Action remoteAction = treeNode.getActionArgumentInfo().getAction();
        // triggerActionPopupMenu(event.getComponent(), event.getX(), event.getY(), remoteAction);
        //
        // } else if (selectedObject instanceof RemoteIconNode) {
        //
        // RemoteIconNode treeNode = (RemoteIconNode) selectedObject;
        // DeviceIcon iconInfo = treeNode.getIconInfo();
        // triggerIconPopupMenu(event.getComponent(), event.getX(), event.getY(), iconInfo);
        //
        // } else if (selectedObject instanceof RemoteDeviceNode) {
        //
        // RemoteDeviceNode treeNode = (RemoteDeviceNode) selectedObject;
        // Device remoteDevice = treeNode.getRemoteDevice();
        // triggerDevicePopupMenu(event.getComponent(), event.getX(), event.getY(), remoteDevice);
        //
        // } else if (selectedObject instanceof RemoteServiceNode) {
        //
        // RemoteServiceNode treeNode = (RemoteServiceNode) selectedObject;
        // Service serviceInfo = treeNode.getServiceInfo();
        // triggerServicePopupMenu(event.getComponent(), event.getX(), event.getY(), serviceInfo);
        //
        // } else if (selectedObject instanceof RemoteStateVariableNode) {
        //
        // }
        // }
        //
        // });
    }

    // private void triggerDevicePopupMenu(Component component, int x, int y, Device RemoteDevice) {
    // /* init */
    // this.actionShowDeviceDescription.setUserObject(RemoteDevice);
    // this.actionShowDevicePresentationPage.setUserObject(RemoteDevice);
    //
    // /* set status */
    // this.actionShowDevicePresentationPage
    // .setEnabled(RemoteDevice.getProperty(DeviceProperty.PRESENTATION_URL) != null);
    //
    // /* show popup */
    // this.popupMenuDevice.show(component, x, y);
    // }
    //
    // private void triggerServicePopupMenu(Component component, int x, int y, Service serviceInfo)
    // {
    // /* init */
    // this.actionShowServiceDescription.setUserObject(serviceInfo);
    //
    // /* show popup */
    // this.popupMenuService.show(component, x, y);
    // }
    //
    // private void triggerIconPopupMenu(Component component, int x, int y, DeviceIcon iconInfo) {
    // this.actionShowIcon.setUserObject(iconInfo);
    // this.popupMenuIcon.show(component, x, y);
    // }
    //
    // private void triggerActionPopupMenu(final Component component, final int x, final int y,
    // final info.octaedr.jupnp.cp.Action remoteAction) {
    // this.actionExecuteAction.setUserObject(remoteAction);
    // this.popupMenuAction.show(component, x, y);
    // }

    private void createComponents() {
        /* create functional components */
        this.deviceList = new DeviceListPanel(this.engine.getDeviceListModel());

        // this.elementTree = new ElementTree(this.engine.getElementTreeModel());
        // this.propertyTable = new PropertyTable(this.engine.getPropertyTableModel());
        // this.eventTable = new EventTable(this.engine.getEventTableModel());

        // /* build data split pane */
        // JSplitPane dataSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new
        // JScrollPane(this.propertyTable),
        // new JScrollPane(this.eventTable));
        // dataSplitPane.setDividerLocation(0.70);
        // dataSplitPane.setResizeWeight(0.5);
        //
        // /* build main split pane */
        // JSplitPane mainSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new
        // JScrollPane(this.elementTree),
        // dataSplitPane);
        // mainSplitPane.setDividerLocation(0.3);
        // mainSplitPane.setResizeWeight(0.5);
        // getContentPane().add(mainSplitPane);

        getContentPane().add(this.deviceList);
    }

    @SuppressWarnings("serial")
    private void createActions() {
        this.actionAppExit = new SwingAction();
        this.actionAppExit.setName("Exit");

        this.actionAppAbout = new SwingAction();
        this.actionAppAbout.setName("About");

        this.actionRescanNetwork = new SwingAction();
        this.actionRescanNetwork.setName("Rescan Network");

        this.actionAppExit.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                LightControllerFrame.this.dispose();
            }
        });

        this.actionAppAbout.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                LightControllerFrame.this.showAboutDialog();
            }
        });

        this.actionRescanNetwork.setActionExecutor(new SwingActionExecutor() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                LightControllerFrame.this.engine.rescanNetwork();
            }
        });

        // this.actionShowIcon = new SwingAction("Show Icon") {
        // public void actionPerformed(final ActionEvent e) {
        // DeviceIcon iconInfo = (DeviceIcon) getUserObject();
        // IconDialog iconDialog = new IconDialog(LightControllerFrame.this, iconInfo);
        // iconDialog.setVisible(true);
        // }
        // };
        //
        // this.actionExecuteAction = new SwingAction("Execute Action") {
        // public void actionPerformed(final ActionEvent e) {
        // Action remoteAction = (Action) getUserObject();
        //
        // ExecuteActionDialog executeActionDialog = new
        // ExecuteActionDialog(LightControllerFrame.this,
        // remoteAction);
        // executeActionDialog.setVisible(true);
        // }
        // };
        //
        // this.actionShowServiceDescription = new SwingAction("Show Description") {
        // public void actionPerformed(final ActionEvent e) {
        // Service serviceInfo = (Service) getUserObject();
        // DescriptionDialog descriptionDialog = new DescriptionDialog(LightControllerFrame.this,
        // serviceInfo);
        // descriptionDialog.setVisible(true);
        // }
        // };
        //
        // this.actionShowDeviceDescription = new SwingAction("Show Description") {
        // public void actionPerformed(final ActionEvent e) {
        // Device RemoteDevice = (Device) getUserObject();
        // DescriptionDialog descriptionDialog = new DescriptionDialog(LightControllerFrame.this,
        // RemoteDevice);
        // descriptionDialog.setVisible(true);
        // }
        // };
        //
        // this.actionShowDevicePresentationPage = new SwingAction("Show Presentation Page") {
        // public void actionPerformed(final ActionEvent e) {
        // Device RemoteDevice = (Device) getUserObject();
        // PresentationPageDialog dialog = new PresentationPageDialog(LightControllerFrame.this,
        // RemoteDevice);
        // dialog.setVisible(true);
        // }
        // };

    }

    private void createMenu() {
        /* create file menu */
        final JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(this.actionRescanNetwork);
        fileMenu.addSeparator();
        fileMenu.add(this.actionAppExit);

        /* create help menu */
        final JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        helpMenu.add(this.actionAppAbout);

        /* build menu bar */
        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        /* set menu bar */
        setJMenuBar(menuBar);
    }

}
