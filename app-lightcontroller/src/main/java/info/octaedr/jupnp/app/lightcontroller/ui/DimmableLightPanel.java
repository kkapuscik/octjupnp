/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.ui;

import info.octaedr.jupnp.app.lightcontroller.data.DimmerDeviceItem;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionExecutionFailedEvent;
import info.octaedr.jupnp.cp.ActionFailedEvent;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.ActionRequestListener;
import info.octaedr.jupnp.cp.ActionSucceededEvent;
import info.octaedr.jupnp.cp.light.DimmingService;
import info.octaedr.jupnp.cp.light.SetLoadLevelTargetArguments;
import info.octaedr.jupnp.cp.light.SetTargetArguments;
import info.octaedr.jupnp.cp.light.SwitchPowerService;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class DimmableLightPanel extends DeviceItemPanel {

    private final DimmerDeviceItem deviceItem;

    /**
     * Create the panel.
     *
     * @param item
     */
    public DimmableLightPanel(DimmerDeviceItem item) {
        this.deviceItem = item;

        TitledBorder panelBorder = new TitledBorder(null, "Device", TitledBorder.LEADING, TitledBorder.TOP, null, null);

        setBorder(panelBorder);
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 90, 90, 0 };
        gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        JLabel levelStatusLabel = new JLabel("Level Status");
        GridBagConstraints gbc_levelStatusLabel = new GridBagConstraints();
        gbc_levelStatusLabel.insets = new Insets(0, 0, 5, 5);
        gbc_levelStatusLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_levelStatusLabel.gridx = 0;
        gbc_levelStatusLabel.gridy = 0;
        add(levelStatusLabel, gbc_levelStatusLabel);

        JSlider levelStatusSlider = new JSlider();
        levelStatusSlider.setValue(0);
        levelStatusSlider.setEnabled(false);
        GridBagConstraints gbc_levelStatusSlider = new GridBagConstraints();
        gbc_levelStatusSlider.insets = new Insets(0, 0, 5, 0);
        gbc_levelStatusSlider.fill = GridBagConstraints.HORIZONTAL;
        gbc_levelStatusSlider.gridx = 1;
        gbc_levelStatusSlider.gridy = 0;
        add(levelStatusSlider, gbc_levelStatusSlider);

        JLabel levelTargetLabel = new JLabel("Level Target");
        GridBagConstraints gbc_levelTargetLabel = new GridBagConstraints();
        gbc_levelTargetLabel.insets = new Insets(0, 0, 5, 5);
        gbc_levelTargetLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_levelTargetLabel.gridx = 0;
        gbc_levelTargetLabel.gridy = 1;
        add(levelTargetLabel, gbc_levelTargetLabel);

        final JSlider levelTargetSlider = new JSlider();
        levelTargetSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                DimmableLightPanel.this.levelChangeRequested(levelTargetSlider.getValue());
            }
        });
        levelTargetSlider.setValue(0);
        GridBagConstraints gbc_levelTargetSlider = new GridBagConstraints();
        gbc_levelTargetSlider.insets = new Insets(0, 0, 5, 5);
        gbc_levelTargetSlider.fill = GridBagConstraints.HORIZONTAL;
        gbc_levelTargetSlider.gridx = 1;
        gbc_levelTargetSlider.gridy = 1;
        add(levelTargetSlider, gbc_levelTargetSlider);

        JCheckBox powerStatusCheckbox = new JCheckBox("New check box");
        powerStatusCheckbox.setEnabled(false);
        GridBagConstraints gbc_powerStatusCheckbox = new GridBagConstraints();
        gbc_powerStatusCheckbox.fill = GridBagConstraints.VERTICAL;
        gbc_powerStatusCheckbox.anchor = GridBagConstraints.WEST;
        gbc_powerStatusCheckbox.gridwidth = 2;
        gbc_powerStatusCheckbox.insets = new Insets(0, 0, 5, 5);
        gbc_powerStatusCheckbox.gridx = 0;
        gbc_powerStatusCheckbox.gridy = 2;
        add(powerStatusCheckbox, gbc_powerStatusCheckbox);

        final JCheckBox powerTargetCheckbox = new JCheckBox("New check box");
        powerTargetCheckbox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                DimmableLightPanel.this.powerChangeRequested(powerTargetCheckbox.isSelected());
            }
        });
        GridBagConstraints gbc_powerTargetCheckbox = new GridBagConstraints();
        gbc_powerTargetCheckbox.anchor = GridBagConstraints.WEST;
        gbc_powerTargetCheckbox.gridwidth = 2;
        gbc_powerTargetCheckbox.insets = new Insets(0, 0, 5, 5);
        gbc_powerTargetCheckbox.gridx = 0;
        gbc_powerTargetCheckbox.gridy = 3;
        add(powerTargetCheckbox, gbc_powerTargetCheckbox);

        panelBorder.setTitle(item.getDevice().getFriendlyName());
    }

    protected void powerChangeRequested(boolean selected) {
        SwitchPowerService service = this.deviceItem.getDevice().getSwitchPower();
        if (service != null) {
            try {
                ActionRequest<SetTargetArguments> request = service.createSetTargetRequest();
                request.getArguments().setTarget(selected);

                request.execute(new ActionRequestListener<SetTargetArguments>() {

                    @Override
                    public void actionSucceeded(ActionSucceededEvent<SetTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void actionFailed(ActionFailedEvent<SetTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void actionExecutionFailed(ActionExecutionFailedEvent<SetTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }
                });
            } catch (UpnpException e) {
                e.printStackTrace();
            }
        }
    }

    protected void levelChangeRequested(int value) {
        DimmingService service = this.deviceItem.getDevice().getDimming();
        if (service != null) {
            try {
                ActionRequest<SetLoadLevelTargetArguments> request = service.createSetLoadLevelTargetRequest();
                request.getArguments().setLoadLevelTarget((byte) value);

                request.execute(new ActionRequestListener<SetLoadLevelTargetArguments>() {

                    @Override
                    public void actionSucceeded(ActionSucceededEvent<SetLoadLevelTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void actionFailed(ActionFailedEvent<SetLoadLevelTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void actionExecutionFailed(ActionExecutionFailedEvent<SetLoadLevelTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }
                });
            } catch (UpnpException e) {
                e.printStackTrace();
            }
        }
    }

}
