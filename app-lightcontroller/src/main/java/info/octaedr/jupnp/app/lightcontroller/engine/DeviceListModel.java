/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.engine;

import info.octaedr.jupnp.app.lightcontroller.data.AbstractListModel;
import info.octaedr.jupnp.app.lightcontroller.data.DeviceItem;
import info.octaedr.jupnp.app.lightcontroller.data.DimmerDeviceItem;
import info.octaedr.jupnp.app.lightcontroller.data.SwitchDeviceItem;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.light.BinaryLightDevice;
import info.octaedr.jupnp.cp.light.DimmableLightDevice;

import java.util.ArrayList;

/**
 * Model representing light device list.
 *
 * @author Krzysztof Kapuscik
 */
public class DeviceListModel extends AbstractListModel<DeviceItem> {

    /**
     * Collection of model items.
     */
    private final ArrayList<DeviceItem> deviceItems;

    /**
     * Constructs new model.
     */
    DeviceListModel() {
        this.deviceItems = new ArrayList<DeviceItem>();
    }

    @Override
    public int getSize() {
        return this.deviceItems.size();
    }

    @Override
    public DeviceItem getElementAt(int index) {
        return this.deviceItems.get(index);
    }

    /**
     * Adds device to model.
     *
     * @param device
     *            Device to be added.
     */
    void addDevice(BinaryLightDevice device) {
        addDeviceItem(new SwitchDeviceItem(device));
    }

    /**
     * Adds device to model.
     *
     * @param device
     *            Device to be added.
     */
    void addDevice(DimmableLightDevice device) {
        addDeviceItem(new DimmerDeviceItem(device));
    }

    /**
     * Removes device from model.
     *
     * @param device
     *            Device to be removed. If device is not in model does nothing.
     */
    void removeDevice(Device device) {
        int index = indexOfDeviceItem(device);
        if (index >= 0) {
            removeDeviceItem(index);
        }
    }

    /**
     * Finds index of item for given device.
     *
     * @param device
     *            Device instance.
     *
     * @return Index of item for given device, -1 if not found.
     */
    private int indexOfDeviceItem(Device device) {
        for (int i = 0; i < getSize(); ++i) {
            if (getElementAt(i).getDevice().equals(device)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Adds item to model.
     *
     * <p>
     * The item is added at end of items collections.
     * </p>
     *
     * @param item
     *            Item to be added.
     */
    private void addDeviceItem(DeviceItem item) {
        int index = getSize();
        this.deviceItems.add(index, item);
        fireIntervalAdded(index, index);
    }

    /**
     * Removes item from model.
     *
     * @param index
     *            Index of the item to be removed.
     */
    private void removeDeviceItem(int index) {
        this.deviceItems.remove(index);
        fireIntervalRemoved(index, index);
    }

}
