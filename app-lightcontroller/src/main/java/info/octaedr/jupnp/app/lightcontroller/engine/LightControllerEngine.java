/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.engine;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.cp.ControlPoint;
import info.octaedr.jupnp.cp.ControlPointEvent;
import info.octaedr.jupnp.cp.ControlPointFactory;
import info.octaedr.jupnp.cp.ControlPointListener;
import info.octaedr.jupnp.cp.Device;
import info.octaedr.jupnp.cp.light.BinaryLightDevice;
import info.octaedr.jupnp.cp.light.DimmableLightDevice;
import info.octaedr.jupnp.cp.light.LightPluginFactory;

import java.awt.EventQueue;

/**
 * Engine (document) of light controller
 *
 * @author Krzysztof Kapuscik
 */
public class LightControllerEngine {

    /** Logger for printing debug messages. */
    private static final Logger logger = Logger.getLogger(LightControllerEngine.class);

    /** Control point used by engine. */
    private final ControlPoint controlPoint;

    /** Model with discovered devices. */
    private final DeviceListModel deviceListModel;

    /**
     * Constructs the engine.
     */
    public LightControllerEngine() {
        this.controlPoint = createControlPoint();
        this.deviceListModel = new DeviceListModel();

        if (logger.trace()) {
            logger.trace("Engine created");
        }
    }

    /**
     * Starts the engine.
     */
    public void start() {
        if (logger.trace()) {
            logger.trace("start()");
        }

        this.controlPoint.start();

        if (logger.trace()) {
            logger.trace("start() - finished");
        }
    }

    /**
     * Terminates the engine.
     */
    public void terminate() {
        if (logger.trace()) {
            logger.trace("stop()");
        }

        this.controlPoint.stop();

        if (logger.trace()) {
            logger.trace("stop() - finished");
        }
    }

    /**
     * Rescans the network looking for new devices.
     */
    public void rescanNetwork() {
        if (logger.trace()) {
            logger.trace("rescanNetwork()");
        }

        this.controlPoint.rescanNetwork();
    }

    /**
     * Constructs control point to be used by engine.
     *
     * @return Created control point.
     */
    private ControlPoint createControlPoint() {
        /* initialize factory */
        final ControlPointFactory cpFactory = ControlPointFactory.getInstance();
        cpFactory.registerPlugin(LightPluginFactory.getInstance().createPlugin());

        /* create control point */
        final ControlPoint cp = cpFactory.createControlPoint();

        /* setup listeners for device addition/removal */
        cp.addControlPointListener(new ControlPointListener() {

            @Override
            public void rootDeviceAdded(final ControlPointEvent event) {
                // ignore
            }

            @Override
            public void rootDeviceRemoved(final ControlPointEvent event) {
                // ignore
            }

            @Override
            public void deviceAdded(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        LightControllerEngine.this.deviceAdded(event);
                    }
                });
            }

            @Override
            public void deviceRemoved(final ControlPointEvent event) {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        LightControllerEngine.this.deviceRemoved(event);
                    }
                });
            }

        });

        return cp;
    }

    /**
     * Processes event about device addition.
     *
     * @param event
     *            Event description.
     */
    private void deviceAdded(final ControlPointEvent event) {
        if (logger.trace()) {
            logger.trace("deviceAdded() device=" + event.getDevice());
        }

        final Device device = event.getDevice();

        if (device instanceof BinaryLightDevice) {
            if (logger.trace()) {
                logger.trace("deviceAdded() BinaryLightDevice = " + device);
            }

            this.deviceListModel.addDevice((BinaryLightDevice) device);
        } else if (device instanceof DimmableLightDevice) {
            if (logger.trace()) {
                logger.trace("deviceAdded() DimmableLightDevice = " + device);
            }

            this.deviceListModel.addDevice((DimmableLightDevice) device);
        }
    }

    /**
     * Processes event about device removal.
     *
     * @param event
     *            Event description.
     */
    private void deviceRemoved(final ControlPointEvent event) {
        if (logger.trace()) {
            logger.trace("deviceRemoved() device=" + event.getDevice());
        }

        final Device device = event.getDevice();

        this.deviceListModel.removeDevice(device);
    }

    /**
     * Returns light devices model.
     *
     * @return Model instance.
     */
    public DeviceListModel getDeviceListModel() {
        return this.deviceListModel;
    }

}
