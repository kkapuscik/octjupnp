/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.ui;

import info.octaedr.jupnp.app.lightcontroller.data.DeviceItem;
import info.octaedr.jupnp.app.lightcontroller.data.DimmerDeviceItem;
import info.octaedr.jupnp.app.lightcontroller.data.ListModelEvent;
import info.octaedr.jupnp.app.lightcontroller.data.ListModelListener;
import info.octaedr.jupnp.app.lightcontroller.data.SwitchDeviceItem;
import info.octaedr.jupnp.app.lightcontroller.engine.DeviceListModel;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class DeviceListPanel extends JPanel {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -5907055328639298264L;

    private static class Entry {
        // public DeviceItem deviceItem;
        public DeviceItemPanel itemPanel;

        public Entry(DeviceItem item, DeviceItemPanel panel) {
            // this.deviceItem = item;
            this.itemPanel = panel;
        }
    }

    private final DeviceListModel deviceListModel;
    private final ArrayList<Entry> entries;
    private JPanel entriesPanel;

    public DeviceListPanel(DeviceListModel deviceListModel) {
        /* create elements */
        this.entries = new ArrayList<Entry>();

        /* remember the model */
        this.deviceListModel = deviceListModel;

        setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane = new JScrollPane();
        add(scrollPane, BorderLayout.CENTER);

        entriesPanel = new JPanel();
        scrollPane.setViewportView(entriesPanel);
        entriesPanel.setLayout(new GridLayout(0, 1, 0, 0));

        /* monitor model changes */
        this.deviceListModel.addListDataListener(new ListModelListener<DeviceItem>() {

            @Override
            public void intervalRemoved(ListModelEvent<DeviceItem> event) {
                DeviceListPanel.this.processIntervalRemoved(event.getIndex0(), event.getIndex1());
            }

            @Override
            public void intervalAdded(ListModelEvent<DeviceItem> event) {
                DeviceListPanel.this.processIntervalAdded(event.getIndex0(), event.getIndex1());
            }

            @Override
            public void contentsChanged(ListModelEvent<DeviceItem> event) {
                DeviceListPanel.this.processContentsChange();
            }
        });

        /* refresh contents */
        processContentsChange();
    }

    /**
     * Processes complete contents change.
     */
    protected void processContentsChange() {
        /* remove old contents */
        if (this.entries.size() > 0) {
            processIntervalRemoved(0, this.entries.size() - 1);
        }

        /* add new contents */
        if (this.deviceListModel.getSize() > 0) {
            processIntervalAdded(0, this.deviceListModel.getSize() - 1);
        }
    }

    /**
     * Processes elements addition.
     *
     * @param index0
     *            Start index (inclusive) of added elements range.
     * @param index1
     *            Last index (inclusive) of added elements change.
     */
    protected void processIntervalAdded(int index0, int index1) {
        /* normal order to make sure indexes will not change due to addition */
        for (int i = index0; i <= index1; ++i) {
            DeviceItem item = this.deviceListModel.getElementAt(i);
            DeviceItemPanel panel;

            if (item instanceof DimmerDeviceItem) {
                panel = new DimmableLightPanel((DimmerDeviceItem) item);
            } else if (item instanceof SwitchDeviceItem) {
                panel = new SwitchPowerPanel((SwitchDeviceItem) item);
            } else {
                throw new IllegalArgumentException("Invalid item type: " + item);
            }

            this.entriesPanel.add(panel, i);
            this.entries.add(i, new Entry(item, panel));
        }
        this.entriesPanel.validate();
    }

    /**
     * Processes elements addition.
     *
     * @param index0
     *            Start index (inclusive) of removed elements range.
     * @param index1
     *            Last index (inclusive) of removed elements change.
     */
    protected void processIntervalRemoved(int index0, int index1) {
        /* reverse order to make sure indexes will not change due to removal */
        for (int i = index1; i >= index0; --i) {
            Entry entry = this.entries.remove(i);
            this.entriesPanel.remove(entry.itemPanel);
        }
        this.entriesPanel.validate();
    }

}
