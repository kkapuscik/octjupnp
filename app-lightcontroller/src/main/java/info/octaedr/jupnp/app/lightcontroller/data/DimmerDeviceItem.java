/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

import info.octaedr.jupnp.cp.light.DimmableLightDevice;

public class DimmerDeviceItem extends DeviceItem {

    public DimmerDeviceItem(DimmableLightDevice device) {
        super(device);
    }

    @Override
    public DimmableLightDevice getDevice() {
        return (DimmableLightDevice) super.getDevice();
    }

}
