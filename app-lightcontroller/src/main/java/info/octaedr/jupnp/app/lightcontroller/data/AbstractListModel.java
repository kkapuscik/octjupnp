/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

import info.octaedr.jcommons.event.EventListenerManager;
import info.octaedr.jcommons.event.SynchronousEventDispatcher;

/**
 * Abstract list model implementation
 *
 * @author Krzysztof Kapuscik
 *
 * @param <ElementType>
 *            Type of the elements in modle.
 */
public abstract class AbstractListModel<ElementType> implements ListModel<ElementType> {

    /**
     * Manager of model listeners.
     */
    private class EventManager extends
    EventListenerManager<ListModelListener<ElementType>, ListModelEvent<ElementType>> {

        /**
         * Constructs new manager with synchronous event dispatcher.
         */
        protected EventManager() {
            super(
                    new SynchronousEventDispatcher<ListModelListener<ElementType>, ListModelEvent<ElementType>>() {
                        @Override
                        protected void notifyEvent(final ListModelListener<ElementType> listener,
                                final ListModelEvent<ElementType> event) {
                            AbstractListModel.this.notifyEvent(listener, event);
                        }
                    });
        }
    }

    /**
     * Event listeners manager.
     */
    private final EventManager eventManager;

    /**
     * Constructs new model.
     */
    protected AbstractListModel() {
        this.eventManager = new EventManager();
    }

    @Override
    public void addListDataListener(final ListModelListener<ElementType> listener) {
        this.eventManager.addListener(listener);
    }

    @Override
    public void removeListDataListener(final ListModelListener<ElementType> listener) {
        this.eventManager.removeListener(listener);
    }

    /**
     * Notifies listener about an event.
     *
     * @param listener
     *            Listener to be notified.
     * @param event
     *            Event descriptor.
     */
    private void notifyEvent(final ListModelListener<ElementType> listener,
            final ListModelEvent<ElementType> event) {
        switch (event.getType()) {
        case INTERVAL_ADDED:
            listener.intervalAdded(event);
            break;

        case INTERVAL_REMOVED:
            listener.intervalRemoved(event);
            break;

        case CONTENTS_CHANGED:
            listener.contentsChanged(event);
            break;
        }
    }

    /**
     * Sends CONTENTS_CHANGED event to all registered listeners about.
     */
    protected void fireContentsChanged() {
        this.eventManager.scheduleEvent(new ListModelEvent<ElementType>(this,
                ListModelEvent.Type.CONTENTS_CHANGED, -1, -1));
    }

    /**
     * Sends INTERVAL_ADDED event to all registered listeners about.
     *
     * @param index0
     *            Lower end of the interval (inclusive).
     * @param index1
     *            Upper end of the interval (inclusive).
     */
    protected void fireIntervalAdded(final int index0, final int index1) {
        this.eventManager.scheduleEvent(new ListModelEvent<ElementType>(this,
                ListModelEvent.Type.INTERVAL_ADDED, index0, index1));
    }

    /**
     * Sends INTERVAL_REMOVED event to all registered listeners about.
     *
     * @param index0
     *            Lower end of the interval (inclusive).
     * @param index1
     *            Upper end of the interval (inclusive).
     */
    protected void fireIntervalRemoved(final int index0, final int index1) {
        this.eventManager.scheduleEvent(new ListModelEvent<ElementType>(this,
                ListModelEvent.Type.INTERVAL_REMOVED, index0, index1));
    }

}
