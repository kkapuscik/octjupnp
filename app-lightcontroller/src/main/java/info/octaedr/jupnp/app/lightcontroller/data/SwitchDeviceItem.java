/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

import info.octaedr.jupnp.cp.light.BinaryLightDevice;

public class SwitchDeviceItem extends DeviceItem {

    public SwitchDeviceItem(BinaryLightDevice device) {
        super(device);
    }

    @Override
    public BinaryLightDevice getDevice() {
        return (BinaryLightDevice) super.getDevice();
    }
}
