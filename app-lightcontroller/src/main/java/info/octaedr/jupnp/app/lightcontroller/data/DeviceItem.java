/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

import info.octaedr.jupnp.cp.Device;

public class DeviceItem {

    private final Device device;

    protected DeviceItem(Device device) {
        this.device = device;
    }

    public Device getDevice() {
        return this.device;
    }

}
