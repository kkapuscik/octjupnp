/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

/**
 * The base interface for all list model.
 *
 * <p>
 * This interface defines the methods components like JList use to get the value of each cell in a list and the length
 * of the list. Logically the model is a vector, indices vary from 0 to ListDataModel.getSize() - 1. Any change to the
 * contents or length of the data model must be reported to all of the ListDataListeners.
 * </p>
 *
 * @param <ElementType>
 *            Type of the elements in model.
 */
public interface ListModel<ElementType> {

    /**
     * Returns the length of the list.
     *
     * @return The length of the list.
     */
    int getSize();

    /**
     * Returns the value at the specified index.
     *
     * @param index
     *            The requested index.
     *
     * @return The value at <code>index</code>.
     */
    ElementType getElementAt(int index);

    /**
     * Adds a listener to the list that's notified each time a change to the data model occurs.
     *
     * @param listener
     *            The <code>ListDataListener</code> to be added.
     */
    void addListDataListener(ListModelListener<ElementType> listener);

    /**
     * Removes a listener from the list that's notified each time a change to the data model occurs.
     *
     * @param listener
     *            The <code>ListDataListener</code> to be removed.
     */
    void removeListDataListener(ListModelListener<ElementType> listener);

}
