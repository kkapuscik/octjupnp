/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.ui;

import info.octaedr.jupnp.app.lightcontroller.data.SwitchDeviceItem;
import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionExecutionFailedEvent;
import info.octaedr.jupnp.cp.ActionFailedEvent;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.ActionRequestListener;
import info.octaedr.jupnp.cp.ActionSucceededEvent;
import info.octaedr.jupnp.cp.light.SetTargetArguments;
import info.octaedr.jupnp.cp.light.SwitchPowerService;

import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class SwitchPowerPanel extends DeviceItemPanel {

    private final SwitchDeviceItem deviceItem;

    /**
     * Create the panel.
     *
     * @param item
     */
    public SwitchPowerPanel(SwitchDeviceItem item) {
        this.deviceItem = item;

        TitledBorder panelBorder = new TitledBorder(null, "Device", TitledBorder.LEADING, TitledBorder.TOP, null, null);
        setBorder(panelBorder);
        setLayout(new GridLayout(0, 1, 0, 0));

        JCheckBox statusCheckBox = new JCheckBox("Status");
        statusCheckBox.setEnabled(false);
        add(statusCheckBox);

        final JCheckBox targetCheckBox = new JCheckBox("Target");
        targetCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                SwitchPowerPanel.this.targetChangeRequested(targetCheckBox.isSelected());
            }
        });
        add(targetCheckBox);

        panelBorder.setTitle(item.getDevice().getFriendlyName());
    }

    protected void targetChangeRequested(boolean selected) {
        SwitchPowerService service = this.deviceItem.getDevice().getSwitchPower();
        if (service != null) {
            try {
                ActionRequest<SetTargetArguments> request = service.createSetTargetRequest();
                request.getArguments().setTarget(selected);
                request.execute(new ActionRequestListener<SetTargetArguments>() {

                    @Override
                    public void actionSucceeded(ActionSucceededEvent<SetTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void actionFailed(ActionFailedEvent<SetTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void actionExecutionFailed(ActionExecutionFailedEvent<SetTargetArguments> event) {
                        // TODO Auto-generated method stub

                    }
                });
            } catch (UpnpException e) {
                e.printStackTrace();
            }
        }
    }

}
