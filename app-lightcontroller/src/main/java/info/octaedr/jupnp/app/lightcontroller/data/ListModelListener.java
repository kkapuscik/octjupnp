/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-app-lightcontroller project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.app.lightcontroller.data;

import java.util.EventListener;

/**
 * Listener of {@link ListModel} events.
 *
 * @param <ElementType>
 *            Type of the elements in model.
 */
public interface ListModelListener<ElementType> extends EventListener {

    /**
     * Sent after the indices in the index0,index1 interval have been inserted in the data model.
     *
     * <p>
     * The new interval includes both index0 and index1.
     * </p>
     *
     * @param event
     *            Event descriptor.
     */
    void intervalAdded(ListModelEvent<ElementType> event);

    /**
     * Sent after the indices in the index0,index1 interval have been removed from the data model.
     *
     * <p>
     * The interval includes both index0 and index1.
     * </p>
     *
     * @param event
     *            Event descriptor.
     */
    void intervalRemoved(ListModelEvent<ElementType> event);

    /**
     * Sent when there was a complex modification of the list contents.
     * <p>
     * Sent when the contents of the list has changed in a way that's too complex to characterize with the previous
     * methods. For example, this is sent when an item has been replaced. Index0 and index1 bracket the change.
     * </p>
     *
     * @param event
     *            Event descriptor.
     */
    void contentsChanged(ListModelEvent<ElementType> event);

}
