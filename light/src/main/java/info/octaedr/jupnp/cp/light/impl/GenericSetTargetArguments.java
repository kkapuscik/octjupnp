/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;
import info.octaedr.jupnp.cp.light.SetTargetArguments;

/**
 * Generic implementation of {@link SetTargetArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericSetTargetArguments extends GenericActionRequestArguments implements SetTargetArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericSetTargetArguments(Action action) {
        super(action);
    }

    @Override
    public void setTarget(boolean target) throws UpnpException {
        setInputArgumentValue("newTargetValue", target ? Boolean.TRUE : Boolean.FALSE);
    }

}
