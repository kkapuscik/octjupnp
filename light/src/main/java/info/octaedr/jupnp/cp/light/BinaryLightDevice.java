/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.cp.Device;

/**
 * BinaryLight UPnP device.
 *
 * @author Krzysztof Kapuscik
 */
public interface BinaryLightDevice extends Device {

    /** Binary light version 1 device type. */
    public static final String DEVICE_TYPE_BINARY_LIGHT_1 = "urn:schemas-upnp-org:device:BinaryLight:1";

    /**
     * Returns SwitchPower service if available.
     *
     * @return Service object or null if not available.
     */
    SwitchPowerService getSwitchPower();

}
