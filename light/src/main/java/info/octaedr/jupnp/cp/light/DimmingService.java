/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.Service;

/**
 * Dimming UPnP service.
 *
 * @author Krzysztof Kapuscik
 */
public interface DimmingService extends Service {

    /** Dimming version 1 service type. */
    public static final String SERVICE_TYPE_DIMMING_1 = "urn:schemas-upnp-org:service:Dimming:1";

    /**
     * Creates SetLoadLevelTarget action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<SetLoadLevelTargetArguments> createSetLoadLevelTargetRequest() throws UpnpException;

    /**
     * Creates GetLoadLevelTarget action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<GetLoadLevelTargetArguments> createGetLoadLevelTargetRequest() throws UpnpException;

    /**
     * Creates GetLoadLevelStatus action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<GetLoadLevelStatusArguments> createGetLoadLevelStatusRequest() throws UpnpException;

}
