/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;
import info.octaedr.jupnp.cp.impl.GenericDevice;
import info.octaedr.jupnp.cp.impl.GenericService;
import info.octaedr.jupnp.cp.impl.WrapperPluginAdapter;
import info.octaedr.jupnp.cp.light.BinaryLightDevice;
import info.octaedr.jupnp.cp.light.DimmableLightDevice;
import info.octaedr.jupnp.cp.light.DimmingService;
import info.octaedr.jupnp.cp.light.SwitchPowerService;

/**
 * Generic implementation of AV plugin.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericLightPlugin extends WrapperPluginAdapter implements ControlPointFactoryPlugin {

    /** Logger for printing debug messages. */
    private static final Logger logger = Logger.getLogger(GenericLightPlugin.class);

    @Override
    public GenericDevice wrap(final GenericDevice device) {
        GenericDevice rv = null;

        if (device.isCompatible(BinaryLightDevice.DEVICE_TYPE_BINARY_LIGHT_1)) {
            rv = new GenericBinaryLightDevice(device);
        } else if (device.isCompatible(DimmableLightDevice.DEVICE_TYPE_DIMMABLE_LIGHT_1)) {
            rv = new GenericDimmableLightDevice(device);
        }

        if (logger.trace()) {
            logger.trace("Wrapping device: " + device + " => " + rv);
        }

        return rv;
    }

    @Override
    public GenericService wrap(final GenericService service) {
        GenericService rv = null;

        if (service.isCompatible(SwitchPowerService.SERVICE_TYPE_SWITCH_POWER_1)) {
            rv = new GenericSwitchPowerService(service);
        } else if (service.isCompatible(DimmingService.SERVICE_TYPE_DIMMING_1)) {
            rv = new GenericDimmingService(service);
        }

        if (logger.trace()) {
            logger.trace("Wrapping service: " + service + " => " + rv);
        }

        return rv;
    }

}
