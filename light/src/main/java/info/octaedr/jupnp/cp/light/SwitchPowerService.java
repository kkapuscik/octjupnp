/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.Service;

/**
 * SwitchPower UPnP service.
 *
 * @author Krzysztof Kapuscik
 */
public interface SwitchPowerService extends Service {

    /** Switch power version 1 service type. */
    public static final String SERVICE_TYPE_SWITCH_POWER_1 = "urn:schemas-upnp-org:service:SwitchPower:1";

    /**
     * Creates GetStatus action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<GetStatusArguments> createGetStatusRequest() throws UpnpException;

    /**
     * Creates GetTarget action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<GetTargetArguments> createGetTargetRequest() throws UpnpException;

    /**
     * Creates SetTarget action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<SetTargetArguments> createSetTargetRequest() throws UpnpException;

}
