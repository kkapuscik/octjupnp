/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;
import info.octaedr.jupnp.cp.light.GetStatusArguments;

/**
 * Generic implementation of {@link GetStatusArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericGetStatusArguments extends GenericActionRequestArguments implements GetStatusArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericGetStatusArguments(Action action) {
        super(action);
    }

    @Override
    public boolean getStatus() throws UpnpException {
        return (Boolean) getOutputArgumentValue("ResultStatus");
    }

}
