/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.impl.GenericActionRequest;
import info.octaedr.jupnp.cp.impl.GenericService;
import info.octaedr.jupnp.cp.light.DimmingService;
import info.octaedr.jupnp.cp.light.GetLoadLevelStatusArguments;
import info.octaedr.jupnp.cp.light.GetLoadLevelTargetArguments;
import info.octaedr.jupnp.cp.light.SetLoadLevelTargetArguments;

/**
 * Generic implementation of {@link DimmingService}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericDimmingService extends GenericService implements DimmingService {

    /**
     * Constructs shallow copy of other service.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Service to be copied.
     */
    protected GenericDimmingService(GenericService other) {
        super(other);
    }

    @Override
    public ActionRequest<SetLoadLevelTargetArguments> createSetLoadLevelTargetRequest() throws UpnpException {
        Action action = getAction("SetLoadLevelTarget");
        if (action == null) {
            throw new UpnpException("Action is not available in service");
        }

        return new GenericActionRequest<SetLoadLevelTargetArguments>(new GenericSetLoadLevelTargetArguments(action));
    }

    @Override
    public ActionRequest<GetLoadLevelTargetArguments> createGetLoadLevelTargetRequest() throws UpnpException {
        Action action = getAction("GetLoadLevelTarget");
        if (action == null) {
            throw new UpnpException("Action is not available in service");
        }

        return new GenericActionRequest<GetLoadLevelTargetArguments>(new GenericGetLoadLevelTargetArguments(action));
    }

    @Override
    public ActionRequest<GetLoadLevelStatusArguments> createGetLoadLevelStatusRequest() throws UpnpException {
        Action action = getAction("GetLoadLevelStatus");
        if (action == null) {
            throw new UpnpException("Action is not available in service");
        }

        return new GenericActionRequest<GetLoadLevelStatusArguments>(new GenericGetLoadLevelStatusArguments(action));
    }

}
