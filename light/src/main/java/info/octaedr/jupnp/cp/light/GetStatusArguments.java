/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequestArguments;

/**
 * Arguments for PowerService GetStatus action.
 *
 * @author Krzysztof Kapuscik
 */
public interface GetStatusArguments extends ActionRequestArguments {

    /**
     * Returns status argument value.
     *
     * @return Value returned by remote service.
     *
     * @throws UpnpException
     *             on any UPnP related error.
     */
    boolean getStatus() throws UpnpException;

}
