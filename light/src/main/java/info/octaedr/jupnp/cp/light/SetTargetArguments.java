/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequestArguments;

/**
 * Arguments for PowerService SetTarget action.
 *
 * @author Krzysztof Kapuscik
 */
public interface SetTargetArguments extends ActionRequestArguments {

    /**
     * Sets target argument value.
     *
     * @param target
     *            Value to set.
     *
     * @throws UpnpException
     *             on any UPnP related error.
     */
    void setTarget(boolean target) throws UpnpException;

}
