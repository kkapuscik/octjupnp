/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequestArguments;

/**
 * Arguments for Dimming GetLoadLevelTarget action.
 *
 * @author Krzysztof Kapuscik
 */
public interface GetLoadLevelTargetArguments extends ActionRequestArguments {

    /**
     * Returns load level target argument value.
     *
     * @return Value returned by remote service.
     *
     * @throws UpnpException
     *             on any UPnP related error.
     */
    short getLoadLevelTarget() throws UpnpException;

}
