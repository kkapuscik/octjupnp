/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequestArguments;

/**
 * Arguments for Dimming SetLoadLevelTarget action.
 *
 * @author Krzysztof Kapuscik
 */
public interface SetLoadLevelTargetArguments extends ActionRequestArguments {

    /**
     * Sets load level target argument value.
     *
     * @param value
     *            Value to set.
     *
     * @throws UpnpException
     *             on any UPnP related error.
     */
    void setLoadLevelTarget(short value) throws UpnpException;

}
