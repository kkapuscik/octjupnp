/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light;

import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;
import info.octaedr.jupnp.cp.light.impl.GenericLightPluginFactory;

/**
 * Factory of UPnP light plugins.
 *
 * @author Krzysztof Kapuscik
 */
public abstract class LightPluginFactory {

    /**
     * Returns instance of plugins factory.
     *
     * @return Factory instance.
     */
    public static LightPluginFactory getInstance() {
        return GenericLightPluginFactory.getInstance();
    }

    /**
     * Constructs light plugin.
     *
     * @return Created plugin instance.
     */
    public abstract ControlPointFactoryPlugin createPlugin();

}
