/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;
import info.octaedr.jupnp.cp.light.GetLoadLevelTargetArguments;

/**
 * Generic implementation of {@link GetLoadLevelTargetArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericGetLoadLevelTargetArguments extends GenericActionRequestArguments implements
        GetLoadLevelTargetArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericGetLoadLevelTargetArguments(Action action) {
        super(action);
    }

    @Override
    public short getLoadLevelTarget() throws UpnpException {
        return (Short) getOutputArgumentValue("retLoadLevelTarget");
    }

}
