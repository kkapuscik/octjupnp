/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;
import info.octaedr.jupnp.cp.light.LightPluginFactory;

/**
 * Generic implementation of {@link LightPluginFactory}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericLightPluginFactory extends LightPluginFactory {

    /** Factory singleton. */
    private static LightPluginFactory theInstance;

    /**
     * Returns factory instance.
     *
     * @return Factory object.
     */
    public synchronized static LightPluginFactory getInstance() {
        if (theInstance == null) {
            theInstance = new GenericLightPluginFactory();
        }

        return theInstance;
    }

    @Override
    public ControlPointFactoryPlugin createPlugin() {
        return new GenericLightPlugin();
    }

}
