/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.impl.GenericDevice;
import info.octaedr.jupnp.cp.light.DimmableLightDevice;
import info.octaedr.jupnp.cp.light.DimmingService;
import info.octaedr.jupnp.cp.light.SwitchPowerService;

/**
 * Generic implementation of {@link DimmableLightDevice}.
 *
 * @author Krzysztof
 *
 */
public class GenericDimmableLightDevice extends GenericDevice implements DimmableLightDevice {

    /** Switch power service (null if not present). */
    private SwitchPowerService switchPower;
    /** Dimming service (null if not present). */
    private DimmingService dimming;

    /**
     * Constructs shallow copy of other device.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Device to be copied.
     */
    protected GenericDimmableLightDevice(GenericDevice other) {
        super(other);
    }

    @Override
    protected void postInit() {
        super.postInit();

        for (Service service : getServices()) {
            if (service instanceof DimmingService) {
                this.dimming = (DimmingService) service;
            } else if (service instanceof SwitchPowerService) {
                this.switchPower = (SwitchPowerService) service;
            }
        }
    }

    @Override
    public DimmingService getDimming() {
        return this.dimming;
    }

    @Override
    public SwitchPowerService getSwitchPower() {
        return this.switchPower;
    }

}
