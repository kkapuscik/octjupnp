/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;
import info.octaedr.jupnp.cp.light.SetLoadLevelTargetArguments;

/**
 * Generic implementation of {@link SetLoadLevelTargetArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericSetLoadLevelTargetArguments extends GenericActionRequestArguments implements
        SetLoadLevelTargetArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericSetLoadLevelTargetArguments(Action action) {
        super(action);
    }

    @Override
    public void setLoadLevelTarget(short value) throws UpnpException {
        setInputArgumentValue("NewLoadLevelTarget", value);
    }

}
