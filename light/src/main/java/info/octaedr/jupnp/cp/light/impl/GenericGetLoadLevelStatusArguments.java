/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;
import info.octaedr.jupnp.cp.light.GetLoadLevelStatusArguments;

/**
 * Generic implementation of {@link GetLoadLevelStatusArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericGetLoadLevelStatusArguments extends GenericActionRequestArguments implements
        GetLoadLevelStatusArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericGetLoadLevelStatusArguments(Action action) {
        super(action);
    }

    @Override
    public short getLoadLevelStatus() throws UpnpException {
        return (Short) getOutputArgumentValue("retLoadLevelStatus");
    }

}
