/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.impl.GenericActionRequest;
import info.octaedr.jupnp.cp.impl.GenericService;
import info.octaedr.jupnp.cp.light.GetStatusArguments;
import info.octaedr.jupnp.cp.light.GetTargetArguments;
import info.octaedr.jupnp.cp.light.SetTargetArguments;
import info.octaedr.jupnp.cp.light.SwitchPowerService;

/**
 * Generic implementation of {@link SwitchPowerService}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericSwitchPowerService extends GenericService implements SwitchPowerService {

    /**
     * Constructs shallow copy of other service.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Service to be copied.
     */
    protected GenericSwitchPowerService(GenericService other) {
        super(other);
    }

    @Override
    public ActionRequest<GetStatusArguments> createGetStatusRequest() throws UpnpException {
        Action action = getAction("GetStatus");
        if (action == null) {
            throw new UpnpException("Action GetStatus is not available in service");
        }

        return new GenericActionRequest<GetStatusArguments>(new GenericGetStatusArguments(action));
    }

    @Override
    public ActionRequest<GetTargetArguments> createGetTargetRequest() throws UpnpException {
        Action action = getAction("GetTarget");
        if (action == null) {
            throw new UpnpException("Action GetTarget is not available in service");
        }

        return new GenericActionRequest<GetTargetArguments>(new GenericGetTargetArguments(action));
    }

    @Override
    public ActionRequest<SetTargetArguments> createSetTargetRequest() throws UpnpException {
        Action action = getAction("SetTarget");
        if (action == null) {
            throw new UpnpException("Action GetStatus is not available in service");
        }

        return new GenericActionRequest<SetTargetArguments>(new GenericSetTargetArguments(action));
    }

}
