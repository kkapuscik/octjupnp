/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.light.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;
import info.octaedr.jupnp.cp.light.GetTargetArguments;

/**
 * Generic implementation of {@link GetTargetArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericGetTargetArguments extends GenericActionRequestArguments implements GetTargetArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericGetTargetArguments(Action action) {
        super(action);
    }

    @Override
    public boolean getTarget() throws UpnpException {
        return (Boolean) getOutputArgumentValue("RetTargetValue");
    }

}
