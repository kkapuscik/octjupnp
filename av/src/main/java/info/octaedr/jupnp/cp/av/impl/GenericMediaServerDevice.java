/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.av.AVTransportService;
import info.octaedr.jupnp.cp.av.ConnectionManagerService;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;
import info.octaedr.jupnp.cp.av.MediaServerDevice;
import info.octaedr.jupnp.cp.av.ScheduledRecordingService;
import info.octaedr.jupnp.cp.impl.GenericDevice;

class GenericMediaServerDevice extends GenericDevice implements MediaServerDevice {

    private ContentDirectoryService contentDirectory;
    private ConnectionManagerService connectionManager;
    private AVTransportService avTransport;
    private ScheduledRecordingService scheduledRecording;

    public GenericMediaServerDevice(GenericDevice device) {
        super(device);
    }

    @Override
    protected void postInit() {
        super.postInit();

        for (Service service : getServices()) {
            if (service instanceof ContentDirectoryService) {
                this.contentDirectory = (ContentDirectoryService) service;
            }
            if (service instanceof ConnectionManagerService) {
                this.connectionManager = (ConnectionManagerService) service;
            }
            if (service instanceof AVTransportService) {
                this.avTransport = (AVTransportService) service;
            }
            if (service instanceof ScheduledRecordingService) {
                this.scheduledRecording = (ScheduledRecordingService) service;
            }
        }
    }

    @Override
    public ContentDirectoryService getContentDirectory() {
        return this.contentDirectory;
    }

    @Override
    public ConnectionManagerService getConnectionManager() {
        return this.connectionManager;
    }

    @Override
    public AVTransportService getAVTransport() {
        return this.avTransport;
    }

    @Override
    public ScheduledRecordingService getScheduledRecording() {
        return this.scheduledRecording;
    }

}
