/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.av.BrowseArguments;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;
import info.octaedr.jupnp.cp.av.SearchArguments;
import info.octaedr.jupnp.cp.impl.GenericActionRequest;
import info.octaedr.jupnp.cp.impl.GenericService;

public class GenericContentDirectoryService extends GenericService implements ContentDirectoryService {

    /**
     * Constructs shallow copy of other service.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Service to be copied.
     */
    public GenericContentDirectoryService(GenericService other) {
        super(other);
    }

    @Override
    public ActionRequest<BrowseArguments> createBrowseRequest() throws UpnpException {
        Action action = getAction("Browse");
        if (action == null) {
            throw new UpnpException("Action Browse is not available in service");
        }

        return new GenericActionRequest<BrowseArguments>(new GenericBrowseArguments(action));
    }

    @Override
    public ActionRequest<SearchArguments> createSearchRequest() throws UpnpException {
        Action action = getAction("Search");
        if (action == null) {
            throw new UpnpException("Action Search is not available in service");
        }

        return new GenericActionRequest<SearchArguments>(new GenericSearchArguments(action));
    }

}
