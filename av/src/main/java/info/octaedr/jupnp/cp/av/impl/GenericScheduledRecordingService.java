/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.cp.av.ScheduledRecordingService;
import info.octaedr.jupnp.cp.impl.GenericService;

public class GenericScheduledRecordingService extends GenericService implements ScheduledRecordingService {

    /**
     * Constructs shallow copy of other service.
     *
     * <p>
     * This constructor shall be used for wrapping.
     * </p>
     *
     * @param other
     *            Service to be copied.
     */
    public GenericScheduledRecordingService(GenericService other) {
        super(other);
    }

}
