/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;
import info.octaedr.jupnp.cp.av.AVPluginFactory;

public class GenericAVPluginFactory extends AVPluginFactory {

    private static AVPluginFactory theInstance;

    public synchronized static AVPluginFactory getInstance() {
        if (theInstance == null) {
            theInstance = new GenericAVPluginFactory();
        }

        return theInstance;
    }

    @Override
    public ControlPointFactoryPlugin createPlugin() {
        return new GenericAVPlugin();
    }

}
