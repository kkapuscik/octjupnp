/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av;

import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;
import info.octaedr.jupnp.cp.av.impl.GenericAVPluginFactory;

/**
 * Factory of UPnP AV plugins.
 *
 * @author Krzysztof Kapuscik
 */
public abstract class AVPluginFactory {

    /**
     * Returns instance of plugins factory.
     *
     * @return Factory instance.
     */
    public static AVPluginFactory getInstance() {
        return GenericAVPluginFactory.getInstance();
    }

    /**
     * Constructs AV plugin.
     *
     * @return Created plugin instance.
     */
    public abstract ControlPointFactoryPlugin createPlugin();

}
