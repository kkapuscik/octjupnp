/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequestArguments;
import info.octaedr.jupnp.cp.av.cds.ContentObject;

/**
 * Arguments for ContentDirectoryService Browse action.
 *
 * @author Krzysztof Kapuscik
 */
public interface BrowseArguments extends ActionRequestArguments {

    enum BrowseFlag {
        BROWSE_DIRECT_CHILDREN,
        BROWSE_METADATA
    }

    void setObjectID(String objectID) throws UpnpException;
    void setBrowseFlag(BrowseFlag flag) throws UpnpException;
    void setFilter(String filter) throws UpnpException;
    void setStartingIndex(long index) throws UpnpException;
    void setRequestedCount(long count) throws UpnpException;
    void setSortCriteria(String criteria) throws UpnpException;

    String getResult() throws UpnpException;
    long getNumberReturned() throws UpnpException;
    long getTotalMatches() throws UpnpException;
    long getUpdateID() throws UpnpException;

    ContentObject[] getResultObjects() throws UpnpException;

}
