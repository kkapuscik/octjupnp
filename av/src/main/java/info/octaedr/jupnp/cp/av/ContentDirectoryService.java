/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.ActionRequest;
import info.octaedr.jupnp.cp.Service;

public interface ContentDirectoryService extends Service {

    public static final String SERVICE_TYPE_CONTENT_DIRECTORY_1 = "urn:schemas-upnp-org:service:ContentDirectory:1";

    /**
     * Creates Browse action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<BrowseArguments> createBrowseRequest() throws UpnpException;

    /**
     * Creates Search action request.
     *
     * @return Created request.
     *
     * @throws UpnpException
     *             if action was not available or any other error occurred.
     */
    ActionRequest<SearchArguments> createSearchRequest() throws UpnpException;

}
