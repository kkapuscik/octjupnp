/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av;

import info.octaedr.jupnp.cp.Service;

public interface ScheduledRecordingService extends Service {

    public static final String SERVICE_TYPE_SCHEDULED_RECORDING_1 = "urn:schemas-upnp-org:service: ScheduledRecording:1";

}
