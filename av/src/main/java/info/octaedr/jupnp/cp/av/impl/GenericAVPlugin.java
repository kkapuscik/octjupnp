/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jcommons.logging.Logger;
import info.octaedr.jupnp.cp.ControlPointFactoryPlugin;
import info.octaedr.jupnp.cp.av.AVTransportService;
import info.octaedr.jupnp.cp.av.ConnectionManagerService;
import info.octaedr.jupnp.cp.av.ContentDirectoryService;
import info.octaedr.jupnp.cp.av.MediaRendererDevice;
import info.octaedr.jupnp.cp.av.MediaServerDevice;
import info.octaedr.jupnp.cp.av.RenderingControlService;
import info.octaedr.jupnp.cp.av.ScheduledRecordingService;
import info.octaedr.jupnp.cp.impl.GenericDevice;
import info.octaedr.jupnp.cp.impl.GenericService;
import info.octaedr.jupnp.cp.impl.WrapperPluginAdapter;

/**
 * Generic implementation of AV plugin.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericAVPlugin extends WrapperPluginAdapter implements ControlPointFactoryPlugin {

    private static final Logger logger = Logger.getLogger(GenericAVPlugin.class);

    @Override
    public GenericDevice wrap(final GenericDevice device) {
        GenericDevice rv = null;

        if (device.isCompatible(MediaServerDevice.DEVICE_TYPE_MEDIA_SERVER_1)) {
            rv = new GenericMediaServerDevice(device);
        } else if (device.isCompatible(MediaRendererDevice.DEVICE_TYPE_MEDIA_RENDERER_1)) {
            rv = new GenericMediaRendererDevice(device);
        }

        if (logger.trace()) {
            logger.trace("Wrapping device: " + device + " => " + rv);
        }

        return rv;
    }

    @Override
    public GenericService wrap(final GenericService service) {
        GenericService rv = null;

        if (service.isCompatible(ConnectionManagerService.SERVICE_TYPE_CONNECTION_MANAGER_1)) {
            rv = new GenericConnectionManagerService(service);
        } else if (service.isCompatible(ContentDirectoryService.SERVICE_TYPE_CONTENT_DIRECTORY_1)) {
            rv = new GenericContentDirectoryService(service);
        } else if (service.isCompatible(AVTransportService.SERVICE_TYPE_AV_TRANSPORT_1)) {
            rv = new GenericAVTransportService(service);
        } else if (service.isCompatible(RenderingControlService.SERVICE_TYPE_RENDERING_CONTROL_1)) {
            rv = new GenericRenderingControlService(service);
        } else if (service
                .isCompatible(ScheduledRecordingService.SERVICE_TYPE_SCHEDULED_RECORDING_1)) {
            rv = new GenericScheduledRecordingService(service);
        }

        if (logger.trace()) {
            logger.trace("Wrapping service: " + service + " => " + rv);
        }

        return rv;
    }

}
