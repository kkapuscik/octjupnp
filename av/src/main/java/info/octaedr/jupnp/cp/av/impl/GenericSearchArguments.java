/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.av.SearchArguments;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;

/**
 * Generic implementation of {@link SearchArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericSearchArguments extends GenericActionRequestArguments implements
        SearchArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericSearchArguments(final Action action) {
        super(action);
    }

}
