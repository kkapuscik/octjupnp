/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av;

import info.octaedr.jupnp.cp.Device;

public interface MediaRendererDevice extends Device {

    public static final String DEVICE_TYPE_MEDIA_RENDERER_1 = "urn:schemas-upnp-org:device:MediaRenderer:1";

    ConnectionManagerService getConnectionManager();

    RenderingControlService getRenderingControl();

    AVTransportService getAVTransport();

}
