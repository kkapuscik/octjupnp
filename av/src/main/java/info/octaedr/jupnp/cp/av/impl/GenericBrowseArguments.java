/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-light project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.core.UpnpException;
import info.octaedr.jupnp.cp.Action;
import info.octaedr.jupnp.cp.av.BrowseArguments;
import info.octaedr.jupnp.cp.av.cds.ContentObject;
import info.octaedr.jupnp.cp.impl.GenericActionRequestArguments;

/**
 * Generic implementation of {@link BrowseArguments}.
 *
 * @author Krzysztof Kapuscik
 */
public class GenericBrowseArguments extends GenericActionRequestArguments implements BrowseArguments {

    /**
     * Constructs new arguments.
     *
     * @param action
     *            Action for which arguments are created.
     */
    public GenericBrowseArguments(Action action) {
        super(action);
    }

    @Override
    public void setObjectID(String objectID) throws UpnpException {
        setInputArgumentValue("ObjectID", objectID);
    }

    @Override
    public void setBrowseFlag(BrowseFlag flag) throws UpnpException {
        switch (flag) {
            case BROWSE_DIRECT_CHILDREN:
                setInputArgumentValue("BrowseFlag", "BrowseDirectChildren");
                break;
            case BROWSE_METADATA:
                setInputArgumentValue("BrowseFlag", "BrowseMetadata");
                break;
            default:
                throw new IllegalArgumentException("Illegal browse flag: " + flag);
        }
    }

    @Override
    public void setFilter(String filter) throws UpnpException {
        setInputArgumentValue("Filter", filter);
    }

    @Override
    public void setStartingIndex(long index) throws UpnpException {
        setInputArgumentValue("StartingIndex", index);
    }

    @Override
    public void setRequestedCount(long count) throws UpnpException {
        setInputArgumentValue("RequestedCount", count);
    }

    @Override
    public void setSortCriteria(String criteria) throws UpnpException {
        setInputArgumentValue("SortCriteria", criteria);
    }

    @Override
    public String getResult() throws UpnpException {
        return (String) getOutputArgumentValue("Result");
    }

    @Override
    public ContentObject[] getResultObjects() throws UpnpException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getNumberReturned() throws UpnpException {
        return (Long) getOutputArgumentValue("NumberReturned");
    }

    @Override
    public long getTotalMatches() throws UpnpException {
        return (Long) getOutputArgumentValue("TotalMatches");
    }

    @Override
    public long getUpdateID() throws UpnpException {
        return (Long) getOutputArgumentValue("UpdateID");
    }

}
