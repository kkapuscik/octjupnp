/*------------------------------------------------------------------------------
- This file is part of OCTjupnp-av project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jupnp.cp.av.impl;

import info.octaedr.jupnp.cp.Service;
import info.octaedr.jupnp.cp.av.AVTransportService;
import info.octaedr.jupnp.cp.av.ConnectionManagerService;
import info.octaedr.jupnp.cp.av.MediaRendererDevice;
import info.octaedr.jupnp.cp.av.RenderingControlService;
import info.octaedr.jupnp.cp.impl.GenericDevice;

public class GenericMediaRendererDevice extends GenericDevice implements MediaRendererDevice {

    private ConnectionManagerService connectionManager;
    private RenderingControlService renderingControl;
    private AVTransportService avTransport;

    protected GenericMediaRendererDevice(GenericDevice other) {
        super(other);
    }

    @Override
    protected void postInit() {
        super.postInit();

        for (Service service : getServices()) {
            if (service instanceof ConnectionManagerService) {
                this.connectionManager = (ConnectionManagerService) service;
            }
            if (service instanceof RenderingControlService) {
                this.renderingControl = (RenderingControlService) service;
            }
            if (service instanceof AVTransportService) {
                this.avTransport = (AVTransportService) service;
            }
        }
    }

    @Override
    public ConnectionManagerService getConnectionManager() {
        return this.connectionManager;
    }

    @Override
    public RenderingControlService getRenderingControl() {
        return this.renderingControl;
    }

    @Override
    public AVTransportService getAVTransport() {
        return this.avTransport;
    }

}
